---
title: "Tentative"
date: 2010-3-30
categories: ['songs']
tags: ['Writing','3/4']
---
Sometimes you start writing and even after you have finished, you have
no idea what you just wrote is really all about.
<!--more-->
# Tentative

Time-Signature: 3/4 Capo: 0 Comments: play Bb on 6th fret Style: Tempo:
{{< highlight none >}}

    Dm        Dm
    Tentative feelings
    C            C
    question the muse
    Bb         Bb
    wonder the words
    A             A
    match light a fuse
    Dm        Dm
    simple as starlight
    C          C
    quiet as a ghost
    Bb             Bb
    the wordplay engaging
    A            Dm
    completely engrossed

        Dm            Dm
    And several lines later
    C               C
    we still can't agree
        Bb        Bb
    the meaning attached
           A             A
    to the words that we see
          Dm         Dm
    Still further we plunge
                C         C
    down depths murky and dark
    Bb          Bb
    seeking the source of the
    A         Dm
    tentative spark

    Dm        Dm
    Tentative meaning
          C          C
    we're not at the core
        Bb         Bb
    the package is wrapped
           A              A
    but we don't know who for
        Dm         Dm
    the addressing label
         C          C
    must still be enclosed
           Bb             Bb
    but we keep it locked up
         A              Dm
    in a heart that has dozed

       Dm         Db
    To whom do we give
      C               C
    a thought that is not
     Bb         Bb
    accordingly thought out
         A             A
    with lines we were taught
        Dm              Dm
    The meaning's still hidden
           C          C
    though shadows we see
        Bb           Bb
    I'm still not convinced
             A          Dm
    it's for you or for me.

{{< / highlight >}}
