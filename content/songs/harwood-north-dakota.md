---
title: "Harwood North Dakota"
date: 2002-1-31
categories: ['songs']
tags: ['stories','4/4']

---
I wrote this after listening to an NPR interview with people in
Harwood, North Dakota in 1997 after floods had surrounded the entire
town. As it later turned out, 1997 was one of the better years.
<!--more-->
# Harwood North Dakota

Time-Signature: 4/4 Capo: 3 Comments: Style: Strum Tempo:
{{< highlight none >}}

    (Chorus)
    Am       G     F             C       G     C
    Sheyenne River rising 'round Harwood North Dakota
         G                F                Am
    Five Hundred years or more the water's never been this high
    Am       G        F           C       G     C
    Sandbags piled up high 'round Harwood North Dakota
       G                 F              Am
    We built a ring dike round the city tight to keep us dry

       Em                   C             F G
    We feared we'd lose our town in April 1997, the
    C              F                    Dm                   G
    flood demanded all the strength and heart that we'd been given
       Em         F                C         G
    At 7 feet the water spread for 50 miles around
    F                                           G
    Flooding every North Dakota praire road and town

    (chorus)

       Em              C                 F                   G
    We built that dam across the highway running through the town
        C               F             Dm               G
    Without the cars it made a runway that would never drown
       Em                F                    C               G
    We flew the children out for school while Fargo was still dry
        F                                                 G
    but all the able bodied stayed to pile those sandbags high


    (chorus)

       Em                    C              F               G
    We fought that flood for seven days and seven sleepless nights
    C                  F
    Some folks came to stay and help,
         Dm                    G
    some prayed with all their might
    Em               F             C              G
    College kids and farmers, from nearby and far away
    F                                                        G
    Friends and strangers built a ring to keep that flood at bay

    (chorus)

{{< / highlight >}}
