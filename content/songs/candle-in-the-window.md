---
title: "Candle in the Window"
date: 2020-09-03
categories: ['songs']
tags: ['love','childhood','4/4']

---
Growing up from the parent's point of view.
<!--more-->
# Candle in the Window

Time-Signature: 4/4 Capo: 2 Comments: Style: strum Tempo: 118
{{< highlight none  >}}

    A G D A A
    (Part A)
    A                   G              D            A
    The summer heat was dying when you went away to school
         E            D                     A              E
    Your mother and I watched you pack your car and speed away
       D                E                A                  F#m
    We blinked a couple tears back as we saw your childhood end
        D              E               D               E
    the grown up world awaits that you don't yet comprehend

    (Part B)
                  D            E          D                E
         and your mother lit a candle and placed it on the sill
                D                A
         and we hope that candle draws you back
                D C#m E            A     Asus4 A
         to our home      upon the hill

    (Part A)
    A              G      D             A
    Then a card or letter seldom did we get
        E                 D        A             E
    you called but once a quarter, just to get a check
    D                  E       A                F#m
    Now you're on your own and home is far from view
            D             E         D                  A    Asus4 A
    but the candle in the window is there and calls to you

    (Part C)
             E             G      D              E
         The candle in the window quivers in the air
            A                 G               D             E
         it bravely shines, a beacon, you can see it if you care
            D               E                  A                 F#m
         it sits there as a symbol that you're still in both our hearts
            D                E         D            A      A Asus4 A
         no matter where you go, we'll never really part


    (Part A)
    A                 G                D              A
    Now you've family of your own, you call us once a week
        E                      D                 A                  E
    you battered through those years to find the self that children seek
           D                E           A                   F#m
    You're coming home this weekend, we know what’s on your mind
          D               E             D               E
    we’ll talk about your children, how you feel left behind

    Chorus (Part B and C)

{{< / highlight >}}
Notes. Remember the second verse ends on A, not on E. Two reasons, one to lower the feeling - the child is not calling home. Second, it gives a pull to the E at the beginning of part C
