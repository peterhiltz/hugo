---
title: "Slicing the Sourdough"
date: 2021-07-16T14:18:19-04:00
categories: ['songs']
tags: ['music']
showToc:  "true"
author: "Peter Hiltz"
---
How many of you have a kitchen utility drawer? How many of you buy your bread pre-sliced, but have a breadknife you never use? How many of you keep it in the kitchen drawer?

Usually I write my own melodies, but this is written to the tune of Waltzing Matilda
<!--more-->
# Slicing the Sourdough
{{< highlight none>}}

C            G               Am             F
Once a shiny bread knife was dropped into a kitchen drawer
   C         C               G         G
It lay there stunned at this senseless act
C           G         Am              F
What had it done to deserve life in a kitchen drawer
C             C             G           C
banged up and nicked and forgotten like that


C           C          F           F
Slicing the sourdough, slicing the sourdough
C         C           G              G
You'll go slicing the sourdough with me
       C         G          Am               F
and it sang to itself as it lay there in the kitchen drawer
C         C           G              C
You'll go slicing the sourdough with me


C                G                 Am               F
Weeks passed and months passed, it lay there in the kitchen drawer
C           C             G        G
dreaming of when it might be used again
       C                   G             Am            F
and it talked from time to time with the hammer in the kitchen drawer
       C           C            G               C
of the existential angst that beset things like them


Chorus

C         G           Am                F
One day a party guest brought baguettes along with cheese
    C           C      G         G
The wine flowed freely that fine day
        C              G        Am                 F
And the host knew that he had a bread knife in his kitchen drawer
    C         C            G             C
and pulled it out from the bottom of the bin


Chorus

C           G            Am                     F
Once a dull bread knife, scratched and battered in a drawer
C           C                    G
finally was given a chance to be free
        C          G            Am              F
And the guest was aghast and he took the knife away with him
C             C              G         C
now there's a song that they sing with Chablis

Chorus

{{< / highlight >}}
