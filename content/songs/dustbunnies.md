---
title: "Dust Bunnies"
date: 2021-07-06T14:18:19-04:00
categories: ['songs']
tags: ['music']
showToc:  "true"
author: "Peter Hiltz"
---
Inadvised use of percussive maintenance, but a good result from a sarcastic comment to someone who can actually change directions.
<!--more-->
# Dust Bunnies
{{< highlight none>}}
Dust Bunnies (4/4 Capo 2 flatpick)
Dm G FG C

       Am            F           Em                   Am
It was late one afternoon when a light flashed on the screen
F                  Dm      F       G
Computer just shut down in closet 23
C                    G        Em                  Am
Jack had to pull the records, he didn't know that room
Dm             G             F        G     C
Apparently the second floor, hospital annex 2

Dm                 G        Em             Am
Jack opened up the door and much to his surprise,
F                     Em      F       G
the room had not been cleaned since 1955
Em               Am          Em                 Am
he opened up the server box, smacked it once or twice (mistake)
Dm               G                   F       G          C
The fans started right back up, dust bunnies filled his eyes



Am                Dm                 Em           Am
He stumbled out into the hall, could barely see a thing
  G                  F          Dm            G
A washroom's what he wanted, to wash away the sting
   F            Em     C                Dm
he saw a moving shadow that might comprehend
Am                    G               F         G         C
but when he stammered "hey", he heard "I've got my own boyfriend"


Am                F             Em                       Am
"Well give him my condolences", Jack heard his own voice say
    F               Dm     F             G
The shadow wheeled around, fury plain as day
C                 G           Em                  Am
Jack said "I need directions, I need to wash this off"
Dm              G             F             G      C
The shadow said "Oh shit" and pulled Jack's hands aloft


Dm               G               Em            Am
Grabbing her own cell phone, the shadow made a call
F               Em    F             G
"I need a team ASAP,  annex eastern wall".
Em                  Am                Em               Am
"Some guy has got asbestos dust, it's covered both his eyes"
Dm                 G                F           G        C
"The trauma unit's just next door, I know these guys can fly"


Am                  Dm           Em                 Am
They put him out at once as they worked to save his sight
G                F           Dm              G
You can't wash asbestos out, if you like the light
F                    Em                  C                 Dm
He never learned the shadow's name, just grateful that she took
  Am             G               F            G       C
a second glance, reacted fast and changed her cold outlook.


F                    Em                  C                 Dm
He never learned the shadow's name, just grateful that she took
  Am             G               F            G       C
a second glance, reacted fast and changed her cold outlook.



{{< / highlight >}}
