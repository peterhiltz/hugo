---
title: "Now or Later"
date: 2016-2-31
categories: ['songs']
tags: ['religion','4/4']
---
Written for a friend thinking about the meaning of God.
<!--more-->
# Now or Later

Time-Signature: 4/4 Capo: Comments: Style: Tempo:
{{< highlight none  >}}

        Am        G            F               C
    The chapel is patient, the stained glass a-flame,
       F          Am             Dm              G
    ka-leidoscope saints sending prayers in your name,
       Am          Dm             Em          G
    un-ceasing, un-tiring, poised for your re-claim,
      F         Dm           G     G
    a-waiting a sign of your love

       Am        G           F              C
    So many have twisted the words that you gave,
         F             Am        Dm               G
    They kill for your glory and think themselves brave,
        Am   Dm           Em         G
    mis-understanding the reason you save;
       F           Dm         G     G
    ma-ligning the concept of love

    C        G             F           Am
    Have you travelled the road to Nir-vana,
      Dm        G         Am    Am
    a-bandoning us to our fate?
       F            Dm        Em      Am
    Or are we, your children, finally facing
        Dm            G          Am
    the truth that is ours to cre-ate?

       Am             G          F            C
    As I sit here and ponder the stillness so deep,
    F         Am           Dm           G
    trying to sense do you sleep or you weep?
           Am         Dm        Em           G
    But_no matter how quiet, my faith I will keep.
        F           Dm        G     G
    The lesson will always be love

       Am          G           F          C
    So much like a parent, you live in my heart.
           F         Am              Dm     G
    Though many will claim we're for-ever a-part,
         Am            Dm            Em         G
    When death reaches out, and this world I de-part,
          F            Dm        G     G
    we'll join back to-gether in love

    C           G             F           Am
    If you have travelled the road to Nir-vana
      Dm            G         Am     Am
    I know you will keep me a place, There's
    F             Dm         Em   Am
    still much to do, on the road behind you,
             Dm          G             Am
    and some-day we will share that em-brace

{{< / highlight >}}
