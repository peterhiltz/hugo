---
title: "You Never Drew a Breath"
date: 2018-12-30
categories: ['songs']
tags: ['Memorial','4/4']
---
For some friends after the death of a child.
<!--more-->
# You Never Drew A Breath

Time-Signature: 4/4 Capo: 3 Comments: Style: Tempo:
{{< highlight none >}}

    D

            Em    Em         D                D
    We both cried   when the doctor shook his head, there's no
    Am        Am     Bm               Bm
    heartbeat,  were all the words he said, and we
    C        C  D                  Em
    wondered,   what we could have done, that
    C                 Am            D     D
    you could see the early morning sun     you touched our

    Em    Em            D            D
    lives,   though you never drew a breath, all our
    Am     Am  Bm                Bm
    plans,     shattered at your death, all our
    C     C          D            Em
    hopes,    turned instantly to fears
    C               Am                D      D
    Would we now be childless all our years      will we

    Em   Em       D                   D
    know,    what caused your life to end, will we
    Am  Am     Bm                Bm
    find   the strength to try a-gain? Its not
    C    C          D                Em
    fair,   but our families will be there, some-
     C                 Am                D
    -times the pain is something you can share
    _        Em
    Or never bare
{{< / highlight >}}
