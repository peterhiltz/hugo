---
title: "Winds of Change"
date: 2023-03-09
categories: ['songs']
tags: ['life','4/4']
---
I'm trying to get back to songwriting. This was in response to a challenge on writing a song involving listening to the wind. I started playing around with the theme of "winds of change" and how everything is always changing. So often we don't hear the first whispers and, when we do, we try to fight it instead of accepting it and going with the flow. The changes can be physical (I can't stop getting older), they can be cultural, they can be economic or environment or scientific or lots of other things.

Someone once said "Things were simpler when we were younger" and the response was "No, we were simpler". We can't go back to the naïveté of our younger days, but we can reset how we engage with life, the universe and others.

For those of you who care, this was actually written in an open D minor tuning.
<!--more-->
# Winds of Change

Time-Signature: 4/4  Style: Fingerpick
{{< highlight none >}}

        Dm             Am                    Gm           Dm
    The whisper of the winds of change start softly and unclear
       Gm           F                  Am           Dm
    We focus on the day to day and the customs we revere
           Bb             F              Gm                Am
    In the shelter of our daily life the days just roll on by
        Dm            Am               Gm               Dm
    The echoes of the hours past drown out the breeze's sigh.

    Chorus
    D                 Em       G                D
    Nothing stays the same and seasons come and go, the
    F#m               G             Em               A
    sands of time are driven as the winds of changes blow, why
    F#m           G              Em               D
    try to hold a storm when you hear the tempest roar?
    A               G                Em              D
    Just reset your bearings, we can find our way to shore

    Dm             Am              Gm               Dm
    We can track a hurricane as it blows across the sea
    Gm              F               Am              Dm
    knowing when to batten down and knowing when to flee, but we
    Bb                     F                   Gm            Am
    miss the hint when the winds of change are still a quiet breeze
    Dm                    Am                Gm              Dm
    not prepared when the twister howls and we're buried in debris

    Chorus

       Dm         Am            Gm                 Dm
    We'don't like curiosity, we know that leads to storms
        Gm              F       Am                 Dm
    but when we finally realize some things need reforms
       Bb             F                  Gm            Am
    we first deny and then we attack the bearer of bad news
              Dm        Am              Gm             Dm
    as if the gale that buffets us will care about our views

    Chorus


{{< / highlight >}}
