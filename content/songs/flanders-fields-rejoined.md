---
title: "Flanders Fields Rejoined"
date: 2020-09-06
categories: ['songs']
tags: ['conflict','4/4']

---
The poem "Flanders Fields" was written by a Canadian doctor during
WWI. It is patriotic and, unfortunately, glories in the continuation
of conflict. Must so many people insist on having an enemy to unite
and fight against?
<!--more-->
# Flanders Fields Rejoined

Time-Signature: 4/4 Capo: 0 Comments: Style: fingerpick Tempo: 118
{{< highlight none>}}

    Instrumental chorus       Am G Dm E F C G Am

    Bm                       C           G                  F
      In Flanders fields the poppies blow and they in other countries grow.
    E                      F              C                  G
      Why do you think the dead that lie 'cross all borders ‘neath the sky
    F                 E
    Continued quarrel justify?



    Instrumental chorus

    Bm                      C             G                     F
      You say the dead from graveside cry Revenge! So torch you hold on high
    E                         F             C                  G
      Bring up old wrongs and fan the flame, twisting facts so you can claim
    F                   E
      Who is friend and who to blame


    Instrumental chorus

    Bm                    C              G                F
      Between the crosses row on row, in spite of all the tears that flow
    E                    F           C                 G
      Withered grass and flowers die, demons laugh and angels cry
    F                  E
      At our hate that dwarfs the sky


    Instrumental chorus

{{< / highlight >}}
