---
title: "Happiness and Tears (2001)"
date: 2001-12-31
categories: ['songs']
tags: ['stories','4/4']

---
2001 was an eventful year for many reasons. I wrote this to sing with
some friends at a new years eve party.
<!--more-->
# Happiness and Tears (2001)

Time-Signature: 4/4 Capo: 2 Style: Flatpick Tempo:
{{< highlight none >}}

    Am           F           G                 Am
    Many songs I sing for you I write in minor key
    Am                F          G             Am
     some of them are happy tunes some of history
    F                    C        Dm                   E
    And each one carries something I  hope for all who hear
    Am                       F           G                Am
      A thought that somehow touches you in happiness and tears

    Chorus
    G           F           G           F
    It's been a long year,  It's been a long full year
    G           F                  C   D       C
    It's been a long year, And the melody goes on


    D                   C       G               D
      My sister had her wedding upon the eve of war
    C                        G          F                   D
      My brother's wife bore them a son Before the year was o'er
    C                    G             C                 D
      And you and I went up a hill And came down wearing rings
    C             G              D             C
      Music was a part of it and helped us savoring

    Chorus

    Am                     F             G               Am
      September brought us tragedies and made our senses reel
    Am                F               G                 Am
      music helped us cope with pain, expressing how we feel
    F               C        Dm               E
      The patriotic anger or pain, or burning fears
    Am                 F               G             Am
      music speaks our hearts to us In happiness and tears


    Chorus

    D              C           G                 D
      Now we face another year full of hopes and dreams
    C                     G             F                 D
      Will the world find peace at last or more important themes
    C                     G       C                    D
      Tonight we're all together, this year's verse complete
    C                 G            D                 C
      about to start another verse to life's eternal beat

{{< / highlight >}}
