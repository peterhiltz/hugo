---
title: "Staying Home in Quarantine"
date: 2020-4-30
categories: ['songs']
tags: ['parody','fun songs','4/4']
---
A parody written to the tune of Rolling Down to Old Maui.
<!--more-->
# Staying Home in Quarantine (to the tune of Rolling Down to Old Maui)
{{< highlight none  >}}

    Its a damn tough life, full of toil and strife EmB7 (2x)
    while we stay in quarantine EmB7 Em
    and we pray for the safety of our friends EmB7 (2x)
    in essential industry EmB7 Em
    And the kids are bored and they miss their friends G D
    and they might drive us insane Em B7
    So outside they go, grateful for no snow EmB7 (2x)
    and I hope it doesn't rain EmB7 Em


     Staying home in quarantine, my friends, staying home in quarantine G D
     Get your quality time, with the family now, while you stay in quarantine EmB7 Em

    I work from home, always on the phone EmB7 (2x)
    while the cat sits on my lap EmB7 Em
    and I try to schedule conference calls EmB7 (2x)
    when the kids are trying to nap EmB7 Em
    But I get distracted by the mail G D
    and now nothing is done Em B7
    and I hope my boss does not get cross EmB7 (2x)
    and is sitting in the sun EmB7 Em

     Staying home in quarantine, my friends, staying home in quarantine G D
      Get your quality time, with the family now, while you stay in quarantine EmB7 Em

    We got out games, we got out books, EmB7 (2x)
    and we turned on the TV EmB7 Em
    We tried a little exercise EmB7 (2x)
    but I quickly hurt my knee EmB7 Em
    We practice patience all the day G D
    and avoid reading the news Em B7
    We pass the time making silly rhymes EmB7 (2x)
    And we hope it does amuse EmB7 Em

     Staying home in quarantine, my friends, staying home in quarantine G D
      Get your quality time, with the family now, while you stay in quarantine EmB7 Em

    And my boss's frown says with travel down EmB7 (2x)
    we will have more time to work EmB7 Em
    The responding rhyme almost writes itself EmB7 (2x)
    but I think I better lurk EmB7 Em
    If I just stay mum when the layoffs come, G D
    I can hope my job will keep Em B7
    The bills will still keep piling up EmB7 (2x)
    so that email I will bleep EmB7 Em
{{< / highlight >}}
