---
title: "Meridan Road"
date: 2017-1-31
categories: ['songs']
tags: ['home','3/4']
---
Written about an old farmhouse in New Hampshire. Some people grow up
and leave their childhood homes without a further thought. Others
seem to grow up and have left their hearts behind.
<!--more-->
# Meridan Road

Time-Signature: 3/4 Capo: 2 Comments: Style: Flatpick Tempo:
{{< highlight none >}}

    Bm        Bm         Bm      Bm
    Bobby and Carol grew up in a farmhouse, the
    Bm          Bm           D       A
    old Benwood place out on Meridan Road, for
    Bm          Bm           Bm         Bm
    two hundred years it had stood as a landmark, some
    Bm         Bm           D          A
    years as a tavern, some years as a home


    C         C           C               C
    Generations of Yankees had lived there and played there
    D               D               A       G G7
    Drunk there and danced there on Meridan Road
    D             D        A             D
    Some years in red, and some years in white, the
    G              G         A            Bm
    farmhouse that Bobby and Carol called home


    Bm        Bm         Bm           Bm
    Bobby and Carol grew up and moved onward, when
    Bm      Bm           D            A
    Lebanon Valley could hold them no more
    Bm            Bm            Bm          Bm
    He joined the Air Force and went off to Britain, she
    Bm         Bm             D           A
    married in Boston and had kids of her own, but
    C              C           C          C
    still in their minds was a sturdy red farmhouse
       D          D         A       G  G7
    surrounded by maples on Meridan Road, and
    D               D          A          D
    back they would travel, no matter the distance, to
    G           G            A       Bm
    spend every Christmas on Meridan Road


    Bm            Bm           Bm              Bm
    Finally their parents grew tired and moved onward
    Bm          Bm           D       A
    leaving the farmhouse on Meridan Road, for
    Bm           Bm           Bm           Bm
    all of their lives it had stood in the background, no
    Bm         Bm          D           A
    matter how far, it had always been home, for
    C         C                C      C
    Carol and children, life’s stages continue
    D           D               A       G   G7
    it’s been a long time since Meridan Road
    D            D          A              D
    that story's ended, her children’s has started
    G              G                A             Bm
    home's now the place that their children call home, but


    C       C           C             C
    Bobby’s heart never left that old farmhouse, his
    D          D              A       G   G7
    ghost will return back to Meridan Road,   to
    D             D          A                D
    join with the others who down through the years
    G             G          A       Bm
    found home in Benwood on Meridan Road

{{< / highlight >}}
