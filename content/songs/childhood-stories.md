---
title: "Childhood Stories"
date: 2020-09-03
categories: ['songs']
tags: ['childhood','growing up','love','friends','4/4']

---
Friends thinking back to our childhood dreams and where we ended up instead.
<!--more-->
# Childhood Stories

Time-Signature: 4/4 Capo: 2 Comments: Style: Tempo:
{{< highlight none  >}}

       G           Bm        C                G
    We reminisce together we gather close and tell
        G              D                   C               D
    the stories of our childhoods when our dreams began to jell
         C            Bm        C                     D
    When we foresaw a ladder of hopes and schemes and dares
    G            Bm                C              G
    leading up into the clouds and castles in the air


        Am               E        F                E
    The creatures of our dreaming returning to our thoughts
        Am                 E               G                  Am
    are precious handholds back in time to which our mem'ries caught
         F                 E           F                E
    Back then we were much younger and thought we could become
        Dm           C            G                     E
    the heroines and heros of the songs that we'd heard sung


    G               Bm          C                  G
    Now that we are older those childhood mem'ries come
    G                   D   C                     D
    Dreams of what we'd do, songs which we'd have sung
        C          Bm        C                 D
    And some of us succeeded some who took the dare
       G                Bm                C             G
    to climb the ladder of those dreams into the starry air


    Am                E                 F            E
    Others left their dreams behind and sang another song
       Am          E              G                Am
    distracted by another goal to which they now belong
      F                  E         F                E
    tonight we're back together comparing notes and schemes
    Dm                  C                   G                  Am
    what we've done and where we've been compared to childhood dreams

{{< / highlight >}}
