---
title: "Dark Stone Wall"
date: 2020-09-06
categories: ['songs']
tags: ['Memorial','War','4/4']

---
My only obviously autobiographical song. Families pay a price for the
games that politicians play that those politicians never pay.
<!--more-->
# Dark Stone Wall

Time-Signature: 4/4 Capo: 3 Comments: Style: fingerpick Tempo: 124
{{< highlight none >}}

    C G Em
    G               D    C           G
    On a dark stone wall in a former swamp
    G           Am7 Am          D7
    Is carved a name I remember well
    G  D   Am          D     C   G
    He was calm and he loved my mom
    G            Em         D        G
    But he went away To the gates of hell

    C              G                D                    Em
     I remember he watched me play, with my soldiers one Winter's day
    C                    G     Em
     And he said, as I recall,   People on both sides will fall
    C                  G           D              Em
      Then one day his orders came, no one will accept the blame
    C             G               Em
     Far away, we could not know, whether he faced friend or foe


    (Bridge)

    Bm              Em         F#           Em
      Every week my sister and I received a letter
    G               D                    F                  D
      Telling us he wished that he could come back home and stay
    G                  C       D                 G
      Please obey your mother. Take care of one another
    Em             Bm          C            D7
      And try to explain to us why he went away


    C                    G             D                     Em
      Late one night, my mother cried,   I struggled to grow up inside
    C                      G             Em
      The man who tried to keep the peace   Was himself now fast asleep
    C                     G                  D               Em
      I know some battles must be fought but do we know what they have wrought?
    C                G           Em
      did we make an end to pain   or was the sacrifice in vain?

    G               D    C           G
    On a dark stone wall in a former swamp
    G           Am7 Am          D7
    Is carved a name I remember well
    G  D   Am          D     C   G
    He was calm and he loved my mom
    G            Em         D        G
    But he went away To the gates of hell

{{< / highlight >}}
