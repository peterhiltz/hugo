---
title: "Old Memories"
date: 2005-2-31
categories: ['songs']
tags: ['memorial','relationships','3/4']
---
Written for my grandmother who spent many years alone after her husband passed.
<!--more-->
# Old Memories

Time-Signature: 3/4 Capo: 0 Comments: Style: Tempo:
{{< highlight none  >}}

        D            G              A          D
    The pictures are tarnished, the once shiny frames
         G         A         D    D
    need polishing yet once again
    D            G            A         D
    weeping with sadness o'er dusty old photos
    C           G/B            A   A
    filling the shelves in the den

    G           A          D              G
    And, in her heart, she knows that the love
        F             E        Am     Am
    was worth all the pain and tears
        G            F
    She calls to her memory, it
    E        Am     D       E    E
    runs and flies, hiding away

    D           G         A                D
    What can be said of a life that's gone on
    G            A       D     D
    sixteen long years before
    D   G               A               D
    She knows that he's somewhere still watching
    C          G/B         A     A
    waiting to welcome her home

    G           A          D              G
    And, in her heart, she knows that the love
        F             E        Am     Am
    was worth all the pain and tears
        G            F
    She calls to her memory, it
    E        Am    D             E      E
    comes at last, brushing back tears

    D           G             A              D
    Here is the garden, where once they were married,
        G           A              D      D
    and apple trees bloomed in the spring
    D             G                 A             D
    There was the house where their children were raised,
        C          G/B       A        A
    and life, they felt, was complete

    G           A          D              G
    And, in her heart, she knows that the cost,
       F           E            Am    Am
    of missing him all of these years
        G           F
    was worth every penny, of
    E       Am          D           E      E
    life to share those fifty great years

        D            G              A          D
    The pictures are tarnished, the once shiny frames
         G         A         D    D
    need polishing yet once again
    D            G            A         D
    weeping with sadness o'er dusty old photos
    C           G/B            A   A
    filling the shelves in the den

{{< / highlight >}}
