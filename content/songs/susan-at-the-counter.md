---
title: "Susan at the Counter"
date: 2010-4-30
categories: ['songs']
tags: ['stories','fun Songs','4/4']
---
Denver Airport, United ticket counter, 1989. Or an urban myth. In any
event, I wrote this version for a flight crew going from Detroit to
New York after police were called to eject an unruly first class
passenger from the plane.
<!--more-->
# Susan at the Counter

Time-Signature: 4/4 Capo: 2 Comments: Style: strum Tempo: 126
{{< highlight none >}}

    D A G D

    G                A7
    Susan worked the counter
           G              A7
    for an airline in the west
            D                 Bm
    Through flight delays and budget cuts
        G              A7
    she always did her best
        Bm                   Em
    One day the flights were canceled
         Bm         Em
    'mid curses all around
        D                 A
    She formed a queue of passengers
        G              D
    who slowly settled down

      G                 A7
    A man shoved to the front
              G                A7
    demanding service here and now
        D                Bm
    She told him he must get in line;
       G            A7
    He started up a row
       Bm               Em
    He said Do you know who I am?,
        Bm            Em
    but Susan was not tame
        D                A
    She quickly took her microphone
        G                   D
    and asked the crowd his name

      A             A
    A man at ticket counter three
         G               G
    does not know who he is.
       D          Bm
    If anyone can ID him,
      G             A7
    a reward may be his.
        Bm               Em
    Now get in line, she told him
          Bm                     Em
    as he cursed and steamed and stewed
       D                    A
    He screamed at her Well F____ you then
                G                D
    She smiled, That takes lines too.

       G               A7
    At that a loud and hearty laugh
     G               A7
    erupted from the crowd
          D               Bm
    Their tension quickly drained away
       G                 A7
    as she stood there unbowed
        Bm               Em
    Sometimes a sense of humor
        Bm                  Em
    can get you through the day,
        D                A
    and angry crowds can simply be
       G              D
    an instrument you play.

{{< / highlight >}}
