---
title: "Mondays"
date: 2015-1-31
categories: ['songs']
tags: ['Life','3/4']
---
Mondays. Everyone's "favorite" day of the week
<!--more-->
# Mondays

Time-Signature: 3/4 Capo: 2 Comments: Style: Strum Tempo: 116
{{< highlight none >}}

    Am  Am/G  Am/F  Am/E

    G          F          Dm          Am           G    F Em G
    All of the teachers I suffered in college were vain
        Am         Dm           G     G          Em      Em Em
    and all of the girls that I dated were quite insane
      F       F                G           G          Am   Amsus4 G
    I somehow survived all the torture and 4 years of pain
    G             F             Am          G
    And now I can state without question or hate
       Em              Am      Am/G  Am/F  Am/E
    It prepared me for Mondays

      G          F           Dm        Am             G     F Em G
    I work in an office, the customers think they are right
        Am          Dm         G           G          Em     Em Em
    The boss has no clue and I get nothing done until night
        F              F          G         G             Am     Amsus4 G
    But checks come on Friday and I can pay bills without fright
          G                  F         Am        G
    So my (kitten/puppy) and I can sit under the sky
        Em         Am      Am/G  Am/F  Am/E
    and play until Monday

      G         F           Dm       Am        G   F Em G
    I googled myself to see if I was famous at all
          Am      Dm             G   G            Em   Em Em
    There were no results and my ego took quite a fall
        F          F            G           G           Am   Amsus4 G
    But it doesn't matter, I’ll not turn my face to the wall
         G            F        Am    G
    I'll hold my head high and I can defy
      E    E       Am       Am/G  Am/F  Am/E
    A life full of Mondays
                      Am  Am/G  Am/F  Am/E  Am

{{< / highlight >}}
Note: throw in an 13th (C ) in the second Em in the second line of each verse
