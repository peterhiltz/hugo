---
title: "Wind in the Rigging"
date: 20013-2-31
categories: ['songs']
tags: ['sailing','stories','work','3/4']
---
Many sailing ships wrecked on the southeast coast of Australia. This
is one that survived.
<!--more-->
# The Rigging

Time-Signature: 3/4 Capo: 0 Style: Strum Tempo:
{{< highlight none  >}}

    Em Em D D A G Em Em Em Em

            Em       Em       D         D
    We were out on a run down Australia way
             A          A            G           D
    the ship heavy with goods and we counted our pay
            Em        Em          D        D
    when we get home again, we'll spend it away
       A        G           Em   Em
    on lassies, whiskey and gin

            D           D
    And the wind in the rigging
            C         C
    makes a whistling sound
         G           G
    It's late in the night
           A             A
    and we hear the surf pound
          D           D
    so we pray to the gods
             Em      Em
    watching over us now
               A         A
    that we'll make it
       G         Em
    to Scotland again

        Em        Em       D          D
    The heavy fog hit some three days ago
        A             A          G            D
    the stars and the sun hidden deep in that dough
       Em           Em       D           D
    no sightings we had to determine our lie
           A             A           G           Em
    but we knew that the rocks of Australia were nigh

    Chorus
        Em              Em           D        D
    Our clipper, Jeanne Rae, ran the circular route
          A           A             G           D
    we've seen the Antarctic, we've brought it about
          Em              Em           D         D
    we've run through the fifties with hurricane winds
             A            A
    Would we wreck on the shores
             G              Em
    where we sent those who sinned?

    Chorus

        Em             Em              D       D
    But helmsman Black Davey threw the rudder around
       A              A    G             D
    we clawed back to sea away from that sound
          Em          Em      D         D
    we'll live to see pay and spend it away
       A        G           Em   Em
    on lassies, whiskey and gin

    Chorus
{{< / highlight >}}
