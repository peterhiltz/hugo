---
title: "Expat Community"
date: 2020-09-03
categories: ['songs']
tags: ['expats','3/4']

---
Expats are temporary immigrants. But there are different types of
expats. Some just treat it as an extended vacation. Others are
seasonal and tend to forget the real truth that they are guests and
the local rules apply. Some become more native than the natives. A
fourth group is more internationalist and are more at home crossing
borders and dealing with multiple cultures than they are back home.

This song is about that last group.
<!--more-->
# Expat Community

Time-Signature: 3/4 Capo: Comments: Style: Strum Tempo:
{{< highlight none >}}

    G                     C            Em
    Karla's from Denmark, Roberto from Spain
    Am               G         D            D7
    Our families all wonder if we've gone insane
    Em                       D         G
      We meet in Jakarta, in London, Dakar; Our
    C               G         D          G
    Homeland's each other, no matter how far

    [Chorus]
    C               Em       G          D7
      We're not the jet set, trendy and rich
    C                      F        G
      Multiple cultures is our only niche
    Em             C          G         Am
      We do not go native, we do not go home
    C            G             D       G
      We are the expats, where ever we roam

    G                     C          Em
    If ever we go back to our native land, our
    Am         G          D           D7
    cousins at home, they can't understand
        Em                   D           G
    The news of the world is part of our stage
        C          G             D         G
    And to us, our families, all live in a cage

    [Chorus]

    G                                C       Em
    We're the expat community, where-ever we stay, we
    Am          G            D        D7
    all pick up something we can take away, no
    Em                  D         G
    longer a Yankee, Brazilian or Brit, a
    C        G         D           G
    cultural afghan is what we all knit

    [Chorus]

{{< / highlight >}}
