---
title: "New Fallen Snow"
date: 2020-09-05
categories: ['songs']
tags: ['growing up','childhood','quiet','3/4']

---
We as adults are afraid of things that we as children were ignorant
of. Sometimes we think it would be nice to be back at that age, but
now we are the ones with responsibility.
<!--more-->

# New Fallen Snow

Time-Signature: 3/4 Capo: 2
{{< highlight none  >}}

              Am          G      C             G
    There are deer in the meadow searching for fodder
    Am              G          F          Am           G
    digging through new fallen snow and I stand at the edge
           C           F          Am          G        F   F
    of the woods and I watch them thinking of so long ago


    Chorus
            Dm    Am             G     G
    And the world seemed so much safer
           F           G         Am
    and as children we played unaware
           Dm              Am           G         G
    of the things that our parents were scared of
        Am       G           F    F
    and now it's our turn to care
        Am       G           Am    Am
    and now it's our turn to care


      Am           G        C            G
    A noise in the distance startles the deer
    Am        G          F     F
    nervously they look around,  I slip
    Am          G
    back in the woods, where its
    C           F
    soft and so quiet
       Am           G          F      F
    pretending that I can't be found

    Chorus

        Am            G          C          G
    The world keeps revolving, ignoring our fears, you
    Am           G       F        Am         G
    face them or hide or go but today I will walk, under
    C         F      Am           G          F    F
    quiet old trees, grateful for new fallen snow

    Chorus

{{< / highlight >}}
