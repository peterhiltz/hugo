---
title: "Library at Wizard U"
date: 2014-1-31
categories: ['songs']
tags: ['fun songs','4/4']
---
Actually based on Terry Pratchett's books, not J.K. Rowling's.
<!--more-->
# Library at Wizard U

Time-Signature: 4/4 Capo: Comments: Style: Tempo:
{{< highlight none >}}

    D                  A             G              A
    If you would study wizardry then you must go to school
        G                     D          A               G
    and there you'll find the library is not a place for fools.
        D                A               G                A
    The books there are alive you see so careful what you do
        D                       C                    B              E
    for though you think you're reading them they're really reading you

    [Chorus]
      A          G          D              A
    A library of magic is a scary place to be
       G                D                A                   G
    If you would wander in my friend you won't go there with me

    D              A                     G                  A
    Some books are attached by chains to keep them in their place
        G              D                  A               G
    for left unguarded they would eat the others in their case.
        D             A          G                   A
    The wisdom of the ages is distilled within their spines
        D              C                    B                E
    but do not bring a glass for they might see you as their wine

    [Chorus]

    D             A        G                A
    Herds of wild thesauri thunder down the aisles
        G               D              A              G
    and footstool crabs forage on your lost forgotten files.
    D                  A                   G                A
    Student tribes are lost in there where few will venture deep,
       D              C           B           E
    so take a map and compass and never go to sleep.

    [Chorus]

{{< / highlight >}}
