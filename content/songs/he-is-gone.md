---
title: "He Is Gone"
date: 2023-01-31
categories: ['songs']
tags: ['stories','relationships','4/4']
---
Trigger warning: Song about domestic violence.

This originally started out as a song about recovering from the end of a marriage involving domestic violence. Then after watching some relationships close to me, it took a twist at the end and got more complicated, with a flavor of co-dependency so the ending is sadder and more wistful than the original practically dancing on the grave.
<!--more-->
# He is Gone

Time-Signature: 4/4 Capo: 3 Style: Flatpick
{{< highlight none >}}

    Am                C               G            Am
    Colored glass and trinkets on the bed table at night
    C              G           Am
    starting back again on her own
    Am                G                Am
    lonely nights and paying bills and cooking just for one
    G                     F              Am
    now she's changed the message on the phone

    Chorus
          C                    D             Em      F      Am
    He is gone, thank God he's gone, no more bruises now to hide
            C                   G           Am
    no more screaming, slamming doors he is gone
          C                    D             Em        F      Am
    He is gone, thank God he's gone. No more sleepless beaten nights
            C             G           Am
    no more drunken angry rages he is gone

    Am               C          G               Am
    She held out for forever or so it seemed to her
    C             G                 Am
    never knowing when a blow would fall
    Am              G             Am
    hiding deep within herself to cower in the night
    G             F                 Am
    thinking of a shelter she could call

    Chorus

    Am                C              G               Am
    One late night of driving in the whiskey and the rain
    C                  G              Am
    black ice where it hit the frozen ground
    Am             G                Am
    now she's been delivered from a further life of pain
    G                             Am
    life began again on his grave mound

    (slower)
          C                    D
    He is gone, thank God he's gone

    (spoken, sigh)
    Still

    Am                G                Am
    lonely nights and paying bills and cooking just for one
    G                     F              Am
    now she's changed the message on the phone


{{< / highlight >}}
