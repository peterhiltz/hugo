---
title: "Character"
date: 2021-01-14
categories: ['songs']
tags: ['life','4/4']

---
Early early draft. Sometimes you drop nuance and need to be heavy handed. This song is a bit unusual in that it starts with an accidental.
<!--more-->
# Character

Time-Signature: 4/4 Capo: 2 Comments: Style: fingerpick
{{< highlight none >}}

Bm           C9                G   G
Sitting at a hometown football game, with my
F                Em
daughter and her boyfriend,
  Dm         Em
I forget his name
Bm                   C9                G    G
The team was down by four and time was short, it
F                     Em
looked like they were coming back
Dm                Em     Em
  till Billy got ejected, He


    [B Part]

    Am          G                F        C
    hit another player while his back was turned
    G                    Am
    officials saw it clearly
    G                    Am     Am
    and that was when we learned,   while

        [Chorus]

        F            C9               G                 Am
        Character is what you do when no one's watching you
        Dm                Em              G
        A foul's a foul regardless of the team
        F               C9              G                 Am
        If your view of right and wrong depends on who it is
        G           Em           Am
        I don't see paths  to unity. The

Bm                  C9            G
boyfriend jumped up screaming to "kill the ref"
F                     Em
My daughter looked at me
Dm           Em
anger in her eyes, "If
Bm        C9                G
he's like this when it's a game
F            Em
What will he be like
Dm             Em
when it really matters?"


    Am            G                      F      C
    Picked up her purse and said  "Let's Go"
    G           Am
    Leaving him alone
    G              Am   Am
    In his pool of hate,   while

[Chorus]

        F            C9          G                      Am
        Character is what you do when no one's watching you
        Dm                Em              G
        A foul's a foul regardless of the team
        F               C9               G                 Am
        If your view of right and wrong depends on who it is
        G           Em          Am
        I don't see paths to unity.


{{< / highlight >}}
