---
title: "Drip, Drop"
date: 2020-09-03
categories: ['songs']
tags: ['fun songs','4/4']

---
Written for my daughter when she was young and believed that animals
were nicer than people. Sometimes I even think she was right.
<!--more-->
# Drip, Drop

Time-Signature: 4/4 Capo: 0 Comments: Style: Tempo:
{{< highlight none >}}

    F                 Dm           G     Am    G    Am
    Old Noah built himself a boat. Drip, drip, drip drop
            E                  F               Am         G          Am
    All the animals hoped that it would float. Drip drip, drip drip, drop

    Chorus
    Am                    G               F                    E
    God saved the animals two by two.  If it rained now, would he save you?
        Dm               E           Am         F          E    Am
    and if he won't then what to do? Drip drip, drip drip, drip drop

    F                 Dm           G     Am    G    Am
    Noah thought he'd found a way. Drip, drip, drip drop
       E              F              Am         G          Am
    to save the human race that day. Drip drip, drip drip, drop

    [Chorus]

    F             Dm             G     Am    G     Am
    Noah built it long and wide. Drip, drip, drip, drop
            E               F              Am         G          Am
    all the animals found a place to hide. Drip drip, drip drip, drop

    [Chorus]

        F                   Dm             G     Am    G     Am
    The animals hatched themselves a plan. Drip, drip, drip, drop
       E                   F             Am         G          Am
    To save their children all from man. Drip drip, drip drip, drop


         Am            G                  F             E
    They made old Noah walk the plank. He hit the water and he sank
    Dm              E             Am         F          E    Am
    now the animals run the bank. Drip drip, drip drip, drip drop
    Am         F          E    Am
    Drip drip, drip drip, drip drop

{{< / highlight >}}
