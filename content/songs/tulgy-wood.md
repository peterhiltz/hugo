---
title: "Tulgy Wood"
date: 2014-3-30
categories: ['songs']
tags: ['fun songs','stories','3/4']
---
Sometimes the Jabberwock wins. And sometimes it should.
<!--more-->
# Tulgy Wood

Time-Signature: 3/4 Capo: 2 Comments: Style: Tempo:
{{< highlight none >}}

       Am                   E               F      G        Am
    As I did travel through Tulgy Wood with Vorpal blade in hand
      C                 G             F              E
    I saw a Chesire cat on a limb and to that tree I ran
       Am               G                F                E
    It winked at me, it disappeared, and then its smile returned
        Am                  G              F           E          Am
    the tail came back, but not the cat, I wondered if I had been spurned


       Am                  E                   F             G      Am
    It opened its eyes and said "Dear boy, you really should not be here.
        C             G              F             E
    The Jabberwock is on its way and very hungry I fear."
        Am                  G             F              E
    The woods they suddenly burbuled, the Jabberwock was here
      Am             G                F       E           Am
    I kept my Vorpal blade in hand, hoping to cut off its ear.


      Am             E            F       G     Am
    I did not have a license, for hunting Jabberwocks
       C                   G             F                 E
    So I could not kill it outright, and must depend on my luck
        Am             G                   F                E
    The Vorpal blade remained in hand, but both were on the ground
    Am                     G                       F         E     Am
    As was my head - and I found myself dead - and lying without a sound


      Am                    E           F         G     Am
    I hear you question the singer "Why sing this as an I?"
    Am                           G
    The singer's possessed by my ghost, I can't rest
        F     E       Am
    and Lewis Carroll lies

{{< / highlight >}}
