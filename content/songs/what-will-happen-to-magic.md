---
title: "What Will Happen to Magic"
date: 2018-12-30
categories: ['songs']
tags: ['Politics','4/4']
---
There is a place for warriors. There is a place for story
tellers. Sometimes both groups forget there is a need for the other.
<!--more-->
# What Will Happen to Magic

Time-Signature: 4/4 Capo: 2 Comments: Style: Tempo:
{{< highlight none >}}

    E Am F G G Am

              Am         G      F                    Dm
    There’s a pen on the table. There’s a gun by the door
    C                     G          Am             Dm
      I hear you shouting beside me, you won’t hide anymore
    F                        G       F                  Dm
      Play the fife and drum loudly, calling all to the war
    E                             Am                F                G
      But when it ends will there be anyone who can say what is life for?

    Am                           G                  F            Dm
      You say that searching for meaning is for the privileged e-lite
    C                          G            Am              Dm
      But your heart may still yearn though we reach equali-ty
    F                     G       F                        Dm
      What will happen to magic?  Don’t lose the wonder of all
    E                        Am       F                       G
      We need you warriors a-mong us, but we can’t let beauty fall
    G/B G    Am
    We  need all


    Am                 G        F                Dm
      I am a teller of stories; I am a writer of songs
    C                         G           Am                   Dm
      meant to tell what your soul needs, I’m not a righter of wrongs
    F                  G       F                   Dm
      As you ready for battle, tempers hot in your veins
    E                      Am        F                    G
      I’ll not sing of the bloodshed but of the hope that remains


    F                     G       F                        Dm
      What will happen to magic?  Don’t lose the wonder of all
    E                        Am       F                       G
      We need you warriors a-mong us, but we can’t let beauty fall
    G/B G    Am
    We  need all

{{< / highlight >}}
