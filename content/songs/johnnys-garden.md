---
title: "Johnny's Garden"
date: 2012-1-31
categories: ['songs']
tags: ['fun songs','4/4']
---
Genetic manipulation gone a bit astray.
<!--more-->
# Johnny's Garden

Time-Signature: 4/4 Capo: 0 Comments: Style: Slow and Soft Tempo:
{{< highlight none >}}

    Bm                        D
    Johnny was a gardener but Johnny dealt in drugs
       Em                              Bm
    He worked on speed for orchids, he worked on speed for spuds
         C                         Bm
    When spring would come around, Johnny's plants were seen
        C                               G            F#m      Bm
    and every single one of them, their sprouts were shooting green

    (Chorus)
    A
    Johnny what is in that stuff?
    G
    Johnny, don't you know
    D
    that drugs are really dangerous
        Em       D          Bm (End on E note)
    and all that stuff must go!


        Bm                                  D
    The birds all learned to stay away from eating Johnny's fruits
         Em                                  Bm
    They all found out the hard way that his apple trees were brutes
    C                         Bm
    Pollination was okay, but never eat the seeds
    C                                             G         F#m   Bm
    Johnny's plants had bites themselves and were known for dirty deeds


    Chorus

       Bm                   D
    Johnny took some plants to the county fair
        Em                                  Bm
    The other entries shrank away  from his roses’s deadly stare
         C                           Bm
    With tattoos on their petals and rings among their thorns
    C                                  G       F#m       Bm
    Johnny’s roses made you feel Their parents somewhere mourned


    Chorus

    Bm                        D
    Then one day it happened, John was seen no more
       Em                              Bm
    We called the Sheriff’s office and they broke down the door
    C                         Bm
    Vines as thick as pythons crawled from room to room
    C                   G        F#m     Bm
    Everybody knew that Johnny’d met his doom

    Chorus

{{< / highlight >}}
