---
title: "Wordsmith"
date: 2018-12-30
categories: ['songs']
tags: ['fun songs','4/4']
---
Just a little fun pretending wordsmiths exist the same as blacksmiths did.
<!--more-->
# Wordsmith

Time-Signature:4/4 Capo: 0 Comments: Style: Tempo:
{{< highlight none >}}

    (Intro and Final)
    E E G A
    We have a wordsmith in our town,
    G A G A
    E
    He makes the words for all around
    A A G E E D E
    Fa La Diddle Diddle Day (3 times)

    Verses
    A G D A
    He hammers verbs for you and me, common nouns he makes for free
    G Bm
    But there's a charge for artistry
    A A G E E D E
    Fa La Diddle Diddle Day (3 times)

    A G D A
    The preacher does his shopping there, Words of fire are forged with care
    G Bm
    But brimstone we do not ask where
    A A G E E D E
    Fa La Diddle Diddle Day (3 times)

    A G D A
    The poet has him turn a phrase on a metaphoric lathe
    G Bm
    On his prose we later gaze
    A A G E E D E
    Fa La Diddle Diddle Day (3 times)

    A G D A
    He can make a pickup line or a love song pure and fine
    G Bm
    The buyer's heart inspires design
    A A G E E D E
    Fa La Diddle Diddle Day (3 times)

    A G D A
    The salesmen go there for a pitch,sSimiles to sell their kitsch
    G Bm
    We wish he'd throw them in a ditch
    A A G E E D E
    Fa La Diddle Diddle Day (3 times)

    E G A
    We have a wordsmith in our town,
    G A G A
    bum, bum, bum, bum
    E
    He makes the words for all around
    A A G E E D E
    Fa La Diddle Diddle Day (3 times)

{{< / highlight >}}
