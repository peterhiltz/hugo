---
title: "One True Ogre"
date: 2005-2-31
categories: ['songs']
tags: ['fun songs','english country dance','3/4']
---
The Ogre is San Francisco Bay Area English Country Dance slang for the Dancemaster at the English Country Dance Playford Ball. If you don't know what English Country Dance is, think of the kind of dancing you might see in movies like "Pride and Prejudice".
<!--more-->
# The One True Ogre

Time-Signature: 3/4 Capo: Comments:  Style: Tempo:
{{< highlight none >}}

    D        Bm      C           D
    I am the ogre, I have you in hand, and
    Em            C         G         Am
    poussette pre-cision is what I de-mand,

      C         G           F             Am
    I said up a double, the lines are not straight,
        G                F            E        E
    you know things like that make me simply i-rate,
       C           D             Bm         E
    at least I can see that your gait is se-date,
       C            G        Am          Em
    so just for the moment I will not be-rate,

       C            G           F          Am
    my god, all the kicks I see out on the floor,
            G            F         E            E
    this is English, not contra, I want some dé-cor,
        C          D         Bm              E
    the Morris and Ceili are through the far door
    C           G          Am            Em
    if for some reason you feel you must soar,

          C          G     F         Am
    as to Morris and Ceili I must ad-vise
    G            F              E             E
    they will en-sure that your kicks synchro-nize,
         C                D        Bm          E
    come back when you're ready to gently styl-ize,
       C          G              Am          Em
    re-member I'm miked and will loudly chas-tise

{{< / highlight >}}
