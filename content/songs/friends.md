---
title: "Friends"
date: 2020-09-05
categories: ['songs']
tags: ['friends','6/8']

---
Lifelong friends who will pick you up when you are down are to be treasured.
<!--more-->
# Friends

Time-Signature: 6/8 Capo: 2 Comments: Style: flatpick Tempo: 125
{{< highlight none  >}}

    Am C G Am Am Am Am

    Am        C           G           Am
    Chris and Shannon and I had grown tall
    Dm              C           G         Dm
    Three friends together down by the seawall
         C           G          C             G
    When Shannon was married we danced at her ball
      Am        C            G             Am
    remembering dancing with leaves in the fall

        G        G             F             C
    The color of friendship is deep and it's bright
            G          G             C           G
    but you never know how strong it is till the night
         F        C                G          Dm
    When darkness falls and you're full of despair
           F           C               G          Am
    that's when a true friendship your life can repair

    Am           C            G         Am
    Then when he left her she wanted to go
       Dm            C            G             Dm
    to hide from the pain that he caused her to know
        C            G          C           G
    the bridges were burning as Chris and I fought
       Am             C             G       Am
    to bring her back safe from the suicide thought


    [Chorus]

    [Instrumental]
    Bm D A Bm
    Em D A Em
    D A D A
    Bm D A Bm
    Bbm Am Am7 Am

         Am             C                G          Am
    It's now been three weeks since that darkest of nights
        Dm            C                G           Dm
    She says that our love brought her back to the light
        C            G               C           G
    The love of true friends is most precious of all
        Am        C           G         Am
    and we walk together down by the seawall

    [Chorus]

{{< / highlight >}}
