---
title: "My Streets"
date: 2005-1-31
categories: ['songs']
tags: ['home','3/4']
---
You can never go home again. Or, if you do, it has changed and is not
the place you left.
<!--more-->
# My Streets

Time-Signature: 3/4 Capo: Comments: Style: Tempo:
{{< highlight none >}}

          C          C              F        G
    These used to be my streets not so long ago
          C          C                Am           G
    These used to be my streets now I don't know a soul
    Em           Em              F           Dm  Dm  Dm
    Walk up that hill bring back memories of old
          F          C              Am            G G G
    These used to be my streets But now they seem cold.


      F           F            Em      G
    A lifetime of living while moving around
        Am               Am           F          G
    the friends that you made, now in some other town
        Em         Em          Am              Am
    The distance increases and finally they're gone
         F        F          G      G
    till only the blood ties remain

        F            F            Em            G
    I'm back here on business and walk the high street
        Am         Am               F        G
    The faces have changed, there's no one I greet
        Em            Em          Am          Am
    The buildings all say I could move back today
          F         F          G   G
    But facades are all that I know

        F         F            Em          G
    One move and another, this country and that,
         Am         Am          F           G
    I'll fit in wherever, don't worry 'bout that
        Em           Em            Am      Am
    The ghosts of my presence will linger around
        F        F        G    G
    and greet me when I return

{{< / highlight >}}
