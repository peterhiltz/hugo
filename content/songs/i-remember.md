---
title: "I Remember Back"
date: 2012-1-31
categories: ['songs']
tags: ['childhood','politics','history','4/4']
---
Sometimes you wonder if the human race will ever stop the fighting.
<!--more-->
# I Remember

Time-Signature: 3/4 Capo: 0 Comments: Style: fingerpick Tempo:
{{< highlight none >}}

      Am       D         C     D
    I remember back when I was young
        F        C            G        E    Esus2
    the poems we read and the songs we sung
             F          C              E        Am
    when the stars were bright and the moon was high
    D       Am               F           Am  Amsus2 Am Amsus2
    ghostly galleons sailing untroubled skies

          C          G      F     Am
    Those tunes we remember yet today
       F     C         E         Am
    create a debt that we must repay
        F           C        G           Am
    the writers are gone but their art remains
        C            G        F          Am  Amsus2 Am Amsus2
    and their life's work our singing sustains


      C        G         F       Am
    I wish the world was still today
        F       C    E               Am
    the smiling face those lines did say
        F            C       G        Am
    and when'ere the world intrudes upon
       C        G      F               Am Amsus2 Am Amsus2
    my childish mind I sing till it's gone

      Am       D         C     D
    I remember back when I was young
        F        C            G        E    Esus2
    the poems we read and the songs we sung
             F          C              E        Am
    when the stars were bright and the moon was high
    D       Am               F           Am  Amsus2 Am Amsus2
    ghostly galleons sailing untroubled skies

{{< / highlight >}}
