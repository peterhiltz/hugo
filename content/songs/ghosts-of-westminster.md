---
title: "Ghosts of Westminster"
date: 2020-09-05
categories: ['songs']
tags: ['Politics','3/4']

---
There is a difference between _politician_: "one engaged in party
politics, especially as a trade; one who promotes the interests of a
political party, one concerned with public affairs for the sake of
profit or of a clique." and _statesman_: "enlightened, disinterested,
and high-minded service to the state or the people of the state".

We usually have too many politicians and not enough statesmen.
<!--more-->
# Ghosts of Westminster

Time-Signature: 3/4 Capo: 0 Comments: Style: flatpick Tempo: 120
{{< highlight none >}}

    C C E E F F E E

    C                             E
    The ghosts of Westminster are crowded together
    F                      E
    soldiers and heroes of wars long ago
    Am                   G
    they fought and they died for empire and monarch
    F                        E
    remembered in marble and prayed to in woe

    Am                   F
    Many a churchyard is filled with the memories
    G              E
    of parents and cousins and children and such
    Am                         Dm
    Here in the Abbey, they're stacked like an attic
    E                           Am
    the rich and the famous, so seldom in touch
    Dm                                   C
    with the needs of the people, as our dreams start to fall
    G                                      Am
    while the wars of the mighty spill the blood of us all

    C                               E
    The ghosts of Westminster, dead men of the nation
    F                    E
    morally certain that their cause was right
    Am                    G
    and maybe it was, and maybe it wasn't
    F                              E
    but we're fodder for cannon in dawn's early light

    Am                           F
    Yet the populace has need of leadership too
    G                       E
    insisting compassion is needed today
    Am                          Dm
    still historians bicker and viewpoints will differ
    E                              Am
    if they were cowards when they turned  away
    Dm                                  C
    from the needs of the people as our dreams start to fall
    G                             Am
    while the wars of the mighty take the life of us all


{{< / highlight >}}
Note: Lots of tempo changes
