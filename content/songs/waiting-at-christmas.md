---
title: "Waiting at Christmas"
date: 2015-12-30
categories: ['songs']
tags: ['christmas','stories','work','3/4']
---
Spouses have been waiting for their loved ones to return from business
trips for centuries. This story happens at Christmas.
<!--more-->
# Waiting at Christmas

Time-Signature: 3/4 Capo: 4 Comments: Style: Relatively fast picking Tempo:
{{< highlight none >}}

         G         C         D7     G
    It's late in December in 18 and 80
        G                C               F            D
    the snow has stopped falling but the winter winds cold
    C          Em            F          D
    Marjorie's wrapped in an old woolen blanket
        C         Em           C           G
    and paces the porch as she watches the bay


    C     D        G      Em        C          D
    Now I wait for you to call, its been three weeks
        Em           G         C         D       G        Em
    the children are small and Christmas time is here you know
       C        D      G
    We wait for you at home


    G          C               D7        G
    Marjorie's husband's first mate on a clipper
       G            C             F          D
    He sails out of Rockport; She misses him so
       C           Em            F          D
    He promised to her that he'd be back by Christmas
           C             Em          C           G
    so she waits and she worries and watches the bay


    C   D      G    Em       C       D
    You left upon a plane to do some deal
         Em          G            C        D        G       Em
    they needed your name but the children need you just as much
         C        D      G
    they wait for you at home

    G           C           D7             G
    Finally she sees it the storm-battered clipper
    G           C         F        D
    passing the point and into the bay
    C            Em             F           D
    Christmas is here and God's granted her wishes
        C            Em        C                G
    the best present she could receive from the bay


    C     D         G        Em            C      D
    Child-ren, your mother's called. She's on her way,
         Em           G       C         D       G        Em
    with presents for all and Christmas time is here, we know
          C           D        G     Em
    We're waiting for Margy at home
               C            D                      G
    Marjorie's great, great granddaughter's coming home

{{< / highlight >}}
