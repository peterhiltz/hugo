---
title: "Call of the Loon"
date: 2020-09-02
categories: ['songs']
tags: ['autumn','love','3/4']

---
Fall is my favorite time of year. Fall in New England is especially
entrancing. This has also been called a love song.
<!--more-->

# The Call of the Loon

Time-Signature: 3/4 Capo: 0 Comments: Style: flatpick Tempo: 128
{{< highlight none >}}

    G A7 D D

    D               Dsus4      E
    The hillside is shimmering yellow and golds
    G                 D
    Splashes of red as autumn unfolds
    Bm                       E
    Fall in New England, the harvest is done
        G           A7              D
    The maples have dressed for the ball

    [Chorus]
            Em                       F#
    And the call of the loon echoes deep in your mind
    C                        G
    Mournful and searching, another to find
        Bm                       C
    The sound of the wind in the trees
    A7                 D
    Signals changes to come
    A
    W[h]eather for traveling
    G                           F#
    W[h]eather for laughter and fun


    D          Dsus4         E
    Winter and spring seemed so long ago
    G                     D
    Summer was heavy, the air was so slow
            Bm                   E
    Now the chill of October has quickened your blood!
        G          A7      D
    You want to go dancing again

    [Chorus]


    D                 Dsus4      E
    Though others are fearful of snow yet to come
        G                        D
    And make plans for traveling south to the sun
           Bm                     E
    You'll answer the loon with a cry of your own
             G            A7           D
    And your soul will go dancing with him


{{< / highlight >}}
Note: Don’t slow down on chorus
