---
title: "Melancholy Dancer"
date: 2014-1-31
categories: ['songs']
tags: ['sadness','6/8']
---
Thinking about people who can't recover from sadness and listening to
an eclectic mix of music from Sibelius tone poems to Portuguese Fado.
<!--more-->
# Melancholy Dancer

Time-Signature: 6/8 Capo: Comments: Style: Tempo:
{{< highlight none >}}

    D          D           D     D Gmaj7      Gmaj7   Gmaj7    Gmaj7
    Softly the music would start,  Orchestral strains tint the air
     A     A              A      A  G         G        A      A
    Soon I would hear the dancer,   Scuffling gently upstairs
    G          G             Cmaj7 Cmaj7 D           D          Gmaj7 Gmaj7
    Turning in graceful slow arcs,       lost in her own closet world
      A              A       A     G           G           A    A
    Expressing quiet pity, regret, loss of the dreams of a girl

                  Bm     Bm  D            D
    And she would dance, all alone in her chambers
       Em          Em           A     A
    My heart would ache for her loss
        Bm        Bm      A         A
    But she would not let go of the sadness
       Em         Em            A     A
    No matter how much it could cost

    D       D             D   D     Gmaj7        Gmaj7
    Dancing alone in her room, to a sad waltz by Sibelius
    A            A         A    A  G        G    A   A
    Poignant and beautiful swan,   Circling Tuonela
    G              G        Cmaj7 Cmaj7 D        D        Gmaj7 Gmaj7
    wrapped in her gossamer wings,      solace internally dealt
    A             A         A    A     G        G        A     A
    She would not now be at ease,   if ever she joyfully felt


                  Bm     Bm  D            D
    And she would dance, all alone in her chambers
       Em          Em           A     A
    My heart would ache for her loss
        Bm        Bm      A         A
    But she would not let go of the sadness
       Em         Em            A     A
    No matter how much it could cost
    G   Em    D
    She would dance

{{< / highlight >}}
