---
title: "River Kerry"
date: 2014-3-31
categories: ['songs']
tags: ['home','stories','4/4']
---
Fictional small town life with no need for city excitement.
<!--more-->
# The River Kerry

Time-Signature: 4/4 Capo: 0 Comments: Style: Strum Tempo: 130
{{<  highlight none >}}

    Em D C Em

        Em          Em                    D                D
    The River Kerry runs down through the town that I love best
       C               C            G             G
    It carries all the children far away from the nest
         Em             Em                  D                  D
    They go down to the cities where folk's hearts are made of stone
    C              B     Em                   Em     Em
    Then the River Kerry brings them all back home

            G                    D
    You can keep your glittering neon
            C                     Em
    You can keep your late nights out
            G                    D             Em
    You can keep your search for riches to yourselves
             G               D
    For this little town and river
          C                  Em
    is my home and here I'll stay
         G             D               Em    Em
    I'll never need to roam along your way

        Em          Em             D                D
    The River Kerry falls from the mountains in the west
      C                 C                  G                 G
    I walk its banks, I fish its pools, it keeps my heart at rest
       Em             Em             D                D
    It runs under the ancient bridge beyond its final fall
         C               B             Em   Em
    I'll always heed the River Kerry's call

    Chorus

         Em           Em             D                 D
    I've no desire to see the cities touched by it downstream
         C              C                 G                G
    They can't begin to know the depth it brings into your dream
         Em                  Em               D           D
    Here hearth and home and fishing skiff it all is intertwined
       C                B             Em              Em    Em
    No matter where you go, the River Kerry's in your mind

    Chorus

{{< / highlight >}}
