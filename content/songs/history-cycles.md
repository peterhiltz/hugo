---
title: "History Cycles"
date: 2012-1-31
categories: ['songs']
tags: ['politics','history','4/4']
---
Sometimes you wonder if the human race will ever stop the fighting.
<!--more-->
# History Cycles

Time-Signature: 4/4 Capo: 4 Comments: Style: Tempo:
{{< highlight none >}}

    Am                   G              F                     G
      See the child that we have taught to fight the wars our fathers fought
    Am              G              Am
      and history returns again in cycles

    G                    F         G                       F
      Regardless what we say today;   no matter what their mothers say
    G                          F             Am
      they'll fight all of our wars again in cycles

    E                                     F
      The wars will all come back around,   the calling drum and trumpet sound
    G                       F             E7 (slower)
      'gainst sickness, for those in need,   or simply cause we are in greed

    Am                  G          F                        G
      But God is surely on our side   and therefore we will fight with pride
    C                     G                  Am  Asus4 Am
      while people by the thousands died and history   remains

    Am                        G       F                         G
      Some will fight 'gainst poverty   or for the people to be free
    Am                G              Am
      while history repeats again in cycles

    G                   F               G                      F
      Others make their fights for land   and take again their father's stand
    G                         F                Am
      "Let's throw the others out again"; It's cycles

    E                               F
      Still we hope the day arrives   when death and pain are not our lives
    G                            F            E7 (slower)
      we'll break the links that keep us tied   with chains held heavy at our side

    Am                       G              F                     G
      We'll think not back upon these days.   The sullen word and angry haze
    C                   G               Am  Asus4  Am
      that kept us in a circle maze and history is past

{{< / highlight >}}
