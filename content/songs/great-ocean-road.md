---
title: "Great Ocean Road"
date: 2019-09-05
categories: ['songs']
tags: ['stories','4/4']

---
I wrote this on a paper placemat in a restaurant in Melbourne,
Australia after doing a tour of the Great Ocean Road and hearing a bit
of its history. Nearby diners were either amused or confused as the
waitress kept trying to put the food on the placemat and I kept pushing
it away because I didn't want to lose the melody and my train of thought.
<!--more-->
# Great Ocean Road

Time-Signature: 4/4 Capo: 4 Style: Strum Tempo:
{{< highlight none >}}

       Am               C                G        F         Am
    Me mates and I came back from war in nineteen eight and ten
          C               G              F              E
    There was no work for anyone but the guvn'r said "I ken"
           F                C              G                Am
    "We'll put you building roads upon the southern coastal cliffs"
    Am                  C             G       F        Am
    "Reaching down from Melbourne and helping stricken ships"

         Am           C             G         F  Am
    They gave to me a pickaxe and a barrow to me mate
    C                 G                   F                E
    Pointed south and at the mouth of the Murray we was to wait
        F                 C                G                  Am
    And there we joined a thousand men all come back from the war
    Am              C                   G       F       Am
    This would be a different hell than what we faced before

        Am                C         G            F   Am
    The first year it was easy, our muscles just got tough
        C               G               F                E
    But then we hit the cliffsides, and knew we done the fluff
        F                C          G              Am
    The first step was a big one, a hundred meters down
      Am               C                G       F      Am
    upon the rocks and crashing surf in service to the crown

        Am                C              G       F       Am
    The next ten years we lost almost as many as been in war
        C                  G           F                  E
    the first slip and the second, and then there'd be no more
       F                    C                   G             Am
    We built that road with sweat and blood, it never done us in
        Am            C        G            F      Am
    and now the gypsy traveler drives where we hae been
        Am            C        G            F      Am    G Dm F E Am
    and now the gypsy traveler drives where we hae been

{{< / highlight >}}
