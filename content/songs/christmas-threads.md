---
title: "Christmas Threads"
date: 2020-09-03
categories: ['songs']
tags: ['Christmas','Friends','4/4']

---
Remember christmas cards and sitting down at the kitchen table,
sending out those once a year notes to friends and relatives. Facebook
is not quite the same.
<!--more-->
# Christmas Threads

Time-Signature: 4/4 Capo: 0 Comments: Style: Tempo:
{{< highlight none >}}

    D                A                 G                A
    Behind the crass commercials there yet remains some threads
       D                 A                    G                F#
    of Christmas, and of what it means in the cupboards of our heads,
        D                  G               A               D
    And many of us, though miles away pull out our address list
        G             D              A         G          D
    and pen a note or simple card to those our lives have missed

      D                A           G                A
    A card's receipt reminds us of patterns we have spun
        D                A            G           F#
    the thread remains unbroken, long years ago begun
        D               G       A                 D
    And those of us who wander across the endless miles
        G                 D            A     G        D
    are glad to find that somewhere we lit a few glad smiles

        D                A          G                A
    The thread creates a fabric our lives create the weave
        D                A           G                 F#
    the woof and warp connect us all down the years we leave
      D             G           A              D
    behind us and between us we still remember when
        G                 D             A         G       D
    the thread began long years ago and trace its course again

       D             A          G               A
    To those of us religious to those of us now not
        D              A           G                  F#
    the season still reminds us of blessings love has wrought
        D                  G           A                D
    and past the Christmas glitter the stores and candy canes
           G             D             A      G        D
    though far we go and long away the loving thread remains

{{< / highlight >}}
