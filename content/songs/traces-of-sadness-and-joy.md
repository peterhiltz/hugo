---
title: "Traces of Sadness and Joy"
date: 2015-3-30
categories: ['songs']
tags: ['wedding','stories','3/4']
---
Written for a relative's wedding. Nothing deep, just a pleasant little
song.
<!--more-->
# Traces of Sadness and Joy

Time-Signature: 3/4 Capo: 0 Comments: Written for a niece's wedding Style: Tempo:
{{< highlight none >}}

       G          C              Am          Em
    On top of the dresser sits a small empty box
      Am         G         F            Am
    occasionally buried in t-shirts and socks
         F           C            G          Am
    when asked if it should be in one of the drawers
    G         Em          G            Em
    Amy would smile - and pause in her chores
    C            D       F               Am
    hold the box gently against her left hand
    G            C      D           Am
    matching the box to her wedding band
        G            D           G           D
    and softly would sing to the box and the ring

    [Chorus]
    Dm        C          Dm        Am
    Traces of happiness, traces of tears
    G         Bm        C         D7
    Traces of hopes and traces of fears
    G            Dm       C          G
    Just a small box with rings to enjoy
    Am      Dm        G           Am
    holding traces  - sadness and joy


        G           C                Am           Em
    Its been thirty years since they married that spring
    Am            G            F           Am
    learning that loving needs more than a ring
           F               C                G        Am
    they'd kiss and they'd fight and they'd struggle along
    G               Em                   G          Em
    somedays they'd think - they've done everything wrong
               C            D            F          Am
    but they'd match up the box to their circles of gold
      G           C            D         Am
    reminded that life can not simply be strolled
        G            D           G           D
    and softly would sing to the box and the ring

    [Chorus]

    G               C            Am            Em
    Rings, they are pretty, with symbols we've known
        Am          G         F         Am
    the circle, the gold, the shiny cut stone
    F           C             G            Am
    all were contained in the box that you see
         G         Em           G           Em
    with all the potential - of life yet to be
    C              D             F          Am
    love's so much more than the top of the hill
           G            C   D             Am
    If you fall and get up, holding hands still
    G            D           G           D
    Then you can sing to the box and the ring

    [Chorus]

    Dm  C G Am

{{< / highlight >}}
