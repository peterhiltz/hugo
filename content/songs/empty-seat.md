---
title: "Empty Seat"
date: 2020-09-03
categories: ['songs']
tags: ['relationships','4/4']

---
After the relationship is over and the fights have ceased.
<!--more-->
# Empty Seat

Time-Signature: 4/4 Capo: Comments: Play the Bb as an A# barre on 6th fret Style: fingerpick Tempo:
{{< highlight none >}}

    Am                  Gsus4    C
    There's no fighting oe'r the rolls
    E                 Am
    There's no voices raised in anger
    Dm         Am      G      Am
    There's no stories left untold
    Dm      E     Am
    Just an empty seat of wicker

    E         C        Em        Am
      I don't know how long it's been
    Bb      Gm      Am    FAm
    I don't care to see a stranger
    D      Dm       Am     G
    I will raise my glass again
    Am  C           G     Am
    And salute your empty chair

    Am            Gsus4     C    Am
    All I know is that it's peaceful
    E             Am
    Having dinner all alone
    Dm      Am     G        Am
    And the menu's lost the danger
    Dm       E      Am
    Of which one is on the throne.

    E         C        Em        Am
      I don't know how long it's been
    Bb      Gm      Am    FAm
    I don't care to see a stranger
    D      Dm       Am     G
    I will raise my glass again
    Am  C           G     Am
    And salute your empty chair

{{< / highlight >}}
