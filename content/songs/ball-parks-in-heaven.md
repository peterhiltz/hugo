---
title: "Ball Parks in Heaven"
date: 2020-09-01
categories: ['songs']
tags: ['memorial','4/4']

---
Memorial to a minor league baseball fan.
<!--more-->
# Ball Parks in Heaven

Time-Signature: 4/4 Capo: 0 Comments: Style: Tempo:

    G                        Em               C              D
      There's a ball park in heaven where the minor leaguers play,
    Em                D               C          Am
      those who never made it big but love it anyway,
    Em                  C                  G                D
      the popcorn still tastes as good the hot-dogs are the same
    Em                      C                 D                G
      the fans who come all love the game and not the players' names

                  Am          D       G               Em
    And there's a seat in the crowd engraved with his name,
      C                 D                   Em              Am              D
    a seat just halfway down the first base line. It's been waiting for him there
           G                Em       Am                    C              D
    and he gladly takes his chair to watch the sport where players number nine


              G         Em          C               D
    It's been many many years since he saw his last park
            Em        D                   C                 Am
    but the radio maintained his link and kept him from the dark
    Em                  C         G                    D
      he knew a hundred stations, announcers who would call
    Em                 C                   D                 G
      what this umpire judged a strike and that one judged a ball

                  Am          D       G               Em
    And there's a seat in the crowd engraved with his name,
      C                 D                   Em              Am              D
    a seat just halfway down the first base line. It's been waiting for him there
           G                Em       Am                    C              D
    and he gladly takes his chair to watch the sport where players number nine
       Am                    D              G
    to watch the sport where players number nine
