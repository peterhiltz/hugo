---
title: "Sing Me a Valentine"
date: 2016-3-31
categories: ['songs']
tags: ['relationships','stories','6/8']
---
When your SO wants to know what to buy you for your birthday/christmas
etc.
<!--more-->
# Sing Me a Valentine

Time-Signature: 6/8 Capo: 2 Comments: Style: Tempo:
{{<  highlight none >}}

         Dm            Dm           Em      Em
    Last Christmas you asked what I wanted
           C       C             G      G
    like a grocery list from the store
           F              F           C          C
    Well I hope you won't mind when I ask you to find
    G              G             E    E
    Something that means so much more

    (Chorus)
    G         G
    Sing me a valentine
    C       C
     I want something
       Dm       C             G       G
    of you, not something you bought
    G         G
    Sing me a valentine
    Dm            C
      You're more precious than
    G           Am    Am
    diamonds or gold

    Dm   Dm           Em    Em
    All around us the media ads
    C          C           G     G
    promise my love if you buy     but
    F               F        C             C
    they don't know me and I hope that you see   the
    G         G            E     E
    key to my heart if you try

    (Chorus)

    Dm         Dm        Em         Em
    Better for me than a big screen TV  is
    C              C               G      G
    something that comes from your heart
    F           F             C         C
    Sing it off key, it won't matter to me, a
    G             G            E     E
    touch of your soul is like art

    (Chorus)

        Dm             Dm         Em       Em
    You may think your words seem terribly trite
       C             C          G    G
    compared to some prose on a card    but
    F              F             C        C
    you shine much brighter than any card writer  and a
    G              G           E     E
    song from your soul like a bard

    (Chorus)

{{< / highlight >}}
