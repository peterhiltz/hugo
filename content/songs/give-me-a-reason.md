---
title: "Give Me a Reason"
date: 2020-09-05
categories: ['songs']
tags: ['childhood','politics','4/4']

---
For my daughter when she was in junior high school. If you insisted on
a rule and justified it because the authority figure said so, you
guaranteed a fight. If you gave reasonable reasons for the rule, there
wouldn't be any problem. I've seen reports that up to 30% of any human
population consists of authoritarians. Those in power want mindless
obedience. Even those not in positions of power want to be
thoughtlessly led and will insist on mindless obedience to authority
and punishment otherwise.
<!--more-->
# Give Me a Reason

Time-Signature: 4/4 Capo: 3 Style: All downstroke strums Tempo:
{{< highlight none  >}}

    A                                 C Bm  A
    Don't want your preaching or your rule -book
    G                          A
    That's not a bible in your hands
    A                 C    D    A
    If you want me to play your game
    G                     A
    Give me a reason, not demands

    [Chorus]
    D                        C
    Give me a reason, not an order
    D                           Bm
    Give me a reason that makes sense
    D                         C    D    Em
    Give me a reason and I'll walk with you
    F                E      A      E
    Give me a reason, not a fence

    A                         C  Bm A
    If you want that I should go along
    G                      A
    and you don't want any fight
    A                      C    D  A
    Tell me why, that will make it strong
    G                       A
    Tell me why the rule is right

    [Chorus]

    A                     C    Bm  A
    If you want for me to hang the moon
    G                         A
    Tell me why it would make sense
    A                           C DA
    Then you'll wake up, in the morning
    G                           A
    to find it on your own back fence

    [Chorus]

{{< / highlight >}}
Notes:  The E chord at the end of the chorus has no words, it is an interlude
The 3 chords together in both verse and chorus should be strummed once each
