---
title: "Kathy's Waltz"
date: 2014-1-31
categories: ['songs']
tags: ['wedding','3/4']
---
Written for the wedding of one of my sisters. She teaches kindergarten
and some of the children were worried that she would leave after she
got married.
<!--more-->
# Kathy's Waltz

Time-Signature: 3/4 Capo: 0 Comments: Style: strum Tempo: 126
{{< highlight none >}}

    D A G A Em A D D

    D           A        G            A
    Christopher Robin is watching the stage
    G          A       D             A
    all of his friends sit there and gaze
             D          A          G               A
    and they worry that she is now gone from their midst
        Em       A           D
    for Kathy is dancing her waltz

    D        A           G         A
    Kathy is dancing and spinning around
    G                 A            D         A
    that much they've seen without causing a frown
             D             A          G           A
    but it's not with them carried up high in the air
    Em       A            D
    Kathy is dancing with Him!

    (Chorus)
    Em                       A
    Dancing and spinning and twirling
        D         A             Bm
    the music has lightened her feet
          Em                         A
    while others watch breathless in silence
    D            E              A
    wishing they could dance as sweet

        D             A         G          A
    The song has just ended and they turn away
        G        A             D         A
    and Eeyore decides he will call it a day
        D          A            G             A
    but Kathy just snatches him back from the door
        Em       A            D
    now Kathy is dancing with him!

    D        A           G         A
    Kathy is dancing and spinning around
      G                A                D         A
    although she's now married, they'll still be around
        D          A               G           A
    and Kathy will dance with them high in the air
         Em            A            D
    even though she is dancing with Him

     (Chorus)

    D        A           G         A
    Kathy is dancing and spinning around
      G                A                D         A
    although she's now married, they'll still be around
        D          A               G           A
    and Kathy will dance with them high in the air
         Em            A            D
    even though she is dancing with Him

{{< / highlight >}}
