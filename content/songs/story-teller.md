---
title: "Story Teller"
date: 2020-4-30
categories: ['songs']
tags: ['stories','4/4']
---
A tale of someone who wrote stories for his children and now wants to
write them down for posterity.
<!--more-->
# The Story Teller

Time-Signature: 4/4 Capo: 2 Comments: Style: Tempo:
{{< highlight none >}}

    Em A

      D              G
    I met him in To-ronto, he was
    A           D
    90 years of age, he
    G              G
    spoke to me of stories he had
    A               A
    written without paper, He
    Em               Em              A    A
    told them to his children in the dark    the

    D                G
    stories they had heroes, they had
    A                  D
    villains, they had puppies, while
    G                G
    others talked of writing he had
    A             A
    done so every bedtime, his
    Em                     Em            A     A
    children learned their lessons every night    what was
    Em                 A     A
    wrong and what was right

    D                 G
    Now his eyes were fading, he had
    A                   D
    meant to write them down
    G               G
    And he asked me kindly would I
    A          A
    write them down on paper, now
    Em            Em              A
    all of us are children in the dark ,all be-
     Em                A       A
    -cause of one mans spark      I

    D             G
    met him in To-ronto, he was
    A           D
    90 years of age, he
    G              G
    spoke to me of stories he had
    A                A
    written, without paper, he
    Em               Em              A    A
    told them to his children in the dark

{{< / highlight >}}
