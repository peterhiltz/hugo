---
title: "Susanna the Healer"
date: 2024-11-02
categories: ['songs']
tags: ['life','Caring']

---
This song was written about an episode in a cozy fantasy novella called Andras Hill, which can be found at [Anthracyda.org](https://anthracyda.org). 

<!--more-->

# Susanna the Healer
Susanna The Healer         (4 /4 Capo 3       Bm  Strum 105 bpm)

{{< highlight none >}}

(Start quiet)
F F Em Am C D D Am C D D

Verse 1
      Bm             Bm                   C            C
Su-sanna was 'prenticed to an old wise woman
       Em                (Am7 Am)     B7
She brought some peace to the sick
C     C                  D               Em
Just 15 when the old woman died,
       G               D                  Em    Em
now Susanna is all they have left
       G                D                 Em    Em   Em
now Susanna is all they have left
      C           C                     D                  Em
The villagers knew she was younger than young
      F           F                 (Em  G Dm)  Dm
Her medical training had just begun
          Am      C G Em
In the year of 1645
    F                 F              Em     Em
Susanna must keep them aLIVE
    F                 F                 Em     Em
Susanna must keep them aLIVE


Chorus
      G       G                 F         C
The Vicar says  "God's will be done"
        Dm      Dm          Am        G
Just pray to him, he'll send his Son
    F         F          B7           C
Susanna are you much too young
     G          G         Dm       G
To heal the sick or will you run?
      Em            Em       F        G
We know your life has just begun
       F             F        Em          Am     C D D
But don't give up, or we'll have none


Verse 2
Bm               Bm               C             C
Witchfinders search to the east and west
              Em    (Am7 Am)      B7
There's a fever running         round
   C           C           D      Em
If patients die, will she be blamed
       G         D             Em      Em
and end up hanged or burned
       G         D              Em      Em  Em
and end up hanged or burned
       C        C           D                   Em
She hasn't slept for three straight days
           F               F               (Em   G      Dm)    Dm
As she mops their brows and gives them tea
        Am     C             G       Em
with willow bark and wintergreen
    F                 F               Em    Em
Susanna must keep them aLIVE
    F                 F               Em    Em
Susanna must keep them aLIVE


Verse 3
Bm            Bm     C         C
Finally the fevers start to break
       Em         (Am7 Am)   B7
She rests her wea - ry      head
       C                 C          D             Em
The church bells ring out loud and clear
G             D                          Em     Em
Praise for those who've been saved
G             D                          Em     Em   Em
Praise for those who've been saved
           C       C            D       Em
Let the Vicar claim his victory
       F          F    (Em  G   Dm)   Dm
The Baker's wife said softly
Am              C                  G              Em
Here's six loaves for the girl at my side
       F            F          Em    Em
The one who kept me aLIVE
       F           F           Em     Em
The one who kept me aLIVE

{{< / highlight >}}

