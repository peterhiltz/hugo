---
title: "About"
description: "Onion peeling commentary on the world. Sometimes tears and always lots of layers."
date: "2020-07-04"
author: "Peter Hiltz"
---
Onion peeling commentary on life and the world. Sometimes tears,
sometimes songs and always lots of layers.
