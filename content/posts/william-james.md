---
title: "I think William James Undercounted"
date: 2021-02-07T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
One of many quotes attributed to William James is "*Whenever two people meet, there are really six people present. There is each man as he sees himself, each man as the other person sees him, and each man as he really is.*" Interestingly, no collection of quotes actually provides a cite to when or where he said it. A similar concept was raised by [Oliver Wendell Holmes in "The Autocrat of the Breakfast Table"](http://www.ibiblio.org/eldritch/owh/abt03.html). In any event, I think they both may have under-counted.

My point is maybe a little clearer looking at the Oliver Wendell Holmes version when he refers to "John's **ideal** John". I'll ignore the change in usage of "ideal" over the years. I fully expect my daughter the clinical psychologist to correct me here, but I'll suggest that people may have multiple versions of themselves. There is the "Me at my best" (what I think of as the "ideal me" which may or may not actually be an ideal me), and lots of different versions of "Me when I'm not at my best" which may depend on whether its "Angry Me" or "Sad Me" or "Bored Me" or "Morning Me" or "Nighttime Me" or "Exhausted Introvert After Trying to be Friendly for Three Hours Me" or "Me After You've Just Said Something I Consider Really Stupid or Really Insensitive" ... Each one a little different and both I and the other person in the room need to handle the different versions of "Me" maybe slightly differently if the engagement is to be optimized.

Anyway, just another thought about how personal interactions are complicated and difficult. Now back to trying to figure out whether a particular customer service representative just doesn't care about their job or they are trying to meet metrics imposed by their employer which incentivizes delay and non-resolution of problems..

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
