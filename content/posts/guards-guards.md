---
title: "Sound Familiar? Extracts from Guards, Guards"
date: 2020-11-19T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
These are extracts from Guards, Guards, a fantasy novel written by the late Terry Pratchett in 1989. Sound familiar to any current events? Historical events?
### The Supreme Grand Master
"What a shower, he told himself. A bunch of incompetents no other secret society would touch with a ten-foot Sceptre of Authority. The sort to dislocate their fingers with even the simplest secret handshake. But incompetents with possibilities, nevertheless. Let the other societies take the skilled, the hopefuls, the ambitious, the self-confident. He'd take the whining resentful ones, the ones with a bellyful of spite and bile, the ones who knew they could make it big if only they'd been given the chance. Give him the ones in which the floods of venom and vindictiveness were dammed up behind thin walls of ineptitude and low grade paranoia."
...
"The Supreme Grand Master listened to this with a slightly light-headed feeling. It was as if he'd known that there were such things as avalanches, but had never dreamed when he dropped the little snowball on top of the mountain that it could lead to such astonishing results."
...
"I think, said the Supreme Grand Master, tweaking things a little, that a wise king would only, as it were, outlaw showy coaches for the undeserving. There was a thoughtful pause in the conversation as the assembled Brethren mentally divided the universe into the deserving and the undeserving, and put themselves on the appropriate side."
...
"The Supreme Grand Master smiled in the depths of his robe. It was amazing, this mystic business. You tell them a lie, and then when you don't need it any more you tell them another lie and tell them they're progressing along the road to wisdom. Then instead of laughing they follow you even more, hoping that at the heart of all the lies they'll find the truth. And bit by bit they accept the unacceptable. Amazing."
...
### The Patrician
"'I believe you find life such a problem because you think there are the good people and the bad people. You're wrong, of course. There are always and only, the bad people, but some of them are on opposite sides.' He waved his thin hand towards the city and walked over to the window. 'A great rolling sea of evil. Shallower in some place, of course, but deeper, oh, so much deeper in others. But people like you put together little rafts of rules and vaguely good intentions and say, this is the opposite, this will triumph in the end. Amazing.'"

"Down there are people who will follow any dragon, worship any god, ignore any inequity. All out of a kind of humdrum, everyday badness. Not the really high, creative loathsomeness of the greater sinner, but a sort of mass-produced darkness of the soul. Sin, you might say, without a trace or originality. They accept evil not because they say yes, but because they don't say no."

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
