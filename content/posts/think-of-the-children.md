---
title: "Think of the Children and Other Political Lies"
date: 2020-11-05T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
If politicians actually believed in their pleas to 'think of the children', they wouldn't make so many outright lies. Think of the bad example it sets. And that goes double for the people writing the lies in the political ads. Criminal defense lawyers are not allowed to lie for their clients. Why is it allowed in the political process?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
