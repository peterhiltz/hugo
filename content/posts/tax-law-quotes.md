---
title: "Tax Law Quotes"
date: 2020-09-08T12:18:19-04:00
categories: ['posts']
tags: ['tax law', 'quotes']
showToc:  "true"
author: "Peter Hiltz"
---


1. The problem is not just that the law is overly complex. The problem
   is also that we change it all the time.

2. We [Judges of the U.S. Tax Court] have from time-to time complained
   about the complexity of our revenue laws and the almost impossible
   challenge they present to taxpayers or their representatives who
   have not been initiated into the mysteries of the convoluted,
   complex provisions affecting the particular corner of the law
   involved. . . . Our complaints have obviously fallen upon deaf
   ears.

3. [T]wo things are abundantly clear. One, tax simplification is
   desperately needed. Two, any effort is likely to be an unmitigated
   disaster.

4. Lawyers are trained to ask questions and raise issues about
   everything. This is especially true for tax lawyers, who tend to
   approach issues from a more analytical perspective. The result is
   that in many commercial endeavors, tax lawyers are likely to raise
   a number of perplexing, and often unanswerable, tax issues. Their
   clients may be presented with dense explanations of confusing
   statutory rules implemented by sleep-inducing regulations. The
   issues may be publicly vetted at tax seminars; then "fascinating"
   law review articles may be generated and even treatises may be
   published with unlimited footnotes raising eternal questions. This
   is the state of play that keeps the tax bar occupied (and for most
   of us, happy) day after day, year after year, and, as it has turned
   out, decade after decade.

5. All the Congress, all the accountants and tax lawyers, all the
   judges, and a convention of wizards all cannot tell for sure what
   the income tax law says.

6. Every stick crafted to beat on the head of a taxpayer will
   metamorphose sooner or later into a large green snake and bite the
   IRS commissioner on the hind part.

7. Most tax planning adds little or nothing of worth to our
   society. . . . Tax lawyers perform a legitimate role of
   interpreting the law, instructing clients in the sometimes bizarre
   requirements the law imposes to get a given tax treatment, and
   planning transactions to avoid the occasional warts in the
   system. It is an honorable profession, and I am a proud member of
   it. But let's not kid ourselves that most tax planning is
   productive.

8. The problem with practicing tax law is that the general rule never
   seems to apply to anything.

9. Lots of people talk about simplifying the tax law. And lots of
   people agree that simplifying the tax law should be a policy
   priority. But the problem is that simplification is complicated,
   and it is politically dangerous work.

10. Complexity does not enter the tax code so much out of malevolence
    as through misguided reform efforts and excessive demands made on
    tax laws as the vehicle for implementing public policy.

11. Transfer pricing may be an art rather than a science, but why does
    it have to resemble Pablo Picasso?

12. The tax bar is the repository of the greatest ingenuity in America
    and given the chance those people will do you in.

13. There is an ancient belief that the gods love the obscure and hate
    the obvious. Without benefit of divinity modern men of similar
    persuasion draft provisions of the Internal Revenue Code. Section
    341 is their triumph.

14. A society which turns so many of its best and brightest into tax
    lawyers may be doing something wrong.
