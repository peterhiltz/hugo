---
title: "Tax Policy by Politicians? Who Knew?"
date: 2020-11-10T14:18:19-04:00
categories: ['posts']
tags: ['tax law']
showToc:  "true"
author: "Peter Hiltz"
---
Warning: Tax geek post with no attempt to explain references for non-international tax readers.

There was once a law firm at the forefront of partnership tax shelters. The actual statutory law was ambiguous to say the least and the partners at this law firm believed, in their hearts, that they were the ones who actually defined what the law and tax policy was in this area. Eventually the politicians change the law and the firm no longer exists.

I was reminded of this recently when I heard, for the umpteenth time, that "arms-length pricing is sacrosanct" but the statement had the additional twist that "politicians don't set tax policy". Really? Its not like Moses came down from the mountain with a third tablet setting out the arms-length pricing commandments. There are times when legal experts are so invested in their understanding of the legal analysis that they think the law is immutable. If the law changes, their analysis (and consequent business models) needs to change. Amazing as it might sound, sometimes that happens and when it does, it isn't driven by the people who were the legal experts in the old law.

Lets talk international tax policy from the standpoint of the people who actually write the laws - the politicians and their staffs. (This post is not about arguing technicalities of the law as it exists today.) Everyone knows the old saying "Don't tax me, don't tax thee, tax the guy behind the tree." Or in the case of international tax, tax the foreigner or at least make it look like you are taxing the foreigner. Governments were looking for additional sources of tax revenue before COVID-19 and they will really be looking for it post COVID-19. That is reality. Local tax policy will mutated to deal with that reality. The question is how will those changes in local tax policy override the historic understanding of international tax policy.

In many countries the public has already been primed by the successful re-framing of the debate on "taxing where value is created". The local media trots out revenue numbers and then compares what they think the net income tax in the country against revenue. Yes, apples and oranges but politicians are driven by voter demand. The public mind set is gradually moving on "location of value creation" to the destination location, not the location of development, production, IP ownership or anything else. Western European government economists don't like where this leads because there is no principled reason why destination location should be limited to cross border digital goods and services. Cross border sales are cross border sales.

But destination location itself is ambiguous and differs depending on the context. In the case of advertising, viewer location is arguably the destination of final use, not the location of the advertiser itself. But at the same time viewers are the product being sold to advertisers. The theory that has been trotted out is that viewers are raw materials and that governments have a right to charge for extraction of raw materials from their country. That theory means that the argument revolves around valuing the raw data v. the value of processed data. To sidestep the valuation issue, a second rationale, not talked about as much, is that the round trip - viewer being both product and final use location - means that the value is created at the user location. Effectively this means that the location of production of the end product being sold to advertisers happens at the location of the viewer, not where the analysis is done or the analytic tools are developed.

This second rationale has a lot of potential consequences outside the digital advertising space because it effectively denies all the arguments for allocating income to DEMPE function locations. Suddenly everyone who manufactures goods in Asia and allocates the bulk of the income to decision makers in the US may find taxable income getting allocated to the location of actual manufacturing. Remember, this isn't just a Western Europe issue. When considering how governments will tax foreign companies, you need to think where this might lead in Asia, Africa and Latin America as well.

Even US tax law is overriding arms-length pricing with the "commensurate with income" provision. Everyone accepts the fact that Congress can override the arms-length principle. Altera was just about whether the IRS followed the APA.

Pre-COVID, it might have been a sensible strategy to throw GAFA in front of the bus of unilateral digital service taxes and hope that countries would be satisfied with that corporate sacrifice. Post-COVID, I think the local tax revenues needs will be too great. They will be coming for every foreign multinational. Clutching the cross of arms-length pricing won't help. I have no problem with people worshiping old gods, but sometimes you have to ask whether they are worshiping dead gods.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
