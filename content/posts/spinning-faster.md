---
title: "The Earth is Spinning Faster"
date: 2021-01-12T14:18:19-04:00
categories: ['posts']
tags: ['science']
showToc:  "true"
author: "Peter Hiltz"
---

For quite a while now, the earth's rotation has been slowing just slightly. [This year, it sped up](https://www.livescience.com/earth-spinning-faster-negative-leap-second.html). July 19 last year was the shortest day ever was recorded — it was 1.4602 milliseconds shorter than the standard. [No one is sure why](https://phys.org/news/2021-01-earth-faster.html). If you think of an ice skater, they spin faster if they pull in their arms to preserve angular momentum. The earth doesn't have arms, but in theory the same thing could happen if global warming means enough water in the mountains has melted down to the sea or if heavy material in the mantle has subsided towards the core. There might be something about the moon's gravitational pull given the that earth is not completely symmetric. All the conspiracy theories about windmills had them slowing down the earth, not speeding it up. At this point people just seem to be throwing ideas at the wall and seeing if anything sticks.

Computer engineers are currently [trying to think through whether they have a problem or not](https://news.ycombinator.com/item?id=25684661). They are used to leap seconds, they had to deal with problems in 2005 when a leap second crashed version 2.4 of the Linux operating system kernels. but they are not sure how much testing has been done with negative leap seconds. (Anything that was scheduled to happen during that second, wouldn't happen. And that negative second would occur at different times depending on your time zone.) Interestingly, the FreeBSD operating system kernel (another computer operating system similar to Linux but with a different code base) does get tested a couple times a year against negative leap seconds just because some customers require it get demonstrated to handle both positive and negative leap seconds.

If no one needed to synchronize anything, then no problem, but with all the networking these days, stuff does need to synchronize. The computer engineers know they can't adjust the earth's spin (or at least it would be very expensive), so everything will have to be fixed in software. On the other hand, some computer people seem to think that the only people it would affect are the astronomers and they caused it anyway.

Of course, it could always be that the earth was just as disgusted with 2020 as the rest of us and decided to end it a few milliseconds early.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
