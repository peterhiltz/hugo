---
title: "2020 Philosophy Survey Part 1m: Time, Immortality, Politics"
date: 2022-02-24T16:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1 with the topics being "Time", "Immortality" and "Politics". I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Time
See [Time](https://plato.stanford.edu/entries/time/). This question revolves around the [A-Theory and the B-Theory](https://plato.stanford.edu/entries/time/#TheoBTheo) Consider propositions. Tensed propositions can change truth values over time (as the world changes). Tenseless propositions are always just true or false - they never change - either because what they reference never changes or because they reference a particular point in time.

- B-theorists think change can be described in before-after terms. They would use tenseless propositions like "The leaf is red at Oct 7". "The leaf is not red at August 7. You can think of B-theorists as subscribing to time as a fourth dimension, so you need to specify where on the time vector you are.
- A-theorists believe that some important forms of change require classifying events as past, present or future. Time is something that is flowing.

| A-theory | B-theory | Agnostic |
|----------|----------|----------|
| 27%      | 38%      | 23%      |
N = 1123


### Immortality
I think this one is obvious, so no additional description provided other than to note that this does not require belief in God to say yes.
| Yes | No  | Agnostic |
|-----|-----|----------|
| 45% | 41% | 7%       |
N = 1113

### Politics
I think this question was what the political leaning of the respondent was. Interestingly, the respondents were really only given possible answers of capitalism or socialism.

| Capitalism | [Socialism](https://plato.stanford.edu/entries/socialism/) | Combination | Alternative | Unclear | Agnostic |
|------------|-----------|-------------|-------------|---------|----------|
| 30%        | 53%       | 6%          | 5%          | 8%      | 4%         |
N = 1094


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
