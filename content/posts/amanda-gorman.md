---
title: "Amanda Gorman - Inaugural Poem - The Hill We Climb"
date: 2021-01-20T14:18:19-04:00
categories: ['posts']
tags: ['politics','life','poetry']
showToc:  "true"
author: "Peter Hiltz"
---
I thought I would just put Amanda Gorman's Inauguration Poem "The Hill We Climb" here. I have no idea where line breaks should be. Note to self - there are poetry readings on Youtube, but check out the ones by the poet, not by actors reading the poetry. Do poets do poetry readings on Zoom?

"*Mr President, Dr Biden, Madam Vice President, Mr Emhoff, Americans and the world: when day comes we ask ourselves where can we find light in this never-ending shade? The loss we carry, a sea we must wade. We’ve braved the belly of the beast. We’ve learned that quiet isn’t always peace. In the norms and notions of what just is, isn’t always justice. And yet, the dawn is ours before we knew it. Somehow we do it. Somehow we’ve weathered and witnessed a nation that isn’t broken, but simply unfinished. We, the successors of a country and a time where a skinny black girl descended from slaves and raised by a single mother can dream of becoming president only to find herself reciting for one.*"

"*And yes, we are far from polished, far from pristine, but that doesn’t mean we are striving to form a union that is perfect. We are striving to forge our union with purpose. To compose a country committed to all cultures, colours, characters, and conditions of man. And so we lift our gazes not to what stands between us, but what stands before us. We close the divide because we know to put our future first, we must first put our differences aside. We lay down our arms so we can reach out our arms to one another. We seek harm to none and harmony for all. Let the globe, if nothing else, say this is true. That even as we grieved, we grew. That even as we hurt, we hoped. That even as we tired, we tried that will forever be tied together victorious. Not because we will never again know defeat, but because we will never again sow division.*"

"*Scripture tells us to envision that everyone shall sit under their own vine and fig tree and no one shall make them afraid. If we’re to live up to her own time, then victory won’t lie in the blade, but in all the bridges we’ve made. That is the promise to glade, the hill we climb if only we dare. It’s because being American is more than a pride we inherit. It’s the past we step into and how we repair it. We’ve seen a forest that would shatter our nation rather than share it. Would destroy our country if it meant delaying democracy. This effort very nearly succeeded.*"

"*But while democracy can be periodically delayed, it can never be permanently defeated. In this truth, in this faith we trust for while we have our eyes on the future, history has its eyes on us. This is the era of just redemption. We feared it at its inception. We did not feel prepared to be the heirs of such a terrifying hour, but within it, we found the power to author a new chapter, to offer hope and laughter to ourselves so while once we asked, how could we possibly prevail over catastrophe? Now we assert, how could catastrophe possibly prevail over us?*"

"*We will not march back to what was, but move to what shall be a country that is bruised, but whole, benevolent, but bold, fierce, and free. We will not be turned around or interrupted by intimidation because we know our inaction and inertia will be the inheritance of the next generation. Our blunders become their burdens. But one thing is certain, if we merge mercy with might and might with right, then love becomes our legacy and change our children’s birthright.*"

"*So let us leave behind a country better than one we were left with. Every breath from my bronze-pounded chest we will raise this wounded world into a wondrous one. We will rise from the hills of the West. We will rise from the wind-swept Northeast where our forefathers first realised revolution. We will rise from the lake rim cities of the Midwestern states. We will rise from the sun-baked South. We will rebuild, reconcile and recover in every known nook of our nation, in every corner called our country our people diverse and beautiful will emerge battered and beautiful. When day comes, we step out of the shade aflame and unafraid. The new dawn blooms as we free it. For there is always light. If only we’re brave enough to see it. If only we’re brave enough to be it.*"

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
