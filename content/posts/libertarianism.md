---
title: "If We Close Our Eyes - Libertarian Edition"
date: 2020-09-21T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I was browsing some libertarian social media sites trying to understand the different variations of libertarians and I was struck by the similarity with religious converts. If you believe in the invisible market hand, then everything will be fine is the same as if you believe in my version of God, then everything will be fine.

When asking about exploitation, I was pointed to this quote:

"With the State - biggest, baddest exploiter of all time - out of the picture, exploitation, in terms of aggression, would all but vanish. It would be a voluntary society, an anarchy." [http://www.ozarkia.net/bill/anarchism/faq.html](http://www.ozarkia.net/bill/anarchism/faq.html).

Think about this, really. The only situation in which these two sentences can possibly be correct is if the State is not only "the biggest, baddest exploiter of all time", it is the **only** exploiter. This is because people don't exploit other people? I've worked with some of the biggest companies in the world; I've worked with people in small companies. Surprisingly enough, a certain percentage of the human species seems to want to exploit other people. It doesn't matter the size of the organization, so yeah, I think we can safely assume that people exploit other people and getting rid of the government will not magically make the problem go away.

When asking about cartels, I was told that cartels don't last because of game theory - explicitly the [Prisoner's Dilemma](https://en.wikipedia.org/wiki/Prisoner%27s_dilemma). This reminds me of the joke about the economist feeding himself from canned foods by assuming a can opener. The Prisoner's Dilemma explictly states that no communication is allowed. In the real world, communication between cartel members may be limited because they are trying to avoid government regulators. There would be no limitations on communications in a libertarian world, so the math underlying the Prisoner's Dilemma is not applicable. As a matter of fact, cartels would seem to be enforceable contracts in a libertarian world.

When asking about how children would be protected, I was told that "insurance and protection companies would not insure someone who would harm children". Obviously insurance companies wouldn't know when people signed up whether they would harm children or not. I'm assuming the answer really meant that insurance companies wouldn't pay off if the insured deliberately harmed a child. But seriously, people commit crimes against children today even knowing it is against the law and police will look for them. Do people think that evildoers will check their insurance policies before committing an act - let's see, I'm covered for this, but not that...?

Another person asked what happens if one person refuses to agree to a judge. The response was that would be a bad decision because the alternative is shooting it out or people would avoid the bad guy and agreeing to a judge is the civilized way of resolving differences. So people wouldn't refuse to agree to a judge. But, if that is their decision, "Maybe a free society will be less forgiving to irrational, brute, troubled people. They won't last long. The rest will be alright." Oh good. let me make sure I have several contract killers in my rolodex if we end up in this society.

One person asked about whether lawyers would be needed. The response was "Nothing says they can't be. But 100% proof of a crime is need to lock someone up. None of this "he raped me I'm pretty sure" bullshit and then 30 years later when DNA comes back "oops sorry". Ok. As a lawyer now I'm laughing. What is 100% proof of a crime and how is that different from "guilty beyond reasonable doubt" or whatever the test is in your country?

When asked what prevents companies from becoming tyrannical, the response was that if companies do something tyrannical that motivated consumers to stop supporting them, the company would not be able to survive. This assumes, of course that the bad thing was so bad that a huge percentage of consumers stopped supporting them. How many people have stopped supporting Google or Facebook or Nestle or Amazon or .... because of the things they do?

My understanding is that there are lots of different libertarian categories including
classical liberalism, plain libertarianism, voluntarism, minarchism, anarcho-capitalism (ancap), objectivism, agorism, paleolibartarianism and others. I certainly do not understand the differences.

However, at the end of the day, the underlying theory seems to be there is one overriding priority - no forcing people to do or not do anything. Societal goals, like peaceful living, are subordinate to the individual. In fact, ancap (anarchical capitalism) seems to reject any societal rights. No one seems to have figured out how to get to societal goals while still meeting the overriding priority without wishful thinking. Even the wishful thinking seems to assume everyone (a) has enough wealth/income to live on (b) any critical information about anyone is available to everyone (c) everyone has unlimited time to check up on whoever they might possible interact with each day and (d) somehow the insurance/protection companies with voluntary militias wouldn't become the exact same government that libertarians oppose.

By the way, I'm still trying to figure out the libertarian theory of property rights without a single enforced definition of what is "property" and how does any individual have a right to it. There seem to be as many definitions as there are libertarians.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
