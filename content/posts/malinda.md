---
title: "An Honest 2020 Christmas Medley"
date: 2020-12-07T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Back to less serious stuff. Malinda just put up a highly enjoyable [parody mashup of Christmas songs](https://www.youtube.com/watch?v=z9xwXJvXBIw) sung from the standpoint of both her optimistic and pessimistic sides looking at 2020. She also has done this for [2019](https://www.youtube.com/watch?v=mKVY0onkmh8) and [2018](https://www.youtube.com/watch?v=70QwQ5kgxzk).

She has two channels on youtube. The first, [Translator Fails](https://www.youtube.com/channel/UCP2-S6-M9ZvlY8t7cRn4O6A) she runs various things through Google translate into multiple languages and back until it makes absolutely no sense. The second, just entitled [Malinda](https://www.youtube.com/c/Malinda/) is both more personal and has more original stuff.  She has also done things like collaborating with her followers to write songs like this one about [Puffins](https://www.youtube.com/watch?v=TnRXgqOiFe0) with the process documented [here](https://www.youtube.com/watch?v=TAemYMUFE68).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
