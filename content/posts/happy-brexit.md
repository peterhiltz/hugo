---
title: "Happy? Brexit UK - Now About the Paperwork"
date: 2021-01-01T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
So today the initial transition period for the UK leaving the EU is over and the next stage of transition periods begin. Since the UK government did sod all to actually prepare, at 11PM on Dec 31 the government issued notices saying, among other things, that shipments from Great Britain to Northern Ireland don't need export paperwork for another 12 months (because no one knows what the paperwork should look like). Stena Line Ferries reported that six freight loads traveling from Wales to Ireland (the Republic, not Northern Ireland) had to be turned away due to not having the correct paperwork.

Reading the news reports from the UK over the last several months, it is fairly obvious that business who exported to non-EU countries or imported from non-EU countries knew what was going to be needed in terms of processes and paperwork in general, but there had been few processes actually detailed from the UK government on deviations. At the same time, people not involved in non-EU business had no clue about customs forms and place of origin rules or ..... Even if there is no tariffs, there are still a lot of trade reporting compliance that needs to be in place or be subject to fines. All that business that used to just happen within the EU now becomes international trade across borders. Congratulations, you have achieved full "sovereignty". By the way, that also means that the 27 remaining members of the EU also have "sovereignty" vis-a-vis you. It works both ways.

The mentality reminds me a bit of when GM sold a majority interest in GMAC (now Ally Bank). Business management who fervently believe that mergers create synergies found it difficult to get their heads around the fact that de-mergers create dis-synergies.

At the same time, I don't think Brussels has ever fully understood local level push back on the "Great European Project". Maybe it is less now than when I lived in Brussels in the 90s, but I always had the feeling that every time the EU tried to force people to be "European" a large contingent said "No, I am [insert local region here, E.g. Catalan, Flemish, Galacian, etc.]". I will leave it up to the reader to decide if the same thing can be said in the US.

There was an interesting [opinion piece in the Guardian](https://www.theguardian.com/commentisfree/2020/dec/31/the-left-brexit-economic-uk) yesterday by the Guardian's economics editor Larry Elliott who is a pro-Brexit cheerleader on the left. He thinks that nation states can react faster than federal communities like the EU and that the UK government will no longer have anyone to blame when something goes wrong, so eventually the current right wing government will fall.

He makes a statement at the end that seems a bit wishful thinking to me. "*Many on the remainer left accept the EU has its faults, but they fear that Brexit will be the start of something worse: slash and burn deregulation that will make Britain a nastier place to live. This, though, assumes that Britain will have rightwing governments in perpetuity. It used to be the left who welcomed change and the right that wanted things to remain the same. The inability to envisage what a progressive government could do with Brexit represents a political role reversal and a colossal loss of nerve.*"

Polls in the UK pretty clearly show that Brexit voters on the right were thinking Brexit would be a return to "Rule Britannia". They didn't want things to "remain the same", they wanted things to go back to an earlier nostalgian fantasy. I don't think that is the "welcoming change" on the right that Elliott implies.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
