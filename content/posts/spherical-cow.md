---
title: "Spherical Cow or Assume a Can Opener?"
date: 2021-04-14T14:18:19-04:00
categories: ['posts']
tags: ['life','politics']
showToc:  "true"
author: "Peter Hiltz"
---
I was told yesterday that business people don’t cheat or cut corners because if they did, investors would punish them and that if I had studied finance I would understand that. It was all I could do to avoid just responding with maniacal laughter.

Last year I had an economics professor tell me that Google and Amazon are not profit maximizing companies because they do not operate the way that microeconomic theory says that they will. As a result economic theory doesn't apply to them, they are boring and not worth studying. I'm really not sure what he was putting in his coffee.

There's a difference between (a) "Assume a spherical cow" and (b) a cultish belief that just denies reality. "Assume a can opener" can fall on either side of that line.

First, a quick explanation for those who haven't heard these references before.

"Assume a spherical cow" comes from [physics](http://www.physics.csbsju.edu/stats/WAPP2_cow.html) and is a reference to when you assume away all the complications of reality to get to equations that can be solved. If your equations describe the simplified reality correctly, then maybe you are on the right track and can start adding in reality complications one fact at a time and see if it still works. Spherical cow approaches are predicated on the idea that the simplified assumptions are only an early indicator that you might be on the right path. At any point in time, reality might prove your hypothesis wrong. (The television show "Big Bang Theory" had a one of the characters tell a joke assuming spherical chickens.)

"Assume a can opener" comes from economics and is different and is about unrealistic assumptions. Generally it will be told as a joke where an economist, an engineer and a physicist are stuck on a desert island with a large amount of canned food and they are trying to figure out how to open the cans. The version I heard in the late 1970s had the engineer go searching for rocks that could bash the cans open and  physicist go off in search of things that would accelerate the cans into a solid object at a velocity that would break the cans open. They come back to discover the economist calmly eating. When asked how he did it, the economist said that he just assumed a can opener.

Whether the can opener assumption is a reality-denying cultish belief or just a simplifying step that will be revisited depends on whether you are willing to test your assumptions against reality and accept reality when reality proves the belief wrong. The same question applies to people espousing capitalism, or marxism or trumpism or ...

It's one thing to explain a concept to a non-expert audience using what I refer to as "lies to children". The description is framed in a way the audience can understand the big picture even if they don't have the background for the details. At least the big picture generally matches reality even if the deatils don't. The conversations I referenced at the beginning of this post involved beliefs that could be proven wrong, but the believers still held desperately to their world views and doubled down for the cult.

I wouldn't mind if their beliefs only impacted themselves but often their beliefs affect their interactions with others. If that is limited to clapping in the belief that tinkerbell will live, fine. When their beliefs in an alternate reality allow their demons into this world, not fine and I wish their demons would take them home and leave the rest of us in peace.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
