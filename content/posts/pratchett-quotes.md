---
title: "Useful Terry Prachett Quotes"
date: 2022-03-03T14:18:19-04:00
categories: ['posts']
tags: ['quotes','life']
showToc:  "true"
author: "Peter Hiltz"
---
Just a collection of quotes from the British author Terry Pratchett that people have found useful for various contexts:

1. Give a man a fire and he'll be warm for a night. Set a man on fire and he'll be warm for the rest of his life.
2. In ancient times, cats were worshipped as gods. They have not forgotten this.
3. Personal’s not the same as important. People just think it is.
4. The phrase 'Someone ought to do something' was not, by itself, a helpful one. People who used it never added the rider 'and that someone is me'.
5. Better to light a flamethrower than curse the darkness
6. Walk a mile in another man's boots. Then you'll be a mile away and you'll have his boots!
7. 1, 2, many, lots [Counting]
8. Once you had a good excuse, you opened the door to bad excuses.
9. Multiple exclamation marks are a sure sign of a diseased mind.
10. You do the job that's in front of you.
11. Fear is a strange soil. It grows obedience like corn, which grow in straight rows and is easy to weed. But sometimes it grows the potatoes of defiance, which flourish underground.
12. You may think otherwise, but it was me standing there. [Response to armchair decision makers].
13. All bastards is bastards, but some bastards are "bastards".
14. All funghi are edible. Some are only edible once
15. Goodness is about what you do. Not who you pray to.
16. Those who can must do for those who can't. And someone must speak for those who have no voices.
17. Humans need fantasy to be human... to be the place where the falling angel meets the rising ape.
18. Carpe Jugulum [Seize the jugular instead of the day]
19. The trouble with having an open mind, of course, is that people will insist on coming along and trying to put things in it.
20. Some humans would do anything to see if it was possible to do it. If you put a large switch in some cave somewhere, with a sign on it saying 'End-of-the-World Switch. PLEASE DO NOT TOUCH', the paint wouldn't even have time to dry.
21. It is said that your life flashes before your eyes just before you die. That is true, it's called Life.
22. Stories of imagination tend to upset those without one.
23. Why do you go away? So that you can come back. So that you can see the place you came from with new eyes and extra colors. And the people there see you differently, too. Coming back to where you started is not the same as never leaving.
24. If cats looked like frogs we'd realize what nasty, cruel little bastards they are. Style. That's what people remember.
25. Wisdom comes from experience. Experience is often a result of lack of wisdom.
26. Light thinks it travels faster than anything but it is wrong. No matter how fast light travels, it finds the darkness has always got there first, and is waiting for it.
27. I'll be more enthusiastic about encouraging thinking outside the box when there's evidence of any thinking going on inside it.
28. If you trust in yourself. . .and believe in your dreams. . .and follow your star. . . you'll still get beaten by people who spent their time working hard and learning things and weren't so lazy.
29. Coming back to where you started is not the same as never leaving.
30. She was already learning that if you ignore the rules people will, half the time, quietly rewrite them so that they don't apply to you.
31. If complete and utter chaos was lightning, then he'd be the sort to stand on a hilltop in a thunderstorm wearing wet copper armour and shouting 'All gods are bastards!
32. The presence of those seeking the truth is infinitely to be preferred to the presence of those who think they've found it.
33. Evil begins when you begin to treat people as things.
34. Inside every old person is a young person wondering what happened.
35. His philosophy was a mixture of three famous schools -- the Cynics, the Stoics and the Epicureans -- and summed up all three of them in his famous phrase, 'You can't trust any bugger further than you can throw him, and there's nothing you can do about it, so let's have a drink
36. Anyway, if you stop tellin' people it's all sorted out afer they're dead, they might try sorting it all out while they're alive.
37. If you don't turn your life into a story, you just become a part of someone else's story.
38. The intelligence of that creature known as a crowd is the square root of the number of people in it.
39. There isn't a way things should be. There's just what happens, and what we do.
40. The enemy isn't men, or women, it's bloody stupid people and no one has the right to be stupid.
41. What have I always believed? That on the whole, and by and large, if a man lived properly, not according to what any priests said, but according to what seemed decent and honest inside, then it would, at the end, more or less, turn out all right.
42. But here's some advice, boy. Don't put your trust in revolutions. They always come around again. That's why they're called revolutions.
43. Seeing, contrary to popular wisdom, isn't believing. It's where belief stops, because it isn't needed any more.
44. It was so much easier to blame it on Them. It was bleakly depressing to think that They were Us. If it was Them, then nothing was anyone's fault. If it was us, what did that make Me? After all, I'm one of Us. I must be. I've certainly never thought of myself as one of Them. No one ever thinks of themselves as one of Them. We're always one of Us. It's Them that do the bad things.
45. A good plan isn't one where someone wins, it's where nobody thinks they've lost.
46. What kind of man would put a known criminal in charge of a major branch of government? Apart from, say, the average voter.
47. +++++++OUT OF CHEESE ERROR++++++
48. Most witches don’t believe in gods. They know that the gods exist, of course. They even deal with them occasionally. But they don’t believe in them. They know them too well. It would be like believing in the postman.
49. The first draft is just you telling yourself the story.
50. The truth may be out there, but the lies are inside your head.
51. Just because you can explain it doesn't mean it's not still a miracle.
52. There's always a story. It's all stories, really. The sun coming up every day is a story. Everything's got a story in it. Change the story, change the world.
53. Sometimes the truth is arrived at by adding all the little lies together and deducting them from the totality of what is known.
54. He was determined to discover the underlying logic behind the universe. Which was going to be hard, because there wasn't one.
55. It is well known that a vital ingredient of success is not knowing that what you're attempting can't be done.
56. The reason that clichés become clichés is that they are the hammers and screwdrivers in the toolbox of communication.
57. His progress through life was hampered by his tremendous sense of his own ignorance, a disability which affects all too few.
58. It was sad music. But it waved its sadness like a battle flag. It said the universe had done all it could, but you were still alive.
59. Crivens [Just an exclamation]
60. buggerit, millennium hand and shrimp [just quiet swearing]
61. He's out of his depth on a wet pavement.
62. The thing is, I mean, there’s times when you look at the universe and you think, “What about me?” and you can just hear the universe replying, “Well, what about you?”
63. Oh, I feel very angry a lot of the time," said Tiffany, "but I just put it away somewhere until I can do something useful with it.
64. The reason that the rich were so rich, Vimes reasoned, was because they managed to spend less money. Take boots, for example. He earned thirty-eight dollars a month plus allowances. A really good pair of leather boots cost fifty dollars. But an affordable pair of boots, which were sort of OK for a season or two and then leaked like hell when the cardboard gave out, cost about ten dollars. Those were the kind of boots Vimes always bought, and wore until the soles were so thin that he could tell where he was in Ankh-Morpork on a foggy night by the feel of the cobbles. But the thing was that good boots lasted for years and years. A man who could afford fifty dollars had a pair of boots that'd still be keeping his feet dry in ten years' time, while the poor man who could only afford cheap boots would have spent a hundred dollars on boots in the same time and would still have wet feet. This was the Captain Samuel Vimes 'Boots' theory of socioeconomic unfairness.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
