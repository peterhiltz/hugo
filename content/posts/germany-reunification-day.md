---
title: "Happy Reunification Day Germany"
date: 2020-10-03T14:18:19-04:00
categories: ['posts']
tags: ['Germany']
showToc:  "true"
author: "Peter Hiltz"
---
Happy Reunification Day to Germany. I've been in Berlin both before and after the wall fell. Its nicer to be able to just walk across the street. Maybe we can all work towards reducing the polarizing behavior that is acting like a psychic Berlin wall in so many countries.

By the way, can anyone tell me why east and west Berlin still use different street lamp bulbs? Nightime aerial photos clearly show a color difference. Metal Halide vs High Pressure Sodium? But why?

That is all.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
