---
title: "Adding Injury to Injury - Ransomware Victims That Pay May Be Penalized By The Government"
date: 2020-10-01T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Suppose you are a business that has been hacked and discover that all your files are locked and you will need to pay some unknown X to get them unlocked. Not Fun. Paying ransoms just make it more enticing to criminals and fund future attacks, but not paying may cause your business to fail. The [US Treasury now says if you pay and the unknown person is subject to US sanctions, then you and everyone involved in the payments may be committing a crime.](https://home.treasury.gov/system/files/126/ofac_ransomware_advisory_10012020_1.pdf). Now double not fun. With respect to the civil law penalties, it doesn't matter that you don't know that the unknown criminal is subject to US sanctions because the US Treasury rules are strict liability rules, according to Ginger Faulk at the international law firm Eversheds Sutherland.

Basically, the government is trying to create herd immunity here by making it less lucrative for the hackers by convincing victims not to pay. Maybe that is a good long term strategy. It becomes more difficult to tell a critically ill patient's family that a hospital is not going to make the ransom payment and they won't be able to unlock their computer systems and so the patient might die.

Businesses should pay more attention to cybersecurity, particularly health, safety and utilities industries. Senior executives and board members should be held more responsible for short cutting safety and security. Maybe that responsibility should include personal visits to every family harmed by their cost cutting.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
