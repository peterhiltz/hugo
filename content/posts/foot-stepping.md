---
title: "You are Stepping on My Foot"
date: 2021-03-25T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I've been read some of the excuses for bad behavior, most recently discussions about Richard Stallman being neurodivergent and therefore he did nothing wrong. In this context I and others thought to dig up this comment by someone calling themselves Hershele Ostropoler about [sexual harassment at public gatherings](https://whatever.scalzi.com/2012/07/31/readercon-harassment-etc/#comment-346433) in John Scalzi's blog in 2012:

- "*If you step on my foot, you need to get off my foot.*"

- "*If you step on my foot without meaning to, you need to get off my foot.*"

- "*If you step on my foot without realizing it, you need to get off my foot.*"

- "*If everyone in your culture steps on feet, your culture is horrible, and you need to get off my foot.*"

- "*If you have foot-stepping disease, and it makes you unaware you’re stepping on feet, you need to get off my foot. If an event has rules designed to keep people from stepping on feet, you need to follow them. If you think that even with the rules, you won’t be able to avoid stepping on people’s feet, absent yourself from the event until you work something out.*"

- "*If you’re a serial foot-stepper, and you feel you’re entitled to step on people’s feet because you’re just that awesome and they’re not really people anyway, you’re a bad person and you don’t get to use any of those excuses, limited as they are. And moreover, you need to get off my foot.*"

- "*See, that’s why I don’t get the focus on classifying harassers and figuring out their motives. The victims are just as harassed either way.*"

Yes, Richard Stallman may have autism and in spite of the numerous attempts by people to coach or educate him that he needs to stop harassing women, he still is so self centered that he does not realize they have rights as well. I only met him once and don't have first hand knowledge, but his behavior toward women is well documented. The problem, as Hershele Ostropoler points out, is that many people try to excuse his behavior and completely ignore that there was a victim. This seems to be true with any group that decides to idolize someone and then claim that their idol is really being victimized and minimizes any impact on the true victim when their idol hurts someone else.

Your idols may have clay feet. Accept it and deal with it; don't pretend that they are really solid gold.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
