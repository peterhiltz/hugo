---
title: "Business Plans For Space Pirates"
date: 2021-01-17T14:18:19-04:00
categories: ['posts']
tags: ['science fiction']
showToc:  "true"
author: "Peter Hiltz"
---
Have you ever really thought about the business plan for space pirates or smugglers as portrayed in science fiction? Upfront investment, maintenance, and expected revenue and variability of the expected revenue are the first things that come to mind.

We have to assume really high value, small size items that can be transported. Let's call it unobtainium. We also have to assume technology is not only discovered, but becomes relatively inexpensive and can be handled by small groups. For space smugglers, maybe the best analog today would be commercial fishing or used cargo vessels. For modern day pirates, the boat of choice seems to be large zodiac (used at 10k), but you still need to factor in the cost of the ship that gets the zodiacs out into the shipping lanes.

I started looking at prices for used commercial boats. As I write, you can buy an offshore tug boat for $75k. Granted, it was built in 1965 and had $100k of fresh paint and refurbished systems in 2017 and has been for sale since then (presumably with the price dropping every year), and it has no accommodations, so better be short trips and no cargo space. Maybe this is within the budget of your basic space smuggler that doesn't have external investors. A better comparison might be a 54 foot crab and salmon seine boat built in 1955 that does have sleeping accommodations for 6, but now your price point is $579k. Ok. Manageable with a mortgage and you live at work, but those fish (or space cargo ships with unobtainium) really need to be there.

With respect to cargo ships, there is one 119 foot vessel available that was built in 1952 for $425k, but anything else I saw was well over $1 million.

The basic thrust of this note is not only does science fiction have to deliver on faster than light technology, it has to get the prices to this level for space pirates and smugglers to have any feasible business plan and you still need to have unobtainium to make it worthwhile.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
