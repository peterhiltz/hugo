---
title: "Talking Past Each Other We Are"
date: 2020-12-06T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I was reading a discussion on social media about a request for a book recommendation and, after awhile, thought maybe I should be diagramming arguments. The request was for science fiction book recommendations from a conservative, non-libertarian view. The person submitting the request specifically asked for books where "the central tenets of conservatism - tradition, hierarchy and authority -  are working for humanity; where tradition is used to help, guide and comfort people rather than cynically used as a tool to keep people down." This harkens back to my post [Enough with the Name Calling](https://www.peterhiltz.com/en/posts/enough-with-the-name-calling/) and the moral foundation theory. If you recall, the study referred to in that post claimed that "US liberals" focus almost entirely on

- Fairness, reciprocity and justice (Fairness or ethic of justice)
- caring, nurturing and protecting vulnerable individuals (Harm or ethic of care).

and conservatives, both in the US and elsewhere give equal or almost equal weight to three other societal virtues:

- Loyalty, patriotism and self sacrifice for the group (Ingroup/loyalty)
- Obedience and respect for authority (hierarchy) (Authority/Respect)
- Purity and sanctity rules which include marking off the group’s cultural boundaries as well as selfishness suppression. (Purity/sanctity)

I was interested to see if some of the conversations would play out on those lines. Most of the conversations (the original poster did not participate) went as you might expect.

There was the initial calling out that science fiction is generally about change. As a result, if the system is working then there is nothing to write about vis a vis the protagonists interaction with the system. The more thoughtful conversations that kept to science fiction and the actual question circled around technology getting out of hand and maybe we shouldn't have invented X. The less thoughtful conversations merely tossed out books with some type of religious orders or inherited nobility running things.

I thought there might be some conversation about the distinction between conservative and libertarian, because there are huge differences from the authority/hierarchy perspective, but there were none. Again, if the system is working then there is nothing for the individualist to defend against.

The problem child argument that attracted my attention centered around whether the "system" is fundamentally sound and any changes will likely be worse. I was looking for insights into how people would define conservative in the context of the original question. As I might have expected, this is where the conversation got off the rails and became arguments about the current state of affairs in the US.

Person A. "*Conservatives think things are good and right because that's how they've always been done or because that's how their superiors tell them it's supposed to be. Which is just kind of fundamentally dumb and dangerous, which is why it's hard to build a story where people with those beliefs are the good guys.*"

Sigh. This is not the way to handle a conversation. The original question was asking about recommendations for books where the system is working. Person A has jumped to a different topic and said that doing things the way they have always been is **dumb** and **dangerous** and then makes a further leap to effectively say that people who are dumb and dangerous **cannot** be good guys. (Personally I would throw Harry Potter into that category, but the victor gets to write the histories.) Does Person A understand how sanctimonious this sounds to the other side and that he or she has already thrown coherent discussion under the bus?

Person A again. "*So yeah, once you set up a utopia and conjure some benevolent authority figures, then you can pretend that conservatives are right about how things outa run.*" Again, starting a fight where there didn't need to be a fight.

Person B. Posits that there is no better alternative case 99% of the time and then proceeds to build irrelevant strawnmen against other countries. But Person B then offers this interesting take.

"*The conservatives job is to be dull monotonous people who preserve the good of society.*"

"*The liberals job is to find the flaws and to offer up solutions to the problem.*"

"*Society is best when this relationship is in tact. Conservatives support liberals because they do the dirty work of figuring things out. Liberals support conservatives because they are the ones keeping those ideas going. The dirty secret of conservatives is that they love bureaucracies, when your country enacts socialist healthcare it is the conservatives that maintain the system. It's why a country like Canada is culturally identical to the states, yet the cons are staunch supporters of the public system.*"

"*It isn't people that believe in no change versus the people who want to change everything.*"

I'll ignore the bit where he asserts that Canada is culturally identical to the US. He/she tries to draw a distinction between liberal technicians engineering change (good) and protestors (bad). If he/she had left it at this, they might have been able to redirect the conversation back to something productive. But Person A has poisoned the water and Person B doubles down.

First Person B states that you need to be paying attention to the correct people but the media makes this nuance impossible. (I can agree that the media often make nuance impossible, but I would disagree that people in general listen to nuance anyway, elitist intellectual toad that I am.)

But then Person B's points really degenerate.
(1) Liberal sex and drugs (apparently opiates) have caused chaos in our society,
(2) the system is flawed and we need change [now I'm confused - change to what?],
(3) men shouldn't be blamed because the problems span all of society and need to be addressed broadly,
(4) the leading cause of inequality is whether you have a father in your life,
(5) almost every policy conservatives embrace was at one point a left wing idea [??] and
(6) prohibition and eugenics were inseparable from womens rights.

Person A's response: "*you just have a pretty insane picture of the world and aren't very rational*"

Person B. "*If it were left up to liberals Hitler would of ran unimpeded. Because all of the things done to fight the war were based on the idea that our structure was superior.*" [I'll ignore the revisionist history - apparently FDR was a conservative; my great grandfather would be shocked.]

Person B then slides into discussing an article in Discover Magazine entitled [Your Brain on Politics, the Cognitive Neuroscience of liberals and conservatives](https://www.discovermagazine.com/mind/your-brain-on-politics-the-cognitive-neuroscience-of-liberals-and-conservatives) from 2011. That article characterizes liberalism as adaptability and conservatism as stability and claimed that liberals used reappraisal strategies, working through alternatives to reassess a situation. Conservatives, on the other hand, tend to process information initially using emotion when dealing with a situation and respond to threatening situations with more aggression and are more sensitive to threatening facial expressions. In order for conservatives to find an idea valuable, it has to be meaningful to them personally.

Person B takes that and makes the following extension:

"*[t]hey are conflating disgust sensitivity with anxiety. Anxiety makes your heart level go up, disgust makes your heart rate go down. Conservatives are far more disgust sensitive than liberals.*"

If true, this would partly explain the conservative reaction to LGBQT issues.

Person B continues:
"*It also misses the part where logical thinking doesn't translate to logical action. Logic is based on a set of conditional arguments. If a part of it is flawed your entire chain of logic won't work. For this reason liberals tend to make very illogical arguments because they are scouring through as many arguments as possible. I.e. carbon is increasing in the atmosphere => the atmosphere will absorb more energy from the sun => Therefore we'll experience global warming => Therefore we need to stop it => Electric cars will help => Therefore we need to give money to electric cars. The missing part comes from the fact that electric cars are part of a infrastructure(roads and parking lots) that are also very bad for the environment. As I said 99 percent of the time they're wrong but when they are right it's a big fucking deal.*"

As someone who has dealt with groups dealing with some of these issues, I have to agree there is a lot of "A is bad, we must tax X to throw money at A" without addressing what a solution to A actually looks like.

Person B continues:
"*The leading problem with liberals is that they have real issues with consistency and structure. They have a perpetual desire to change things that typically leads to chaotic situations. It also means they are poorly adapted to the modern industrial world, where repetitiveness and consistency is rewarded. And yes ability to adapt to new situations is an asset to liberals, but it is an asset that doesn't cross the IQ spectrum. Which means liberal politics predictability leave low iq individuals in the cold. The left lives in a perpetual state of babel. Because they are hard wired to seek out new and novel ideas they often pick up radically different sets of ideas. Conformity is hella "intelligent" and "rational" in that context.*"

I think this really overstates the problem on the liberal side, but I think that liberals really need to grasp that this is what conservatives think of them and they need to address this concern. The ad hominem attacks are counter productive.

In any event, I didn't come away thinking I had found useful support for the moral foundation study found in [Enough with the Name Calling](https://www.peterhiltz.com/en/posts/enough-with-the-name-calling/).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
