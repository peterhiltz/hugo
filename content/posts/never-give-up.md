---
title: "When are Aphorisms Profound or Trite?"
date: 2021-01-04T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I was helping a friend with some data analysis which included a file of roughly 500k quotes collected from the internet. Looking at the "quotes", I thought a dismaying number were trite aphorisms from motivational speakers/writers and religious feelgood writers. I began to wonder why are sayings I consider "trite" obviously not "trite" to enough people that there is a market? Actually, I seem to be misusing the word "trite". The dictionary indicates that trite means "not evoking interest because of overuse or repetition". What I should have been thinking is "Why do I find a quote boring and other people are willing to pay to hear/read it and a thousand variations"?

When I think of collections of quotations, I tend to think of things like Virginia Woolf's

- "*History is too much about wars, biography too much about great men*" from 'A Room of One's Own'.

I don't think of:

- "*Every Christian has an opportunity to be successful*"

by Sunday Adelaja (an evangelical mega church pastor in the Ukraine with 8,226 quotes on goodreads.com). But they are both quotes and at least two people (one for each) thought the statement was important enough to save for posterity. Sidenote: The number one quote for Sunday Adelaja on goodreads.com is "*How tragic it is to find that an entire lieftime is wasted in pursuit of distractions while purpose is neglected.*" This falls under "Do as I say, not as I do" as Mr. Adelaja has admitted to sleeping with lots of parishioners of his church.

Take a quote like

- "*I have a mustard seed, and I am not afraid to use it*" by Joseph Ratzinger.

This quote requires some actual background knowledge to understand what he is saying as well as some side references. I found it interesting while other people might think it comedic. Now compare that with

- "*One of the only ways to get out of a tight box is to invent your way out*" by Jeff Bezos.

Personally I get so wrapped up in the singular/plural "only ways" that I find it difficult to get to the question of why osmquote.com considered this a "brilliant business quote".

A lot of the motivational quotes were from the never give up, winners never quit, high school football coach school of philosophy. After reading too many of those, I started wondering if any of these people had heard of the sunk cost fallacy.

- "*Never stop and never sit! Work hard and stay fit! Success will come one day, To those who dream adn never quit!*",Avijeet Das (1,709 quotes on goodreads.com)

Some of the quotes made me wonder what the author was talking about. Consider:

- "*Plan strategically and strike it once to win. You can achieve maximum dues with minimum dice if only you are willing to clearly calculate and throw your efforts with optimism!*" by 2015 Best Male Leadership Mind Ambassadors Young Leader Award winner Israelmore Ayivor (2,213 quotes on goodreads.com).

- "*Try to be patient and wait for God time to come and see how thing will work for you. Keep trying and never stop trying because there's no harm in that and one day you will hit your target.*" Don Simon

I'm really at a loss on what to think after reading those two. At least when I see something like:

- "*A bird does not fly because of the wind around it, but because of the strength within it.*"

I get that someone is trying to use new words for the concept of "look inside yourself for success". The bird quote, by the way, was from Matshona Dhliwayo, whose bio reads "Canadian based Philosopher, Entrepreneur and book author and has 7,766 quotes on goodreads.com." Mr. Dhliwayo seems to like bird imagery. He also says things like "*A bird must overcome its fear of heights before it can rule the skies.*" and "*A bird is born knowing how to fly, but has to learn to conquer the skies.*"

A lot of the religious oriented quotes were obviously intended to comfort people and that I can understand:

- "*When you walk with God, you will never be lost*" Karen  Gibbs.

- "*You are my Beloved, on you my favor rests.' It certainly is not easy to hear that voice in a world filled with voices that shout: 'You are no good, you are ugly; you are worthless; you are despicable, you are nobody- unless you can demonstrate the opposite.' These negative voices are so loud and so persistent that it is easy to believe them. That's the great trap. It is the trap of self-rejection.*",Henri Nouwen (only 784 quotes on goodreads.com, come on Henri, you can move up the league tables!)

- "*The word of God is enough wealth.*" Lailah Gifty Akita (17,936 quotes on goodreads.com)

But some obviously went over my head if they are actually profound:

- "*Holy Scripture is Holy Scripture.*" Lailah Gifty Akita

At the end of the day, I don't really have an answer to my initial question. People are different and statements that catch my interest don't catch the interest of others and vice versa. Motivational speakers adding dog whistles about team solidarity against the enemy regardless of reasons find that I'm deaf at that frequency. At the same time there are a lot of people who buy into the concept of "my team right or wrong". The answer should be important to the political types reading this - you need to understand how to get the positive attention from everyone, not just your echo chamber.

For those of you wondering about the goodreads.com quote numbers, no, I am not about to try to figure out how many quotes on goodreads.com from each author are actually duplicates. I just wanted a common base.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
