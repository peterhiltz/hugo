---
title: "Where is Peru's President?"
date: 2020-11-16T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
As a distraction from US politics, where is Peru's president? Let's set the scene. More than half of the members of Congress are currently under investigation for corruption. Congress removed the popular former President Martín Vizcarra on Nov 9 claiming he had mismanaged the COVID situation and claiming he had taken bribes while as a regional governor years ago. The head of Congress, Manuel Merino, took over as President and faced immediate huge protests, leading to police killing two protestors and injuring dozens of others on Saturday. By yesterday most of Merino's cabinet has resigned and Merino has now stepped down. Polls indicate that 78% of the country opposed the impeachment. If you are keeping score, the last seven presidents have resigned or been impeached over corruption charges. One would hope that there is some institution in Peru which isn't corrupt which could step in, but someone much better informed on Peruvian politics than I am would have to answer that question.

UPDATE: The Congress has chosen a centrist from a small party that voted against the ouster of Vizcarra. We will have to see if he can deal with a recalcitrant probably corrupt Congress when he is from a small party. Peru certainly could use some good news. It currently has the world's highest per-capita COVID-19 mortality rate.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
