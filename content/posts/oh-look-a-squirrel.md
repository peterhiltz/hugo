---
title: "Oh Look, A Squirrel! Christopher Columbus Edition"
date: 2020-09-16T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
## Introduction (because this will be long)
There is an old proverb that you should never meet your heroes or heroines. If you do, you will see their feet of clay. How you do react? If you have idolized and attached a sense of yourself to the hero or heroine, people often either hide the issues and double down on the hero/heroine worship or they are so upset they throw away the good with the bad.

As soon as you identify with someone and make them a hero, it gives you vicarious glory. But that connection also means that any disparaging or pointing out flaws of that person you will internalize as disparaging you. And you strike back.

This could be solved by believing in yourself and not needing to live in the reflection of some athlete, media star, politician etc. But most people seem to want the feeling of belonging to a group, "Team XYZ". This allows you to both share in the group glory and give and receive mutual support from your teammates in the downtimes.

Personally, I’m on "Team Treat Everyone Fairly". That means I not on "Team USA", "Team [insert political party/ethnic group/sports team/moviehere]", etc. Treating everyone fairly is not the same as giving two sides equal time. Most issues have multiple sides and MOST OF THEM ARE WRONG.  (How is that for ego?) This generally means that everyone treats me as the enemy because I am not unquestioningly on their side.

How does this relate to Christopher Columbus? Many people are aware of the current arguments in the United States over the fact that Columbus Day is a national holiday. The main current argument is whether it should continue to be named for Christopher Columbus or whether it should be named "Indigenous Peoples Day". If you look at Federal Holidays, the only Federal Holidays observed with respect to individuals are Martin Luther King Jr, President Washington (aka President's Day, combining with remembrance of President Lincoln) and Christopher Columbus. As a result, there is a lot of prestige at stake.

I have two structural issues with the discussions about the Columbus Day holiday. First is the difference between recognition and honor, commemoration and celebration. Second, and even more important is the self identification problem. Two sizable communities have explicitly self identified with Columbus (Italian Americans and Catholics) and another sizable community has self identified with Columbus as a cause célèbre vis-a-vis other communities. On the other side, large communities have taken severe offense at both the historical actions of Columbus and the defenses laid out by the first group of communities.

## Holidays are What Today's Culture Celebrates, not Yesterday's Culture
Let's start with my first issue. Holidays can be memorial observances, or they can be celebrating events or they can be honoring a person. (I'll treat Mother and Father's Days as honoring a person). What isn't mentioned at all in the arguments is the fact that they are not laws of gravity, immutable for all eternity. As cultures change, what was celebrated previously may no longer meet the current criteria for what should be celebrated. That doesn't mean the event or person is erased from history if the holiday is re-purposed or eliminated. It just means that the person or event is no longer honored or celebrated under current culture norms. It doesn't matter that the person or event followed the cultural norms of the times - the question is what is the holiday telling people today. So let's assume that Columbus was following the cultural norms at the time by selling slaves. Do we want today's children celebrating (and possibly self-identifying with) a slave trader? Are the other things he accomplished so important that we tell the children that he did bad things, but they were okay then, so he has an excuse, don't do it now, and celebrate and honor him for doing something else? That's a lot of nuance for children.

## Were His Achievements Sufficient to Celebrate Today as One of Three People with Their Own National Holiday?
Let's talk about what he accomplished and see if that provides sufficient reason to be one of three people honored by a Federal Holiday in the US.

### University of Pennsylvania Statesman
 [UPennStatesman](https://upennstatesman.org/2018/10/08/defense-columbus-day/)

The U. Pennsylvania Statesman tried to claim that Columbus was a skilled captain, ahead of his time arguing the world was round and a capitalist. Say what? Everyone in Europe since 500 BC knew that the world was round. However, no Europeans other than Scandinavians and Irish and English cod fishermen knew there was a continent between Europe and Asia. As a result, most people who did the math did not believe that ships could carry enough food and water to sail west from Europe to get to Asia. Columbus screwed up his math and convinced himself that the earth was smaller than it actually is - in fact just small enough that you could sail west to Asia without dying. As a navigator, once he accidentally ran into Hispaniola, he could successfully repeat running into it on purpose. So, Columbus severely miscalculated the size (everyone else was right) and he got lucky running into an island instead of dying. That doesn't make him a skilled captain; it makes him a lucky idiot. In my mind his greatest accomplishment was convincing the Spanish Court to fund the attempt on spec. Any everyone was a capitalist at the time, so I'm grading U Penn an "F" on justifying why Columbus should be one of three people honored by a Federal Holiday in the US.

See [UPennStatesman quotes]({{< relref "oh-look-a-squirrel.md#UPennStatesman" >}})
### Minneapolis Star Tribune
[Minneapolis Star Tribune](https://www.startribune.com/in-defense-of-christopher-columbus/279629112/?refresh=true)

The Minneapolis Star Tribune tries to justify the honor by claiming he was a technocrat and embodies many of the qualities we respect today, referencing Bill Gates and Steve Jobs. The article then claims that people who achieve big things often make big mistakes. Of course Bill Gates and Steve Jobs don't have holidays named after them, so why Columbus? They even got their math right and, as far as I can tell, didn't sell anyone into slavery. By the way, I'll recognize Steve Jobs' accomplishments, but he didn't respect other people and I have no problem reciprocating.

See [Minneapolis Star Tribune quotes]({{< relref "oh-look-a-squirrel.md#MST" >}})

### Newsday
[Newsday](https://www.newsday.com/opinion/commentary/in-defense-of-the-great-columbus-1.14371854)?

Newsday tried justifying the honor by claiming he was a prime mover of Western civilization and the today's Hispanic world would not exist without Columbus. This is a really weird claim because, of course, we are talking an American holiday, not a Latin American holiday. Is he celebrated in Latin America? No. Today's Hispanic world replaced Columbus Day with "Day of the Race or Die de la Raza" in Colombia, Guatemala and Uruguay, "Day of Indigenous Resistance" in Venezuela, "Day of Encounter of Cultures" in Costa Rica, "Day of the Encounter of the Worlds" in Chile, "Day of Respect for Cultural Diversity" in Argentina, "Day of the Americas" in Belize. No, that is not celebrating Christopher Columbus; these holidays replaced Columbus Day and now celebrate the colonized instead of celebrating the colonizer. Mexico treats Columbus Day observances as celebrations of Hispanic heritage, not a celebration of the person.

Interestingly, my Spanish friends tell me that Columbus is not considered that important in the colonization of the Americas. Their take is that he is idolized by Italian-American immigrants because so few of the North American heroes are non-Anglo and they can point to him and claim that they too are Americans (the self-identification problem discussed below).

Newsday then claims that his indomitable will and superb faith in God and in his own mission made him great. We have constitutionally defined separation of church and state in the United States, so superb faith in God doesn't justify his own National holiday. There are probably millions of Americans in history who meet the criteria of indomitable will and faith in themselves, so again, this doesn't qualify Columbus as in the top three people in American history. In fact, he was never in America. By these metrics, we should have Genghis Khan day.

Newsday claims that Columbus' epic feat sparked the bold treks of his fellow Italian navigators: Giovanni da Verrazzano, Giovanni (John) Caboto and Amerigo Vespucci. Sorry, running into an island you didn't think existed doesn't qualify as an "epic feat" in my book. The claim about his fellow Italian navigators is interesting because it ignores the Portuguese and Spanish navigators, some of whom actually found Brazil while sailing around Africa, so would have found the Americas regardless of Columbus. See, e.g. the Portuguese Pedralvares Cabral stumbled upon the coast of Brazil in April of the year 1500 while going from Cape Verde (Africa) to Good Hope while using a maneuver to advantage of currents by sailing west from the coast of Africa. A couple months prior, Vicente Yáñez Pinzón of Spain had reached the coast of Brazil using the same maneuver. He continued his journey following Brazil's coast to the North, discovering the Amazon river in the process. Diego de Lepe (Spain) reached Brazil in February, and eventually joined Pinzón around the mouth of the Amazon. These voyages starting from the African coast had nothing to do with Columbus in 1492, but demonstrate that Europeans would have reached the Americas within eight years, regardless (even if Cabral found Brazil several weeks after Vicente Yáñez Pinzón of Spain).

Newsday finally claims that he had immense maritime abilities. First of all, his math was so bad that he would have starved to death if he hadn't run into an island he didn't think existed. Second, there are lots of people with immense maritime abilities. They don't get their own national holidays.

The question is whether we should celebrate him **today**. Whether the American Pledge of Allegiance was penned in 1892 to celebrate the 400th anniversary of Columbus’ first voyage is irrelevant.

See [Newsday quotes]({{< relref "oh-look-a-squirrel.md#Newsday" >}})

### New York Times
[Mary Ann Castronovo Fusco in the New York Times](https://www.nytimes.com/2000/10/08/nyregion/in-person-in-defense-of-columbus.html)?

I was hoping that a New York Times article might provide more substance for defending Columbus. Certainly it quoted more people. It claimed Columbus as symbolizing a voyage of freedom and globalization. Of course, it didn't end up in freedom for the natives and all the merchants involved in the Silk Road would disagree that he started globalization. Another person quoted in the article says that his arrival marks where we as a country began our identity. Since he never set foot in what became the US, I would think Jamestown and Plymouth have a better claim for that than Columbus.

he only historian quoted disdained the Viking's earlier arrival because there wasn't a significant or important tradition that survived from the Vikings, but didn't cite a tradition that Columbus started in America. I agree a historical event happened. You can commemorate the date that mainstream Europe realized the Americas existed. My question is whether the day should be recognized as an event or a flawed man should be honored and celebrated and, if so, honored and celebrated for what? That historian did say that "A more appropriate word for what happened would be 'commemorate." Hey, at least we've gotten away from  "celebrate", but I'm still questioning the characterization of him as a "genius".

See [New York Times quotes]({{< relref "oh-look-a-squirrel.md#NYT" >}})

### Knights of Columbus
[Christopher Columbus Fake History](https://www.kofc.org/en/columbia/detail/christopher-columbus-fake-history.html) Christopher Columbus and Fake History 9/1/2017 by Gerald Korson

The Knights of Columbus article actually gets a positive mark by at least claiming that a poll they conducted showed 55% of Americans in favor of Columbus Day. Finally, someone actually begins to address the question of whether Columbus should be one of three individuals who get a National Holiday in the US. I don't know anything about the poll, but at least this gets to one of the important questions.

Unfortunately the article spends all the rest of its time complaining that people are discriminating against him because he was Italian and Catholic and he did what was allowed at the time. Being Italian and Catholic don't qualify you for your own national holiday. Whether or not he did what was allowed at the time is also not relevant to whether he should have a holiday today. We are in different times, different cultural beliefs, and holidays for today should reflect today's values.

See [Knights of Columbus quotes]({{< relref "oh-look-a-squirrel.md#KOC" >}})

### The National Review?
[National Review](https://www.nationalreview.com/2019/10/columbus-day-a-defense-of-christopher-columbus/)

The National review article has the same nonsense about indomitable will as the Newsday article. As I said above, I have two problems with this. First, there are hundreds of thousands of people in the United States who have indomitable will and a superb faith in their own mission. Why aren't they honored. (I'll skip the bit about superb faith in God because the United States is supposed to have separation of church and state.)  Second, this criteria would suggest we should honor mass murderers if they have an indomitable will and they believe god told them to murder people. Seriously?

The National Review quotes someone making the absolutely absurd claim that "Columbus may well have been the first man in history to engage in substantive diplomacy." Seriously? No substantive diplomacy in the world until 1492? Does the author never read any history? There were thousands of years of substantive diplomacy between kingdoms before 1492.

The National Review then quotes a letter from Columbus complaining that he is being measured against criteria that applied to a governor in a civilized country instead of a nomadic wartime people. Setting aside the "oh poor me", the letter from Columbus after the first voyage to Ferdinard and Isabella, just before the paragraphs where he offered the natives as sales to the Spanish Crown:  "They have no iron or steel, nor any weapons; nor are they fit thereunto; not because they be not a well-formed people and of fair stature, but that they are most wondrously timorous… such they are, incurably timid… They are artless and generous with what they have, to such a degree as noone would believe but him who had seen it. Of anything they have, if it be asked for, they never say no, but do rather invite the person to accept it, and show as much lovingness as though they would give their hearts" ... And apparently makes them  tractable slaves.…

Overall, I'm not really seeing why **today** we should venerate Columbus as one of the top three people in the history of the United States.
See [The National Review quotes]({{< relref "oh-look-a-squirrel.md#TNR" >}})

## The Self-Identification Problem
As stated above, I view the self-identification problem as communities elf-identifying with a person and then viewing "attacks" on this person as an attack on themselves. The consequent emotional response precludes the ability to discuss whether Columbus should continue at the same level of honor (one of the top three in American history) justifying celebrations by today's cultures.

### Minneapolis Star Tribune
Let's go back to the Minneapolis Star Tribune and see how the self-identification problem arises. That article said "[i]t is ironic that Columbus should take the blame, for he was our first “multicultural” hero. An Italian Catholic who sailed for Spain, Columbus was one of a handful of celebrated Americans who did not fit neatly into the white Anglo-Saxon Protestant mold." Just stop! The complaint is that Columbus is solely taking the blame for mistreatment of natives and gee, he wasn't an Anglo-Saxon Protestant so he should get excused. First, no one is saying that Columbus is solely to blame. There are lots of targets for that, many of them are Anglo-Saxon Protestants. Second, if you are bad, the fact that other people were bad and your particular ethnic group needs a hero does not give you a pass. Are you excusing Benito Mussolini as well because Hitler and Stalin were worse? Hey, he kept the trains running on time!

The article then points out that the Pilgrims were religious bigots.  Yup. Several of them were my ancestors. I agree they were not nice people. They didn't come to America for religious tolerance, they wanted to be free to practice religious intolerance. That doesn't give other wrongdoers a pass.

See [Minneapolis Star Tribune quotes]({{< relref "oh-look-a-squirrel.md#MST" >}})

### Knights of Columbus
Back to the Knights of Columbus who, unfortunately, are poster children for this issue. First they claim that denigrating Columbus apparently denigrates Italians. That is like saying denigrating Mussolini is denigrating Italians. An individual is not a culture. This is a problem when people identify with heroes with feet of clay. If someone points out the clay, suddenly the populace is getting denegrated, not the "hero".

Then they say that denigrating Columbus is anti-Catholic. No, it is not anti-Catholic. If you were really paying attention, it is anti-colonialist and anti-imperialist and if you think that is an attack on you, then maybe you have something in your sub-conscience to be defensive about?

See [Knights of Columbus quotes]({{< relref "oh-look-a-squirrel.md#KOC" >}})

## Miscellaneous Rhetorical BS

The following is just a compilation of rhetorical misdirections, mischaracterizations and non-equivalence arguments scattered around the articles cited above.

Several of the articles claim that "[p]re-Columbian civilizations in the Americas were not the peaceful paradises that some may imagine them to be." This is completely irrelevant to the question of whether today's America should give Columbus one of the three national holidays dedicated to an individual. This is the same stupid argument that gets rolled out if police shoot and kill someone doing absolutely nothing wrong, but the victim is later found to have a criminal record. That doesn't excuse the extrajudicial shooting and killing by the police.

Several of the articles use a standard trick of overstating the claims of the opposition, for instance claiming that the people opposed to Columbus Day claim that Columbus and his men were singlehandedly responsible for the suffering of indigenous people in America. This is obviously absurd and no one is claiming that. No one is exonerating any perpetrators of the mistreatment of the natives. Columbus is one of a large group who does not live up to the standards of today. Remember it is a holiday for today, not a holiday for 500 years ago. No one is saying that he initiated the African slave trade.

Several articles try to claim that Columbus justified slavery as a way of bringing people into the Christian faith. Again this is deliberate BS. Columbus stopped trying to convert the natives to Christianity as soon as he was told that Christians couldn't be sold as slaves under Spanish law.

New York Times article made the bizarre statement that claims that Columbus's followers wiped out the Taino people of the Caribbean were inaccurate because while many were murdered, others were sold into slavery and, because people had to pay taxes on slaves they owned, they undercounted the number of slaves to avoid taxes. <start sarcasm here> Oh, don't disparage Columbus and his followers because they didn't wipe out the Taino - some survived as uncounted slaves. Well, I guess that settle it, no problem.<end sarcasm here>

From the standpoint of today, celebrating someone whose deeds do not fit today's culture is effectively whitewashing that person. Renaming or eliminating Columbus Day is not erasing history. History still stands, the events happened. Renaming or eliminating Columbus Day is saying that under today's morality this event should not be **celebrated or honored**. It is not being expunged from the records. Keep the history for lessons learned.

The fact that he commissioned someone to study the customs of the inhabitants he was enslaving does not excuse the enslaving. Let's see. On his first day in the New World, he ordered six of the natives to be seized, writing in his journal that he believed they would be good servants. I wonder why the Natives fought back and I think I'll exercise some caution on his "adoption" of a Native American boy.

Newsday notes that "his political and administrative skills may have fallen far short of his immense maritime abilities." We already know that he only had competent maritime abilities - He did manage to find Hispaniola again. But that doesn't justify his own national holiday in a country in which he never set foot.

I find the New York Times article particularly full of BS in this regard. The article admits he didn't start slavery, but he brought the entrepreneurial form of slavery to the New World. I don't think that is something we should be celebrating. It claimed that European slavery was rooted in the Aristotelian concept that "if a person is captured in war, they're legitimately a slave and, therefore there was nothing racial about it". This is truly misstating the facts. The concept of slavery as anyone captured in war died in Europe in late medieval times. You should know very well that by this time in Europe slavery was limited to pagans and infidels. See e.g. the 1452 Papel Bull Dum Diversas authorizing the King of Portugal to enslave any Saracens or pagans or other enemies of Christ he encountered. This actually created a problem for Columbus because he wanted to sell slaves, but couldn't if they converted to Christianity due to Spanish law. So he decided not to try to convert them to Christianity in order to sell them as slaves (particularly since he couldn't find as much gold as he had promised the Spanish Crown. Sure.. not racial at all. Just classify them as subhuman and do whatever you want.

There is an interesting point that isn't discussed much. When the Spanish Crown heard what was going on in Hispaniola, they arrested him and kept him in prison for six weeks, then pardoned him but took away his right to govern. It isn't clear to me whether taking away his right to govern was punishment for what they were hearing about conditions in Hispaniola or because he exaggerated what he could deliver in the way of gold, spices, cotton and aloe wood. See a [translation of his letter to Ferdinand and Isabella](https://books.google.com/books?id=NPXCTYxu-R4C&pg=PA10#v=onepage&q&f=false)

The Knights of Columbus article quotes Carol Delaney, a former professor who claimed that Columbus gave orders to treat the natives with respect and it was some of his men that disobeyed. Prof. Delany, may I quote from Columbus's first letter to Ferdinard and Isabella cited above? "Their Highnesses may see that I shall give them as much gold as they may need, with very little aid which their Highnesses will give me; spices and cotton at once, as much as their Highnesses will order to be shipped, and as much as they shall order to be shipped of mastic… and aloe-wood as much as they shall order to be shipped; **and slaves as many as they shall order to be shipped.**" Slavery as benign. What can I say? According to documents discovered by Spanish historians in 2005, Columbus imposed iron discipline on what is now the Caribbean country of Dominican Republic. In response to native unrest and revolt, Columbus ordered a brutal crackdown in which many natives were killed; in an attempt to deter further rebellion, Columbus ordered their dismembered bodies to be paraded through the streets. Yup, all around good guy. At the end of Christopher's second expedition, he sent 500 natives as slaves to Queen Isabella, the queen was apparently horrified as she believed that any people Columbus “discovered” were Spanish subjects who could not be enslaved—and she promptly and sternly returned the explorer’s gift. Oops.

## Conclusion
Good, bad or indifferent, Columbus has a place in world history and changing or eliminating a holiday celebrating him does not erase him from history. As previously stated, I really think the question should be whether today's culture in the United States should celebrate and honor Columbus as one of the top people in history. If so, we certainly shouldn't whitewash his, or any other persons' story because we don't want emulation of the bad along with the good.

I also think that separating one's self from identifying with Columbus would actually allow the debate to occur on substantive grounds, not emotional grounds. Seriously, even under the morality of the day, he lost his governorship over his treatment of natives. From a comparison standpoint, Genghis Khan had great accomplishments, but he is acknowledged, not celebrated.

Attacks on Columbus are not anti-Italian American. They are not anti-Catholic. They most certainly are attacks on beliefs in "expansionism, imperialism, colonialism, racism, and sexism because "God intended us to be an empire over all the Earth." If that is what you self identify with, then we probably don't have any common ground to discuss the subject because that runs counter to my desire to treat everyone fairly.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.

## Footnotes Only if You Really Want Them
### UPennStatesman {#UPennStatesman}
[UPennStatesman](https://upennstatesman.org/2018/10/08/defense-columbus-day/)

- *"Columbus was a man ahead of his time who was a skilled captain and argued that the world was a sphere."*
- *"He was a capitalist, in that he sought to open free trade routes between Europe and the surrounding world."*
- *"When he discovered the American coast, he made sure to document his journey in such a way that those who followed him could find it again."*

- *"Columbus’ detractors would have us believe that he and his men were singlehandedly responsible for the suffering of indigenous people in America."*

  Complete misdirection. No they don't think he is singlehandedly responsible for anything. Even his supporters agree that He and his men were responsible for some of the suffering of indigenous people in the West Indies. More on that later.

- *"[p]re-Columbian civilizations in the Americas were not the peaceful  paradises that some may imagine them to be."*

  Again,  Misdirection. Other people were bad, so he is excused. No, that doesn't give him a pass. This is the same stupid argument that gets rolled out if police shoot and kill someone doing absolutely nothing wrong, but the victim is later found to have a criminal record. That doesn't excuse the extrajudicial shooting and killing by the police.

- *"[T]hose who blame the struggles of the indigenous people on Columbus are exonerating countless other perpetrators of such crimes."*

  Misdirection again. No they are not exonerating anyone. This is the equivalent of saying that if 100 people commit murder and someone happens to write a biography of one of them, then he must not be guilty because the author didn't write a biography of each of the 100.

- *"The purpose of studying history is neither to edit it nor to erase the cultural context in which past events took place."*

  Which is exactly what this author is trying to do by whitewashing Columbus. Eliminating Columbus Day is not erasing  history, it is saying that under today's morality this event should not be **celebrated or honored**. It is not being expunged from the records. Keep the history for lessons learned.

- *"Renaming Columbus Day is nothing but a superficial attempt to right our wrongs."*

  At least this statement implies agreement that wrongs occurred, but I am actually surprised that they said **our wrongs**.  Again, explain why should "Columbus Day" be   **celebrated** as opposed to observed?

- *"As for me, I’d rather pin the blame on Amerigo Vespucci. He’s the one who wrote the book that put America on the map, and he’s the one who gave our country his name. That way, when we blame Amerigo for the mistakes of the past, we will really be blaming ourselves. And that’s an irony I can live with."*

  Huh? Vespucci certainly publicized the Americas (and lied about two of the four trips he claimed to have been on), but I certainly don't remember histories saying that he paraded dismembered bodies through the streets to  discourage rebellion.

### Minneapolis Star Tribune {#MST}
[Minneapolis Star Tribune](https://www.startribune.com/in-defense-of-christopher-columbus/279629112/?refresh=true)

- *"It is unfortunate that Columbus has to go, for he embodies many of the qualities we respect in the powerful and famous of our own  day. Like Bill Gates and the late Steve Jobs, Columbus came from an ordinary family with no pedigree or family power to pave his way. Like them, he was a technocrat, a master navigator in a day when sailing a small vessel across the ocean was as much of a mystery as the inner workings of the Internet."*
- *"Perhaps we are too eager to find fault, too unwilling to accept the fact that few people get anything of importance done without making mistakes along the way. People who achieve big things often make big mistakes, and so it is for Columbus."*
- *"[i]t is ironic that Columbus should take the blame, for he was our first “multicultural” hero. An Italian Catholic who sailed for Spain, Columbus was one of a handful of celebrated Americans who did not fit neatly into the white Anglo-Saxon Protestant mold."*

    Oh good, being an Italian Catholic excuses everything because of the bad Anglo-Saxon Protestants. No it doesn’t. If you are bad, the fact that other people were bad and your particular ethnic group needs a hero does not give you a pass. Are you excusing Benito Mussolini as well because Hitler and Stalin were worse? Hey, he kept the trains running on time!

- *“The Pilgrims of New England who braved the storm-tossed Atlantic seeking the freedom to worship God were also religious bigots, ready to jail, flog or exile anyone who did not share their views on Christian doctrine."*

    Yup. Several of them were my ancestors. I agree they were not nice people. They didn’t come to America for religious tolerance, they wanted to be free to practice religious intolerance. That doesn’t give other wrongdoers a pass.


### Newsday {#Newsday}
[Newsday](https://www.newsday.com/opinion/commentary/in-defense-of-the-great-columbus-1.14371854)?

Newsday tried justifying the honor by claiming he was a prime mover of Western civilization.
- *"[h]e was a prime mover of Western civilization who remains an integral part of humanity’s global patrimony."*

  I have idea what this author is claiming here. What does "humanity's global patrimony" mean anyway?

- *"His voyages across the Atlantic led to the discovery of the New World, ushering in an age of exploration and a globalization of peoples, plants, microbes and cultures. The cultural richness and diversity of today’s Hispanic world would not exist without Columbus."*

  I agree that the cultural richness of today's Hispanic world would be different (better or worse, who knows). This might be a stronger argument if the today's Hispanic world hadn't replaced Columbus Day with "Day of the Race or Die de la Raza" in Colombia, Guatemala and Uruguay, "Day of Indigenous Resistance" in Venezuela, "Day of Encounter of Cultures" in Costa Rica, "Day of the Encounter of the Worlds" in Chile, "Day of Respect for Cultural Diversity" in Argentina, "Day of the Americas" in Belize. No, that is not celebrating Christopher Columbus; these holidays replaced Columbus Day and now celebrate the colonized instead of celebrating the colonizer. Mexico treats Columbus Day observances as celebrations of Hispanic heritage, not a celebration of the person.

  Interestingly, my Spanish friends tell me that Columbus is not considered that important in the colonization of the Americas. Their take is that he is idolized by Italian-American immigrants because so few of the North American heroes are non-Anglo and they can point to him and claim that they too are Americans (the self-identification problem discussed below).

- *"Yes, Columbus was a complicated icon. As Samuel Eliot Morrison noted, “He had his flaws and his defects, but they were largely the defects of the qualities that made him great — his indomitable will, his superb faith in God and in his own mission.”*

  So, by these metrics Hitler was great and should have his own day as well?

- *"Columbus’ epic feat sparked the bold treks of his fellow Italian navigators: Giovanni da Verrazzano, Giovanni (John) Caboto and Amerigo Vespucci."*

  Sorry, running into an island you didn't think existed doesn't qualify as an "epic feat" in my book. For that matter, The Portuguese Pedralvares Cabral stumbled upon the coast of Brazil in April of the year 1500 while going from Cape Verde (Africa) to Good Hope while using a maneuver to advantage of currents by sailing west from the coast of Africa. A couple months prior, Vicente Yáñez Pinzón of Spain had reached the coast of Brazil using the same maneuver. He continued his journey following Brazil's coast to the North, discovering the Amazon river in the process. Diego de Lepe (Spain) reached Brazil in February, and eventually joined Pinzón around the mouth of the Amazon. These voyages starting from the African coast had nothing to do with Columbus in 1492, but demonstrate that Europeans would have reached the Americas within eight years, regardless (even if Cabral found Brazil several weeks after Vicente Yáñez Pinzón of Spain).

- *"Though he never set foot in the heartland of North America, Columbus has long been revered in the United States. Indeed, our Pledge of Allegiance was penned by Francis Bellamy to celebrate the 400th anniversary of Columbus’ first voyage."*

  But the question is whether we should celebrate him **today**.

- *"In “Columbus: The Great Adventure,” Paolo Emilio Taviani wrote, “Christopher Columbus of Genoa was the greatest and most spectacular actor at the beginning of the modern age.”*

  So having a good publicist also excuses behavior. Sort of like how movie stars and star athletes manage to avoid jail time for some of their behavior.

- *"He enlarged the world, altered the course of human events and made possible “the last best hope of earth” — the United States of America."*

  Can you possibly get any more egotistical? Sorry, I just don't see a specific claim here, just hand waving.


- *"The radical assault on the Admiral of the Ocean Sea amounts to a declaration of war on history. Columbus was neither a treasonous Confederate general nor a nefarious neo-Nazi. "*

  No, it is a declaration of war against celebrating severely flawed people who were bad at math.

- *"Columbus did not initiate the African slave trade. Nor did he plan the genocide of indigenous peoples. In fact, he commissioned Fray Ramon Pane to study the customs, religion and folklore of the Tainos, aboriginal inhabitants of the Caribbean."*

  And we have misdirection yet again. No one said he initiated the African slave trade. How do you possibly think that commissioning someone to study the customs of the inhabitants he was enslaving excuses the enslaving?

- *"As an Italian sailing for Spain, Columbus was caught in the crossfire among the Spanish crown, its conquistadors and the indigenous peoples. And his political and administrative skills may have fallen far short of his immense maritime abilities."*

  What immense maritime abilities? Navigators are supposed to be good at math, not bad at math.

### New York Times {#NYT}
[Mary Ann Castronovo Fusco in the New York Times](https://www.nytimes.com/2000/10/08/nyregion/in-person-in-defense-of-columbus.html)?
Do we get better arguments from the New York Times? Maybe Mary Ann Castronovo Fusco in the New York Times?

- *“Certainly we don’t want to downplay the tragedies that happened,” said John Sebastiano, president of the Montville chapter of UNICO. “But for a lot of individuals, the symbolism of Christopher Columbus was that of a voyage of freedom. It really was the start of the globalization of our world."*

  How was Columbus' voyage a “voyage of freedom”? What I think he may be talking about here is that the discovery of the Americas opened up new lands for emigrants from Europe looking for opportunities unavailable where they live. Ok. The people actually inhabiting the land at the time lose out, but they were losers, so don’t count. It also wasn’t the start of globalization of our world. Does the “Silk Road” mean anything to you? It just meant you could go west without running out of water as well as east or south.

- *“I don’t think that it should go to the point of taking away from the tremendous achievement of Columbus.Though modern textbooks no longer state that Columbus discovered the New World, his arrival marks “where we as a country and as a hemisphere began our identity,” said Mr. Connell. (William J. Connell, a historian at Seton Hall University) “It’s a question of the contact that matters. There wasn’t a significant or important tradition that survived from the voyages of the Vikings."*

    I agree a historical event happened. You can commemorate the date that mainstream Europe realized the Americas existed. My question is whether the day should be recognized as an event or a flawed man should be honored and celebrated and, if so, honored and celebrated for what?

- *“That said, Mr. Connell does not consider himself an apologist for Columbus. ‘I’m just doing my job as a historian,” he said. ‘Celebrate’ is a word we could use for Columbus’s genius, his persistence against the odds in getting people who were much more powerful than he was to back him in a risky enterprise that had results way beyond anyone’s imagination. We can celebrate his enterprise and ingenuity. A more appropriate word for what happened would be ‘commemorate."*

    Hey, at least we’ve gotten away from “celebrate”, but I’m still questioning the characterization of him as a “genius”..

- *"As a man of the Renaissance, Columbus operated under a set of assumptions that 'sound terrible to modern ears,'" Mr. Connell allowed. "He justified slavery in the Caribbean as being a way of bringing people into the Christian faith. But the European concept of slavery was rooted in the Aristotelian concept that "if a person is captured in war, they're legitimately a slave," he explained. "There was nothing racial about it."*

  Come on Professor Connell. The concept of slavery as anyone captured in war died in Europe in late medieval times. You should know very well that by this time in Europe slavery was limited to pagans and infidels. See e.g. the 1452 Papel Bull Dum Diversas authorizing the King of Portugal to enslave any Saracens or pagans or other enemies of Christ he encountered. This actually created a problem for Columbus because he wanted to sell slaves, but couldn't if they converted to Christianity due to Spanish law. So he decided not to try to convert them to Christianity in order to sell them as slaves (particularly since he couldn't find as much gold as he had promised the Spanish Crown. Sure.. not racial at all. Just classify them as subhuman and do whatever you want.

- *"Moreover, widely spread accounts that Columbus's followers wiped out the Taino people of the Caribbean were inaccurate, says Jorge Estevez, himself of Taino lineage, who is a program coordinator at the National Museum of the American Indian in Manhattan. Mr. Estevez says that although many natives were murdered, fell victim to European diseases, or were taken captive, others intermingled with the Spanish settlers. And the settlers who were given Tainos as slaves were required to pay taxes on them, resulting in the undercounting of the Tainos as a form of tax evasion and leading to reports of their eradication."*

  So much stupid. <start sarcasm here> Oh, don't disparage Columbus and his followers because they didn't wipe out the Taino - some survived as uncounted slaves. Well, I guess that settle it, no problem.<end sarcasm here>

- *"Columbus didn't start slavery," said Mr. Connell. "He brought the entrepreneurial form of slavery to the New World. It was a phenomenon of the times. With all great figures of the past, we need  more understanding, critical understanding that sees the person's flaws and the inaccuracies and myths that have arisen around him, but we shouldn't forget the tremendous changes that they created."*

  No one said Columbus started slavery. I'm supposed to rejoice in an entrepreneurial form of slavery? Better hope there is no hell. By the way, cute attempt at resetting the benchmark. Did you notice the shift to creating changes from creating good things?

- *"The scholar went on: 'I think we have to be very careful about applying 20th-century understandings of morality to the morality of the 15th century.'"*

  Uh huh. This actually gets to an interesting point that isn't discussed much. When the Spanish Crown heard what was going on in Hispaniola, they arrested him and kept him in prison for six weeks, then pardoned him but took away his right to govern. It isn't clear to me whether taking away his right to govern was punishment for what they were hearing about conditions in Hispaniola or because he exaggerated what he could deliver in the way of gold, spices, cotton and aloe wood. See a [translation of his letter to Ferdinand and Isabella](https://books.google.com/books?id=NPXCTYxu-R4C&pg=PA10#v=onepage&q&f=false)

### Knights of Columbus {#KOC}
Maybe the Knights of Columbus? From [Christopher Columbus Fake History](https://www.kofc.org/en/columbia/detail/christopher-columbus-fake-history.html) Christopher Columbus and Fake History 9/1/2017 by Gerald Korson

- *"Despite animus among some groups today, the majority of Americans view the explorer positively and with pride. In a K of C-Marist poll from December 2016, 62 percent of Americans expressed a favorable opinion of the explorer and 55 percent said they were in favor of Columbus Day, the holiday named for him. By contrast, fewer than 3 in 10 view Columbus unfavorably and only 37 percent oppose the holiday named for him."*

  Finally, some one talks about whether today's culture agrees that he should be celebrated. I don't know anything about the poll, but at least this gets to one of the important questions.

- *"Unfair attacks on Columbus, past and present, should not be allowed to obscure the truth about the man, his voyage and his motives. Born in Genoa, Italy, Columbus was a deeply Catholic explorer who was willing to go against the grain. He believed he could reach the shores of Asia by sailing a mere 3,000 miles west across the Atlantic. Such a passage would establish faster and easier trade routes than were possible through overland travel or by sailing south and east around Africa."*

  Sigh. And then we lose the plot and resort to dogwhistles. Sorry, "deeply Catholic" and "willing to go against the grain" convinced that his bad math was right don't rise to the level of him being one of three people celebrated with National Holidays in the United States.

- *"... Carol Delaney, a former professor at Stanford and Brown universities, in her book 'Columbus and the Quest for Jerusalem' (2011). In her opinion, “we must consider his world and how the cultural and religious beliefs of his time colored the way he thought and acted.”*

  The question should not be whether he acted consistent with the cultural and religious beliefs of the time, the question is whether his actions should be honored **today** - in a different time with different cultural beliefs.


- *"Eugene F. Rivers III, founder and president of the Seymour Institute for Black Church and Policy Studies, published an op-ed article Dec. 2, 2016. “To celebrate one cultural group does not require that we denigrate another,” he wrote. “Rather than renaming Columbus Day, why not add another holiday, Indigenous Peoples Day, to Baltimore’s calendar in honor of Native Americans?”*

  What a minute. Suddenly the charge is that changing Columbus day denigrates another culture? Whose culture is getting denigrated? This is a problem when people identify with heroes with feet of clay. If someone points out the clay, suddenly the populace is getting denegrated, not the "hero".

- *"Today, one can still hear echoes of anti-Catholic prejudice in the modern attacks. For some, Columbus’ sponsorship by Spain and introduction of Christianity and Western culture to the lands he discovered make him immediately suspect. The new wave of anti-Columbus attacks go so far as to say that Columbus intended nothing good. “These criticisms primarily charge Columbus with perpetrating acts of genocide, slavery, ‘ecocide,’ and oppression,” explained Robert Royal, president of the Faith and Reason Institute and author of 1492 and All That: Political Manipulations of History (1992). Nonetheless, a closer examination of the record reveals a different picture. “The dominant picture holds him responsible for everything that went wrong in the New World,” wrote Carol Delaney, a former professor at Stanford and Brown universities, in her book Columbus and the Quest for Jerusalem (2011). In her opinion, “we must consider his world and how the cultural and religious beliefs of his time colored the way he thought and acted.”*

  No, it is not anti-Catholic. If you were really paying attention, it is anti-colonialist and anti-imperialist and if you think that is an attack on you, then maybe you have something in your sub-conscience to be defensive about?

  I have two things wrong in this section. First no one hold Columbus responsible for everything that went wrong in the New World. Second, the question should not be whether he acted consistent with the cultural and religious beliefs of the time, the question is whether his actions should be honored **today** - in a different time with  different cultural beliefs.


- *"Later, as a nation began to coalesce out of the American colonies, its leaders recognized the admiral’s legacy. “Columbia” served as an informal name for what would become the United States of America. The eventual designation of the nation’s capital reflects the esteem the founders had for the Genoese explorer...A decade later, as the Order celebrated its patron on the 400th anniversary of his discovery, President Benjamin Harrison proclaimed a national Columbus holiday. He called for “expressions of gratitude to Divine Providence for the devout faith of the discoverer, and for the Divine care and guidance which has directed our history and so abundantly blessed ourpeople."*

  Things change. Nothing is static. The question is whether today's culture should hold Columbus up to their children as one of the top three people in history.

- *"The new wave of anti-Columbus attacks go so far as to say that Columbus intended nothing good. “These criticisms primarily charge Columbus with perpetrating acts of genocide, slavery, ‘ecocide,’ and  oppression,” explained Robert Royal, president of the Faith and Reason Institute  and author of 1492 and All That: Political Manipulations of History (1992). The dominant picture holds him responsible for everything that went wrong in the New World,”*

  No one holds Columbus responsible for everything that went wrong in the New World.

- *"In a 2012 Columbia interview, Delaney further explained that Columbus found the native peoples to be “very intelligent” and his relations with them “tended to be benign.” He gave strict instructions to the settlers to “treat the native people with respect,” though some of his men rebelled and disobeyed his orders, particularly during his long absences, Delaney added."*

  Prof. Delany, may I quote from Columbus's first letter to Ferdinard and Isabella cited above? "Their Highnesses may see that I shall give them as much gold as they may need, with very little aid which their Highnesses will give me; spices and cotton at once, as much as their Highnesses will order to be shipped, and as much as they shall order to be shipped of mastic… and aloe-wood as much as they shall order to be shipped; **and slaves as many as they shall order to be shipped.**" Slavery as benign. What can I say?

- *"The writings of Bartolomé de las Casas — a 16th-century Spanish Dominican priest, historian and missionary — exposing the abuse of the native peoples are often cited in an effort to impugn Columbus. But while de las Casas lamented the suffering of indigenous people, he also admired and respected Columbus for his “sweetness and benignity” of character, his deep faith and his accomplishments."*

  Setting aside the fact that las Casas was 18 when he met Columbus, I will note in passing that according to documents discovered by Spanish historians in 2005, Columbus imposed iron discipline on what is now the Caribbean country of Dominican Republic. In response to native unrest and revolt, Columbus ordered a brutal crackdown in which many natives were killed; in an attempt to deter further rebellion, Columbus ordered their dismembered bodies to be paraded through the streets. Yup, all around good guy.

- *"According to Delaney, Columbus “fervently believed it was the duty of every Christian to try to save the souls of non- Christians,” and it was this passion that “led him on a great adventure, an encounter such as the world has never seen.”*

  Except for that small problem of if you convert them to Christianity, you can't sell them as slaves, so he decided not to convert them.

### The National Review {#TNR}
Maybe the [National Review](https://www.nationalreview.com/2019/10/columbus-day-a-defense-of-christopher-columbus/) can help us out.

- *"As Morison pointed out, Columbus “had his flaws and his defects, but they were largely the defects of the qualities that made him great — his indomitable will, his superb faith in God and in his own mission.” That will and faith make him a man worthy of this, the day on which we honor him."*

  I have two problems with this. First, there are hundreds of thousands of people in the United States who have indomitable will and a superb faith in their own mission. Why aren't they honored. (I'll skip the bit about superb faith in God because the United States is supposed to have separation of church and state.)  Second, this criteria would suggest we should honor mass murderers if they have an indomitable will and they believe god told them to murder people. Seriously?

- *"Such complexities — of warring tribes, alien cultures, and internecine Spanish disputes — were part of the reality that Columbus had to navigate. The commentator Michael Knowles aptly notes that Columbus may well have been the first man in history to engage in substantive diplomacy."*

  Seriously? No substantive diplomacy in the world until 1492? Does Michael Knowles not read any history?

- *" Columbus himself described the harshness of life in the New World:"They judge me over there as they would a Governor who had gone to Sicily, or to a city or town placed under regular government, and where the laws can be observed in their entirety without fear of ruining everything; and I am greatly injured  thereby. I ought to be judged as a Captain who went from Spain to the Indies to conquer a numerous and warlike people, whose customs and religion are very contrary to ours; who live in rocks and mountains, without fixed settlements, and not like ourselves; and where, by the divine will, I have placed under the dominion of the King and Queen, our sovereigns, another world, through which Spain, which was reckoned a poor country, has become the richest."*

  Setting aside the "oh poor me", the letter from Columbus after the first voyage to Ferdinard and Isabella, just before the paragraphs where he offered the natives as sales to the Spanish Crown:  "They have no iron or steel, nor any weapons; nor are they fit thereunto; not because they be not a well-formed people and of fair stature, but that they are most wondrously timorous… such they are, incurably timid… They are artless and generous with what they have, to such a degree as noone would believe but him who had seen it. Of anything they have, if it be asked for, they never say no, but do rather invite the person to accept it, and show as much lovingness as though they would give their hearts" ... And apparently makes them  tractable slaves.…

Overall, I'm not really seeing why **today** we should venerate Columbus as one of the top three people in the history of the United States.

- *"Columbus was not, as is well-known to the ungrateful modern-day legatees of his conquest, a perfect man. A famous example of his errant moral judgment was the exploitation of Native labor he sanctioned in Santo Domingo. It was markedly aberrant behavior from Columbus, who took well-noted pains to treat the Native peoples with basic dignity even as many of his contemporaries couldn’t be bothered. It also tends to be presented by Columbus’s critics shorn of its context: It was an ill-considered political concession he granted to La Isabela’s alcade mayor, Francisco Roldán, who had staged a revolt against the rule of Columbus’s brother, Bartholomew,  whom he’d left in charge of Hispaniola upon returning to Spain from his second voyage to the New World. Roldán, having established a competing regime on the western side of island, used his newfound leverage to force Columbus to permit the exploitative labor practices upon the latter’s return."*

  By all accounts Columbus's brother probably falls in the same category of people that Leopold II of Belgium hired to exploit the Belgian Congo (don't read that history if you have a weak stomach). But we aren't talking about Bartholomew, we are talking about Christopher. So let's talk about Christopher's second expedition which happened  before the "ill-considered political concession". When Columbus sent 500 natives as slaves to Queen Isabella, the queen was apparently horrified as she believed that any people Columbus “discovered” were Spanish subjects who could not be enslaved—and she promptly and sternly returned the explorer’s gift. Oops.

- *"Columbus, in spite of his many faults, was a man of character. His devout Catholicism informed his treatment of his fellow sailors, whom he enjoined in shipwide prayer every half-hour on the long journey from Spain to the New World. Even Bartolomé de las Casas, the “Defender and Apostle to the Indians” whose lurid, if hyperbolic, treatises on the treatment of Native peoples made him famous, spoke favorably of Columbus and his treatment of indigenous people. Columbus famously adopted a Native American boy and urged his fellow voyagers to exercise restraint and mercy toward the Natives, even, for instance, after they had burned down an entire Spanish settlement and killed all of the Spaniards in the area."*

  Let's see. On his first day in the New World, he ordered six of the natives to be seized, writing in his journal that he believed they would be good servants. I wonder why the Natives fought back and I think I'll exercise some caution on his "adoption" of a Native American boy.

- *"Contrary to the simplistic picture painted by academics, the indigenous cultures Columbus encountered were as assorted as those of any other peoples in history. While it might be true that some such cultures fit the nomadic, tranquil image pushed by the revisionists, not even close to all of them did. Which leads to an inevitable follow-up to those who would eliminate Columbus Day in favor of “Indigenous People’s Day”: Which “indigenous people” do you have in mind? Is it the Kalinago people, who ate roasted human flesh, with a particular affinity for the remains of babies and fetuses? Is it the Aztecs, who killed an estimated 84,000 people in four days in their consecration of the Great Pyramid of Tenochtitlan?"*

  Again, overstating the position of the opposition (not just academics). None of them paint a tranquil image and no one said the West Indies natives were "nomadic". Immediately followed up by - hey, they were bad guys, so excuse our bad guys. I'm not excusing anyone and it still doesn't answer the question of whether Columbus should be honored now, under today's morality.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
