---
title: "What do Peace and Liberty Mean?"
date: 2020-10-08T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Mike Lee, US Senator from Utah [tweeted](https://twitter.com/SenMikeLee?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1314016169993670656%7Ctwgr%5Eshare_3&ref_url=https%3A%2F%2Fnymag.com%2Fintelligencer%2F2020%2F10%2Fmike-lee-not-a-democracy-republican-trump-authoritarian.html) the following on Oct 7, 2020: "Democracy isn’t the objective; liberty, peace, and prosperity are. We want the human condition to flourish. Rank democracy can thwart that." Subsequent tweets included "We're not a democracy", Government is the official use of coercive force-nothing more and nothing less. The Constitution protects us by limiting the use of government force" and "The word 'democracy' appears nowhere in the Constitution, perhaps because our form of government is not a democracy.  It’s a constitutional republic.  To me it matters.  It should matter to anyone who worries about the excessive accumulation of power in the hands of the few."

Each of these seems to preach that coercive force by a few is fine so long as "the few" are not "the government". At that point I would argue that you have a shadow government. Why does it get to have coercive force?

But let's talk about something else today. I want to focus on his statement that liberty and peace are the objective and what do those terms mean?

[Nick Kitchener said](https://permies.com/t/121691/Peace)

> *"Contrary to popular opinion, there are two paths to peace.*

> *The first is the path of resolution through love, honor, and mutual respect.*

> *The second is the path of resolution through death. This second path is why the V symbol was adopted to communicate peace. It is historically the sign of Typhon, god of destruction and death (a.k.a Set). The symbolism was adopted by the allied forces near the end of WWII to rally the population in the cause of achieving peace through annihilation of the enemy.*"

So what is the "Peace" of which Mike Lee speaks? The positive first peace of people getting along with each other and accepting that there are different viewpoints? Or does he mean the negative second peace of no dissent, no thoughts contrary to Newspeak and no first amendment activity because you have achieved the peace the grave?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
