---
title: "Microsoft Patents Chat bot Based on a Specific Person"
date: 2021-01-29T14:18:19-04:00
categories: ['posts']
tags: ['technology']
showToc:  "true"
author: "Peter Hiltz"
---
Microsoft was just granted a [patent on creating a conversational chat bot of a specific person](https://pdfpiw.uspto.gov/.piw?PageNum=0&docid=10853717&IDKey=6E72242A6301&HomeUrl=http%3A%2F%2Fpatft.uspto.gov%2Fnetacgi%2Fnph-Parser%3FSect1%3DPTO2%2526Sect2%3DHITOFF%2526p%3D1%2526u%3D%25252Fnetahtml%25252FPTO%25252Fsearch-bool.html%2526r%3D31%2526f%3DG%2526l%3D50%2526co1%3DAND%2526d%3DPTXT%2526s1%3Dmicrosoft.ASNM.%2526OS%3DAN%2Fmicrosoft%2526RS%3DAN%2Fmicrosoft). The chat bot's personality would apparently be based on images, voice data, social media posts, electronic messages and more personal information.

The Independent claims [Microsoft Patent Shows Plans to Revive Dead Loved Ones as Chat bots](https://www.independent.co.uk/life-style/gadgets-and-tech/microsoft-chatbot-patent-dead-b1789979.html?s=03). Seances would take on new life, but it could also prevent someone from moving on. I leave it to the psychologists as to whether this would be a good thing or bad thing. There have actually been stories about people trying to accomplish this. [Verge](https://www.theverge.com/a/luka-artificial-intelligence-memorial-roman-mazurenko-bot) and [Qz](https://classic.qz.com/machines-with-brains/1018126/lukas-replika-chatbot-creates-a-digital-representation-of-you-the-more-you-interact-with-it/), the science fiction book [Neuromancer](https://williamgibson.fandom.com/wiki/Construct) from 1984 and an episode of Black Mirror ("Be Right Back" - S2E1) among others. As one of the articles quiped: "‘Here is a chatbot of your dead friend, unfortunately he didn’t authorize a paid account, so we have to fund his digital after-life with advertising. This means he will occasionally try to sell goodyear tires to you. Please press accept."

There is also the actual experience of a [college lecturer who died, but the university continues to run his lectures](https://boingboing.net/2021/01/22/when-your-professor-is-dead-but-teaches-anyway.html) and students don't necessarily know that he is actually dead. Presumably any in person contact is with a teaching assistant. Not sure how you get a recommendation from the deceased.

While that might be interesting - or not depending on the person, what happens when someone creates a chat bot based on you? Who is liable if someone believes the chat bot is you and takes some action based on a conversation? For all the people who immediately start talking about the Turing test and that people would know it was a chat bot, think first about how many people have totally believed QAnon conspiracy theories. I'd also be concerned about stalkers that could more and more fixated by interactingg with a chat bot simulacrum that you can't block.

On the positive side, you could, in theory, duplicate yourself a la this [Silicon Valley episode](https://youtu.be/IWIusSdn1e4), but how would you manage to integrate all the information from conversations you yourself did not actually have?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
