---
title: "2020 Philosophy Survey Part 1i: Zombies, Teletransporter and Laws of Nature"
date: 2022-02-21T15:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1i with the topics being Zombies, Teletransporter and Laws of Nature. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Zombies
See [Zombies](https://plato.stanford.edu/entries/zombies/)
Ok. We are not talking about horror movie zombies here. We are talking about beings exactly like us, but without conscious experiences (whatever that means). It really is an attack on physicalism and supports a cartesian duality (mind-body being two separate things).

| Inconceivable | Conceivable but not possible | Possible | Agnostic | Unclear |
|---------------|------------------------------|----------|----------|---------|
| 16%           | 37%                          | 24%      | 11%      | 5%      |
N = 1610

### Teletransporter
Consider the Star Trek fictional teleporter. It desconstructs a living being and reconstructs the exact same pattern somewhere else. The question is whether the original being has survived or whether the original being has died and a cloned being has been created.
| Survival | Death | Agnostic |
|----------|-------|----------|
| 35%      | 40%   | 10%      |
N = 1575

### Laws of Nature
See [Laws of nature](https://plato.stanford.edu/entries/laws-of-nature/)
See also [Laws of Nature](http://www.ninaemery.org/uploads/5/3/2/8/53281417/emery_laws_of_nature.pdf).
Humeans think that laws of nature are descriptive - what regularly happens or is universally the case. Non-Humeans think that natural laws actually govern - what a law says must happen (or a law forbids what can't happen). Consider a regularity. Every road in Michigan has potholes. But it is surely not a law that every road in Michigan has potholes.

| Humean | Non-Humean | Agnostic |
|--------|------------|----------|
| 31%    | 54%        | 9%       |
N = 1554


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
