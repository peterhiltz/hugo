---
title: "Re: Christ in Christmas Slogans"
date: 2024-12-13T10:18:19-04:00
categories: ['posts']
tags: ['religion','living']
showToc:  "true"
author: "Peter Hiltz"
---
I have no problem with the "Keep Christ in Christmas" slogans so long as its proponents remember it is an admonition to Christians, not the rest of the population (you can't co-opt non-Christian Yule celebrations and then claim "Keep Christ in Christmas"). Furthermore, it needs to be  limited to Christmas the religious holiday, and is not expanded to Christmas the secular holiday and the religious holiday of Christmas is limited to December 25.

They should also bear in mind that the "holiday season", stretching from roughly November 1 to January 15 includes 29 religious holidays from 7 different religions (assuming you count Christianity as a single religion).

I would also note that if one of the popes hadn't told church missionaries to co-opt Winter Solstice/Yule celebrations, Christians would probably be celebrating Christmas in the summer.

I'm here for everyone. 

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.


