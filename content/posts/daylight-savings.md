---
title: "Changing the Clocks for Daylight Saving"
date: 2020-11-01T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Today I woke up at either 6:10 am or 5:10 am depending on whether I was looking at a clock that automatically reset itself for the end of daylight savings time in the US. I remember writing a song 20 years ago about the hassle of resetting all the digital clocks in the house. You had to hold down buttons and some of them didn't go backwards or get faster, so you had to wait while it slowly advanced 23 hours. This was in comparison to the relatively quick turn of a knob on the older analog clocks.

Now the digital clocks have gotten much smarter. My phone and digital watch are "automatic" (except that yesterday they were in sync and now they disagree by three minutes). That leaves the alarm clock, two clocks in the kitchen and the clock in the car. The house clocks allow you to go backwards and speed up as you hold the button, so those are easy. The car clock. Well, I likely won't think about it until I'm actually driving and we will likely have to pull out the manual to find the magic incantation to change it. Even though it has a GPS smart enough to change the clock if we drive across time zones, daylight savings time still confuses it. That, in and of itself, is a reason to end daylight savings time.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
