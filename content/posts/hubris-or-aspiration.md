---
title: "Hubris or Aspiration"
date: 2020-09-25T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
There is a trope throughout history about hubris. Wikipedia defines it as "foolish pride, dangerous overconfidence or arrogance". The Encyclopedia Britannica has a more nuanced definition: "overweening presumption that leads a person to disregard the divinely fixed limits on human action in an ordered cosmos." The general theory of the trope is that hubris will lead to your downfall. As examples, in Greek mythology, think Icarus flying too close to the sun; in Christian theology think the fall of Lucifer.

Now think about the trope of hero and companion. The hero is there to save the day and get all the glory while the companion is there to either the servant or the entrance by which the audience views the story. There seems to be a large segment of society that tends to prefer an authoritarian/hierarchical view of the world. There also seems to be a large segment of society that gets upset if the companion has aspirations of also becoming a hero and attempts to emulate the hero. That gets classified as "hubris" and the companion must be put in their place - they can't have "ideas above their station". That last quote shows up in socially stratified cultures throughout history and today. It doesn't matter whether you are talking about Europe, the US, India or [insert a lot of other cultures here]. Entire sections of society are told that they need to be subordinate to others in order to an ordered cosmos to exist. So too with the individual companion.

It is pure speculation on my part that the authoritarian/hierarchical preference group and the anti-companion development preference group substantially overlap.

[Wikipedia](https://en.wikipedia.org/wiki/Hubris) also makes the interesting comments that "According to studies, hubris, arrogance and pretension are related to the need for victory (even if it doesn't always mean winning) instead of reconciliation". That immediately started me thinking about people who view every interaction in terms of winners and losers - there is no such thing as a win-win because, in their minds, one person needs to come out ahead of the other person. This seems to lead to interactions that outside observers would think of as lose-lose, both parties end up worse off, but one is more worse off than the other.

This leads me to wonder whether some people think that if the companion develops to be more like the hero, that changes the alpha balance, upsets the hierarchical structure and, relatively speaking, the companion has "won" more than the hero has. If so, then the hero has "lost" and that cannot be allowed to happen.

Why? I understand that foolish pride, dangerous overconfidence and arrogance are bad. But why is aspirational development a bad thing that will destroy an ordered cosmos?

By the way, why is it almost always "hero" and almost never "heroine"?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
