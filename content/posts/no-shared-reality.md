---
title: "Unity Requires Shared Reality"
date: 2021-01-10T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I spent my career finding ways to thread through conflicting laws and  priorities in multiple countries. Once you found a way to thread through the conflicts, you could pull it together in a way that made sense and reduce the conflicts. The most difficult situations were when governments were intentionally writing rules that favored their national interests and penalized other countries' businesses. Sometimes the public and private rhetoric was different. This generally happened only when you had achieved a respectful relationship with the government officials - they still wouldn't change their positions, but they would admit to the politics involved rather than the logic.

Finding a way to pull threads together so that you and others can make the unity of a patchwork quilt requires a shared reality.

I look at the calls for unity from the Republicans in the US political landscape and split those callers into a few different groups. This is just my thinking and I could easily be wrong.

- There are a few moderates who believe that if they click their heels three times, the world will dissolve from color back to black and white, everyone will recoil from evil, it will have all been just a bad dream and they won't have to deal with the difficulty of nuance. This group is potentially reachable, but keeps closing their eyes and putting their hands over their ears.

- There is a larger group that is, shall I say, intellectually dishonest and morally bankrupt. If they are losing they call for a truce (never a peace treaty) and if they are winning, then "death to the infidels". If I actually believed in hell, it would have a special place for this group. I was told so many many years ago in law school that you can put any protections you want in a contract, but if you don't trust the other side, don't do the deal.

- There is an even larger group that talks the same way - if they are losing, they are being victimized, and if they are winning, the other side had it coming - but don't actually understand the cognitive dissonance in what they say. I would approach this group from an angle. Get them on board something else that you both can agree on, like corruption. Taking them head-on is a waste of time.

- There is a another large group that believes that if someone strong tells them that the other side is evil and tells them 1 + 27 = "B", they will believe them. Think of the sheep that never wonder why they never see John-Baa anymore and don't realize that their shepherd also has a subscription to Mutton Recipes Monthly. With this group, it doesn't help to tell them that Peter Pan actually killed off Lost Boys who grew up in the original book, which was why he had to venture away from Never Never Land occasionally to get new playmates. This group could be incredibly vicious towards their former leader if they think they are betrayed, but don't think the sheep will turn on the shepherd just because you show them pictures of the shepherd killing and eating John-Baa. You could show them pictures of the Disney animal handlers stampeding lemming off the cliff and they wouldn't recognize their own faces on the lemming or their leaders' faces on the animal handlers. They are not in the same reality as you are. This group needs strong leadership. Either you figure out how to gradually direct them into a new reality, or you ensure they are not in contact with the leaders leading them to slaughter.

None of this is to say that these groups don't exist on both sides. Beware the beam in your own eye, etc. It's all too easy for those of us in the center to say "A pox on both your houses", but we all live in the same building and I'm just trying to find if there is a way to get people working together on a patchwork quilt rather than burning the building down.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
