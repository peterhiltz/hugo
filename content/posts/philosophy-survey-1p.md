---
title: "2020 Philosophy Survey Part 1p: Analysis of Knowledge, Arguments for Theism and Morality"
date: 2022-02-27T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1p with the topics being Analysis of Knowledge, Arguments for Theism and Morality. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Analysis of knowledge
See [Analysis of knowledge](https://plato.stanford.edu/entries/knowledge-analysis/).
I "know" some things and I don't "know" other things. What does "know" mean? It is different than belief. Is it different than the "truth"? The SEP's example is Susan knows that Alyssa is a musician. That means she has knowledge of the proposition that Alyssa is a musician. "Justified True Belief" has three components: S knows that p if and only if (1) p is true, (2) S believes that p is true and (3) S is justified in believing that p is true.

Most contemporary philosophers don't believe that the three conditions for Justified True Belief are sufficient for "knowledge". One issue is what are called Gettier problems. Indian philosopher Dharmottara presented the following example: Imagine that we are seeking water on a hot day. We suddenly see water, or so we think. In fact, we are not seeing water but a mirage, but when we reach the spot, we are lucky and find water right there under a rock. Can we say that we had genuine knowledge of water? Condition 1 is met because there was actually water there. Condition 2 is met because we believe we see water. Condition 3 seems to be met because we have no reason to know we were seeing a mirage. Most contemporary philosophers would say that we don't have knowledge in this case, we were just lucky. There are all sorts of proposals for different additional conditions to add to the three Justified True Belief conditions. There are also many philosophers who say that you can't ever analyze "knowledge".

| Justified True Belief | Other Analysis | No Analysis | Agnostic |
|-----------------------|----------------|-------------|----------|
| 24%                   | 32%            | 31%         | 7%       |
N = 1025

### Arguments for theism
The question was which of the following classes of arguments are stronger for theism, regardless of whether the respondent is theist?

- [Cosmological](https://plato.stanford.edu/entries/cosmological-argument/) arguments for the existence of God try to argue against there being turtles all the way down. There are various versions, but they all require that the universe be finite, temporary and contingent and that necessary beings (God) are possible. Or as one person put it, its turtles all the way down until you get to the last turtle who can actually levitate.
- [Intelligent Design](https://plato.stanford.edu/entries/teleological-arguments/) basically argues that the universe is finely tuned for our existence. Or as one critic said, consider a conscious puddle of water arguing that the depression it was in was finely tuned just for the puddle's particular shape. See also [Fine tuning](https://plato.stanford.edu/entries/fine-tuning/).
- [Ontological](https://plato.stanford.edu/entries/ontological-arguments/) arguments are supposed to derive from reason alone. Consider Anselm of Canterbury's claim: "In his Proslogion, St. Anselm claims to derive the existence of God from the concept of a being than which no greater can be conceived. St. Anselm reasoned that, if such a being fails to exist, then a greater being—namely, a being than which no greater can be conceived, and which exists—can be conceived. But this would be absurd: nothing can be greater than a being than which no greater can be conceived. So a being than which no greater can be conceived—i.e., God—exists."
- [Pragmatic](https://plato.stanford.edu/entries/pragmatic-belief-god/) arguments are not arguments for the proposition that God exists; they are arguments that believing in God is rational.
- [Moral](https://plato.stanford.edu/entries/moral-arguments-god/) arguments for the existence of God try to go from some claim that objective morality exists to moral truths can best be explained by God's existence. In other words, we believe there are moral laws, and moral laws imply a lawmaker.

| [Cosmological](https://plato.stanford.edu/entries/cosmological-argument/) | [Design](https://plato.stanford.edu/entries/teleological-arguments/) | [Ontological](https://plato.stanford.edu/entries/ontological-arguments/) | [Pragmatic](https://plato.stanford.edu/entries/pragmatic-belief-god/) | [Moral](https://plato.stanford.edu/entries/moral-arguments-god/) | Combination | Alternative | Agnostic |
|--------------|--------|-------------|-----------|-------|-------------|-------------|----------|
| 21%          | 18%    | 9%          | 14%       | 9%    | 20%         | 8%          | 8%         |
N = 1025

### Morality
See also [The Definition of Morality](https://plato.stanford.edu/entries/morality-definition/)

- [Moral Non-naturalism](https://plato.stanford.edu/entries/moral-non-naturalism/) is the idea that moral philosophy is fundamentally autonomous from the natural sciences. In other words, it is a form of moral realism where moral properties exist and are not identical with or reducible to any natural property or properties in some interesting sense of ‘natural’.
- [Naturalist Realism](https://plato.stanford.edu/entries/naturalism-moral/) holds that there are objective, mind-independent moral facts and properties and that these are natural moral facts and properties. The initial problem is that it is difficult to define the naturalness of a moral fact or property.
- Constructivism in morality is the view that correctnes of moral claims is determined by contruction of human practical reason (the use of reason to decide how to act) , not discovered by theoretical reasoning (the use of reason to decide what to follow).
- Expressivism in morality takes the position that moral claims are not descriptive or fact stating - terms such as "wrong", "good", "just" etc do not refer to real in-the-world properties.. As such, the moral claim does not have any truth value.
- Error Theory in morality takes the position that most "moral claims" presuppose there are objective moral values, but there are no such thing. Thus all moral statements are either false or they are neither true nor false.

| Non-naturalism | naturalist realism | Constructivism | Expressivism | Error Theory | Combination | Agnostic |
|----------------|--------------------|----------------|--------------|--------------|-------------|----------|
| 27%            | 32%                | 21%            | 11%          | 5%           | 6%          | 6%         |
N = 1024


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
