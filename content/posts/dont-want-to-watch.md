---
title: "Suggestions? I don't want to watch assholes be assholes"
date: 2020-11-28T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I don't want to watch assholes be assholes. This seems to eliminate all news, any social media where echo chambers interact, all sitcoms (really, is there any sitcom which doesn't consist of making fun of people?), most dramas, most movies and most social media or forum where people are anonymous.

Any suggestions would be appreciated.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
