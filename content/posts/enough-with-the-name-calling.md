---
title: "Enough With the Name Calling"
date: 2020-11-06T14:18:19-04:00
categories: ['posts']
tags: ['life','politics']
showToc:  "true"
author: "Peter Hiltz"
---
All the jokes and insults about the other side just make the polarization worse. So stop it. We are wired differently, we think differently we respond to different motivations. For example, one study [Political Ideology and the Perceived Impact of Coronavirus Prevention Behaviors for the Self and Others](https://www.journals.uchicago.edu/doi/pdf/10.1086/711834) indicates that conservatives apparently are more likely to wear masks if you point out it keeps them safer and less likely to wear masks if you point out it keeps others safer. Liberals are the other way around. (I'm using "liberal" in the American political sense, not the European economic sense.) They are more likely to wear masks if you point out it keeps others safer and less likely to wear them if you talk about how it keeps them safe. This is discussed below under the "Personal Responsibility Theory."

I want to talk solely about theories of how liberals and conservatives think and value differently. If you are looking for something that talks about which side lies more and which side is more evil, this is not the post for you.

Lets ignore the outright lies that both sides get told by the people who want to influence them and that their echo chambers repeat. Marketing to conservatives uses a different set of words, emotions and facts than marketing to liberals. I would suggest that even if the words were the same, conservatives and liberals would walk away with different reactions. This is discussed below under the "Different Moral Foundations Theory".

### Different Theories
There are different theories about the what are the different motivations which drive this behaviors and, to a certain extent, those theories conflict. For context, think about Finland which tends to think about itself as very individualistic, but there is a fundamental core belief shared by everyone that education, healthcare and some amount of social safety is fundamental to a stable society.

#### Personal Responsibility Theory
[One theory](https://www.journals.uchicago.edu/doi/pdf/10.1086/711834) is that conservatives have a stronger belief in personal responsibility and liberals look at intergroup interdependence. According to that theory, US conservatives would disagree with the Finns about education, healthcare and any amount of social safety. US liberals would agree with the Finns. But if personal responsibility was the only driving force, one would expect conservatives to all be libertarians and not care about e.g. gay marriage.

#### Different Moral Foundations Theory
The [Moral Foundations Theory](https://fbaum.unc.edu/teaching/articles/JPSP-2009-Moral-Foundations.pdf) using data from cultures around the world, claims that liberals and conservatives look to different sets of virtues to determine their positions. Liberals believe that people should be left alone to pursue their own course of personal development. Personal development is not the same as personal responsibility. For example, liberals might believe that you should be free to be a gay minority street dancer as your personal development. Liberals might also believe that you should not be held personally responsible for the decision to be a street dancer if the system is such that cis ethnic majority street dancers can make a living but gay minority street dancers cannot.

Conservatives take a more jaundiced view of human nature and think that "people need the constraints of authority, institutions and traditions to live civilly with each other." Because of this, conservatives will have a strong adverse emotional reaction to anything which seems to threaten the social order. That focus on a belief that constraints are necessary for civil life also means that conservatives accept inequality as a price for that civil stability.

These are ideological and moral commitments, they pay us in compensation that is not counted in dollars. So all of the comments about "they are voting against their economic self interest" is completely missing the point.

Digging a bit deeper into the Moral Foundations paper, they claim that there are two foundational virtues that they label "individualizing" because they focus on the rights and welfare of individuals:

- Fairness, reciprocity and justice (Fairness or ethic of justice)
- caring, nurturing and protecting vulnerable individuals (Harm or ethic of care).

Importantly, the study claims that most cultures do not stop at the individualizing foundations, but also look at three additional societal virtues that have at least equal priority:

- Loyalty, patriotism and self sacrifice for the group (Ingroup/loyalty)
- Obedience and respect for authority (hierarchy) (Authority/Respect)
- Purity and sanctity rules which include marking off the group's cultural boundaries as well as selfishness suppression. (Purity/sanctity)

These last three they label binding foundations because they are "the source of the intuitions that make many conservative and religious moralities, with their emphasis on group-binding loyalty, duty, and self-control, so learnable and so compelling to so many people."

According to their data, liberals gave the highest priority to the first two foundational virtues and low priority to last three foundational virtues and conservatives focus on all five. This seemed consistent across their pool of subjects from the US, their pool from the UK and a combined pool which was mostly Canadian and Argentinian. This was also consistent across gender, age, household income and education level.

The data indicated that liberals refused to make trade-offs on the individualizing foundation virtues but were more willing to trade-off the binding foundation virtues. Conservatives were less willing to make trade-offs on the binding foundation virtues and, to the extent trade-offs needed to be made, would spread the trade-offs across all five virtues. They concluded from the trade-off analysis that liberals generally justify moral rules in terms on their consequences for individuals. Conservatives accept moral rules handed down from authorities or earlier generations even when the consequences of breaking the rules would, overall, be positive. By the way, the data showed that libertarians were more like liberals than conservatives (they actually had the lowest scores of any group with respect to the purity/sanctity virtue).

The moral foundations study also ran a separate analysis looking at church sermons, taking a sample of 69 Unitarian Universalist (felt to be the most liberal church) and 34 Southern Baptist (felt to be the most conservative) sermons delivered mostly between 2003 and 2006. As expected, the liberals used words associated with the Harm and Fairness virtues more frequently than conservatives and the conservatives used more authority and purity words. Interestingly enough, the liberals used more ingroup/loyalty words. after context analysis however, it was determined that the ingroup/loyalty words were used in the sense of rejecting the foundational concerns of ingroup loyalty and solidarity.

If you think about the phrase "identity politics" it makes sense looking at the binding foundations.

But what does this theory say about whether conservatives would agree with Finns about education, healthcare and some amount of social safety for a stable society?

With respect to education, I think you get into the conservative complaint not about education but about what is being taught at schools. Conservatives are for education if it reinforces the acceptance of society (e.g. the pledge of allegiance) and opposed to education if it teaches independent thinking which challenges those societal structures. The whole argument about Columbus' standing in US history is an example.

With respect to healthcare, I would expect it implies that conservatives would weigh the cost of healthcare (ethic of care) against the impact of that cost on the other four virtues.

All of the above is based on fair dealing liberals and conservatives. I am not talking about the leadership roles who I do view as acting in bad faith. A [twitter thread](https://threadreaderapp.com/thread/1324908316548493313.html) on this point from conservative journalist Matthew Sheffield makes interesting reading on this point.

### Conclusion
What all of this means is that if you want to reduce the polarization and you want to actually accomplish some of your policy goals, you need to draw from both sides. In order to do that, you need to stop characterizing the other side as stupid or evil. They actually have a different set of moral values that they are measuring against. And finally you need to restate and reword your policy goals in a way that make sense not just looking at your morality, but also looking at their morality. Slogans the energize your base, but threaten the moral values of the other side will fail if the purpose is to actually change structural behavior and they succeed if the intent is to only stir up the emotional conflict.

Right now both sides consider the other side to be full of hate because the other side denigrates their moral foundations. Conservatives think liberals denigrate the binding foundations. Liberals think conservatives denigrate individualizing foundations because they give equal or greater weight to binding foundations.

This discussion assumes good faith on both sides. We also need leaders (and media) who value everyone and don't see their paths to personal success and wealth as built on sowing divisiveness. Unfortunately it is fairly easy to point to leaders in politics and business who don't value any of the five foundational virtues. They are only interested in their own personal benefit and will market any or all of the five virtues to any target audience which will achieve those personal benefit goals. I don't have a way to operationalize greed out of the species.

By the way, both sides think that it is only the other side subject to propaganda. Beware of the beams in your own eyes (and the ones in mine, of course).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
