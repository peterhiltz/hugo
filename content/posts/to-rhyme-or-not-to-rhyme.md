---
title: "To Rhyme or Not to Rhyme"
date: 2020-09-11T14:18:19-04:00
categories: ['posts']
tags: ['songwriting']
showToc:  "true"
author: "Peter Hiltz"
---
One of the questions that comes up when writing a song _after_ you have settled on the idea and the feel is whether you are going to rhyme or not. We all know songs that rhyme, don't rhyme, have near rhymes etc. Each has their pros and cons and the strength of those pros and cons can differ depending on the music genre.
<!--more-->
Some of the pros for rhyming are it makes the lyrics easier to remember for both singer and audience and can boost the musical energy of a song or just make the musical phrases more interesting. It can also give the audience just a better feel for the rhythm and flow of a song.

On the other hand, simple rhyming limits the words you can use. Focusing too much on the rhyme can result in something that sounds too corny (except for children's songs). You may have to be willing to throw away a wonderful line because you just can't find a rhyming line can be a equal partner. If the audience can see the rhyme coming from a mile away, you should be making a conscious decision that you want them in on the participation of the joke or whatever and this probably works better with funny songs or children's songs.

Not rhyming gives you a bigger word palate to play with, but may require more attention to how the words create imagery and rhythm because you don't have that additional accent that comes from a rhyme.

Of course you can use both in the same song. Consider the traditional song "Greensleeves" where the verse is an ABAB rhyme scheme and the chorus doesn't rhyme.

> Alas, my love, you do me wrong
> To cast me off discourteously
> For I have loved you well and long
> Delighting in your company

> Greensleeves was all my joy
> Greensleeves was my delight
> Greensleeves was my heart of gold
> And who but my lady Greensleeves

## Kinds of Rhymes
All that being said, let's take a short look at rhyming.

Rhymes can impact the flow of a song based on placement. You can have an inner rhyme that can create a sudden emphasis on that phrase. Having an ABAB rhyme scheme will have a different feel for the song compared to an AABB rhyme scheme which will have a different feel than an ABCB rhyme scheme. Etc.

You can certainly have rhyming sequences where one word has more syllables and matches a multi-word phrase. That will also impact the flow of the song. Consider the first two lines in the "Battle of New Orleans":

> In 18 & 14 we took a little trip
> Along with Col. Jackson down the mighty Mississip'

Ed Sheeran's "The A Team" gets extra credit for two syllable rhymes or near rhymes in an AAAA form sometimes with one word and sometimes with two words:

> And they say
> She’s in the Class A Team
> She’s stuck in her daydream
> Been this way since eighteen
> But lately, her face seems
> Slowly sinking, wasting
>Crumbling like pastries

I think there are probably lots of multi-syllable multi-word rhyme examples in rap music, but (a) I know nothing about rap and (b) I want to stick closer to contemporary folk. [I know, I should get out of my comfort zone].

If you look at rhyming in books or on the internet, everyone seems to have slightly different definitions of what is a "rhyme". I'll talk about some of the different kinds below, but I think what is useful is that "rhymes" and similar sounds are actually broader than you might think. That does give you a bigger audio palate to play with.

One definition of rhymes distinguishes between "perfect rhymes" and "near rhymes". In that definition, a "perfect rhyme" has the same final accented vowel and consonant sound preceded by different consonant sounds. An example would be "meet" and "beet", "walking" and "talking".

According to this definition, it is not necessarily the final sound, it is the final *accented* sound. According to this technical definition, if the last syllable is not accented or if the last consonant is not accented, you can have "inner word rhymes". Some people (me and I daresay most people on the street) might not agree that these are perfect rhymes.

Think of the second verse in the traditional song "If I were a Blackbird":

> Or if I was a scholar & could handle a pen
> One secret love letter to my true love I'd send

To me this only sounds like a "perfect rhyme" if the singer swallows the "d" in send and draws out the vowel sound.

Regardless of how you feel about "inner word rhymes", the general definition concept leads to different types of "near rhymes" such as (a) the consonants rhyme but the vowel doesn't, (b) the vowels rhyme but the consonants don't, (c) there is no consonant sound at the end (e.g. "blue" and "shoe"), (d) same initial consonant and vowel but different ending consonants (e.g. "scenery" and "cedar") and, similarly, where the first syllable in each word sounds the same (e.g. "carrot" and "caring") and there are other types of near rhymes.

This is not the only definition of rhyming however. [Rhymer.com](https://wwwrhymer.com) doesn't follow the accent theory and lists six types of rhymes:

1. End Rhymes are any words with the same final vowel sound and following consonant sound.
2. Last-Syllable Rhymes have the same sounds following the last syllable boundary. The words don't have to have the same number of syllables.
3. Double Rhymes are words with the same vowel sound in the second to last syllable and all following sounds. Again, the words don't have to have the same number of syllables.
4. Triple Rhymes have the same vowel sound in the third to last syllable and all following sounds.
5. Beginning Rhymes have the same initial consonant sound and the same first vowel sound. If you are looking for alliteration (consonant sound repetition) or assonance (vowel sound repetition), these may be what you are looking for.
6. First-Syllable Rhymes have the same sounds preceding the first syllable break.

All these types of rhymes may impact how a singer actually sings the lines and how they focus on the vowel sounds or the consonant sounds. For the remainder of this note, I will just group most of the rhymer.com categories into the catch-all "near rhymes".

As an example of mixing perfect and near rhymes, consider Carrie Newcomer's song "The Gathering of Spirits".

> Let it go my love, my truest, let it sail on silver wings
> Life's a twinkling, that's for certain, but its such a fine thing
> There's a gathering of spirits, there's a festival of friends
> And we'll take up where we left off when we all meet again.

According to at least one person on the internet, "wings" and "thing" are perfect rhymes. Personally I think both those and "friends" and "again" as near rhymes.

Now consider the first verse in Dougie MacLean's "Caledonia".

> I don't know if you can see
> the changes that have come over me
> In these last few days
> I've been afraid
> that I might drift away
> So I've been telling old stories,
> singing songs
> that make me think about where I come from
> And that's the reason
> why I seem
> so far away today

see->me, (day)s->(afrae)->(awae) [he is Scottish], (son)gs->(from), (rea)son->(see)m

## What About Songs that don't seem to Rhyme?
The number of songs with rhymes or near rhymes seems to dwarf the collection of songs without, so let's take a closer look at some of those.

Leonard Cohen's "Suzanne" may not seem to be big on rhymes, but think about the "er" sound in the first two lines in each verse:

> Suzanne takes you down to her place by the river
> You can hear the boats go by, you can spend the night forever

> And Jesus was a sailor when he walked upon the water
> and he spent a long time watching from his lonely wooden tower

> Suzanne takes your hand and she leads you to the river
> She's wearing rags and feather from Salvation Army counters

Now consider these lines in the first verse and think about how he sings it:

> And just when you want to *tell her* that you have no love to *give
> her*
> She gets you on her wavelength and she lets the *river* answer

"Tell her" and "give her" bring back the "er" sound twice in the same line and then "river" comes back (two syllables in each group) and "river" gets stressed by the singer while the trailing word "answer" slides into the following line.

So maybe not rhymes as such, but certainly repeated sound that flow naturally with the melody. And that repeated sound and the cadence of the words together with the melody keeps the listener anticipating, just like they would for more explicit rhymes and creates almost implied rhymes.

For a different song, try "Fields of Gold" by Sting (although my favorite cover is by Eva Cassidy).

> You'll remember me when the west wind moves
> Upon the fields of barley
> You'll forget the sun in his jealous sky
> As we walk in fields of gold.

If you only think about the lyrics, it would seem as if there are no rhymes, but they have been effectively replaced by the repetition in each verse of the second and fourth lines of the first verse. This might be the same concept as rhymes, just on a larger scale.

At the same time, if you think about the melody and harmonies, you could argue that there is also a larger scale rhyming going on in the melody.

The quintessential song without rhymes might be Paul Simon's "America". I have to say as I look at the lyrics that I really don't find the implied rhymes that I feel like I can find in "Suzanne" or "Field of Dreams", but somehow that 3/4 time gives me a rhythmic swing feeling in the melody. Take a look at the first two verses and see what you think.

> Let us be lovers, we'll marry our fortunes together
> I've got some real estate here in my bag
> So we bought a pack of cigarettes, and Mrs. Wagner pies
> And we walked off to look for America

> Cathy, I said, as we boarded a Greyhound in Pittsburgh
> Michigan seems like a dream to me now
> It took me four days to hitchhike from Saginaw
>And I've come to look for America

## Conclusion

If you are writing an 86 verse murder ballad, I would suggest using rhymes, otherwise you are never going to remember the lyrics. For anything else, I think there is still an open question about how and what sound poem you are painting for the singer and the listener.  One thought might be to write two drafts of the song, first without rhymes trying to just develop your concepts and once with rhymes. That will give you something to compare how the different approaches give you a different feel to what you are saying and how an audience might react. If you are going to try that, I would suggest that you do it early in writing that particular song before you've become emotionally invested in your first approach. Of course, if a couplet just pops into existence in your mind with its own rhythm and rhyme, by all means, use that the framework to start with.

At the end of the day, the feel, emotion, energy and imagery of the song are what you should be focused on and rhyming or not rhyming and how you rhyme or don't rhyme tools you use to get that. Don't get artificial trying to force a "perfect rhyme" when a "near rhyme" can allow you to have more depth or imagery or detail.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
