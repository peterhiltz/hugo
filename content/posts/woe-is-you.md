---
title: "Woe is You"
date: 2020-09-19T14:18:19-04:00
categories: ['posts']
tags: ['politics', 'life']
showToc:  "true"
author: "Peter Hiltz"
---
Terry Pratchett once said "Satire is meant to ridicule power. If you are laughing at people who are hurting, it's not satire, it's bullying." I absolutely agree. At the same time, I want you to think about the phrase "people who are hurting". Let's assume that someone is hurting but it is not "justifiable" in your mind. Have you now decided that they aren't really hurting?

There seems to be a tendency by both parties to a disagreement to deny the reality of any hurt feelings by the other side and claim that they are the only side that has the right to feel hurt. I can already feel the knee jerk reaction as people read that previous sentence. I am not going to argue that both sides are equally justified. What I do want to suggest is that if you are actually trying to change the world to be a **better** place, you need to step outside your own feelings for a moment and try to understand is there any way that you can understand the claim by your opponent that they are hurting. If you can do that, then maybe you can see a way to reach past both your anger and their anger and find a partial solution. And then another partial solution. And then another.

This isn't limited to politics or relationships or parenting and whatever the equivalent is for children ("childing" means bearing children, so I can't use that).

Of course, if the person on the other side is only claiming hurt and not actually feeling it, then this doesn't apply and you need other tools to deal with a dishonest party. In that case you might want to look at their associates and see if any of them actually feel hurt or are claiming to feel hurt because their leader told them that they are hurt. In that case, approach the problem from the side.

All I'm really suggesting is that you don't get so wrapped up in your own hurt that you deny the fact that the other person may actually hurt as well. It doesn't matter whether it is justifiable that they hurt if their emotional pain is going to get in the way of solving the problem and you don't deal with that fact.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
