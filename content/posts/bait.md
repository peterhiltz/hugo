---
title: "Rise to the Bait or Not"
date: 2024-01-06T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I was at a Christmas party a couple of weeks ago. A guy at the party was wearing a shirt clearly intended to trigger liberals. The wording on his shirt read, in part "I am a Christian but I am a born warrior. I will fight for ....xyz things." The language was clearly written so that if you objected, you would be played as opposing motherhood, apple pie etc, while at the same time demonstrating that he had no intention of following the Sermon on the Mount.

Younger me would have risen to the bait and tried to engage. Older me knew that engagement was futile and was irritated that the guy was being unnecessarily provocative at a Christmas party.

Last night I was at a post-New Years party and someone was trying to engage an academic librarian on the subject of drag queen storytime at public libraries. The academic librarian sidestepped the discussion by pointing out the difference between acadmic libraries and public libraries. I decided, for the sake of everyone else at the party, to go get another glass of wine.

In these two instances, the "conversation" would have been with someone on the right, but I can also find myself objecting to statements from someone on the left.

The question I am raising for myself is when to engage. If no one in the conversation is actually listening, then it is obviously a waste of time and blood pressure. On the other hand, there might be a non-polarized persont listening in. In that case, maybe, just maybe, you could get them to stop and think.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
