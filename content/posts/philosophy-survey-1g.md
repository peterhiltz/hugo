---
title: "2020 Philosophy Survey Part 1g: Truth, The Experience Machine and Abstract Objects"
date: 2022-02-19T16:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1g with the topics being Truth, The Experience Machine and Abstract Objects. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Truth
See [Truth](https://plato.stanford.edu/entries/truth/).
- The [Correspondence](https://plato.stanford.edu/entries/truth-correspondence/) Theory is that what we believe or say is true if it corresponds to the way things actually are - the facts.
- The [Deflationary](https://plato.stanford.edu/entries/truth-deflationary/) Theory has a few different variations, but they all insist that there is no "property" of "truth" (or at least no substantial "property" of "truth").
- The [Epistemic or Pragmatic](https://plato.stanford.edu/entries/truth-pragmatic/) theories look to how truth is used in epistemic contexts where people make assertions, conduct inquiries, solve problems, and act on their beliefs rather than as some abstract relations between propositions and states of affairs.

| [Correspondence](https://plato.stanford.edu/entries/truth-correspondence/) | [Deflationary](https://plato.stanford.edu/entries/truth-deflationary/) | Epistemic | Agnostic |
See also [Truth-Axiomatic](https://plato.stanford.edu/entries/truth-axiomatic/).
|----------------|--------------|-----------|----------|
| 51%            | 25%          | 10%       | 7%       |
N = 1643

### Experience machine
See [Experience machine](https://philosophicaldisquisitions.blogspot.com/2017/01/understanding-experience-machine.html)
The experience machine is a thought experiment posed by Robert Nozick: "Imagine a machine that could give you any experience (or sequence of experiences) you might desire. When connected to this experience machine, you can have the experience of writing a great poem or bring about world peace or loving someone and being loved in return. You can experience the felt pleasures of these things, how they “feel from the inside”. You can program your experiences for…the rest of your life. If your imagination is impoverished, you can use the library of suggestions extracted from biographies and enhanced by novelists and psychologists. You can live your fondest dreams “from the inside”. Would you choose to do this for the rest of your life?…Upon entering you will not remember having done this; so no pleasures will get ruined by realizing they are machine-produced.”

The philosophical argument here is that if you truly believe in hedonism as the thing that makes life meaningful, you should agree to get plugged into the experience machine. The problem, of course, is that most beliefs are seldom all or nothing.

| Yes | No  |
|-----|-----|
| 13% | 77% |
N = 1642

### Abstract objects
See [Abstract objects](https://plato.stanford.edu/entries/abstract-objects/)
The question is whether you believe that abstract objects are real. For example, consider a red apple. Is "redness" something real that exists separately from the apple or is it a property of this particular apple. Platonic realists think that redness is an abstract universal object that exists separately from all the particular objects that happens to appear red. Aristolean realists think that redness is a real object, but that it is dependent on the particular object that is "red". The survey seems to have put Aristotelian realists into the "Alternative View" category.

Nominalism takes the position that there are no universal abstract objects like "redness". Nominalists are trying to avoid questions such as where does "readness" exist if it is a "real" thing. As a result, nominalists treat "redness" as a property of an actual physical object.

For you lawyers out there, this is a different question than whether corporations (a legal fiction) are real.

| [Platonism](https://plato.stanford.edu/entries/platonism/) | [Nominalism](https://plato.stanford.edu/entries/nominalism-metaphysics/) | Alternative View | Agnostic |
|------------------------------------------------------------|--------------------------------------------------------------------------|------------------|----------|
| 38%                                                        | 42%                                                                      |     6%             |  8%        |
N = 1639


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
