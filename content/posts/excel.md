---
title: "Use the Right Tool For the Job"
date: 2020-10-05T14:18:19-04:00
categories: ['posts']
tags: ['life','politics']
showToc:  "true"
author: "Peter Hiltz"
---
When all you know is a hammer, everything looks like a nail. In the most recent case, the UK managed to drop almost 16,000 COVID-19 cases from their result compilation because they were using Excel in the data gathering process. Excel has a limit on the number of rows or columns and apparently the amount of data exceeded one or the other of those limits. A [BBC Report](https://www.bbc.com/news/uk-54422505) claims that the row limit was breached because someone chose to use a very old file format. Other people are claiming the column limit was breached because someone confused rows and columns.  Seriously people, Excel was developed for accountants to do math, not collect data. That is what databases are for.

It is my understanding that the data has been recreated, but numbers are being adjusted upwards by a considerable amount. In yet another example of people not owning up to failures, the UK government originally called this an "Excel glitch", ignoring the fact that any competent IT person would have been able to say that Excel had these limits and they were documented limits, the old file format had these limits and Excel was the wrong tool anyway. This was not a software "glitch", this was an error between chair and keyboard. In other words - user error.

By the way, for all two of you readers who think that private enterprise is automatically better than the government, this system was not run by the National Health Service, it was run by a group of private companies that went through no tendering process to get the bid. Apparently they've already been paid hundreds of millions of pounds to type everything into an Excel spreadsheet. In other words, corruption - which is something both right and left should be fighting if they cared about their countries instead of their pockets.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
