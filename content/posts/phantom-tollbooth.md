---
title: "Phantom Tollbooth"
date: 2021-03-10T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Norton Juster, the author of "The Phantom Tollbooth" died yesterday at age 91. In my opinion, it is a book that everyone should read (or have read to them - you can choose to take this as snark or not as you see fit). It is possible to appreciate it more as an adult than as a child. Below are some of my favorite quotes.

“You must never feel badly about making mistakes ... as long as you take the trouble to learn from them. For you often learn more by being wrong for the right reasons than you do by being right for the wrong reasons.”

“The only thing you can do easily is be wrong, and that's hardly worth the effort.”

“Everybody is so terribly sensitive about the things they know best.”

“if something is there, you can only see it with your eyes open, but if it isn't there, you can see it just as well with your eyes closed. That's why imaginary things are often easier to see than real ones.”

Now off to search for the Kingdom of Wisdom. As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
