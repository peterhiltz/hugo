---
title: "Procrastination"
date: 2021-06-27T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I recently finished a 260 page analysis of some open source software projects at the request of someone in Switzerland (no one getting paid, this is open source software). That took about a month. It was followed by a week helping a guy in Italy resolve the performance problems of the software he maintains which had come dead last in the analysis. Then four days adding an feature to some software I have found myself as the maintainer at the request of someone else on the intertubes. Finally one of my nephews asked a simple question about the family genealogy and I got sucked into two days of internet searches trying to fill holes in the family tree. It is amazing how many family trees are posted on the internet that have data like - X had three children five years after she died and Y had two children before she was born.

What does this have to do with procrastination? One of the things I have discovered about myself post retirement is that without the discipline imposed by work deadlines, I'm either all consumed with something (the first paragraph stuff) or I have many things that I want to work on but don't do any. It reminds me of the AA Milne Poem "The Old Sailor":

>There was once an old sailor my grandfather knew
Who had so many things which he wanted to do
That, whenever he thought it was time to begin,
He couldn't because of the state he was in.

> He was shipwrecked, and lived on an island for weeks,
And he wanted a hat, and he wanted some breeks;
And he wanted some nets, or a line and some hooks
For the turtles and things which you read of in books.

>And, thinking of this, he remembered a thing
Which he wanted (for water) and that was a spring;

>And he thought that to talk to he'd look for, and keep
(If he found it) a goat, or some chickens and sheep.

>Then, because of the weather, he wanted a hut
With a door (to come in by) which opened and shut
(With a jerk, which was useful if snakes were about),
And a very strong lock to keep savages out.

>He began on the fish-hooks and when he'd begun
He decided he couldn't because of the sun.

>So he knew what he ought to begin with, and that
Was to find, or to make, a large sun-stopping hat.
He was making the hat with some leaves from a tree,
When he thought, " I'm as hot as a body can be,
And I've nothing to take for my terrible thirst;
So I'll look for a spring, and I'll look for it first . "

>Then he thought as he started, " Oh, dear and oh, dear!
I'll be lonely to-morrow with nobody here! "
So he made in his note-book a couple of notes:
" I must first find some chickens "
and " No, I mean goats . "

>He had just seen a goat (which he knew by the shape)
When he thought, " But I must have a boat for escape.
But a boat means a sail, which means needles and thread;
So I'd better sit down and make needles instead. "

>He began on a needle, but thought as he worked,
That, if this was an island where savages lurked,
Sitting safe in his hut he'd have nothing to fear,
Whereas now they might suddenly breathe in his ear!

>So he thought of his hut ... and he thought of his boat,
And his hat and his breeks, and his chickens and goat,
And the hooks (for his food) and the spring (for his thirst) ...
But he never could think which he ought to do first.

>And so in the end he did nothing at all,
But basked on the shingle wrapped up in a shawl.
And I think it was dreadful the way he behaved —
He did nothing but basking until he was saved!

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
