---
title: "Britain takes lead in shellfish conservation!"
date: 2021-02-03T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Britain has taken the lead in shellfish conservation and restoration! By accident.

EU health and safety rules have long required shellfish to be purified and processed and any shellfish brought into the EU must have been purified and processed before entry. While Britain has a shellfish industry, all the purification and processing has been done on the continent. Can you see where this is going? Yeah. Until Britain builds a shellfish purification and processing plant, the entire British shellfish industry is practically shutdown because almost all their sales were to the continent. The British themselves apparently are not big shellfish eaters.

The industry admits that this is not a new EU policy and blames the British government for not safeguarding the industry. The British government, which had told the industry that they would be able to resume live shellfish exports to the EU in late April are now imitating turtles.

Net result, there may be some period of shellfish stock restoration until a processing plant is somehow built. Temporarily happy British shellfish. Not something I would have expected from the Tories.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
