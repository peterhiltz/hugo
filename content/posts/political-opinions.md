---
title: "Joscha Bach on Political Opinions"
date: 2022-02-12T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I like this comment from Joscha Bach, a German cognitive scientist:

"Political opinions are unlike other kinds of opinions: they appear to be without alternative to the one who has them. 'This protest is invalid, because the people who participate in it are wrong.' = 'These people have political opinions that are different from mine.'"

I sometimes need to remind myself that other people's ways of looking at things may or may not be better than how I look at things. If I'm going to learn anything, I at least need to try to put myself into their shoes, look out through their eyes and then compare that against my morals, ethics and way of thinking. At the end of the learning process, I can still disagree, but I need to have tried to learn something first.

This also keeps me out of fruitless internet arguments. See [relevant xkcd](https://xkcd.com/386/). By the time I've spent two weeks doing my own research trying to understand someone's position, my initial emotional outrage has diminished, time has passed and they wouldn't have listened to me anyway. So win-win for everyone. And I now, in the words of my sister-in-law, have tucked away in the dusty filing cabinents in my brain "more useless crap".

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
