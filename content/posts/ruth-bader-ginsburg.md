---
title: "RIP Ruth Bader Ginsburg"
date: 2020-09-18T20:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
RIP Justice Ginsburg. From a pure power politics perspective I can understand why the Republicans said that they will bring the next nominee to a vote before the US election. I can't make people care about other people. But don't for a second believe that there are any morals or ethics left in the bloody carcass of a once respectable party.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
