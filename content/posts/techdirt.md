---
title: "Why Techdirt Is Now A Democracy Blog (Whether We Like It Or Not) "
date: 2025-03-05T10:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
[Techdirt just published a blog post entitiled Why Techdirt Is Now A Democracy Blog - Whether We Like It Or Not](https://www.techdirt.com/2025/03/04/why-techdirt-is-now-a-democracy-blog-whether-we-like-it-or-not/). Techdirt has been a blog for 27 years dealing with the intersection of technology and policy, often targeting copyright, patents, censorship and privacy issues. They are now asking the question "But what happens when the fundamental systems that make all of those conversations possible start breaking down? When the people dismantling those systems aren’t even pretending to replace them with something better?" 

I'm just going to provide selected quotes from their post. Go read the whole thing.

> "We’ve seen this all play out before. When someone talks about “free speech” while  actively working to control speech, that’s not a contradiction or a mistake — it’s the point. It’s about consolidating power while wrapping it in the language of freedom as a shield to fool the gullible and the lazy."

> "One of the craziest bits about covering the systematic dismantling of democracy is this: the people doing the dismantling frequently tell you exactly what they’re going to do. They’re almost proud of it. They just wrap it in language that makes it sound like the opposite. (Remember when Musk said he was buying Twitter to protect free speech? And then banned journalists and sued researchers for calling out his nonsense? Same playbook.)"

> "But what’s happening in the US right now is some sort of weird hybrid of the kind of power grabs we’ve seen in the tech industry, combined with a more traditional collapse of democratic institutions."

> "The destruction is far more systematic and dangerous than many seem to realize. Even Steven Levitsky, the author of How Democracies Die — who has literally written the book on how democracies collapse — admits the speed and scope of America’s institutional collapse has exceeded his worst predictions. And his analysis points to something we’ve been specifically warning about: the unprecedented concentration of political, economic, and technological power in the hands of Elon Musk and his circle of loyal hatchet men as they dismantle democratic guardrails."

> "If you do not recognize that mass destruction of fundamental concepts of democracy and the US Constitution happening right now, you are either willfully ignorant or just plain stupid. I can’t put it any clearer than that."

> "This isn’t about politics — it’s about the systematic dismantling of the very infrastructure that made American innovation possible. For those in the tech industry who supported this administration thinking it would mean less regulation or more “business friendly” policies: you’ve catastrophically misread the situation (which many people tried to warn you about). While overregulation (which, let’s face it, we didn’t really have) can be bad, it’s nothing compared to the destruction of the stable institutional framework that allowed American innovation to thrive in the first place."

> "We’re going to keep covering this story because, frankly, it’s the only story that matters right now, and one that not everyone manages to see clearly. The political press may not understand what’s happening (or may be too afraid to say it out loud), but those of us who’ve spent decades studying how technology and power interact? We see it and we can’t look away."

> "So, here’s the bottom line: when WaPo’s opinion pages are being gutted and tech CEOs are seeking pre-approval from authoritarians, the line between “tech coverage” and “saving democracy” has basically disappeared. It’s all the same thing."

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
