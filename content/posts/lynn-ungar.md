---
title: "Reposting a Lynn Ungar Post"
date: 2022-05-04T14:18:19-04:00
categories: ['posts']
tags: ['religion','politics','rant']
showToc:  "true"
author: "Peter Hiltz"
---
After the leaked draft of the US Supreme Court decision yesterday, I decided to share this post from Lynn Ungar, a UU minister.

Source: [https://www.facebook.com/shirley.worth/posts/10159952661629483](https://www.facebook.com/shirley.worth/posts/10159952661629483)

> OK, this is going to take a minute, but hear me out. I am pro-religion. I've been an ordained minister for 30 years, and not the kind of ordination you buy online. I believe in people coming together in community, being accountable to one another, and to something that is larger than our individual whiny selves. I appreciate the fact that religions throughout time and place have insisted that we could cultivate practices that lift us out of our inherent grabbiness into a loyalty to something grander, more caring, more responsible. I appreciate that religion somehow manages to weave together the experience of boundlessness and the practice of self-control.

> But when your religion devolves into controlling *other* people, when your vision of holy work is telling other people what they can and can't believe, who they can and can't love, what they can and can't do with their own body, what they are and aren't allowed to learn...then you have traded righteousness for self-righteousness. You have manufactured a God of judgement in your own small-minded image, and set that idol on high. And you've done it in service of your own anxious need to feel safe by trying to shove everyone into the limitations of the box you have chosen.

> And I would just like to declare that the Rev. Dr. Lynn Ungar says Fuck. That. Noise. Some of us are over here having religion that declares that the world and all its possibilities are bigger than you have ever dreamed of in your philosophies; that love is a power that moves like a river in flood; that you--yes you!--are precious and holy and unique, but not any more so than your refugee neighbor or your queer neighbor or any other kind of neighbor. We don't need to be shoved inside your box. We will not be saved by being shoved inside your box. We will be only be saved by fierce allegiance to a love that is bigger and wilder and more boldly creative than you have dared to imagine. It is time to grow your imagination.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
