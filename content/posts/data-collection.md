---
title: "What Data do Companies Collect on You?"
date: 2020-10-17T14:18:19-04:00
categories: ['posts']
tags: ['politics','privacy','life']
showToc:  "true"
author: "Peter Hiltz"
---
One of the many cultural differences between the US and Europe seems to be that Americans don't trust the government collecting data about themselves but seem to find it acceptable that companies collect data and Europe seems to be the reverse. The American idea becomes an exercise in futility as soon as you understand that the government buys data from companies if it can't collect the data directly. But what about other geographies and what data is getting collected about you anyway?

If you look at the [privacy policy of one of India's largest telecoms'](https://www.airtel.in/privacy-policy/) you see the following reasons given for collecting personal information:

"*airtel does collect your personal information for a variety of regulatory and business purposes. These include, but are not limited to:*"

- Verify your identity;
- Complete transactions effectively and bill for products and services;
- Respond to your request for service or assistance;
- Perform market analysis, market research, business and operational analysis;
- Provide, maintain and improve our products and services;
- Anticipate and resolve issues and concerns with our products and services;
- Promote and market our products and services which we consider may be of interest to you and may benefit you; and
- Ensure adherence to legal and regulatory requirements for prevention and detection of frauds and crimes.

Basically the fourth through seventh boil down to "anything we can think of today **or** tomorrow".

"*airtel and its authorized third parties will collect information pertaining to your identity, demographics, and related evidentiary documentation. For the purposes of this document, a ‘Third Party’ is a service provider who associates with airtel and is involved in handling, managing, storing, processing, protecting and transmitting information of airtel.*"

"*This definition also includes all sub-contractors, consultants and/or representatives of the Third party.*" [Basically anyone airtel enters into a contract with for any reason whatsoever, including selling your data to them or exchanging your data for data on someone else]

"*We may also collect your personal information when you use our services or websites or otherwise interact with us during the course of our relationship.*" [In other words, any way we can, we will collect data on you.]

"*Personal information collected and held by us may include but not limited to*:
- your name,
- father’s name,
- mother’s name,
- spouse’s name,
- date of birth,
- current and previous addresses,
- telephone number,
- mobile phone number,
- email address,
- occupation and information contained in the documents used as proof of identity and proof of address."

"*airtel and its authorized third parties may collect, store, process following types of Sensitive Personal Information such as*:

- Genetic Data,
- Biometric Data,
- Racial or Ethnic Origin,
- Political opinion,
- Religious & Philosophical belief,
- Trade union membership,
- Data concerning Health,
- Data concerning natural personal's sex life or sexual orientation,
- password,
- financial information (details of Bank account, credit card, debit card, or other payment instrument details),
- physiological information for providing our products, services and for use of our website."

[All of the above are considered [special categories of sensitive personal data and get specific protection](https://ec.europa.eu/info/law/law-topic/data-protection/reform/rights-citizens/how-my-personal-data-protected/how-data-my-religious-beliefs-sexual-orientation-health-political-views-protected_en)  under the [GDPR](https://gdpr-info.eu/), the EU's General Data Protection Regulation.]

"*We may also hold information related to your utilization of our services which may include*:
- your call details,
- your browsing history on our website,
- location details and
- additional information provided by you while using our services."

"*We may keep a log of the activities performed by you on our network and websites by using various internet techniques such as web cookies, web beacons, server log files, etc. *for analytical purposes and for analysis of the amiability of various features on our site*. This information may be used to provide you with a better experience at our portal along with evidentiary purposes.*"

"*In case you do not provide your information or consent for usage of personal information or later on withdraw your consent for usage of the personal information so collected, airtel reserves the right to not provide the services or to withdraw the services for which the said information was sought.*"

So, basically, we will collect any data we can get our hands on and can sell it to third parties and if you want our telcom service, you need to consent.

I've also been told that the largest and second largest UK telcoms also collect all this data and justify it on the grounds that it helps tailor future business offerings. So basically they claim they have no limitations under the GDPR. When it was suggest that a telcom shouldn't need someone's occupation, political opinion or health data, a telcom spokesperson that that was a failure of imagination.

Now, if I was the company's lawyer, I might write something similar on the grounds that the telcom might unintentionally come into possession of the data and this would cover unintentional collection. And I've dealt with enough sales and management people to know that as soon as it is drafted, it immediately becomes proposed as a new business opportunity.

If you want to see a summary of different websites terms of service, what they collect and a security grade, see [tosdr.org](https://tosdr.org/).

# Fingerprinting
If you want to see some of the data that gets collected as soon as you browse a website, click [deviceinfo.me](https://www.deviceinfo.me/). Companies use this to "fingerprint" your computer so that they can combine information from your activities on different sites and compile a picture of you to sell. If they can do this, then even using a VPN doesn't disguise "you". I checked [amiunique.org/fp](https://amiunique.org/fp) and it agreed that on the day I checked, I was unique among the 2,748,768 fingerprints in their database (which is a small subset of what commercial databases are collecting).

Does this blog keep any of your data? No, it is deliberately set up so I don't even know if anyone reads it.

## Personal Data Collection in International Tax
The GDPR question actually has come up in international corporate taxation as countries try to allocate taxable income from "digital businesses" based on the IP location of the consumer. IP location would be personal information that a company should not collect and keep unless it has a valid business purpose. Of course, if the tax law requires companies to collect that information in order to allocate taxable income between countries, that is a valid business purpose.

But it isn't just a requirement to keep the data until the tax return is done. Companies will also need to keep it until the time for any audit has run, so probably 5-10 years. It then becomes a tug of war between the tax department which says keeping the data is necessary, the legal department that wants to destroy it as soon as possible, the IT department which then tries to make the tax department responsible for keeping the data safe, the tax department again which points out that it has no expertise in securing personal data from the rest of the company and the sales department that once it discovers the data exists, wants to bypass internal corporate firewalls and sell it.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
