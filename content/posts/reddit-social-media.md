---
title: "Reddit and Social Media Models"
date: 2023-06-02T14:18:19-04:00
categories: ['posts']
tags: ['internet']
showToc:  "true"
author: "Peter Hiltz"
---
I want to talk briefly about Reddit and social media business models. Like most social media platforms, Reddit's business model is selling (a) advertising and (b) user data. Social media companies (Reddit, Twitter, Facebook et al) have discovered that enabling outrage increasing engagement and, therefore sales and profit.

You can pay for Reddit Premium for $6/month to avoid ads (and get more filtering functionality), but your user data will still be sold and you haven't reduced the companies' incentive to increase outrage

You can limit your viewing of outrage by avoiding anything to do with news, politics, religion or social values (they all seem to have high percentage of people trying to increase the level of animosity and hatred in the world). Reddit has over 138,000 sub-communities dealing with different topics. My collection seems to focus on acoustic music, really specific niche computer programming topics and cat pictures. My wife focuses on pottery and horses.

I wouldn't mind so much about ads if they didn't deliberately interrupt the flow of an article. As a result I run ad-blockers and I'm one of those people who, when confronted by a website which demands I turn off ad-blockers or pay a subscription, just won't view the site. I do have paid subscriptions to a few newspapers and magazines, but I'm certainly not going to subscribe to everything I run across.

With respect to selling your data, I already have credit reporting companies claiming that I lived places I definitely did not. Dealing with unknown companies selling my on-line life is even more difficult - it isn't just a matter of blocking cookies and using a VPN. I really don't need even more companies creating fictional versions of myself.

Even more unfortunate is the realization that if I feed the enabling outrage business model, I increase the amount of hate and harm to people in the world.

Anyone have ideas?

**Update** : YouTube just changed their policy and will now [allow false claims about past US presidential elections](https://apnews.com/article/youtube-election-misinformation-policy-42a6c1b7623c485dbc04eb76ad443247). YouTube said in a blog post that the updated policy was an attempt to protect the ability to “openly debate political ideas, even those that are controversial or based on disproven assumptions.” “In the current environment, we find that while removing this content does curb some misinformation, it could also have the unintended effect of curtailing political speech without meaningfully reducing the risk of violence or other real-world harm,” the blog post said.

* Appendix - Reddit.com

For those who don't know, Reddit.com is a big social media internet site purchased by Condé Nast Publications in 2006 and now trying to go public. As of February 2023, it is the 10th most visited website in the world and between 42-49% of its user base comes from the US. It has over 138,000 active sub-communites that are topic based.

Subreddits have moderators who are all **volunteers**. Free labor for Condé Nast. As you might expect, this leads to some being laissez-faire, some trying to keep the signal to noise ratio at acceptable levels and others who are petty tyrants. Hey, humans.

There are (at the moment) several third party applications which allow you to browse reddit.com without the ads. Reddit just decided to start charging for access - one third party application developer just calculated that the new fees would cost $20 million/year, so will probably shutdown.


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
