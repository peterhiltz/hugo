---
title: "Giving Compliments"
date: 2021-07-23T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
The BBC has an [article](https://www.bbc.com/worklife/article/20210722-why-we-dont-dole-out-many-compliments-but-should) on why people should give compliments to others. There are [studies](https://www.tandfonline.com/doi/abs/10.1080/01973533.2010.497456) that indicate that giving compliments results in a sense of reciprocity. Other [studies](https://doi.org/10.1177/0146167220949003) indicate that people significantly underestimate how happy people would be to receive a compliment.

All that being said, as an older male, there are a few things I need to remind myself about:

- Compliment something someone has done, not something they genetically have.
- Make the compliment specific not general.
- Make the compliment something that shows genuine respect for the other person.
- In the words of someone else - tap into your inner Mr. Rogers to avoid coming off creepy.
- Maybe practice to ensure you are giving compliments to members of your own gender as well as members of the opposite gender.
- Look for something to compliment when interacting with inexperienced people, but make sure it is not going to come across as condescending or not genuine. That means that it probably needs to be mixed with constructive criticism. It has to be sincere.
- Compliments can help relationships with "enemies". You may still be on opposite sides, but they may have more respect for you and that will, at some point, help in the future.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
