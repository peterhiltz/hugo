---
title: "What is Respect?"
date: 2020-09-14T06:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---

I was reading an interchange on social media the other day about a shoplifting incident at a Lowe’s. In the discussion was a short interchange that got me thinking. One commentor said don’t bother reporting it to the police because they won’t do anything. Another commentator responded this is what happens when people don’t respect the police.

My first thought was "What does respecting police have to do with shoplifting". Then I wondered whether the second commentator was saying you don’t respect the police the police will not respond i.e. do their job. That got me to thinking what actually is "respect" and I started thinking about how the definition of the word has changed for me  personally as I’ve grown up.

As an adult, when I meet someone for the first time I have certain default assumptions. I assume that they treat other people fairly, they know what they’re doing and they take pride in their work and I will do the same vis-a-vis them. I view that as giving them the "respect" that they were owed. It doesn’t matter what their job is, or their economic or social status. I will assume, absent any other evidence that a hairdresser, carpenter, doctor, lawyer, engineer, whatever knows their job and is trying to do a good job. With that in mind, I will treat them fairly, listen to what they have to say and let them do their jobs.

In a minority of cases, my default assumptions are wrong, and I will “lose respect” for that person either concerning whether they treat people fairly or whether they know what they are doing  or take pride in their work. In that context, if I disparage them, that would be "disrespecting" them.

If I think really highly of their work or their intelligence I will give them additional respect (the difference between a competent journeyman and a mastercraftsman).

When I was growing up, I was taught that you referred to adults by their prefix and last name, e.g. Mr. Foster, Mrs. Jones, Miss Smith and you obeyed directions given to you by those adults. I was also taught to follow the rules unless there is a valid reason not to follow the rule. Rules were the expression of an adult and so fit in with the teaching about following the directions of an adult. The unstated assumption is that the adult probably knows more than the child, so just go with that and don’t argue. (Beginning in junior high I started to argue, and have been questioning assumptions ever since).

I know other people who won’t follow rules unless they are given a valid rationale to follow them. In fact, demanding obedience to a rule without an apparently valid rationale will immediately cause an adverse reaction. Not only will the rule not be followed, but the person who is the target of the rule will lose whatever respect they had for the person demanding obedience.  This plays into what I think of as an authority view of respect.

To me, persons with an authoritarian personality viewpoint have never left that small child/adult perspective control perspective even in adult/adult circumstances. So, if they are in a position of authority and you do not follow their directions, then you were not acting appropriately subserviently (child obeying their parents) and are disrespecting their authority. If the person is not in a position of authority, but they have an authoritarian personality viewpoint, they will still be upset over someone not showing subservience to authority. This is not because you were disrespecting their authority, but because you were disrespecting society's authority and their belief in how the world should be structured is based on that.

As noted in an earlier post, as far as I can tell, arguing with someone's belief is a waste of time, energy, emotion. On the other hand, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
