---
title: "Gell-Mann Amnesia"
date: 2023-07-02T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
How often do you read a story in the media in your area of expertise that is misleading or even completely wrong, then turn to the next story in a different subject and assume that its correct. This is called Gell-Mann Amnesia. Michael Crichton coined the term. In his words:

> Briefly stated, the Gell-Mann Amnesia effect is as follows. You open the newspaper to an article on some subject you know well. In Murray [Gell-Mann]’s case, physics. In mine, show business. You read the article and see the journalist has absolutely no understanding of either the facts or the issues. Often, the article is so wrong it actually presents the story backward—reversing cause and effect. I call these the “wet streets cause rain” stories. Paper’s full of them.

> In any case, you read with exasperation or amusement the multiple errors in a story, and then turn the page to national or international affairs, and read as if the rest of the newspaper was somehow more accurate about Palestine than the baloney you just read. You turn the page, and forget what you know.'

This is also sometimes called Knoll's Law of Media Accuracy:

> everything you read in the newspapers is absolutely true, except for the rare story of which you happen to have firsthand knowledge.

Now consider a different situation. You read a story in your area of expertise which has been dumbed down for the audience (what I occasionally call 'lies for children'). You catch yourself thinking - 'That is not how I would have explained it'. At this point you need to think about the target audience and understand the difference between making a necessary correction at the level of the target audience's comprehension versus a technical correction as if you were talking with other technical experts in the field..  In the words of Angela Collier (a theorectical physicist):

> The Igon Value Problem: When an expert sucks all the energy out of the room to interrupt a group of 3rd graders learning about mechanical energy to write the piston differential equations on the board. Its not helpful. You look like a jerk.

Now flip these concepts around and approach it looking at the presenter or journalist. Some presenters/writers/journalists/speakers take advantage of Gell-Man Amnesia because it allows them to imply to the audience that they are providing the truth.

In the positive scenario, 'Gell-Mann Recollection' is when you are not a technical expert in the subject (but might be a technical expert in some other area) but you assume you a presenting an elementary explantion exactly. Feel free to comment if you think I fall into this category.

The complement (what Angela Collier calls a Mann-Gell recollection) is when you are very educated about a thing and you knowingly present incorrect information for nefarious purposes (money, power, fame, shilling for someone else or [insert other greed desire here]). I would generalize this to any situation where you knowingly present incorrect information for nefarious purposes regardless of whether you are a technical expert in the field. If I believed in hell, there would be a special place for those who are intellectually dishonest.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
