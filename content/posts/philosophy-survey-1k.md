---
title: "2020 Philosophy Survey Part 1k: Knowledge Claims, Vagueness and Moral Motivation"
date: 2022-02-23T15:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1k with the topics being "Knowledge Claims", "Vagueness" and "Moral Motivation". I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.


### Knowledge Claims
See [Knowledge Claims](https://plato.stanford.edu/entries/contextualism-epistemology/)
See also [Analysis of Knowledge](https://plato.stanford.edu/entries/knowledge-analysis/)

Contextualist views hold that claims about knowledge only have meaning relative to a specified context in which the claims are made.

Relativist views maintain that at least some class of things have the properties they have only relative to a given framework of assessment (e.g. local cultural norms) and that the truth of knowledge claims can only be made once the relative framework is specified. This generally means that relativists do not support claims that there are objective truths about something. The difference between contextualism and relativism is that relativism looks at the standards in play in the context of the assessment whereas contextualism may be looking at the context of the subject or the context of where and when a truth statement was made. Critics think this means that results in uncritical intellectual permissiveness.

Invariant views hold that the truth value of a knowledge claim is not context sensitive. Some say that "what varies with context is not truth, but merely assertibility". There are two schools of thought within the invariant camp. Skeptics would invariably require a high standard of proof for knowledge; Mooreans claim that the standards are invariantly comparatively low.

Consider the following three statements from [Contextualism in Epistemology](https://iep.utm.edu/contextu/):

1. I know I have hands.
2. But I don’t know that I have hands if I don’t know that I’m not a brain-in-a-vat (that is, a bodiless brain that is floating in a vat of nutrients and that is electrochemically stimulated in a way that generates perceptual experiences that are exactly similar to those that I am now having in what I take to be normal circumstances).
3. I don’t know that I’m not a brain-in-a-vat (henceforth, a BIV).

Obviously theses statements can't all be right. A contextualist would say that (1) is false if there is a very high standard in play, but true is there is a lower standard (e.g. ordinary knowledge) in play. Similarly (2) and (3) are true if a very high standard is in play, but (3) false if there is a lower standard in play. Critics think that it is wrong for the standards of knowledge to shift from context to context.

| [Contextual](https://iep.utm.edu/contextu/) | [Relative](https://plato.stanford.edu/entries/relativism/) | Invariant | Agnostic |
|------------|----------|-----------|----------|
| 55%        | 5%       | 26%       | 9%       |
N = 1474

### Vagueness
See [Vagueness](https://plato.stanford.edu/entries/vagueness/)
Adding one grain of sand to another does not create a "heap of sand". One additional grain of sand still does not create a heap. Keep adding grains of sand. At what point do you have a "heap of sand"? How did you get there when any one additional grain of sand doesn't make a difference? This is a paradox.

The Epistemic view is that there is a sharp line between heap and non-heap, but we cannot know where the line is - the paradox is a result of ignorance rather than imprecision.

The Metaphysical vagueness view is that reality itself is vague - there are vague properties and vague objects.

The Semantic view is that the term "heap" is indeterminate in a certain range of cases.

| Epistemic | [Metaphysical](http://www.colyvan.com/papers/russell.pdf) | [Semantic](https://plato.stanford.edu/entries/sorites-paradox/#SemaAppr) | Agnostic |
|-----------|--------------|----------|----------|
| 25%       | 21%          | 52%      | 10%      |
N = 1431

### Moral Motivation
See [Moral motivation](https://plato.stanford.edu/entries/moral-motivation/)
- Internalism holds that a person cannot sincerely make a moral judgment without being motivated at least to some degree to abide by his or her judgment. Internalism has been characterized as claiming either that motivation is internal to moral judgment, in the sense that moral judgment itself motivates without need of an accompanying desire (“strong internalism”) or that there is a necessary connection between moral judgment and motivation (“weak internalism”).
- Externalism holds that any connection that exists between moral judgment and motivation is purely contingent, though it may turn out to rest on deep features of human nature. Moral motivation occurs when a moral judgment combines with a desire, and the content of the judgment is related to the content of the desire so as to rationalize the action.

| Internalism | Externalism | Agnostic |
|-------------|-------------|----------|
| 41%         | 39%         | 12%      |
N = 1429

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
