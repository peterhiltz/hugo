---
title: "Responding to Sophists"
date: 2022-02-01T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I just want to quote from a reddit comment I ran across on how to deal with people who want to argue about terrible positions and are more concerned with scoring debate points than having a reasonable discussion. This is as much a reminder to myself as a note to others.

This comment can be found at [https://old.reddit.com/r/askphilosophy/comments/77hda6/how_to_deal_with_unproductive_gadflies_like/](https://old.reddit.com/r/askphilosophy/comments/77hda6/how_to_deal_with_unproductive_gadflies_like/).

> "Card carrying Sophist here (a rhetorican who teaches philosophy). There is no sure fire way to deal with these folks, but there are a few things worth suggesting.

> As a preface, no one enjoys yelling about how Peterson is wrong more than I do, but it's a futile exercise in certain cases. Peterson succeeds in a lot of his various rhetorical enterprises because he tends to attack (1) bogeymen and (2) people who aren't good at defending themselves. In particular, his favorite targets are the theoretical chimera-ghost named "post modernism" and impassioned but often inarticulate leftist college students. This drives me nuts since, in theory, he's supposed to be a professor and picking on students is generally poor form. So, understand that when you engage with these people they are armed with bad arguments, and they're armed with bad arguments designed to (1) make people like you feel stupid and (2) make the people wielding them feel empowered.

> So, what to do? I think in cases like this the savvy arguer should accept that some arguments are not only not worth having, but better off not being had at all. These people crave your aggression - their whole platform grows off the "intolerant," feels-before-reals left.

> So, take this tip from one of the "post-modernist" lefties - Richard Rorty: my advisor had dinner with Rorty once at some academic function and saw someone lay into him. Rorty took it all in and responded only with this - "I don't quite see why we should talk that way?" Rorty seems to imply that he was entertaining the position as one for "us", but he had no interest to refute it. The other person was deflated entirely.

> The rhetorical lesson here from the arch-bro-pragmatist is the power of ambivalence. Being antagonistic - even agonistic - will get you nowhere in these situations. Neither will being totally dismissive. But the middle ground is very hard to contend with if you're an argumentative person. They see arguments as battles to be won or lost, and it's hard to beat a person who doesn't recognize there's a fight happening.

> When you find yourself in one of these situations, if you want to engage at all (and, remember, you don't have to) do only two things: (1) listen very carefully and (2) ask a lot of questions. Importantly, don't be a devil's advocate and don't try to do fancy Socratic tricks where you lead them into a contradiction. Just listen as hard as you can and be sincerely interested and utterly confused. Ask as often as you can "Interesting - why should we think that's true?" Or "Wow, what kind of evidence do we have for that?" or "Wait, can you redo that part? How do we get from [x] to [y]?" Or "What follows from this?" Or "But doesn't that commit us to [x]?" Etc.

> I can't say this enough - don't try to win. Don't look for "gotcha" moments. If they seem to contradict themselves, point it out in the softest terms possible, "So how do we reconcile that with what you said before?" Or "Oh, I think I got lost somewhere as I understood you to have formerly said [x]." If they sense you're trying to play them, they'll ramp up or accuse you of "bad faith" or whatever. So, the best bet is to honestly not try to play them.

> Be honestly and sincerely confused. It won't be hard! It's totally exhausting to talk to a person who does this and even people who love to hear themselves talk can't do it for very long. Honestly one of the few ways to really move people like this is to get them to externalize some implication that they hadn't realized they were committed to. But if you force it, they'll feel persuaded and will recoil.

> This method the basic model of what any philosophy professor would do when confronted with a ranty and ultimately unsound student. You can't argue with them - they already "know" you're wrong. So, get around that problem by not having any position to be wrong about.

> Some arguers will try to pin you down and get you committed to some position, and sometimes there is no way out. If pressed, you can always say what you think and preclude the follow up by saying, "but honestly I'm not quite sure what led me to that view." Or "Honesly I'm not sure what to think about that."

> If they're the type that will only play through a bad Socrates impression, then look for ways to respond to their questions with questions. Usually a Socratic question is an argument in disguise and you can ask why a particular dilemma emerges (if suggested) or why an implication follows (if implied). But, again, if you lean into this too hard you'll get found out. You have to be sincere and sometimes you just have to disengage.

> You don't owe these people an argument."

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
