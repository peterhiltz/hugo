---
title: "ACLU - What Were You Thinking?"
date: 2021-04-04T14:18:19-04:00
categories: ['posts']
tags: ['privacy']
showToc:  "true"
author: "Peter Hiltz"
---
The ACLU just updated its [privacy statement on its webpage](https://www.aclu.org/about/privacy/statement/). Included in that statement was the following:

"*To enable us to provide the most relevant information on our activities, we may share your personal information with communications platforms, such as Facebook and Mother Jones, including to deliver our content to you or to identify other people who may enjoy our content.*"

"*We may also share ACLU supporter information with organizations that display our advertisements or petitions to their subscribers. This practice helps us engage in effective fundraising and advocacy operations and enables our teams to communicate more effectively with known supporters.*"

Ummm. Say What? This is the Facebook that just was [reported for leaking 533 million users' phone numbers and personal data](https://www.businessinsider.com/stolen-data-of-533-million-facebook-users-leaked-online-2021-4)? Yeah, that Facebook.

This is the ACLU that claims [The ACLU works to expand the right to privacy, increase the control individuals have over their personal information, and ensure civil liberties are enhanced rather than compromised by technological innovation.](https://www.aclu.org/issues/privacy-technology)?

Am I missing something here?

According to [Fortune Magazine](https://fortune.com/2021/04/02/aclu-shares-data-facebook-third-parties-digital-privacy/): 

"*The ACLU has spent more than $5 million in Facebook ads since May 2018, according to data from Facebook’s ad library, a searchable repository of ads on the service. Over the same period, the organization has spent an addition $500,000 on more than 1,100 Google ads.*"

"*Catherine Crump, director of the Samuelson Law, Technology & Public Policy Clinic at the University of California at Berkeley School of Law, suggested that the ACLU's financial staff is responsible for the data sharing and not the organization's legal team, which litigates privacy cases. The two groups don't always work in tandem, said Crump, a former ACLU attorney, according to her LinkedIn profile. 'There was always a tension between what happened on the 17th floor (where the advocacy lawyers were) and what happened on the 18th and 19th floors (where the finance people were located),” Crump tweeted on Friday. “I’m not terribly surprised by that part.'*"

Yes, I'm disappointed. I'd like to order some coherence for delivery to the inhabitants of the third planet in this solar system.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.


