---
title: "Creativity and Semantic Distance"
date: 2021-07-28T14:18:19-04:00
categories: ['posts']
tags: ['life','creativity']
showToc:  "true"
author: "Peter Hiltz"
---
I am in the process of reading a [study](https://www.pnas.org/content/118/25/e2022340118?__cf_chl_jschl_tk__=pmd_69870c7c69cf8b9f0cc319eb33aff76b30a7483b-1627483578-0-gqNtZGzNAeKjcnBszQhO) on creativity and semantic distance. A McGill University newsroom article on it is [here](https://www.mcgill.ca/newsroom/channels/news/measuring-creativity-one-word-time-332050). The concept is fairly simple - more creative people will find connections between words that general usage would indicate are "less" connected. "Cat" and "dog" have the feeling of being "related" words. "Cat" and "test tube" feel like "more unrelated" words. Efforts have been done (see the study link) on measuring the "relatedness" of words by crawling the internet and measuring how close the word pairs are to each other in normal usage.

As an exercise, Poet [Lynn Ungar](http://www.lynnungar.com/poems/) has actually had readers submit 20 random words and written poems using all the words. This is so skillfully done you cannot read the poems and guess what the submitted words were if you did not have the list. That, I think, is an example of the creativity being measured here.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
