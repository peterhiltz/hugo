---
title: "Computer Generated Fan Fic"
date: 2023-06-30T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Now that ChatGpt is claiming the ability to write stories and have conversations, I was talking with someone about whether published authors could have chat rooms where paid subscribers could "talk" to their favorite characters. Requiring a paid subscription would cut down on the internet troll problem and generate additional revenue for the author (and publishing company).

On the other hand, just think about a simple user question like "[Protagonist], when you did [X], what were you thinking?". Unless the author has prepped the computer with a ton of background material and anticipated every question, the computer is basically going to have to write fan fiction and would likely come up with answers not intended by the author. But since it would come from the official chatbot, it would lend an air of authority to the result. That possibly creates a problem for unpublished or unwritten sequels if the author's view is incompatible.

It now becomes a question of money or authoritarial control and integrity. If an author is successful enough to have readers willing to pay a subscription, they probably aren't willing to give up control for the additional money. Or would they view it as a way to generate ideas for sequels? Hmm. Might be a solution to writers block.


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
