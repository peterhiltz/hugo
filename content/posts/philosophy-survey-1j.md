---
title: "2020 Philosophy Survey Part 1j: Metaphilosophy, Political Philosophy and Mental Content"
date: 2022-02-22T15:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1j with the topics being "Metaphilosophy", "Political Philosophy" and "Mental Content". I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Metaphilosophy
See [Metaphilosophy](https://iep.utm.edu/con-meta/)
Metaphilosophy is the study of three questions (1) what is philosophy, (2) what is the point of philosophy and (3) how do you "do" philosophy. The allowed specific answers were "naturalism" and "non-naturalism". As far as I can tell, "naturalism" in this context is a category within Anglo-American aka [Analytic Philosophy](https://iep.utm.edu/analytic/) that asserts philosophy can get answers to philosophical questions from science (either looking to scientists or doing science research themselves). From this standpoint, everything else falls into non-naturalism.

| Naturalism | Non-naturalism | Agnostic | Unclear |
|------------|----------------|----------|-------------|
| 50%        | 31%            | 6%       | 9%          |
N = 1549

### Political philosophy
- [Communitarianism](https://plato.stanford.edu/entries/communitarianism/) is the idea that human identities are largely shaped by different kinds of constitutive communities (or social relations) and that this conception of human nature should inform our moral and political judgments as well as policies and institutions. As a result, standards of justice, for instance, will vary from context to context.
- [Egalitarianism](https://plato.stanford.edu/entries/egalitarianism/) is the view that people should get the same, or be treated the same, or be treated as equals, in some respect. An alternative view expands on this last-mentioned option: People should be treated as equals, should treat one another as equals, should relate as equals, or enjoy an equality of social status of some sort. Egalitarian doctrines tend to rest on a background idea that all human persons are equal in fundamental worth or moral status.
- [Libertarianism](https://plato.stanford.edu/entries/libertarianism/) insists that justice poses stringent limits to coercion. While people can be justifiably forced to do certain things (most obviously, to refrain from violating the rights of others) they cannot be coerced to serve the overall good of society, or even their own personal good. As a result, libertarians endorse strong rights to individual liberty and private property; defend civil liberties like equal rights for homosexuals; endorse drug decriminalization, open borders, and oppose most military interventions.

| [Communitarianism](https://plato.stanford.edu/entries/communitarianism/) | [Egalitarianism](https://plato.stanford.edu/entries/egalitarianism/) | [Libertarianism](https://plato.stanford.edu/entries/libertarianism/) | Agnostic |
|------------------|----------------|----------------|----------|
| 27%              | 44%            | 13%            | 8%       |
N = 1537

### Mental Content
See [Mental content](https://iep.utm.edu/int-ex-ml/)
Per the SEP:

> "Mental content simply means the content of a mental state such as a thought, a belief, a desire, a fear, an intention, or a wish. Content is a deliberately vague term; it is a rough synonym of another vague term, ‘meaning’. A state with content is a state that represents some part or aspect of the world; its content is the way it represents the world as being. For example, consider my belief that water is a liquid at room temperature. The content of this belief is what it says about the world, namely that a certain substance, water, has a certain property, being a liquid, under specified conditions, namely being at room temperature. Whether a belief is true or false depends on its content: it is true if the world really is the way the belief represents it as being; otherwise it is false."

Per the SEP, "In the philosophy of mind, externalism is the view that what is going on in an individual’s mind is not (entirely) determined by what is going on inside her body, including her brain." Obviously internalism would be the view that  what is going on in an individual’s mind is entirely determined by what is going on inside her body, including her brain.

| [Internalism](https://plato.stanford.edu/entries/mental-representation/) | Externalism | Agnostic |
|-------------|-------------|----------|
| 26%         | 58%         | 9%       |
N = 1514


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
