---
title: "Defending the Innocent"
date: 2023-12-03T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
It is a sad commentary on media and the species that so many things are seen only in binary. We are told we are on one side or the other whether we want to be or not - You are with us or you are against us. Either you support X or Y, the A or the B! The innocent noncombatants are conflated with one combatant or the other and then dehumanized and erased from existence. Those of us who support the innocent are then accused of supporting X criminals by Y supporters and accused of supporting Y criminals by X supporters.

I could be talking about Ukraine, the Middle East, LGBTQ+, minority rights, international or local politics or whatever. The cruel and hurtful on either side are not the only people involved. But conflict sells, so everything gets framed in terms of X v. Y (misrepresenting the facts on both sides) and ignoring Z-ZZZZZ.... If I believed in Hell, there would be a special place for those who engage in this behavior.

Its easy to accept the framing of a conflict as binary - a choice between good and evil. I could look at most conflicts and say there is a little bit to be said for each side and those truths are buried in the disinformation, but that isn't the point of this post. The point of this post is that while the Marvel Superheros are fighting the villains, there are a lot of non-combatants getting maimed and killed and their houses destroyed by both. And, of course, it is not just physical or monetary injuries. Psychological and emotional damage is just as bad. If you are contributing to that harm, then you are part of the problem, not part of the solution. I don't care what your politics are.

Be curious, not judgemental. (Yes, I borrowed it from the Ted Lasso television show.)

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
