---
title: "Loving Strangeness or Cruel Sanity"
date: 2024-09-26T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I saw the following recently on reddit: 

>When I worked in a book store, we had a guy come in once looking for waterproof books. I asked why, and he said he wanted something to read to chickens. He went on to say he already had one laminated book of poetry that he read to them every night, but he thought they might want something else.

>I'll take that. I'll take dozens of chicken poets over the angry, hateful folks that we accept as normal every day. To paraphrase a quote, "Perhaps he was strange, but it was better than the cruel, heartless sanity of most people."

Yes, a thousand times yes.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.


