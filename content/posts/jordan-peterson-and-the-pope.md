---
title: "Jordan Peterson and the Pope"
date: 2023-03-07T14:18:19-04:00
categories: ['posts']
tags: ['religion']
showToc:  "true"
author: "Peter Hiltz"
---
Somehow I just cannot pass up this series of tweets:

Pope Francis: "SocialJustice demands that we fight against the causes of poverty: inequality and the lack of labour, land, and lodging; against those who deny social and labor rights; and against the culture that leads to taking away the dignity of others."

Jordan Peterson (a Psychology Professor at University of Toronto): "There is nothing Christian about SocialJustice. Redemptive salvation is a matter of the individual soul."

George Santos L Halper: "I don't know what is funnier, you trying to lecture the Pope on what Christianity is, or that you're loudly proclaiming that acting like Jesus is wrong."

Jordan Peterson fans are responding by noting that you cannot find the term "Social Justice" in the Bible. I guess that is what is meant by literal interpretation of the Bible. You can certainly find the concept of social justice there.

I've also seen Jordan Peterson fans claiming that somehow this all has to do with big government (bad). I've read the Pope's tweet several times now and I cannot find government mentioned literally, conceptually or metaphorically.

I haven't done any research into Calvinism and the concept that the elect will go to heaven regardless of what they do. Can anyone enlighten me or do I need to sit under a Bodhi tree?

Edit:

Yes, I know Jordan Peterson is not Catholic and seems to subscribe to one of those Christian Protestant denominations that believe Catholics are not Christian. That leads to the classic Emo Philips joke:

"Once I saw this guy on a bridge about to jump.

I said, "Don't do it!"

He said, "Nobody loves me."

I said, "God loves you. Do you believe in God?"

He said, "Yes."

I said, "Are you a Christian or a Jew?"

He said, "A Christian."

I said, "Me, too! Protestant or Catholic?"

He said, "Protestant."

I said, "Me, too! What franchise?"

He said, "Baptist."

I said, "Me, too! Northern Baptist or Southern Baptist?"

He said, "Northern Baptist."

I said, "Me, too! Northern Conservative Baptist or Northern Liberal Baptist?"

He said, "Northern Conservative Baptist."

I said, "Me, too! Northern Conservative Baptist Great Lakes Region, or Northern Conservative Baptist Eastern Region?"

He said, "Northern Conservative Baptist Great Lakes Region."

I said, "Me, too!" Northern Conservative Baptist Great Lakes Region Council of 1879, or Northern Conservative Baptist Great Lakes Region Council of 1912?"

He said, "Northern Conservative Baptist Great Lakes Region Council of 1912."

I said, "Die, heretic!" And I pushed him over.

-Emo Phillips (1985)


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
