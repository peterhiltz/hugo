---
title: "We need a new enemy"
date: 2020-11-20T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Given the degree of polarization in the US, I don't see both sides making nice. The obvious answer is to stop trying to shove the two magnets together and get them both focused on a new common enemy. I'd prefer the common enemy to be COVID or climate change, but such a large percentage of the US is anti-science that those would just feed the current polarization. The only thought I've been able to come up with so far is anti-corruption, and it needs to be targeted at both in business and government.

Framed correctly, it can play both with:
- populists and equity and justice groups
- local issues as well as monopolies and corruption in larger government organizations
- those who are actual victims and those who want to feel victimized
- the religious and non-religious

It deals with bad faith actors because it assumes that corrupt people are acting in bad faith. Its not "tax the rich", because so many people aspire to be rich; it is targeting corruption so you can still get rich, but you need to play nice. We might even be able to go after news organizations that lie because that is aiding corruption. It sidesteps the problem that one group has empathy and the other doesn't because you are both going after the bad guys. Maybe in ten years the polarization will have sufficiently dissipated that we can address other things.

Right now I think we are in desperate need of a redirection of animosity and you might have much better ideas. All animosity directions have dangers and any reaction can become excessive. As someone once said, what is more dangerous, the fear or the frightened? However, we have an animosity problem right now. Thoughts?

Sidenote: There is a youtube documentary called [In Search of a Flat Earth](https://www.youtube.com/watch?v=JTfhYyTuT44&ab_channel=FoldingIdeas). Its very well done and traces the mindset through to the current political maneuvers. While they apply it to one side of the political/cultural divide, don't ignore the beams in the eyes of the extremists on either side. When you are an extremist or when you are being led by extremists, there is a dangerous point at which your position becomes belief, which is more important than reality.

Some of the leadership will be acting in bad faith. Some of them truly believe, and therefore don't meet the definition of bad faith. In either situation, they don't care about the truth, they don't care about evidence. It's an act of dominance to them, the ability to reject reality and enforce their will on the world around them. When you exist in a world where the truth is subject to what you WANT it to be, rather than based on facts and evidence, no amount of the latter is ever going to change your mind. Once someone's position becomes a matter of faith and belief instead of learning, it becomes more important than reality. You can't reason someone out of something that they weren't reasoned into in the first place. In this case belief is more dangerous than ignorance. They are in search of a flat earth.

A slightly different way of looking at it with respect to the non-leadership group is Plato's Allegory of the Cave. What people think is reality are just shadows on a cave wall, cast by someone else. It could be the social media echo chamber of your choice or the media directed by the leadership group. Attempts to get people to go outside can result in rejection of the outside as not real (the accepted "real" was the shadows on the wall) or beating the poor Cassandra who tried to show the world outside the cave.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
