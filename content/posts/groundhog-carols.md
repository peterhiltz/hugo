---
title: "Groundhog Carols"
date: 2021-02-02T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Groundhog Carols by Gail Pilgrim. Click the title to get to them.
<!--more-->
# Away on a Hillside (Away in a Manger)
{{< highlight none >}}
    Away on a hillside
    in burrows so deep
    marmota monaxes
    are nestled asleep
    they dream of that morning
    when they do their thing
    to give winter's warning
    or welcome the spring
{{< / highlight >}}
# Oh Come All Ye Woodchucks (Oh come All ye faithful)
{{< highlight none >}}
    Oh come, all ye woodchucks
    Marmota monaxes
    Oh come all ye citizens of Punxsutawney
    Come and observe ye
    in this early morning

    That dark around your big toe
    oh can it be your shadow?
    Oh please don't go back below
    for six more long weeks

    Sing crowds of groundhogs
    we need a consensus
    will your shadow frighten you
    or will Spring appear?
    Barking in chorus
    Prophesying for us

    That dark around your big toe
    oh can it be your shadow?
    Oh please don't go back below
    for six more long weeks
{{< / highlight >}}
# God Rest Ye Little Furry Feet
{{< highlight none >}}
    God rest ye little furry feet
    that shadows you don't see
    We're kind of sick of winter now
    and wish that Spring could be
    Oh how we'd like to see a bird
    A singing in a tree
    Oh, hiding from shadows in the snow (oh please don't go)
    Oh, hiding from shadows in the snow
{{< / highlight >}}
# Here We Come A-Shadowing? (Here we come a'wassailing)
{{< highlight none >}}
    Here We Come A-Shadowing?
    on February 2
    A' digging out of burrows
    to see if winter's through

    All ye groundhogs unite
    keep your shadows out of sight
    and you won't have to go back to your
    claustrophobic nest
    now that winter is finally laid to rest

    God bless the master of this hole
    as to its door he creeps
    and let him see no darkness
    beside him when he peeps

    All ye groundhogs unite
    keep your shadows out of sight
    and you won't have to go back to your claustrophobic nest
    now that winter is finally laid to rest
{{< / highlight >}}
# Help, the Shadow Has Been Seen!
{{< highlight none >}}
    Help, the Shadow Has Been Seen!
    Groundhogs back toward holes careen
    Frightened little woodchuck squeaks
    Say they'll stay there six more weeks
    Warm and snug down deep they stay
    Till the Winter slips away

    Cozy, furry bundles all
    Hairy toes and one foot tall
    Help, the Shadow has been seen!
    Six more weeks till leaves turn green
{{< / highlight >}}
# We Three Hogs
{{< highlight none >}}
    We Three Hogs
    From under the ground
    Praire dogs
    we scurry around
    atavistic
    somewhat mystic
    We're Punxsutawney bound

    Oh
    Sign of Wonder, Sign of fright
    in the early morning light
    slowly growing
    see it showing
    sending us on our flight

    Gather now, behold with our eyes
    As the sun, awakes in the skies
    Shadowed earth
    Provides no mirth
    Oh what a sad surprise

    Oh - Sign of Wonder, Sign of fright
    in the early morning light
    slowly growing
    see it showing
    sending us on our flight
{{< / highlight >}}
# It Came out of a Groundhog's Hole (Midnight Clear)
{{< highlight none >}}
    It came out of a groundhog's hole
    Atop a Pennsy hill
    with Spring's forecasting as a goal
    T'was Punxsutawney Phil!

    Oh Punxy Phil, what can he see
    Oh listen as he squeaks
    Has he decided Spring's to be
    Or winter still six weeks?

    Above that quiet little town
    He gazed with anxious eyes
    Behind him as the sun shone down
    to see his shadow's size

    Oh Punxy Phil, what can he see
    Oh listen as he squeaks
    Has he decided Spring's to be
    Or winter still six weeks?
    If when his shadow then he spies
    and turning around he flees
    then we will know with heavy sighs

    that we have long to freeze
{{< / highlight >}}
# The First Shadow (The First Noel)
{{< highlight none >}}
    The first shadow the groundhog did see
    Was to certain poor creatures,
    no moment for glee
    for to the Earth, it gave the sign
    that for 6 weeks of winter, we all must resign

    Shadow, shadow, below Phil's toe
    Warned him of six more weeks of snow
{{< / highlight >}}
# Oh Little Town of Punsutawney
{{< highlight none >}}
    In Punxsutawney, Pennsylvania lives a little chuck
    Inside a tiny woodside hole, this winter he is stuck
    For on the outside shineth, the sun with all its might
    It's no surprise, his shadow's size
    was twice the groundhog's height
{{< / highlight >}}

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
