---
title: "More Deaths by Pens than by Guns?"
date: 2021-04-22T14:18:19-04:00
categories: ['posts']
tags: ['life','politics']
showToc:  "true"
author: "Peter Hiltz"
---
I've been hearing horror stories from friends in the medical field about COVID deniers even as they are gasping for breath. Many of these people (and their families) simultaneously deny the existence of COVID and insist that the doctors gave it to them to get more money. How did this happen? Because people told them lies.

Children are taught the saying "Sticks and stones can break my bones, but words will never hurt me." Its a nice adage and sometimes it is even true. But feelings can be hurt. Continued taunting can lead to other children taunting which can lead to physical violence or suicide. In the early 17th Century Robert Burton in 'The Anatomy of Melancholy' said "A blow with a word strikes deeper than a blow with a sword".

Similarly, there are various quotes about more money being stolen with a pen than with a gun. E.g. Warren Buffet's "It has been far safer to steal large sums with a pen than small sums with a gun." or Mario Puzo's "One lawyer with a briefcase can steal more than a hundred men with guns". These quotes are generally talking about white collar crime.

I would suggest that many more people are hurt (physically, mentally, emotionally or monetarily) by lies someone wants to believe. As a consequence of believing the lies, the believer takes actions which hurt themselves or others. There seems to be little in the way of society guardrails to prevent the spreading of lies in print, in social media, on the airways.

I'm not interested in debating the merits of the US Constitutions' First Amendment v. most of Europe's limitations on hate speech. I am, however, suggesting that more attention should be paid for consequences to apply to the behind the scenes types who pull the strings via the media (private or social).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
