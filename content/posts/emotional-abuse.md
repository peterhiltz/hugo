---
title: "Is Media Emotionally Abusing its Viewers?"
date: 2021-01-18T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Someone on Reddit made a comment about fear driving the extreme ends of the political spectrum and that the media drives that fear because it creates addiction and keeps their viewers/listeners and enables their market. That leads me to the question of whether the media (at least at the extremes) could be viewed as emotionally abusing their viewers or listeners. Thoughts?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
