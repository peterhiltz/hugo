---
title: "Make the Chili"
date: 2020-09-24T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
There is a Carrie Newcomer song called ["Forever Ray"](https://www.youtube.com/watch?v=iRxPBRYacV4)  where the wife (Ella) shooes the husband (Ray) out of the house and suggests that since it is a nice day, maybe he could do something in the yard. So Ray goes out and buys a little cement statue of a rabbit on its hind legs holding a tray. Then everyday he leaves Ella a note on the tray held down by a stone with just something to say that he loves her. After Ray passes away, Ella continues the tradition, but doesn't put a stone to hold down the note, so it gets picked up by the passing wind.

I was reading a post about a couple of friends who were out running and one asked the other what her dinner plans were. She replied that her husband wanted chili, but she didn't feel like stopping at the store. After a few minutes, the first one (who had lost her husband) said "Make the chili".

There may be lots of little annoying things that you have to do for family. If you have a good relationship with family, if you lose one of them, part of you will be glad that you don't have to pick up wet towels or do whatever those annoying things were. At the same time, not doing them can remind you of absence of a person you love.

This is just a little note into the electronic wind to remind people that while we may all get caught up in politics or work, there are lots of little things that connect you to those you love. Remember them, make the chili and feel the positive connection.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
