---
title: "Stupid, Ignorant, Brain Freeze or Troll?"
date: 2021-07-10T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
When reading stuff on the intertubes, it is sometimes difficult to determine whether a comment is stupid, ignorant, brain freeze or troll. You cannot see the other person and have no knowledge about their background. I would treat a lot of statements as simply trolls if I didn't have the personal experience of an 18 year old American "honors" student ask me why people in Switzerland spoke German because their minds think in English.

Sometimes a comment is just a brain freeze. We've all done it: our mind just locks up and we ask or say something that 10 seconds later our mind shifts back into gear and we realize we just said something that sounds stupid.

Sometimes a comment is simply ignorant. The speaker just doesn't have the experience or knowledge that you have. If you assume all "right thinking people" have this experience or knowledge, and then you treat the person in a condescending manner (like maybe this blog post is doing), both you and the speaker are going to have a bad interaction and the world will be worse off.

Sometimes a person just can't understand. Making fun of them will not help the situation and, sometimes any interaction at all will not help the situation. As other posts have indicated, engaging with wilfull ignorance often results in the person doubling down on their mistaken belief.

And sometimes they are trolls, just trying to get a reaction from someone so they can put another notch on whatever they use as a scoreboard. And yes, I am working with Dante's ghost designing new circles of hell for them and scam artists.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
