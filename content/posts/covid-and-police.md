---
title: "Covid and Police"
date: 2021-09-05T14:18:19-04:00
categories: ['posts']
tags: ['covid','politics']
showToc:  "true"
author: "Peter Hiltz"
---
Per the [Officer Down Memorial page](https://www.odmp.org/search/year/2021), so far 243 police officers have died in the line of duty in 2021 and of those deaths, 132 are from COVID-19. So more deaths from Covid than every other cause combined. Remind me why police unions are opposed to masks and vaccination? Seriously, I don't get it.
<!--more-->

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
