---
title: "You Know You Have Been Living in France Too Long"
date: 2021-05-21T14:18:19-04:00
categories: ['posts']
tags: ['travel']
showToc:  "true"
author: "Peter Hiltz"
---
I haven't posted in awhile due to grandparent duties. This post has no serious content and is just a list of items from an acquaintance living in France. Some of these are familiar to me based on either spending a lot of business travel time in France or living in Belgium, which share some French proclivities. You may or may not be amused. Bonus points if you actually know what a Department is in France without looking it up.

You know you have been in France for a long time when...

- You are convinced that laws were invented to keep the police busy.

- You don't see any reason to respect the law unless there is a police
  officer nearby.

- You order tripe sausage whenever you happen to be in Lyon.

- You actually enjoy going shopping for food, because it gives you the
  opportunity to chat with the merchants.

- You think it is perfectly OK to have a bottle of wine with your
  lunch in the middle of the week.

- You think showing up only half an hour late for a dinner party might
  be too early.

- You can no longer tell the difference between a public sidewalk and
  a dog toilet.

- You can no longer tell the difference between a lawn and a dog
  toilet.

- You think it's normal to walk on the street because parked cars
  prevent you from using the sidewalk.

- You take the afternoon off to go to the post office.

- You know more than 10 different words derived from "shit".

- You use more than 5 of them on a daily basis.

- You consider buying a new 100 EUR fountain pen.

- You actually prefer a fountain pen to a ball-point pen.

- You see a grammatically very complicated phrase, and consider using
  it the next time you write email to your friends.

- You consider cutting down a tree because it might one day grow so
  tall that there is a risk it might fall on your house.

- Pruned trees look normal to you.

- You think the non-smoking sign applies to a 50cm radius around it.

- You consider the possibility of serving sweetbreads to your foreign
  guests.

- You actually serve gizzard salad to your foreign guests.

- The fact that there is no parking place within 10m of the place you
  want to be is the fault of the government, so you invent one right
  there.

- Your arms automatically move when you talk.

- You know the gestures for things like "boring", "fed up", etc.

- You actually use those gestures regularly.

- You can't imagine a meal without bread.

- You can't imagine a meal without wine.

- You can't imagine a meal without cheese.

- You understand the logic behind the department numbers.

- You can tell from the license plate of a car where it is from.

- You instinctively put the number of the department on your
  English-language CV.

- You know the difference between a "department" and a "territory".

- You think area codes are for wimps.

- You think it's normal that you can't pay your bills on the Internet
  even in the 21st century.

- You accept "liver crisis" as a real disease.

- You think antibiotics might after all cure your cold, and in any
  case, it can't hurt.

- You think it is normal for the pharmacy to carry homeopathic
  medications.

- You take the mushrooms you picked to the pharmacy to make sure they
  are not toxic.

- You think it is OK to discuss your medical problems with the staff
  and the other customers of the pharmacy.

- You regularly buy medication without prescription that legally
  requires one.

- You buy 90% alcohol at the pharmacy, but you wouldn't consider
  drinking that disgusting stuff.

- You know which pharmacy in your neighborhood is open the following
  Sunday.

- You actually enjoy spending the first few days of your vacation
  parked on a motorway with all the others that left at the same time
  as you.

- You bring your pocket knife to the beach so that you can snack on
  the odd oyster lying around.

- You buy the ham even though the butcher sneezed in his hands before
  slicing it up.

- You think that it is normal for the butcher to sell wine.

- You think that it is normal for the baker to sell wine.

- You think that it is normal for the tobacco store to sell wine.

- You think that it is normal for the gas station to sell wine.

- You prefer the dubbed version of your favorite TV show.

- You decide to take a shower at the time of your favorite TV show,
  because you know it is going to be at least 10 minutes late.

- You are surprised when there hasn't been any major strike for a few
  months.

- You buy the new-year calendar from your garbage collectors, to make
  sure your trash will be picked up next year.

- You buy the new-year calendar from the police and the fire
  department in case you might need their help during the next year.

- You like to listen to Johnny Halliday.

- Sour cream, gizzards, and duck liver all sound like perfectly
  ordinary pizza ingredients.

- You are longing for some mashed potatoes mixed with cod fish.

- You turn on the TV in order to watch the new-year speech by the
  president.

- You no longer react when the vegetarian salad you ordered has bacon
  pieces in it.

- It seems reasonable for a souvenir store to be closed for vacation
  during the tourist season.

- "nun's farts", "piss-in-bed salad", and "goat's turds" all sound
  like reasonable names for food.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
