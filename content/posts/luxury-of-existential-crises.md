---
title: "The Luxury of Existential Crises"
date: 2024-04-28T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I was once told by a left wing activist that existential crises were a luxury for the rich.

There was a recent discussion on social media (the askphilosophy subreddit) about whether philosophy is a borgeouise hobby. I thought the most interesting comments came from the "third world" perspective.

They distinguished between western academic analytic philosophy and philosophizing done by persons outside that small circle. Some thought the first was becoming a bourgeois hobby, but the second happened every day, every where.

There were a few comments about how academic philosophy teaches critical thinking skills, which can aid anyone in life. While this is true, it doesn't mean that philosophers don't have their own mental blocks where they don't apply that critical thinking. Think abut the story of someone complaining about the speck in another person's eye while denying the log in their own. That story is "philosophizing" and everyone does it. The academics simply claim that they can be more precise in specifying the speck in the other person's eye.

One interesting comment was about whether all "hobbies" are bourgeois.

> "To that point, as the lowest class requires having two or three jobs to get by, having any time for any hobby is a bourgeois luxury - any pursuit that requires surplus time and income beyond paying rent, bills, and groceries. The true search for peace/truth/knowledge itself is a luxury that is not affordable to the lowest earning members of society. "

Another comment pointed out that "borgeouise" was not defined in the sense that both high-hour highly paid and high-hour lower paid have less time available to either read or listen to philosophy discussions than low-hour wealthy or lower-hour positions who can listen at work or only have 40 hour weeks instead of 60 hour weeks.

But I want to circle back to the real question that was posed in starting the reddit discussion:

> So my question is has philosophy become a status symbol/borgeouise hobby rather than a true search for peace/truth/knowledge?

The last half of the question: "rather than a true search for peace/truth/knowledge" was actually somewhat ignored by most of the comments. Setting aside epsistomology, which is getting more and more abstruse by the decade, I would argue that most western analytic philsophy since 1900 has never been a true search for peace/truth/knowledge, but more like the medieval theologians arguing over how many angels could dance on the head of a pin. But, then again, I'm not a trained philosopher, so I'm way out of my area of expertise.

This leads me back to my initial sentence. No, I don't think existential crises are a luxury for the rich. I don't think you can become an activist about anything without having gone through an existential crisis: What is the meaning of life FOR YOU, and, if it is limited because your freedom is circumscribed, then how do you expand your freedom. See Simone de Beauvoir and Frantz Fanon.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
