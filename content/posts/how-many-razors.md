---
title: "How Many Razors Do Philosophers Need?"
date: 2020-10-23T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Think of a generic picture of a philosopher. When I did, I came up with a picture of a bearded old white guy. Now why do they have so many razors if so many of them have beards? All of the following rules of thumb are intended to reduce the explanations you should have to think about.

"[Occam's razor](https://en.wikipedia.org/wiki/Occam%27s_razor)" says that simpler explanations are more likely to be true than complicated explanations.

"Hanlon's razor" says that you should never attribute to malice what can be explained by incompetence (or stupidity).

"[Hitchen's razor](https://en.wikipedia.org/wiki/Hitchens%27s_razor)" says that anything submitted without evidence can be rejected without evidence.

"[Hume's guillotine](https://www.philosophy-index.com/hume/guillotine/)" [apparently different than a razor] says that what ought to be cannot be deduced from what is.

"[Alder's Razor, aka Newton's flaming laser sword](https://web.archive.org/web/20111114041242/http://school.maths.uwa.edu.au/~mike/Newtons%20Flaming%20Laser%20Sword.pdf)" [getting even farther from razors] says that if something can't be settled by experiment or observation, it is not worthy of debate.

Let's play with these for a bit.

## Occam's Razor
The idea that simpler explanations are more likely to be true is a rule of thumb, not a rule of logical analysis. In other words, if it looks like a duck, quacks like a duck and swims like a duck, it is probably a duck.  This certainly doesn't mean that you can't think about more complicated explanations. Once they discovered that Newtonian physics didn't explain movement of the stars sufficiently, the more complicated theories of relativity and quantum theory needed to be considered in spite of the fact they are more complicated than Newtonian physics. But at the same time, you can always create an infinite number of [Rube Goldberg](https://www.digitaltrends.com/cool-tech/best-rube-goldberg-machines/) type explanations for anything.

There is also what I call "lies for children" which means giving a simple explanation that isn't technically correct and is missing a lot of nuance, but it makes sense to the intended audience at their "non-expert" level. Finally there is also what is called the "conservation of ambiguity". In this context, that doctrine would state that every explanation has some ambiguity. Every time you try to explain each individual piece of that explanation, you multiply the number of explanations and each one of them is ambiguous. Ergo, the total amount of ambiguity in your explanation remains the same no matter how many technical levels you descend.

I've seen arguments that simpler explanations are more easy to test and determine if they are false, but I'm not sure that I believe that. Some simple explanations like "its turtles all the way down" leave you wondering if they are simpler or more complicated. Or maybe if it [weighs as much as a duck, it is a witch](https://www.youtube.com/watch?v=yp_l5ntikaU).

William of Ockham was a Franciscan monk and pictures show him beardless.

## Hanlon's Razor
Robert Hanlon wasn't a philosopher and Hanlon's Razor was his submission to a compilation of jokes regarding Murphy's law, but there he certainly did not originate the concept. Johann Wolfgan von Geothe came up with the concept in 1774 that " Misunderstandings and lethargy perhaps produce more wrong in the world than deceit and malice do. At least the latter two are certainly rarer." (Obviously an English translation of what he actually wrote.) And author Jane West said in her book "The Loyalists: An Historical Novel" in 1812 "Let us not attribute to malice and cruelty what may be referred to less criminal motives."

I've certainly cited Hanlon's razor quite often in my career, but it doesn't cover a probably bigger possibility. Assume "they" were pursuing their own ends competently. The word "malice", talking about the other assumed possibility  indicates that someone is out to get you. More often, someone is out for themselves and you are collateral damage. Roadkill as it were. They may or may not have known of your existence and the impact of their actions on you. They may or may not have cared. If someone even brought the possibility of harm to you, they may have decided it was for the greater good, or the greater them.

If you want to have an impact on the decision making, you need to figure out what is driving the other person and then come up with an appropriate response based on that judgment. For example, take an auto company that is shaving costs, a penny here, a penny there, and decides not to spend money on better safety equipment based on an estimate of liability payouts in the future. They've already assumed that people will die because of their actions, so pounding the table and claiming "safety is key" won't change their decision. You would need to either challenge their liability payout assumptions or you would have to convince the government that these types of decisions deserve jail time for the executives and convince the executives that personal liability or jail time is not an empty threat.

von Goethe didn't have a beard and I'm assuming neither did Jane West.

## Hitchen's Razor
Hitchen's Razor is named after Christopher Hitchens, an avowed atheist, who used the concept in a book about religion. Basically it just puts the burden of proof on whoever is making a claim (about anything, not just religion). As a lawyer, I agree with this burden of proof proposal. I would submit the claim, without any proof whatsoever, that citing social media is not evidence for purposes of Hitchen's Razor.

Hitchens didn't have a beard, so he uses some type of razor.

## Hume's Gillotine
David Hume's thesis, from his book "A Treatise on Human Nature" (1739) is really directed at moral arguments, not factual disputes. If you accept the thesis, then it would seem you can't get to moral laws simply from looking at physical reality. [Moral naturalists](https://plato.stanford.edu/entries/naturalism-moral/) disagree with this, but they can't seem to agree on what are moral natural facts.

My personal moral philosophy is possibly succinctly expressed as "do the right thing because its the right thing" doesn't rely on any natural or facts or religious beliefs and I'm certain many people will argue over "the right thing". As a result,  I'm probably not a moral naturalist and would agree with Hume on this point.

Hume didn't have a beard either. Hmmm.

## Alder's Razor, AKA Newton's Flaming Sword
Mike Alder is a mathematician and came up with his razor, aka Newton's Flaming Sword in 2004 and is targeted at the differences between scientists and philosophers on epistemology and theories of knowledge. My first reaction to this concept was "what is a mathematician doing talking about experiment or observation"? But then I discovered that he doesn't work in pure mathematics and so my question goes away. Seriously, read [his paper](https://web.archive.org/web/20111114041242/http://school.maths.uwa.edu.au/~mike/Newtons%20Flaming%20Laser%20Sword.pdf), its a fun read on Plato and language and the universe in 11 pages.

Of course, Alder's proposition is not limited to arguments with philosophers. It also eliminates arguments about politics, whether tv sitcoms have any reason for existence, which type of music is better or anything else that people like me spend time on when we should be doing something else.

Alder doesn't have a [beard](https://web.archive.org/web/20110416065600/http://www.uwa.edu.au/people/mike.alder) either.

## Conclusion
The only thing we really learned from this was that none of the originators of these philosophical razors had beards, so maybe they are useful. Maybe I could learn something.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
