---
title: "Practicing the Little Lies"
date: 2020-12-26T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Ars Technica published an article on Christmas Day (yesterday) entitled [Children's Belief in Santa is more Nuanced than you Think](https://arstechnica.com/science/2020/12/study-childrens-belief-in-santa-claus-is-more-nuanced-than-you-think). The story talks about studies showing that children have hierarchies of belief, with individual figures falling categories like: "Real Person", "Virtually Real", "Cultural Figures", "Ambiguous Figures" and finally "Fictional Figures". So, like so many things, there is no binary on/off, but rather a spectrum between "real" and "nonreal". One of the studies argued that three factors influence children's belief and placement of the figures in the spectrum: Testimony (being told about the figure), indirect evidence (which included rituals like leaving milk and cookies for Santa or hunting for Easter Bunny eggs, and direct evidence like visiting Santa in the mall.

For me, the more interesting commentary on people in general came from reading the roughly two hundred comments on the article.

The Ars Technica readership is a tech community, so you expect responses like "Schrodinger Claus?", references to [Calvin and Hobbes](https://i0.wp.com/thecuriousbrain.com/wp-content/uploads/2012/12/calvin-hobbes-santa-claus-god.jpg?resize=499%2C633) and immediate jumps to whether religion falls into a similar category. (Is Santa Claus a "starter faith"?) If you read the comments, skip the religion ones because they fairly quickly degenerate into name calling.

I was a bit disappointed that a couple of people wondered whether this was children expressing degrees of doubt on real v. not real (still trying to force reality into a binary state) rather than children understanding that there are different "reals". I shouldn't be surprised, however.

There was speculation that this sort of thing helps children's minds develop and spot fraud and lies, countered by vehement arguments about just telling the truth directly. There were lots of reminisces about when someone first figured out that Santa, Tooth Fairy, etc was not real and whether as a child they decided to play along in order to keep the presents coming. (In our family growing up, Santa brought the gifts in the stocking - a new toothbrush, a tangerine, a candy cane, some nuts and one small present - and all the big presents under the tree were from family members. I don't really recall what age I was when Santa morphed into the "Spirit of Christmas".)

Ars reader Navalia Vigilate said "*It's been interesting raising a child and being carefully open and honest about all topics such as what is real and what is not, what is life and what death means and looks like; still too young for sex but there is a plan for that conversation as well. The speed with which they can process and adapt into their understandings is remarkable to watch and it brings comfort as a parent to know that the child is being set up with no unnecessary baggage. Life, the Universe, and Everything, is difficult enough without without unduly burdening a young person with the accumulated garbage of centuries of human superstitions. Better they reach adulthood with a firm grip on reality and decide for themselves what to do with it all.*"

There is no hard break between children and adults. I would have been disappointed (and wasn't) if someone failed to insert this quote from Terry Pratchett:

"*HUMANS NEED FANTASY TO BE HUMAN. TO BE THE PLACE WHERE THE FALLING ANGEL MEETS THE RISING APE.*

"*Tooth fairies? Hogfathers? Little—*"

*YES. AS PRACTICE. YOU HAVE TO START OUT LEARNING TO BELIEVE THE LITTLE LIES.*

"*So we can believe the big ones?*"

*YES. JUSTICE. MERCY. DUTY. THAT SORT OF THING.*

"*They're not the same at all!*"

*YOU THINK SO? THEN TAKE THE UNIVERSE AND GRIND IT DOWN TO THE FINEST POWDER AND SIEVE IT THROUGH THE FINEST SIEVE AND THEN SHOW ME ONE ATOM OF JUSTICE, ONE MOLECULE OF MERCY. AND YET—Death waved a hand. AND YET YOU ACT AS IF THERE IS SOME IDEAL ORDER IN THE WORLD, AS IF THERE IS SOME...SOME RIGHTNESS IN THE UNIVERSE BY WHICH IT MAY BE JUDGED.*

"*Yes, but people have got to believe that, or what's the point—*"

*MY POINT EXACTLY.*

— Terry Pratchett, Hogfather"

The big lies are the concepts that form the foundation of cultures and societies. Ars Technica reader Madmax559 said "*If you are going to believe something or instill a belief in a young kid, then it may as well be a good belief to be kind & caring to others & its a good belief to grow up with.*" Unfortunately it seems that we can't all agree on the desirable "big lies".

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
