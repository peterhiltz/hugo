---
title: "Is Heaven Communist?"
date: 2023-07-01T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
TRIGGER WARNING: DO NOT READ IF YOU TAKE THE BIBLE LITERALLY.
<!--more-->
So, the word of the day was whimsical and I had just seen a statement from some minister complaining that left wing policies led to communism. My brain suddenly asked whether heaven was communist?

Before we even get started, if you are going to tell me that "The story of communism is about people who take other's possessions by force in a grapple for extra power." you're missing the point. I'm talking about whether there is equality and post-scarcity sharing in heaven.

I thought it was a whimsical thought. Not surprisingly, I'm not the first person to ask the question and there are very vehement diatribes against it.

According to [Is Heaven Communist?](https://libertarianinstitute.org/articles/is-heaven-communist/) published at the Libertarian Christian Institute and the Libertarian Institute, Heaven (aka New Earth) will be a place of moral perfection and restoration, but we will have physical bodies and, since we will have to work at things like planting crops, there will be some measure of scarcity and also property rights.

"What this means is that we can conclude the New Earth will not be moneyless, because various economic laws (namely the double coincidence of wants) will necessitate a medium of exchange for value to ease transactions (even if all economic actors were glorified and morally perfect that doesn’t mean that they will want to barter salt for beef all the time). Money and prices (based on property) are also essential for economic calculation, which is an essential part of economic activity (which we know will occur in the New Earth, given scarcity and human action)."

The article goes on to say that since there is natural proficiencies and deficiencies in humans, then "There is also, inevitably, a special group of people in any particular skill-based group that rises above the rest due to natural talent."

The author makes it clear that while "incredibly radical and selfless sharing" is nice, "God’s kingdom is a voluntary covenant, and there is no coercion apparent in it."

Apparently its ok to be a slum lord in heaven, and somehow that is a place of "moral perfection"?

According to [Is Heaven a communist country?](https://wng.org/articles/is-heaven-a-communist-country-1617286517), at WNG.org "Sound journalism grounded in facts and Biblical truth", "We won’t be all the same in heaven, because there we are without sin but not without personality. Heaven is a sweet land of liberty, but one way it differs from this world is that in it we will have neither right nor desire to do wrong."

Interesting that there is no desire to do wrong, but apparently also no desire to do right?

The WNG.org article claims that we will have no material needs (unlike the Libertarian article), but we'll still have our personalities and "communism denies liberty and thus personality".

I'm sorry, what "liberty" would "communism" deny in heaven?

I find myself agreeing with a comment on Reddit that "if those people were to find themselves in an actual post scarcity reality they'd hate it because their entire world view has been shaped by the ideology of heirarchical value of individuals."

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
