---
title: "2020 Philosophy Survey Part 1c: Normative Ethics, Footbridge Experiment and the Trolley Problem"
date: 2022-02-16T17:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1c with the topics being Normative Ethics, Footbridge Experiment and the Trolley Problem. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Normative ethics
- [Deontology](https://plato.stanford.edu/entries/ethics-deontological/) argues that decisions should be made considering the factors of one's duties and one's rights. This includes Kant's categorical imperative which asserts certain inviolable moral laws; Rawls' [contractualism](https://plato.stanford.edu/search/r?entry=/entries/contractualism/&page=1&total_hits=225&pagesize=10&archive=None&rank=8&query=utilitarianism) which looks to what morality would we all agree to if we were unbiased and did not know each others' properties; and Natural rights theories such as John Locke which claims that human beings have absolute natural rights.
- [Consequentialism](https://plato.stanford.edu/entries/consequentialism/) argues that the morality of an action is contingent on its outcome. There are lots of variations including [utilitarianism](https://plato.stanford.edu/entries/consequentialism-rule/#Uti), [mohism](https://plato.stanford.edu/entries/mohism/) and many others.
- [Virtue Ethics](https://plato.stanford.edu/entries/ethics-virtue/) focuses on moral character.

Per the SEP: "Suppose it is obvious that someone in need should be helped. A utilitarian will point to the fact that the consequences of doing so will maximize well-being, a deontologist to the fact that, in doing so the agent will be acting in accordance with a moral rule such as “Do unto others as you would be done by” and a virtue ethicist to the fact that helping the person would be charitable or benevolent."

| [Deontology](https://plato.stanford.edu/entries/ethics-deontological/) | [Consequentialism](https://plato.stanford.edu/entries/consequentialism/) | [Virtue Ethics](https://plato.stanford.edu/entries/ethics-virtue/) |
|------------------------------------------------------------------------|--------------------------------------------------------------------------|--------------------------------------------------------------------|
| 32%                                                                    | 31%                                                                      | 37%                                                                |
N = 1741

### Footbridge Experiment
The footbridge thought experiment is a variation on the trolley thought experiment. Assume a trolley is out of control, and will kill its five passengers. Now assume y are standing on a bridge over the tracks, and if you push someone standing next to you off the bridge, your action will kill that person, but stop the trolley, saving the lives of its passengers. Would you push them? Nine people said they would jump themselves.

| Push | Don't Push | Agnostic |
|------|------------|----------|
| 22%  | 56%        | 9%       |
N = 1740

### Trolley problem
See [Doing vs Allowing Harm](https://plato.stanford.edu/entries/doing-allowing/)
The Trolley problem (for those who did not watch "The Good Place") is a thought experiment where you are the trolley driver and you have to decide whether to choose between flipping a switch to turn your trolley so it runs over an innocent man attached to the track or slip the switch and run over five innocent people attached to the track.

| Switch | Don't Switch | Agnostic |
|--------|--------------|----------|
| 63%    | 13%          | 8%       |
N = 1736


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
