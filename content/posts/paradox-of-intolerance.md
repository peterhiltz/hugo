---
title: "There is no Paradox of Tolerance"
date: 2022-10-16T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I occasionally run into people who complain that I am tolerant of X, but I am not tolerant of them, so if I claim to be "tolerant", I need to be tolerant of everything.

NO I DON'T. I am tolerant of people whose actions do not adversely affect others. I do not pretend to be tolerant of people whose actions and beliefs do affect others. Two gay people getting married does not adversely impact anyone else. You telling them that they are evil and trying to emotionally injure them because they don't conform to your standard of behavior does impact them.

Did you know that some people are left handed? Some people are even ambidextrous? I know, shocking. Hitting them with a ruler until they start writing with their right hand is not something I am going to be tolerant about. "Oh, thats different". Why?

Now excuse me while I go collect rocks to stone an adulterer.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
