---
title: "Onion Peeling in a Post Truth World"
date: 2024-11-14T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
It is obvious to the 3 people who read this blog that my enthusiasm for doing the research required for trying to honestly analyze something that caught my eye has waned. Back in 2021 I posted "When an honestly mistaken person is confronted with the truth, he is either no longer mistaken, or no longer honest." I am confronted with the reality of a post-truth world, where onion peeling analysis is irrelevant. So, I need to decide what I am going to do with this blog or what other squirrels I should chase. Anyone know a medium with a good connection to Cassandra?

Also, I understand that the destination based cash flow tax proposal is rearing its ridiculous head again. My apologies to my friends in the international tax world for apparently not killing it sufficiently in 2017. 

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
