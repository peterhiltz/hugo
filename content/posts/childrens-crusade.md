---
title: "Children's Crusade or Bay of Pigs or ???"
date: 2021-01-07T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
President Trump told attendees at the political rally earlier in the day to "take back our country". Rudy Giuliani told the same crowd at the rally "If we are wrong, we will be made fools of, but if we're right a lot of them will go to jail. So let's have trial by combat.trial by combat" and is now condemning the assault on the Capitol as shameful.

I've been watching interviews of reporters who talked to the mob that rushed the Capitol yesterday. Many of the people seemed to have no idea what they were going to do once they got in the Capitol, it just seemed like the thing all their friends were doing, so they were going along for the fun.

One woman from Knoxville Tennessee got maced after she got one foot inside and then pushed back out of the building by Capitol Police. When asked by a reporter why she tried to get in the building, she said "*We were here to storm the capitol, its a revolution.*" She seemed very surprised that there would be consequences. [https://imgur.com/ZLibzBY](https://imgur.com/ZLibzBY)

The woman from San Diego who was shot as she was trying to be first to climb through a broken window in spite of Capitol Police orders not to do so spent the summer tweeting and cheering on heavy handed police tactics on BLM protests. An example tweet found here [https://twitter.com/Ashli_Babbitt/status/1286673535876833280](https://twitter.com/Ashli_Babbitt/status/1286673535876833280). Note the BlackLivesDontMatter hashtag. Two days ago she retweeted "*The President reinstates Death by Firing Squad , Guess What the US penalty for Treason Is ? DEATH BY FIRING SQUAD !*"

CORRECTION: She was climbing over a furniture barricade, wearing a backpack after two explosive devices had been found, after the rioters had broken the glass doors trying to get into the Speaker's Hallway,an area where the Capitol Police and Secret Service may have been protecting Vice President Pence and Speaker Pelosi. See video [here](https://www.reddit.com/r/PublicFreakout/comments/ks8gtj/clearest_view_of_a_terrorist_attempting_to_breach/?utm_medium=usertext&utm_source=reddit&utm_name=SelfAwarewolves&utm_content=t1_gieku7y).

Three others died from "medical emergencies", one allegedly from a heart attack when he accidentally tasered himself, another (a woman) was crushed at some point not clear. The final medical emergency also seems to be a heart attack.

I'm trying to figure out what the attendees were thinking. Was this like the Children's Crusade or more like Bay of Pigs light? Right now I'm leaning towards - they weren't thinking. They were just sheeple. Adults in the room need to figure out how to deal with Pied Pipers of Lies and Destruction.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
