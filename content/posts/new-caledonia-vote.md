---
title: "New Caledonia Narrowly Votes No on Independence From France"
date: 2020-10-04T14:18:19-04:00
categories: ['posts']
tags: ['international']
showToc:  "true"
author: "Peter Hiltz"
---
New Caledonia voted no today on its second of three votes on independence from France. The first vote was in 2018 and the no votes came in at 56.7%. This time, as expected, the no votes came in narrower at 53.26%. Turnout was 85.64%, which was four points higher than the 2018 vote, evidently showing that people felt their votes would count given the expected tight vote. Polling stations were kept open after the scheduled closing time so long as there were people in line to vote. (See, some countries think allowing the vote is a good thing). The vote means that New Caledonia remains part of the European Union - just 16,000 km away.

The third independence vote is expected in 2022. The three votes on independence were part of the 1998 Noumea Accord signed by France, the Kanak and Socialist National Liberation Front and anti-independence leaders.

New Caledonia has a population of only 270,000 but is a major global producer of nickel. I've seen one estimate it has 25% of the world's total known deposits, but haven't verified that. It is still subsidized by France to the tune of about US dollars 1.5 billion per year. Losing that subsidy appeared to be a major reason for the no votes.

It is nice to see independence votes finally happening without violence. Yeah, if you are wondering, France seized the island chain in 1853.

This post was compiled from many different news reports around the world. Let me know if you have favorite international sources that would not be picked up by the big international news groups and that you think can be trusted to provide facts instead of propaganda.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
