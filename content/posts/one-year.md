---
title: "Retired One Year and No Regrets"
date: 2021-09-06T14:18:19-04:00
categories: ['posts']
tags: ['politics','tax','rant']
showToc:  "true"
author: "Peter Hiltz"
---
So, one year into retirement and so far no regrets. I could rant at the abyss about the global state of affairs, but I know exactly how much attention the abyss would pay.

Several people asked me if I was behind the Twitter feed @GoodTaxTakes. The answer is no, but I agree with probably 90% of it. Click on the title of this post for some thoughts about the current state of affairs in international tax which I can get away with because I don't represent anyone anymore.

- The US Treasury continues its almost religious belief that it is the only branch of government with a stake in international taxation. No, you're not.

    - First, you don't get to give orders to Congress; this isn't a parliamentary government, Congress is a co-equal branch of government. "But we promised other countries we could get Congress to deliver" is not a valid excuse. It is your fault for not actually engaging with Congress. If Congress doesn't have the votes, your embarrassment is not Congress' fault.
    - Second, I haven't heard anyone say that Treasury has remembered to talk to State or US Trade on international tax. International tax and international trade really are intertwinned and businesses actually factor both into their operating, structuring and investment decisions. If everyone could stop playing turf wars, a coordinated strategy actually works better than an uncoordinated strategy.
    - Third, foreign governments actually are sovereign and you can't dictate their positions. You might actually think through their concerns and possible reactions and the potential consequences for the US.

- The US Treasury (and Congress) still underestimate the complexity of the international tax system they created when it comes to actually making it work. The IRS still has not been able to write the software for major portions of the 2017 tax act, so telling both taxpayers and the IRS to just "throw software at it" is the equivalent of a a two year old stamping its foot and saying everything is someone else's fault. This stuff is already impossible and doing country by country GILTI is ignoring practical reality. Tax system complications which are passed off as "big companies can afford to figure it out" is a waste of societal resources which could be better used doing something useful. You are responsible for creating a system that works. Grow up and take responsibility for the monster you created.

- Academics (legal and economic) still do not understand the accounting and economic data they are looking at, which means they don't understand the facts. If they don't understand the facts, it doesn't matter if they are the smartest person in the room, their opinion is going to be wrong. They will not understand how business (and economies) will evolve in response and therefore they won't understand the consequences of their proposals/demands.

- A principled tax system is important. A proposal which keeps individual legal entities as separate taxpayers, but then taxing one legal entity based on income of related legal entities strips any pretense of principle out of the system. If you are going to go down that road, admit you are eliminating separate legal entity calculations and you can actually make the system easier. By the way, that will require facing up to the fact that limited liability partnerships and corporations really should be treated the same. Either make everything a pass-through or make everything taxable. Yes, I'm old enough that I actually structured operations before "check the box".

- Raising taxes on corporations simply to raise taxes on corporations is not a good policy. Anyone who says that corporations must pay their "fair share" without being able to define "fair share" does not have a policy. Policies involve actually thinking through things like defining that amount, how to measure it, what are the practicalities of making that calculation, how does that interact with other things and what are the consequences.

- Throwing money at something without actually have a plan of what you are doing in order to achieve your goals is worse than useless. If you don't have a thorough, thought through plan, you are just throwing money away and you haven't calculated the opportunity cost of where you took it from.

- And while I'm ranting into the abyss, let's actually have some honesty. I know, honesty is never good politics, but some of us have principles and I'm not a member of either party. Start with little things that won't even hurt like admitting the US has a worldwide tax system, not a territorial tax system. Then you can move to bigger things like admitting that tax systems designed to punish certain countries is warfare without getting your hands dirty; real human people will still get hurt.



As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
