---
title: "Hanlon's Razor and Useful Idiots"
date: 2021-01-09T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
While lots of social media focused on videos of individual Capitol Police being "friendly" to the rioters I want to focus on Capitol Police leadership. If you might recall, I mentioned "Hanlon's Razor" in an [earlier post](https://www.peterhiltz.com/en/posts/how-many-razors/). Hanlon's razor says that you should never attribute to malice what can be explained by incompetence or stupidity. I also mentioned that malice is often assumed to be someone being out to get you, but more often the person is just out for themselves and you are collateral damage (roadkill). We'll talk more about the "useful idiot" possibility later.

As you may have seen from the news, both the Capitol Police and the FBI claim that they had no intelligence that the protests would turn violent. This intelligence conclusion was in the face of many national media reports about threats against Congress to take place when they were counting the Electoral College votes .... when the top three people in the presidential line of succession would be in one place. This is the same FBI that broke up a conspiracy to kidnap, try and possibly execute Michigan Governor Gretchen Whitmer. Chris Krebs, the fired former head of US cybersecurity told CNN that there had been no question in his mind, that violence would happen at the Capitol. Yet the barricades were the type of barricade you would see for a parade and the Capitol Police staffing for the day was not more than any normal day, no riot gear and the Capitol Police actually turned down offers of assistance while the building was being overrun.

Some of my Republican friends attribute the preparation failure to the fact that the US intelligence operations always seem to be fighting the last war and have never taken white domestic terrorism seriously. The incompetence side of Hanlon's razor. That's the side of Hanlon's razor that I normally agree with. Capitol Police Chief Steven Sund gave his resignation following the riots, but defended the police response to the violence. He said officers' actions were "*heroic given the situation they faced.*" Yeah, I'm not talking about the individual police - I'm wondering about the police leadership plan that left those officers in that situation. Sund stated that they had a "robust plan to deal with First Amendment protests" but that this was a riot, not a First Amendment protest. So they  apparently had no plan to deal with any violence whatsoever.

There is incompetence and there is seriously gross incompetence when you are trying to apply Hanlon's razor. I, and many of my more left leaning friends, can't believe that the leadership was this grossly incompetent and so fall on the malice side of Hanlon's razor in this particular instance.

Let's talk about "useful idiots" for a moment. It generally refers to people who are naive or unwitting allies of a more ruthless person. According to [quoteinvestigator.com](https://quoteinvestigator.com/2019/08/22/useful-idiot/), it may be a little hard to determine the first usage of the phrase because the concept was expressed by quite a few people in Europe, so you could potentially choose which translation of a phrase you wanted. In 1946 Bogdan Raditsa in Yugoslavia became disillusioned by the communist government and thought his decision to join have been naive and misguided. At that time he used the phrase "Useful Innocents" (or "useful fools" depending on your translation from Serbo-Croat) to describe himself. Lenin refers to "simpletons" who were nominally socialists but really were accomplices of his enemies - but in context his usage doesn't quite mean the same. The San Francisco Examiner in 1948 published an article from the International News Service about the situation in Italy at the time:

"*Italian Interior Minister Mario Scelba announced tonight that the Italian elections will be called off “if individual liberty is endangered.” Scelba said he favored reorganization of the Italian police to enable policemen to guarantee freedom of the ballot. He called Socialist Leader Pietro Nenni, who is co-operating with the Communists, the “No. 1 useful idiot assisting Communist aspirations to control Italy.”*"

Regardless of the history of the phrase, the concept is well know and used in business, politics and everyday life when Person A uses Person B to accomplish something and Person B doesn't really understand the consequences nor the fact that they are potentially cannon fodder.

In the case of the insurrection at the Capitol building, there were two potential groups of useful idiots. First, the badly understaffed, underequiped and under briefed individual Capitol Police officers placed by their management in an untenable situation. Second, the crowd itself, egged on by the President.

Tinfoil hat time?

- Assume Capitol Police leadership are not incompetent. Everyone could see this coming. So complicity at the top with the White House?

- It seems as if Trump's firing Defense Secretary Esper and clearing out the top levels of the Pentagon laid the foundations for the ability to delay or neuter National Guard presence. Fortunately Pence and the Army Secretary took it upon themselves to make something useful happen.

- I've seen claims in the media that DC Mayor Bowser's letter to the Administration should be interpreted as rejecting federal assistance. I think the better reading is that she wanted assistance only when she asked and not unidentified "federal agents" wandering around snatching people off the street as happened in the BLM protests in both DC and Portland with no control when not requested.

- There seems to have been at least three different groups among the rioters. (a) Trump supporters who believed that he had a plan and was directing them to go to the Capitol and enter the building, but were directionless on what to do after that, (b) Trump supporters who thought that maybe they could pull off a Gretchen Whitmer style capture of Pence and Congressional members  and (c) professionals (domestic and/or foreign) who went directly to member offices intent on stealing data or planting malware. With respect to group B, what were they going to do with them.

Was that gallows the protestors set up really intended to be used or just a prop? There were certainly videos shown with rioters shouting "Hang Pence". Mob mentality is scary. Any one individual probably would not have done anything. But as Terry Pratchett said "*The IQ of a mob is the IQ of its most stupid member divided by the number of mobsters.*" One wrong move and things could have turned very deadly.

There are also indications that there were off duty police officers and members of the military among the rioters who flashed their ID badges and cards to Capitol police. If true, these could fit into any of the above categories. See e.g. [2 Seattle Police officers reported to be in DC on Wednesday](https://www.king5.com/article/news/local/seattle/seattle-police-investigating-reports-that-2-officers-were-in-dc-during-us-capitol-riots-wednesday/281-76b6126a-e839-44e1-8cd2-a6ea82160089).

There are multiple reports of ziptie handcuffs being seen. A reporter apparently told CNN that in one of the pictures, the zipties were stolen from wherever the emergency gas hoods were kept. It remains to be seen whether is any "excuse" for people such as the individual holding zipties in the pictures in [this article](https://www.usatoday.com/story/news/politics/2021/01/08/zip-ties-tn-thin-blue-line-patch-riot-terrorism-experts/6605206002/). The person in these pictures has been identified, is from nashville and apparently brought his mother to the "event". She is overheard in one of the videos saying "I dunno hwere I'm going, what I'm doing, just folliwing like a sheep here."

- It is clear that Trump incited the rally crowd to "storm" the Capitol, but everything was carefully left ambiguous. If they succeeded in disrupting the count, he could claim he was still President. If they just made a lot of noise, hey, 1st amendment rights. If they actually harmed Pence and members of Congress, then he could declare martial law, ride to the rescue and claim continuing right to rule as a political necessity. If "riding to the rescue" meant that a lot of the rioters got killed, not Trump's problem either. I don't know if groups a and b realize they were being set up, aka "Useful Idiots". Was this more [Children's Crusade](https://en.wikipedia.org/wiki/Children%27s_Crusade), Bay of Pigs light or [Reichstag fire](https://www.history.com/topics/germany/reichstag-fire) moment?

Unfortunately, many conservative social media outlets are twisting the facts and claiming this was a false flag Reichstag fire by the left to seize control. Their belief in a stolen election is stronger than that fact that 1+27 does not equal 2. And, no, I'm not going to link to them.

UPDATE: Suggest reading [Months ahead of Capitol riot, DHS threat assessment group was gutted](https://abcnews.go.com/US/months-ahead-capitol-riot-dhs-threat-assessment-group/story?id=75155673)

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
