---
title: "That which can be destroyed by the truth should be?"
date: 2020-10-11T14:18:19-04:00
publishDate: 2020-10-11T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Author Pat Hodgell in her book Godstalk has a character say "That which can be destroyed by the truth should be." I think we can probably all agree that often little white lies can keep relationships from blowing up, so maybe the statement is too broad. But during the disputes about fake news and lies on Facebook and Twitter, maybe we should look at the corollary: "That which is created by lies shouldn't be".

There is a huge difference between how continental Western Europe and the US view freedom of speech. I find the US hardcore view becoming more and more difficult to justify when it comes to things like hate speech. Oversimplifying things greatly, in the US the government can't do anything unless you are falsely yelling fire in a movie theater and people are likely to die in the stampede for the exits. (And no, I am not interested in arguing the technical legal nuances here. Even in the US there is some limit to free speech.) The question is how far can deliberate falsehoods be pushed, polarizing society and leading to death, injuries or property damage?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
