---
title: "Where is Canada?"
date: 2020-12-08T14:18:19-04:00
categories: ['posts']
tags: ['geography']
showToc:  "true"
author: "Peter Hiltz"
---
It might seem like a silly question, but where is Canada? I mean, when you ask the intertubes for the latitude and longitude of the geographic center of Canada, you get really different answers.

[Maps of the World](https://www.mapsofworld.com/lat_long/canada-lat-long.html) says that Canada's latitude and longitude is 60° 00' N and 95° 00' W. If I drop that into Google maps, I get a point on the line between Manitoba and Nunavut about 5 miles west of Hudson Bay. The same page also says that Canada is located on the geographic coordinates of 62.2270° N latitude and 105.3809° W longitude in North America. That second location is in the Northwest Territories, about 150 miles north of Saskatchewan border and maybe 70 miles west of Nunavut. Those two "geographically central" locations are roughly 380 miles apart. Yeah, I know Canada is a big country.

Wikipedia's article [Geography of Canada](https://en.wikipedia.org/wiki/Geography_of_Canada) says 60°00′N 95°00′W, matching one of the two numbers from Maps of the World.

[Geodatos.net](https://www.geodatos.net/en/coordinates/canada) says that Canada is at 56.1304° N 106.3468° W. According to Google maps, that puts it just north of the middle of Saskatchewan.

Wikipedia's entry [Centre of Canada](https://en.wikipedia.org/wiki/Centre_of_Canada) says that the mid point of the extremities of the Canadian landmass is 62°24′N 096°28′W. Now we are back in Nunavut about 100 miles north of Manitoba and 50 miles west of Hudson Bay.

The distance between Wikipedia's location and Geodatos' location is 554 miles as the Canada goose flies.

I asked in a geography social media forum and was told that all those results are valid since they all are in Canada and there are lots of ways of calculating the central longitude and latitude of an area. Unfortunately, not a single one of these websites bother to say how they define the geographic center.

I guess I'm not passing geography today.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
