---
title: "Irregular Verbs/Noun"
date: 2021-04-02T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Courtesy of [Paul](https://www.antipope.org/mt/mt-cp.cgi?__mode=view&blog_id=1&id=6931) in a comment on [Charlie Stross' blog](https://www.antipope.org/charlie/blog-static/2021/04/official-announcement-april-fo.html#comments).

- "I know the facts."

- "You have opinions."

- "He's biased."

- "They've been brainwashed."

Like everyone else, I occasionally need to be reminded of my own biases and preconceptions. As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.


