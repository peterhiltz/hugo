---
title: "Don't Track Me Like I Track You: Facebook Edition"
date: 2020-10-27T14:18:19-04:00
categories: ['posts']
tags: ['privacy']
showToc:  "true"
author: "Peter Hiltz"
---
Facebook, one of the leading practitioners of [surveillance capitalism](https://news.harvard.edu/gazette/story/2019/03/harvard-professor-says-surveillance-capitalism-is-undermining-democracy/), has apparently [threatened](https://www.cnn.com/2020/10/24/tech/facebook-nyu-political-ad-data/index.html) New York University researchers for tracking Facebook's political ads. The NYU researchers built a tool that allows 6,500 volunteers to keep track of what ads Facebook is showing them. The legal theory behind Facebook' threat is that Facebook's terms of service say that you cannot use automated bulk collection of data from its platform and this tool constitutes automated bulk collection. In the words of reddit.com commenter dungone "*Facebook is basically saying that their terms of service forbid their users from looking at their ads and remembering what they saw.*"

According to [BuzzFeed News](https://www.buzzfeednews.com/article/craigsilverman/facebook-biden-election-ads), the NYU researchers discovered that Facebook was not labeling all political ads to show who had paid for them as [its own disclosure rules have required since 2018](https://www.theverge.com/2018/5/24/17389834/facebook-political-ad-disclosures-united-states-transparency). The unlabeled ads included ads for and against Biden which were treated as "paid partnerships", sort of sponsored posts that magically are not sponsored ads, and therefore were not included in the Facebook Ads Library. The researchers don't think that it is possible for Facebook to label every political ad, but the researchers say that about 8.6% of ads run so far in 2020 did not have the disclosure. I haven't done any research to see if there are studies on whether disclosure actually changes the impact on the reader.

Facebook has threatened legal action if NYU does not stop the research by November 30, but Laura Edelson, one the NYU researchers has said they have no intention of stopping the project.

Setting aside the "I can track you, but you can't track me" position that Facebook is taking, the political landscape of this election and the disinformation that is being spread makes this a critical question. Ramya Krishnan, an attorney with the Knight First Amendment Institute at Columbia University, told the AP that it was “alarming” that “Facebook is trying to shut down a tool crucial to exposing disinformation in the run up to one of the most consequential elections in U.S. history.”

It does make you wonder about Facebook's statement that [it will not accept ads in the last week before the election](https://www.theverge.com/2020/9/3/21419258/facebook-political-ads-deadline-announcements-voter-registration-mark-zuckerberg). I'm not a Facebook user (although I technically have an account created for me by my daughter) so maybe some Facebook users will tell me whether Facebook manages to keep to this promise.

The Wall Street Journal reported that [Facebook is reportedly bracing for US election unrest](https://www.wsj.com/articles/facebook-prepares-measures-for-possible-election-unrest-11603651659?mod=djemalertNEWS) and has deployed "*internal tools designed for what it calls "at-risk" countries to slow the spread of viral content and lowering the bar for suppressing potentially inflammatory posts.*" Zuckerberge [told company employees](https://www.buzzfeednews.com/article/ryanmac/zuckerberg-speaks-qanon-holocaust-denial-employee-meeting) that a decisive victory for either candidate could be helpful in averting the risk of violence or civil unrest in the elections wake. Does anyone else get the feeling that social media companies are today's arms dealers selling to both sides in a war?

As a side note, I was amused that The [Hill's article on the subject](https://thehill.com/policy/technology/technology/522609-facebook-asks-academics-to-stop-using-tool-in-micro-targeting-ad) has a headline of "Facebook **asks** academics to stop using tool in micro-targeting ad research" but the first line says "Facebook is **demanding** that hat researchers at New York University disable a tool used to collect information..." Come on people. There is a difference between "ask" and "demand". Words are supposed to be your profession.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
