---
title: "Definition Oriented v. Culture Oriented"
date: 2021-02-10T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Shirley pointed me towards an interesting article entitled [Why You Can't Understand Conservative Rhetoric](https://weeklysift.com/2021/02/08/why-you-cant-understand-conservative-rhetoric/). I'll change the terminology slightly to "Definition Oriented People" and "Culture Oriented People".

Think of the word "red" as in the color. A physicist or a graphic designer may have an exact definition of what that word means. I can point to something and say it is red and something else is not red (examples), but I wouldn't be able to actually define "red". Similarly I or others may have a specific meaning or definition of "justice", but others have a more amorphous understanding of it.

Now think about a culture that believes in both "He who hesitates is lost" and "Look before you leap". From a definition or logical standpoint, they seem to contradict each other. However both are good advice in different circumstances. You just have know which circumstances your are in and that will determine which aphorism is applicable. You may think of it as culturally oriented people decide what story they are in and what role they play, and then the "appropriate understanding" comes from that story. From this standpoint, it isn't "hypocrisy" because different rules apply in different situations.

Think about the story of "Jack and the Beanstalk".

"*... and then Jack chopped down the beanstalk, adding murder and ecological vandalism to the theft, enticement and trespass charges already mentioned, but he got away with it and lived happily ever after without so much as a guilty twinge about what he had done. Which proves that you can be excused just about anything if you're a hero, because no one asks inconvenient questions.*" Terry Pratchett "The Hogfather".

If Jack is a member of your culture, then he is a hero. If the giant is a member of your culture, then Jack is a criminal. It's US v. Them Land and the rules apply differently depending on which side of the story you view yourself and being on.

The article also posits a different framing, but again in cultural terms. There are the "old norms" and then proposed "new norms". In this sense, conservatives are defending the old norms against the new norms which are challenging and threatening. The old norms "worked" and society as a whole "worked" and the proposed new norms are seen as causing uncertainty and unease and therefore are to be rejected. From this standpoint the definition oriented people are saying that the cultural norms do not work for XYZ individuals or ABC sub-groups. The culture oriented group feels that those individuals or subgroups are not on the same side in the same story and, therefore, different narratives apply. They may think there might be something to the new norms, but they are an overreaction as applied in different stories.

What I am suggesting is that definition oriented people yelling "hypocrisy" at culture oriented people will not actually move the needle if they want to change behavior. They need to reframe the narrative and tell the story differently in a way that shows XYZ and ABC persons should be considered part of the same cultural group as the listener. If you are drowning, do you care what color hand reaches down to save you? This is difficult. Overt reinforcement of the "otherness" of the hand color may jar a culture oriented listener out of the story context you want and into a problematic story context.

You see this a bit in the different approaches to racism in France v. the US. The US position is that you don't know if you have a problem unless you measure it and you don't know if you are succeeding unless you have metrics to measure against. The French position is that the very act of distinguishing groups breaks their societal attempt to get people to think of everyone as French. I think there is some truth in both sides, but academics on both sides seem to have dug in their heels and view the other side as idiots.

Interested in your thoughts.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
