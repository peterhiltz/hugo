---
title: "The Spreadable Butter Assumptions"
date: 2020-12-19T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
You would think after all the years we've been married that we would know each other's little ways and preferences. That is by and large true, but the spreadable butter question had never come up before. Taking a step back, we normally use some type of easily spreadable margarine and real butter is for cooking and special occasions. Just prior to the COVID mandated cocooning, we had been watching the Great British Bake Off and Shirley started baking a couple times a week in the afternoon. Fresh bread right out of the oven seems to suggest using real butter instead of margarine, so no surprise to get butter out of the refrigerator and thinly slice the cold (and very solid) butter.

Last night bread baking was occurring during dinner (red flag alert - a change in historic processes), we had butter at dinner and I was responsible for kitchen dinner cleanup. Peter's brain is now on autopilot; dishes in dishwasher, leftovers put into appropriate plastic containers and into refrigerator, condiments and other items put away, etc. Two hours later, the bread comes out of the oven and the question is raised - "You knew bread was in the oven, why did you put the butter in the refrigerator? If you had left it out it would have been spreadable." ... Fortunately issues like this are quickly resolved by a pillow fight that my brother would not recognize (him being used to my hitting him with a turkey feather pillow weighing several pounds). As Shirley says "I knew you were oblivious when I married you."

All of this goes to the point that even in the most mundane circumstances, "living in the moment" means being "fully aware of the moment" (which would have included the baking going on around me) and not on autopilot.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
