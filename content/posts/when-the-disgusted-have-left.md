---
title: "When The Disgusted Have Left "
date: 2020-09-17T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Apparently there is an old French proverb that says "When the disgusted have left, only the disgusting remain". Painful, but true. There does become a point where leaving someplace because you cannot continue to work there and keep your integrity creates the danger that the place will get worse. However, if you have been sidelined and are no longer effective in trying to keep some integrity, morals, ethics there, then staying doesn't help and maybe society is better placed for change from the outside.

There is, of course, the hope that if current leadership changes, you can return to effectiveness.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
