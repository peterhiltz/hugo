---
title: "Read the words"
date: 2020-09-22T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Trying to have a rational conversation is difficult enough without people fixating on a single word in a sentence and then letting their lizard hind brain take control. A recent case in point was an article in the Washington Post about political scientists and sociologists concerned that the US is backsliding into autocracy. [Link](https://www.washingtonpost.com/business/2020/09/18/united-states-is-backsliding-into-autocracy-under-trump-scholars-warn/). One of the people commenting immediately started saying that this was sensationalist nonsense to sell papers - the President does not have autocratic power and the people around him are managing him very well so as to ensure that he never gets that power.

All the respondents were busy arguing or agreeing with the substance of his comment. In this post I want to focus on the fact that he wasn't addressing the studies at all. The studies weren't claiming that the President was an autocrat **today**. The studies were talking about different factors that **led** to countries moving toward that direction. This wasn't even a case of a misleading headline, let alone not reading the actual article before commenting. Either he intentionally mis-characterized the article or he fixated on the word "autocracy" and his own emotional reaction re-wrote the story for him. The first alternative is impossible to engage with simply because they are being dishonest. The second alternative is difficult to engage with because you have to get them to calm down and slowly escape the fight or flight reaction.

By the way, the studies referenced in the article can be found at [Varieties of Democracy](https://www.v-dem.net/media/filer_public/f9/08/f908eb53-c0e2-40f0-9294-e067537d8f0b/v-dem_policybrief_5_2016.pdf) and [Bright Line Watch](http://brightlinewatch.org/bright-line-watch-august-2020-expert-survey/).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
