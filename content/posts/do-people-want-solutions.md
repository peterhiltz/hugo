---
title: "Do People Actually Want Solutions?"
date: 2021-06-12T14:18:19-04:00
categories: ['posts']
tags: ['politics','life']
showToc:  "true"
author: "Peter Hiltz"
---
It seems like a lot of people would have no meaning to life if they didn't have someone to hate. Fortunately I hate everyone so a lot of targets would have to go away.

From time to time friends in the international tax arena call and update me on the latest goings on. Usually it is fairly clear that most of the parties at the table are engaged in political posturing but do not actually seem to want to resolve the issues. One of the advantages of retirement is that I don't feel responsible anymore to the universe for solving the problems that other people don't want solved.

I can already hear the claims that it is only the "other guys". Get real. For a start, business needs to stop whining, actually show facts and think about long term benefits to society instead of a six month ROI. Similarly, progressives need to stop deliberately conflating revenue with net income in their talking points.

I was asked a few weeks ago about suggestions for experts to be on a panel on international taxation, but it couldn't be "industry people". Now, there are reasons for not putting industry people on panels - mostly because they will just stick to their CFO's agenda. At the same time my definition of "expert" is someone who actually understands the low level details as well as the big picture. That immediately eliminates almost every academic in the field because they don't understand the low level details.Because they don't understand the low level details, they don't understand the data they are looking at and therefore don't understand the big picture either. Civil society and political types generally combine the worst of both groups. At that point maybe empty chairs are best.

Why yes, I've been told I'm very cynical. But I understand how Diogenes felt wandering the streets of Athens looking for an honest man.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
