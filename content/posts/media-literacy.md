---
title: "Media Literacy Index"
date: 2024-04-08T14:18:19-04:00
categories: ['posts']
tags: ['politics']
toc:  "true"
author: "Peter Hiltz"
---
The Open Society Institute attempted to develop a predictor of media literacy [in 2019 here](https://osis.bg/wp-content/uploads/2019/11/MediaLiteracyIndex2019_-ENG.pdf). The intent was to develop ideas for resiliency against fake news, post-truth, etc and offset the diminishing public trust and severely polarized politics. It's an interesting idea but I think it needs more development. This post is a summary of my **overthinking** of their predicator. CAUTION: Long (About 8,200 words). No, I don't expect anyone to read it.
<!--more-->
## OSI methodology
The OSI methodology was to use the following indicators and weights:

| **Indicators**                                            | **Weight** |
| --------------------------------------------------------- | ---------- |
| **Media Freedom indicators**                              |            |
| [Freedom of the Press score by Freedom House](https://freedomhouse.org/freedom-press-research-methodology)               | 20%        |
| [Press Freedom Index by Reporters without Borders](https://rsf.org/en/methodology-used-compiling-world-press-freedom-index-2023?year=2023&data_type=general)          | 20%        |
| **Education indicators**                                  |            |
| PISA score in reading literacy (OECD)                     | 30%        |
| PISA score in scientific literacy(OECD)                   | 5%         |
| PISA score mathematical literacy (OECD)                   | 5%         |
| Share of population (%) with university degree (Eurostat) | 5%         |
| **Trust**                                                 |            |
| Trust in others (Eurostat)                                | 10%        |
| **New forms of participation**                            |            |
| E-participation Index (UN)                                | 5%         |

My immediate reaction to the methodology was to question why freedom of the press was 40% of the total score. That seems to imply that fake news comes from the government, and we have all seen fake news coming from the private sector just as much as from the government.  The more I thought about it, the more I questioned the implicit assumption that the reader/watcher/listener is critically participating. If the reader/watcher/listener is just connecting to sources that confirm their own bias, none of this is useful.

In the most recent report [2023 here](https://osis.bg/wp-content/uploads/2023/06/MLI-report-in-English-22.06.pdf), they do seem to realize some of this, but there was a weird caveat in the discussion and the methodology remained the same as above:

> "But there are caveats. For example, from a Central or Eastern European perspective (and Central Asian if we extend the geographical scope), some governments may not be addressing but rather generating disinformation themselves. Same with many traditional media – instead of being bulwarks against disinformation the controlled media peddle it. E.g. while the focus has been on social media there is a risk to overlook the old-fashioned disinformation and misinformation in the print and online media, controlled by government, political, business, or external influences. Thus social media, as controversial as it is in disinformation, remain at the same time a critical venue for access to information and opinion. Think only of the rise of the “digital autocracies”2 around the world as a reminder of the importance of free internet. Misconstrued and rushed regulation of disinformation in social and traditional media may well be a disaster undermining democracy instead of strengthening it."

They also picked up the problem of expanded use of software tools like CHAT GPT to promulgate misinformation:

> "Chat GPT, followed by other tools, was unleashed upon the unsuspecting public to great enthusiasm by the rank-and-file users. However, experts were quick to point out that AI is likely to worsen the problem of disinformation rather than improve it."

They did flag that:

> "it was found out that concern for misinformation was higher among people with higher education levels. The authors assume that in this case younger and therefore more educated people with higher digital skills, have higher concern of misinformation."


### Education (45% weight)
Their explanation of the Education Indicators:

>"Education. Education is another essential component. For example, Finland’s government considers the strong public education system as a main tool to resist information warfare against the country and “widespread critical thinking skills among the Finnish population and a coherent government response” is thought to be a key element for resisting fake news campaigns. In general, it is thought that people who are more educated are more informed, are better critical thinkers and are less likely to fall into the trap of a fabricated news. But there is also a more complex psychological mechanism at work. A study by Jan-Willem van Prooijen on conspiracy theories has found that more educated people feel more in control of their lives, do not believe so much in easy solutions and have more analytical skills7. The included indicators for education are PISA reading performance, PISA science and PISA mathematics components with the highest importance being attributed to reading in this case. PISA provides picture not only of pupils’ achievements, but also the overall outcomes of the educational system in a country. The indicator “tertiary education enrolment” is also included as education indicator, although with smaller weight."

### Media Freedom (40% weight)
Their explanation of the Media Freedom Indicators is:

> "Media freedom is an essential indicator. The rise of fake news amidst the severely fragmented media landscape or outright weak and controlled media in some countries has accompanied the deterioration of the public and political debates and the overall quality of the democratic process. In the model, suggested in this brief, two commonly accepted indices are used – of Freedom House and of Reporters without Borders - to measure media freedom. In this respect, a certain East-West divide can be observed in the debates."

My initial thought was that I don't disagree that media freedom is important, but I thought making it 40% of the weight for something called "The Media Literacy Index" was overweighting it. Particularly since I thought the indicator was more measuring freedom from government control rather than also taking into account how the private sphere controls the messaging.

Interestingly, while the Media Literacy Index for 2023 ranked the US 17th out of 47 overall, if we just look at "Media Freedom", Freedom House ranked the US 20th out of 47 and Reporters without Borders ranked the US 26th out of 47.

After having looked at the methodologies and conclusions of the two reporting groups, I still think it is overweighted in the Index, but not as much as I initially thought. Score one for learning?

#### Freedom House Scoring Methodology
I was unable to find the actual scores with respect to freedom of the press on Freedom House's webside. I was able to find the actual questions used.

Lets look at the questions used in the  [Freedom House Methodology](https://freedomhouse.org/freedom-press-research-methodology):
A. LEGAL ENVIRONMENT (0–30 POINTS)

1.  Do the constitution or other basic laws contain provisions designed to protect freedom of the press and of expression, and are they enforced? (0–6 points)

- Does the constitution contain language that provides for freedom of speech and of the press?
- Do the Supreme Court, attorney general, and other representatives of the higher judiciary support these rights?
- Does the judiciary obstruct the implementation of laws designed to uphold these freedoms?
- Do other high-ranking state or government representatives uphold legal protections for media freedom?
- Do high-level government leaders contribute to a hostile environment for the press, for example by engaging in repeated animosity toward or negative verbal rhetoric against the media?
- Are crimes that threaten press freedom prosecuted vigorously by authorities?
- Is there implicit impunity for those who commit crimes against journalists?

2.  Do the penal code, security laws, or any other laws restrict reporting and are journalists or bloggers punished under these laws? (0–6 points)

- Are there restrictive press laws?
- Do laws restrict reporting on ethnic or religious issues, national security, or other sensitive topics?
- Are penalties for “irresponsible journalism” applied widely?
- Are restrictions on media freedom clearly defined, narrowly circumscribed, and proportional to a legitimate aim?
- Do the authorities restrict or otherwise impede legitimate press coverage in the name of national security interests?
- Are journalists or media owners regularly prosecuted or jailed as a result of what they write or broadcast?
- Are writers, commentators, or bloggers subject to imprisonment or other legal penalty for accessing or posting material on the internet?
- Is there excessive pressure on journalists to reveal sources, resulting in punishments such as jail sentences, fines, or contempt of court charges?

3.  Are there penalties for libeling officials or the state and are they enforced? (0–3 points)

- Are public officials specially protected under insult or defamation laws?
- Are insult laws routinely used to shield officials’ conduct from public scrutiny?
- Is truth a defense to libel charges?
- Is there a legally mandated “right of reply” that overrides independent editorial control?
- Is libel a criminal rather than merely a civil offense?
- Are journalists or other news providers prosecuted and jailed for libel or defamation?
- Are excessive monetary fines routinely imposed on journalists or media outlets in civil libel cases in a partisan or prejudicial manner, with the intention of bankrupting the media outlet or deterring future criticism?

4.  Is the judiciary independent and do courts judge cases concerning the media impartially? (0–3 points)

- Are members of the judiciary subject to excessive pressure from the executive branch?
- Are the rights to freedom of expression and information recognized as important among members of the judiciary?
- When judging cases concerning the media, do authorities act in a lawful and non-arbitrary manner on the basis of objective criteria?
- Are contempt of court charges filed against journalists who attempt to cover court proceedings or cases?
- Does the judiciary frequently impose gag orders or bans on coverage of legal cases?

5.  Is Freedom of Information legislation in place and are journalists able to make use of it? (0–2 points)

- Are there laws guaranteeing access to government records and information?
- Is there enabling legislation and/or an administrative framework in place to make such laws usable in practice?
- Are restrictions to the right of access to information expressly and narrowly defined?
- Are journalists able to secure public records through clear administrative procedures in a timely manner and at a reasonable cost?
- Are public officials subject to prosecution if they illegally refuse to disclose state documents?

6.  Can individuals or business entities legally establish and operate private media outlets without undue interference? (0–4 points)

- Are registration requirements to publish a newspaper or periodical unduly onerous or are they approved/rejected on partisan or prejudicial grounds?
- Is the process of licensing private broadcasters and assigning frequencies open, objective, and fair?
- Is there an independent regulatory body responsible for awarding licenses and distributing frequencies, or does the state control the allocation process?
- Does the state place extensive legal controls on the establishment of websites and ISPs?
- Do state or publicly funded media receive preferential legal treatment?
- Are nonprofit community broadcasters given distinct legal status?
- Are laws regulating media ownership impartially implemented?

7.  Are media regulatory bodies, such as a broadcasting authority or national press or communications council, able to operate freely and independently? (0–2 points)

- Are there explicit legal guarantees protecting the independence and autonomy of any regulatory body from either political or commercial interference?
- Does the state or any other interest exercise undue influence over regulatory bodies through appointments or financial pressure?
- Is the appointments process to such bodies transparent and representative of different interests, and do representatives from the media have an adequate presence on such bodies?
- Are decisions taken by the regulatory body seen to be fair and apolitical?
- Are efforts by journalists and media outlets to establish self-regulatory mechanisms permitted and encouraged, and viewed as a preferable alternative to state-imposed regulation?

8.  Is there freedom to become a journalist and to practice journalism, and can professional groups freely support journalists’ rights and interests? (0–4 points)

- Are journalists required by law to be licensed, and if so, is the licensing process conducted fairly and at reasonable cost?
- Must a journalist become a member of a particular union or professional organization in order to work legally?
- Must journalists have attended a particular school or have certain qualifications in order to practice journalism?
- Are visas or exit permits for journalists to travel abroad delayed or denied based on the individual’s reporting or professional affiliation?
- Are journalists’ or bloggers’ professional actions or means of communication subject to either electronic or physical surveillance with the object of interfering in their work or ascertaining their sources?
- May journalists and editors freely join associations to protect their interests and express their professional views?
- Are independent journalists’ organizations and other advocacy groups dedicated to their interests able to operate freely and comment on threats to or violations of press freedom?

B.  POLITICAL ENVIRONMENT (0–40 POINTS)

1.  To what extent are media outlets’ news and information content determined by the government or a particular partisan interest? (0–10 points)

- To what degree are journalists subject to editorial direction or pressure from the authorities or from private owners?
- Is hiring, promotion, and firing of journalists done in a nonpartisan and impartial manner? Are journalists subject to job loss because of what they write or broadcast?
- Is media coverage excessively partisan, with the majority of outlets consistently taking either a pro- or antigovernment line?
- Does the government have editorial control over state-run media outlets, or is there a public-service broadcaster that enjoys editorial independence?
- Does the opposition have access to state-owned media, particularly during election campaigns? Do state-owned outlets reflect the views of the entire political spectrum or do they provide only an official point of view?
- Does the government attempt to influence or manipulate online content, for example through propaganda sites, paid commentators, or bots on social media?

2.  Is access to official or unofficial sources generally controlled? (0–2 points)

- Are the activities of government and other public institutions open to the press?
- Is there a “culture of secrecy” among public officials that limits their willingness to communicate with or grant access to journalists?
- Do authorities hold regular press conferences or other briefings to inform the media?
- Is access to officials granted equitably to all journalists regardless of their media outlet’s editorial line?
- Does the government influence or restrict access to unofficial sources (parties, unions, religious groups, etc.), particularly those that provide opposition viewpoints?

3.  Is there official or unofficial censorship? (0–4 points)

- Is there an official censorship body?
- Are publications or broadcast programs subject to pre- or postpublication censorship?
- Are outlets forcibly closed or taken off the air as a result of what they publish or broadcast?
- Are online news outlets, social-media platforms, specific webpages, or pieces of content blocked, filtered, or taken down, either by the authorities or by intermediaries under official pressure?
- Is access to foreign news sources censored or otherwise restricted?
- Are certain contentious issues—such as official corruption, the role of the armed forces or the political opposition, human rights, or religion—officially off-limits to the media?
- Do authorities issue official guidelines or directives on coverage to media outlets?

4.  Do journalists practice self-censorship? (0–4 points)

- Is there widespread self-censorship in the state-owned media? In the privately owned media?
- Are there unspoken rules that prevent a journalist from pursuing certain stories?
- Is there avoidance of subjects that can clearly lead to censorship or harm to the journalist or the institution?
- Is there censorship of or excessive interference in journalists’ stories by editors or managers?
- Are there restrictions on coverage by “gentlemen’s agreement,” club-like associations between journalists and officials, or traditions in the culture that restrict certain kinds of reporting?

5.  Do people have access to media coverage and a range of news and information that is robust and reflects a diversity of viewpoints? (0–4 points)

- Does the public have access to a diverse selection of print, broadcast, and internet-based sources of information that represent a range of political and social viewpoints?
- Are people able to access a range of local and international news sources despite efforts to restrict the flow of information?
- Do media outlets represent diverse interests within society, for example through community radio or other locally focused news content?
- Do providers of news content cover political developments and provide scrutiny of government policies or actions by other powerful societal actors?
- Is there a tradition of vibrant coverage of potentially sensitive issues?
- Do journalists or bloggers pursue investigative news stories on issues such as corruption by the government or other powerful societal actors?
- NOTE: When scoring this question, please take into account the level of penetration of different types of media, e.g., print, broadcast, internet, foreign.

6.  Are both local and foreign journalists able to cover the news freely and safely in terms of physical access and on-the-ground reporting? (0–6 points) [*Note: this question applies to conditions experienced by journalists, bloggers, or media outlets during the course of their work. See also note in B7.]

- To what extent are journalists harassed or attacked while attempting to gather news or cover events in person?
- Are certain geographical areas of the country off-limits to journalists?
- Does a war, insurgency, or similar situation in a country inhibit the operation of media?
- Do authorities require journalists working in danger zones to be “embedded”?
- Is there surveillance of foreign journalists working in the country?
- Are foreign journalists inhibited or barred by the need to secure visas or permits to report from or travel within the country?
- Are foreign journalists deported for reporting that challenges the authorities or other powerful interests?

7.  Are journalists, bloggers, or media outlets subject to extralegal intimidation or physical violence by state authorities or any other actor as a result of their reporting? (0–10 points) [*Note: This question applies to conditions experienced by journalists, bloggers, or media outlets as a result of their work. See also note in B6.]

- Are journalists or bloggers subject to murder, injury, harassment, threats, abduction, arbitrary arrest and illegal detention, or torture in retaliation for their professional activities?
- Do journalists face reprisals in the form of trumped-up criminal charges with no explicit link to their work, such as weapons possession, drug possession, or tax evasion?
- Do armed militias, organized crime, insurgent groups, political or religious extremists, or other organizations regularly target journalists in response to their work?
- Have journalists fled the country or gone into hiding or exile to avoid such repercussions?
- Do journalists under threat from nonstate actors receive adequate protection from state authorities?
- Have media companies been targeted for physical attack or for the confiscation or destruction of property?
- Are there technical attacks—such as hacking or distributed denial-of-service (DDoS) attacks—on news outlets’ websites or on social-media accounts that are used to disseminate news?

C. ECONOMIC ENVIRONMENT (0–30 POINTS)

1.  To what extent are media owned or controlled by the government, and does this influence their diversity of views? (0–6 points)

- To what extent do state-owned media dominate the country’s news and information system?
- Does the state have a monopoly on any news medium?
- Are there privately owned print, broadcast, or internet-based media outlets that carry their own news content?
- Do private news agencies provide content for print, broadcast, and online media?
- Do the state or public media enjoy editorial independence, and do they provide a range of diverse, nonpartisan viewpoints?
- NOTE: Consideration in the scoring should be given to the state/private balance in each medium, so that a country receives credit for a privately owned print sector, for example, even if there is a state monopoly on radio or television.

2.  Is media ownership transparent, thus allowing consumers to judge the impartiality of the news? (0–3 points)

- Is it possible to ascertain the ownership structure of private media outlets?
- Do media owners hold official positions in the government or in political parties, and are these links intentionally concealed from the public?
- Do the formal owners of media outlets have unofficial ties to other powerful actors that compromise the outlets’ objectivity?

3.  Is media ownership highly concentrated and does this influence diversity of content? (0–3 points)

- Are many news outlets owned or controlled by a few industrial or commercial conglomerates, whose resources allow them to suppress competition, limit diversity of content or viewpoints, and dominate the media landscape?
- Is there an excessive concentration of media ownership in the hands of private interests linked to state patronage or that of other powerful societal actors?
- Are there media monopolies, significant vertical integration (control over all aspects of news production and distribution), or substantial cross-ownership?
- Does the state actively and fairly enforce laws that limit concentration, monopolies, and cross-ownership?

4.  Are there restrictions on the means of news production and distribution? (0–4 points)

- Is there a monopoly on the means of production and distribution, such as newsprint supplies, internet service, or telecommunications infrastructure?
- Are there **private and nonstate** printing presses?
- Are distribution intermediaries (newspaper kiosks, transmitters, cable and satellite companies, internet service providers, mobile-phone carriers) able to operate freely?
- Does the **government** exert pressure on independent media through the control of distribution facilities?
- Is there seizure or destruction of copies of newspapers, radio or television transmitters, satellite dishes, or production equipment?
- Do the **authorities** engage in wholesale blackouts of internet or mobile service, or interfere with such service through deliberate throttling and artificially slow connections?
- Does geography or poor infrastructure (roads, electricity, etc.) limit dissemination of print, broadcast, internet, or mobile-based news sources throughout the country?

5.  Are there high costs associated with the establishment and operation of media outlets? (0–4 points)

- Are there excessive fees associated with obtaining a radio frequency, registering a newspaper, or establishing an ISP or website?
- Are the costs of purchasing paper, newsprint, or broadcasting equipment subject to **high additional duties**?
- Are media outlets **subject to excessive taxation** or other levies compared with other industries?
- Are there restrictions on foreign investment or non-investment foreign support/funding in the media?

6.  Do the state or other actors try to control the media through allocation of advertising or subsidies? (0–3 points)

- Are **state subsidies** for privately run newspapers, broadcasters, or websites allocated fairly?
- Do subsidies from private owners distort the market, or are they intended to drive the competition out of business?
- Is **government advertising** allocated fairly and in an apolitical manner, i.e., on the basis of market share?
- Do state or private advertisers use the threat of reduced ad spending or actual boycotts to influence editorial decisions?
- Do the authorities or nonstate actors pressure companies to withhold advertising from certain media outlets?

7.  Do journalists, bloggers, or media outlets receive payment from private or public sources whose design is to influence their journalistic content? (0–3 points)

- Do government officials or other actors pay journalists in order to cover or to avoid certain stories?
- Do journalists or media outlets accept payment to produce certain types of coverage (i.e. sponsored content, native advertising), and if so, do they clearly and lawfully identify such content?
- Are journalists often bribed?
- Are pay levels for journalists and other media professionals sufficiently high to discourage bribery?
- Do journalists or media outlets request bribes or other incentives in order to produce or withhold certain stories?

8.  Does the overall economic situation negatively impact media outlets’ financial sustainability? (0–4 points)

- Are media overly dependent on the state, political parties, big business, or other influential political actors for funding?
- Is the economy so depressed or so dominated by the state that a private entrepreneur would find it difficult to create a financially sustainable news outlet?
- Is it possible for independent news outlets to remain financially viable primarily by generating revenue from advertising or subscriptions?
- Do foreign investors or donors play an unusually large role in helping to sustain media outlets?
- Are private owners subject to intense commercial pressures and competition, causing them to tailor or cut news coverage in order to remain financially viable?

##### My comments
As you can see reading the questions, the majority still focus on government activity instead of both government activity and concentrated economic power activity. I also question whether the issue of transparency is actually relevant. So many people will only look at "their tribe" and it doesn't matter whether their tribe's leader is an SOB.

As pointed out in this [article by Favia Roscini for the Boston University Pardee School of Global Studies](https://sites.bu.edu/pardeeatlas/advancing-human-progress-initiative/back2school/how-the-american-media-landscape-is-polarizing-the-country/):

> "Both traditional news and social media channels exacerbate the spread of misinformation and therefore increase polarization among the U.S. public and political elite. Divisive cable news has real consequences, as it creates a vicious cycle in which journalists cover politics in a more polarized way, anticipating a more polarized audience’s tastes, and creating a more polarized political reality. With the help of algorithms and bots, social media plays an important role in spreading misinformation and in allowing the public to “become an active participant in creating and selectively amplifying narratives that shape realities.”[46] This profoundly affects U.S. democracy and society because “it is hard to bring the country together when each side has its own facts and attributions of responsibility.”

Here she cites "Renée DiResta, “It’s Not Misinformation. It’s Amplified Propaganda,” The Atlantic, October 9, 2021, [https://www.theatlantic.com/ideas/archive/2021/10/disinformation-propaganda-amplification-ampliganda/620334/](https://www.theatlantic.com/ideas/archive/2021/10/disinformation-propaganda-amplification-ampliganda/620334/)" and Darrell M. West, “The Role of Misinformation in Trump’s Insurrection" Brookings Institute, January 11, 2021,  [https://www.brookings.edu/blog/techtank/2021/01/11/the-role-of-misinformation-in-trumps-insurrection/](https://www.brookings.edu/blog/techtank/2021/01/11/the-role-of-misinformation-in-trumps-insurrection/).

> "If the public polarizes further, political and media elites will continue to fuel the division. Although party disagreement is an essential part of the political process, “polarization and animosity based on misconceptions of the other side threatens to misdiagnose problems, leading people to battle imagined enemies and distracting from opportunities for transformative reform.”

Here she cites Anne E Wilson, Victoria A Parker, Matthew Feinberg, “Polarization in the contemporary political and media landscape,” Current Opinion in Behavioral Sciences, Volume 34 2020, Pages 223-228, ISSN 2352-1546, [https://doi.org/10.1016/j.cobeha.2020.07.005](https://doi.org/10.1016/j.cobeha.2020.07.005).

#### [Reporters Without Borders Methodology](https://rsf.org/en/methodology-used-compiling-world-press-freedom-index-2023?year=2023&data_type=general)
Reporters without Borders uses the following definition of press freedom as guidance in preparing their methodology:

> "“Press freedom is defined as the ability of journalists as individuals and collectives to select, produce, and disseminate news in the public interest independent of political, economic, legal, and social interference and in the absence of threats to their physical and mental safety."

Specifically with respect to the US, Reporters without Borders ranks the US as 45th overall out of 180 countries:

> "While the mainstream media in the United States generally operate free from government interference, many popular news outlets are owned by a handful of wealthy individuals. In a diverse global media landscape, local news has declined significantly in recent years. A growing interest in partisan media threatens their objectivity, while public confidence in the media has fallen dangerously."

I did not find the exact questions, but they did indicate the following:

##### Political context

33 questions and subquestions

They aim to evaluate:

- the degree of support and respect for media autonomy vis-à-vis political pressure from the state or from other political actors;
- the level of acceptance of a variety of journalistic approaches satisfying professional standards, including politically aligned approaches and independent approaches;
- the degree of support for the media in their role of holding politicians and government to account in the public interest.

The US ranks 28th in the world on this indicator. The Reporters without Borders summary for the US in this area states:

> "After four years of President Trump constantly denigrating the press, President Biden signaled his administration's desire to see the US reclaim its global status as a model from freedom of expression, thus reinstating regular White House and federal agency press briefings. Despite these efforts, many of the underlying, chronic issues impacting journalists remain unaddressed by the authorities – including the disappearance of local news, the polarisation of the media or the weakening of journalism and democracy caused by digital platforms and social networks."

##### Legal framework

25 questions and subquestions

They aim to evaluate:

- the degree to which journalists and media are free to work without censorship or judicial sanctions, or excessive restrictions on their freedom of expression;
- the ability to access information without discrimination between journalists, and the ability to protect sources;
- the presence or absence of impunity for those responsible for acts of violence against journalists.

The US ranks 40th in the world on this indicator. The Reporters without Borders summary for the US in the area states:

> "There is an ongoing debate about reforming Section 230 of the Communications Decency Act, which states that social media companies and other internet hosts are not liable for content that third parties post on their platforms. There is a growing push to revisit the landmark Sullivan v. New York Times decision, which largely shields the media from defamation lawsuits. The PRESS Act, a federal shield law aimed at protecting journalists and their sources, was narrowly defeated in 2022. The US government continues to pursue the extradition of WikiLeaks publisher Julian Assange to face trial on charges related to the publication of leaked classified documents in 2010. Assange remains detained on remand in the UK, impacting both countries’ press freedom records. More than a dozen states and communities in the US have proposed or enacted laws to limit journalists’ access to public spaces, including barring them from legislative meetings and preventing them from recording the police."

##### Economic context

25 questions and subquestions

They aim to evaluate:

- economic constraints linked to governmental policies (including the difficulty of creating a news media outlet, favouritism in the allocation of state subsidies, and corruption);
- economic constraints linked to non-state actors (advertisers and commercial partners);
- economic constraints linked to media owners seeking to promote or defend their business interests.

The US ranks 24th in the world on this indicator. Reporters without Borders concludes:

> "Economic constraints drastically impact journalists working in the US, where more than 360 newspapers have closed since 2019 and where the biggest national newspapers continue to lose subscriptions. While some public media outlets, and radio stations in particular, have been able to offset this decline thanks to online subscription models, others have found ways to sustain growth through individual donations. Due to an unpredictable economy caused by the Covid-19 pandemic and shrinking advertising revenue, several major news outlets, including CNN, NBC, Buzzfeed, Vox and The Washington Post, announced waves of layoffs in 2022 and 2023. These economic conditions have affected smaller and local media outlets in particular, whose survival is increasingly threatened."

##### Sociocultural context

22 questions and subquestions

They aim to evaluate:

- social constraints resulting from denigration and attacks on the press based on such issues as gender, class, ethnicity and religion;
- cultural constraints, including pressure on journalists to not question certain bastions of power or influence or not cover certain issues because it would run counter to the prevailing culture in the country or territory.

The US ranks 43th in the world (tied with Liberia) on this indicator. Reporters without Borders concludes:

> "According to recent studies, there are unprecedented levels of distrust in the American media. The disinformation affecting American society has created an atmosphere where citizens no longer know who to trust. Online harassment, particularly towards women and minorities, is also a serious issue for journalists and can impact their quality of life and safety. "


##### Safety
- 12 questions and subquestions (⅔ of the safety score)

The questions concern journalists’ safety. For this purpose, press freedom is defined as the ability to identify, gather and disseminate news and information in accordance with journalistic methods and ethics, without unnecessary risk of:

- bodily harm (including murder, violence, arrest, detention, enforced disappearance and abduction);
- psychological or emotional distress that could result from intimidation, coercion, harassment, surveillance, doxing (publication of personal information with malicious intent), degrading or hateful speech, smears and other threats targeting journalists or their loved-ones;
- professional harm (for example, the loss of one’s job, the confiscation of professional equipment, or the ransacking of installations).

- 1 abuses score (⅓ of the safety score)

The US ranks an incredible 120th in the world on this indicator. Reporters without Borders concludes:

> "In the United States, in recent years, journalists have had to work in dangerous conditions and have faced an unprecedented climate of animosity and aggression during protests, where unprovoked physical attacks have occurred on clearly identified reporters. There is a troubling pattern of harassment, intimidation and assault on journalists in the field. In September 2022, Las Vegas Review-Journal reporter Jeff German was stabbed to death. Early 2023, a Clark County public administrator, who lost the primary election after German exposed his misconduct while in office, was charged with his murder. At the time of his death, German was working on a follow-up story. "


##### My comments
While I expected the questions to be more of a focus on government constraint against independent journalists, with just a few questions looking to economic centers of power (or private "wanna be powers behind the throne" of current or future governments) the conclusions in each stated area.


### Trust in People (10% weight)
Their description of the Trust in People indicator is as follows:

> "Trust is another important aspect. The entire post-truth phenomenon is accompanied by extremely high levels of mistrust towards institutions, mainstream media, politicians, experts. Conspiracy theories about the functioning of the world both reflect and bring about low level of confidence in existing institutions. The current model uses a related indicator - “Trust in People”. It measures the level of trust in society and “reflects people's perception of others' reliability”, according to the definition of OECD. As a rule, high level of trust is a hallmark of successful societies and a proxy for the development of civil society."

The [OECD Guidelines for Measuring Trust can be found here](https://www.oecd.org/governance/oecd-guidelines-on-measuring-trust-9789264278219-en.htm). 215 pages, but not like you had anything else to do, right?

But if you look at the breakdown of the indicators, it references a "World Values Survey". links for the data and questions for the World Values Survey can be found [here](https://www.worldvaluessurvey.org/WVSEVSjoint2017.jsp). Caution: the data file is 106 MegaBytes. I have to admit that while I have looked at the data, I am not competent enough in the area to actually draw or contest conclusions from the raw data.

[Analysis of the World Values Survey data](https://www.worldvaluessurvey.org/WVSContents.jsp) by by political scientists Ronald Inglehart and Christian Welzel asserts that there are two major dimensions of cross cultural variation in the world:

1. Traditional values versus Secular-rational values and
2. Survival values versus Self-expression values.

- Traditional values emphasize the importance of religion, parent-child ties, deference to authority and traditional family values. People who embrace these values also reject divorce, abortion, euthanasia and suicide. These societies have high levels of national pride and a nationalistic outlook.
- Secular-rational values have the opposite preferences to the traditional values. These societies place less emphasis on religion, traditional family values and authority. Divorce, abortion, euthanasia and suicide are seen as relatively acceptable. (Suicide is not necessarily more common.)
- Survival values place emphasis on economic and physical security. It is linked with a relatively ethnocentric outlook and low levels of trust and tolerance.
- Self-expression values give high priority to environmental protection, growing tolerance of foreigners, gays and lesbians and gender equality, and rising demands for participation in decision-making in economic and political life.

They provide the following examples of where countries seem to fall relative to these values:

1. Societies that have high scores in Traditional and Survival values: Zimbabwe, Morocco, Jordan, Bangladesh.
2. Societies with high scores in Traditional and Self-expression values: the U.S., most of Latin America, Ireland.
3. Societies with high scores in Secular-rational and Survival values: Russia, Bulgaria, Ukraine, Estonia.
4. Societies with high scores in Secular-rational and Self-expression values: Sweden, Norway, Japan, Benelux, Germany, France, Switzerland, Czech Republic, Slovenia, and some English speaking countries.

One study found here [On the Interpretation of World Values Survey Trust
Question: Global Expectations vs. Local Beliefs](https://docs.iza.org/dp9872.pdf) done in India concluded:

> "At one level our findings corroborate findings from Sapienza et al. (2013) as we find that WVS trust does capture expectations about others’ trustworthiness. However, there is an important difference. WVS trust question does well in capturing long term, globally determined, stable expectations about others’ behavior - such expectations are determined over the long run through cumulative life experiences (e.g. Malmendier and Nagel (2011) show how long run risk attitudes are shaped by personal experiences in times of macroeconomic instability). However, it is ill-suited to capture the locally affected fluctuations in beliefs about how others will behave."

...

> "An interesting aspect of our finding is people do not seem to generalize local experiences to the population at large. In our setting of course subjects were familiar with the population. It will be interesting to see whether subjects generalize local behavior to a wider pan societal contexts immediately in a setting where they are not familiar with the population. The process of generalization has important implications for how stereotypes are formed. This is an angle which needs further investigation."

That is both a little bit positive and a little bit scary. It seems to imply that at least in India, positive and negative local experiences will not change people's stereotypes of the general population. In other words, on the scary side, "I still hate X type of people, but you're one of the good ones" if someone has a good local experience. However the study only used a negative local experience, no positive local experiences, and asserted an untested hypothesis that "This negative belief shock was most likely temporary in nature and perhaps would have had little bearing on decisions outside the immediate context." I really wish they hadn't included that leap of faith in their conclusion.

A different study, [How much should we trust the World Values Survey trust question?](https://www.sciencedirect.com/science/article/abs/pii/S016517651200050X), notes that "Surveys of people’s attitudes have been weak predictors of people’s actual behaviors (Ajzen and Fishbein, 1977)". They concluded "We find strong support for the interpretation that the WVS Trust Question measures the same thing experimenters call “trust” in the lab. By contrast, we find no relationship between our measure of experimental trustworthiness and answers to the WVS Trust Question." Reading the study, it seems that people were more trusting in actual controlled experimental settings than they said they would be in answer to the survey questions.

Now consider the following picture:

![Girls sent home from McKinley High School for wearing slacks and blue jeans, Chicago 1946](/images/women-expelled-for-wearing-pants.jpeg "Girls sent home from McKinley High School for wearing slacks and blue jeans, Chicago 1946")

I suppose these young women violated the deference to authority and conformance with traditional family values (expressed as "women wear dresses"). Personally, I would have assigned it a stylistic choice and wondered why anyone would care.... Which would have caused them to need to do something more "outrageous" to affect my sensibilities. But exactly how this moves from "values" to "trust" I don't know.

I found the following quote interesting:

> "These maps indicate that the United States is not a prototype of cultural modernization for other societies to follow, as some modernization writers assumed. In fact, the United States is a deviant case, having a much more traditional value system than any other postindustrial society except Ireland. On the traditional/secular dimension, the United States ranks far below other rich societies, with levels of religiosity and national pride comparable with those found in some developing societies. The United States does rank among the most advanced societies on the survival/self-expression dimension, but even here, it does not lead the world. The Swedes, the Dutch, and the Australians are closer to the cutting edge of cultural change than the Americans [Source: Chapter 2 from Inglehart, R & C. Welzel. 2005. Modernization, Cultural Change and Democracy: The Human Development Sequence. New York: Cambridge University Press]. "

The General Social Survey in the US has been collecting data about trust attitudes since 1972. The data seems to indicate that both (a) US trust in government and (b) trust in each other is at a historic low. This is corroborated by data from the Pew Research Center which has been collecting data with respect in US government since 1958. This seems to match with data from Canada [Mass Polarization in Canada: What’s Causing It? Why Should We Care?](https://www.mediatechdemocracy.com/all-work/mass-polarization-in-canada-whats-causing-it-why-should-we-care) which found "The average feeling towards their principal opponent has declined from 40 degrees to 27 degrees since 1988. At the same time, feelings towards one’s own party have increased from a low of 52 degrees in 1997 to 71 degrees in 2019. "

You might also find these articles interesting on the subject of the relevance of social trust:

- [Social trust and the stringency of public policies against the COVID-19 pandemic](https://www.tandfonline.com/doi/full/10.1080/23745118.2024.2331156)
- [Trust in government moderates the association between fear of COVID-19 as well as empathic concern and preventive behaviour](https://www.nature.com/articles/s44271-023-00046-5)


## 2023 Results

### Expanded Media Literacy Index 2023
|Ranking (1-47)|Country| Scores (100-0)| Cluster|
|--| ------- | -- | -- |
|1 | Finland | 74 | 1|
|2 | Denmark | 73 | 1|
|3 | Norway | 72 | 1|
|4 | Estonia | 71 | 1|
|5 | Sweden | 71 | 1|
|6 | Ireland | 70 | 1|
|7 | Canada | 68 | 1|
|8 | Switzerland | 67 | 1|
|9 | Netherlands | 64 | 2|
|10 | Australia | 63 | 2|
|11 | Iceland | 62 | 2|
|12 | Belgium | 61 | 2|
|13 | Germany | 61 | 2|
|14 | Portugal | 60 | 2|
|15 | United Kingdom | 60 | 2|
|16 | South  Korea | 60 | 2|
|17 | USA | 60 | 2|
|18 | Austria | 59 | 2|
|19 | Czech Republic | 58 | 2|
|20 | Spain | 58 | 2|
|21 | France | 57 | 2|
|22 | Japan | 57 | 2|
|23 | Latvia | 55 | 2|
|24 | Slovenia | 55 | 2|
|25 | Lithuania | 54 | 2|
|26 | Luxembourg | 53 | 2|
|27 | Poland | 53 | 2|
|28 | Slovakia | 48 | 3|
|29 | Italy | 47 | 3|
|30 | Croatia | 45 | 3|
|31 | Malta | 45 | 3|
|32 | Israel | 42 | 3|
|33 | Hungary | 41 | 3|
|34 | Cyprus | 39 | 3|
|35 | Greece | 38 | 3|
|36 | Ukraine | 38 | 3|
|37 | Serbia | 33 | 4|
|38 | Moldova | 32 | 4|
|39 | Montenegro | 32 | 4|
|40 | Romania | 32 | 4|
|41 | Bulgaria | 31 | 4|
|42 | Turkey | 29 | 4|
|43 | Bosnia and Herzegovina | 24 | 5|
|44 | Albania | 23 | 5|
|45 | North Macedonia | 22 | 5|
|46 | Kosovo | 21 | 5|
|47 | Georgia | 20 | 5|

### Media Freedom Rankings 2023
|Media freedom ranking| Score| Country| Overall Rank|Overall Score|Cluster|
| ------------------- | ---- | ------ | ----------- | -----------| ------|
|1 | 32.6 | Norway | 3 | 72 | 1|
|2 | 30.8 | Denmark | 2 | 73 | 1|
|3 | 30.4 | Sweden | 5 | 71 | 1|
|4 | 30 | Finland | 1 | 74 | 1|
|5 | 29.2 | Estonia | 4 | 71 | 1|
|6 | 28.4 | Netherlands | 6 | 70 | 1|
|7 | 28.2 | Portugal | 14 | 60 | 2|
|8 | 27.8 | Switzerland | 8 | 67 | 1|
|9 | 27.2 | Iceland | 11 | 62 | 2|
|10 | 26.8 | Belgium | 12 | 61 | 2|
|11 | 26.6 | Netherlands | 9 | 64 | 2|
|12 | 26.4 | Luxembourg | 26 | 53 | 2|
|13 | 26.2 | Canada | 7 | 68 | 1|
|14 | 26.2 | Lithuania | 25 | 54 | 2|
|15 | 25.8 | Germany | 13 | 61 | 2|
|16 | 25 | Czech Republic | 19 | 58 | 2|
|17 | 23.4 | Austria | 18 | 59 | 2|
|18 | 23.2 | United Kingdom | 15 | 60 | 2|
|19 | 23.2 | Latvia | 23 | 55 | 2|
|20 | 23 | France | 21 | 57 | 2|
|21 | 23 | Slovakia | 28 | 48 | 3|
|22 | 22.4 | Australia | 10 | 63 | 2|
|23 | 22 | Spain | 20 | 58 | 2|
|24 | 21.8 | USA | 17 | 60 | 2|
|25 | 20.2 | Slovenia | 24 | 55 | 2|
|26 | 19.4 | Cyprus | 34 | 39 | 3|
|27 | 18.8 | South Korea | 16 | 60 | 2|
|28 | 18.2 | Italy | 29 | 47 | 3|
|29 | 17.8 | Japan | 22 | 57 | 2|
|30 | 17.8 | Malta | 31 | 45 | 3|
|31 | 16.4 | Croatia | 30 | 45 | 3|
|32 | 16.4 | Poland | 27 | 53 | 2|
|33 | 16.4 | Romania | 40 | 32 | 4|
|34 | 14.6 | Israel | 32 | 42 | 3|
|35 | 14.4 | Montenegro | 39 | 32 | 4|
|36 | 13.8 | Moldova | 38 | 32 | 4|
|37 | 13.6 | Kosovo | 46 | 21 | 5|
|38 | 12.2 | Bosnia and  Herzegovina | 43 | 24 | 5|
|39 | 12.2 | Bulgaria | 41 | 31 | 4|
|40 | 12 | Hungary | 33 | 41 | 3|
|41 | 11.2 | Serbia | 37 | 33 | 4|
|42 | 10.4 | Greece | 35 | 38 | 3|
|43 | 10.2 | Georgia | 47 | 20 | 5|
|44 | 9.8 | North Macedonia | 45 | 22 | 5|
|45 | 9 | Albania | 44 | 23 | 5|
|46 | 8.2 | Ukraine | 36 | 38 | 3|
|47 | 0 | Turkey | 42 | 29 |4 |

### Education Ranking
|Education ranking| Score| Country| Overall Rank|Overall Score|Cluster|
| ------------------- | ---- | ------ | ----------- | -----------| ------|
|1 | 32.65 | Finland | 1 | 74 | 1|
|2 | 32.5 | South Korea | 16 | 60 | 2|
|3 | 32.35 | Estonia | 4 | 71 | 1|
|4 | 32 | Canada | 7 | 68 | 1|
|5 | 30.55 | Ireland | 6 | 70 | 1|
|6 | 30.3 | Poland | 27 | 53 | 2|
|7 | 30.2 | Australia | 10 | 63 | 2|
|8 | 29.6 | Japan | 22 | 57 | 2|
|9 | 29.3 | Sweden | 5 | 71 | 1|
|10 | 28.95 | USA | 17 | 60 | 2|
|11 | 28.7 | Denmark | 2 | 73 | 1|
|12 | 28.7 | Switzerland | 8 | 67 | 1|
|13 | 28.5 | United Kingdom | 15 | 60 | 2|
|14 | 28.2 | Norway | 3 | 72 | 1|
|15 | 28 | Slovenia | 24 | 55 | 2|
|16 | 27.65 | Germany | 13 | 61 | 2|
|17 | 27.55 | Belgium | 12 | 61 | 2|
|18 | 27.4 | Spain | 20 | 58 | 2|
|19 | 27.2 | Netherlands | 9 | 64 | 2|
|20 | 26.55 | France | 21 | 57 | 2|
|21 | 26.2 | Portugal | 14 | 60 | 2|
|22 | 26.05 | Czech Republic | 19 | 58 | 2|
|23 | 25.9 | Austria | 18 | 59 | 2|
|24 | 25.15 | Latvia | 23 | 55 | 2|
|25 | 23.75 | Iceland | 11 | 62 | 2|
|26 | 23.2 | Lithuania | 25 | 54 | 2|
|27 | 23 | Italy | 29 | 47 | 3|
|28 | 23 | Croatia | 30 | 45 | 3|
|29 | 22.75 | Turkey | 42 | 29 | 4|
|30 | 22.5 | Hungary | 33 | 41 | 3|
|31 | 21.7 | Greece | 35 | 38 | 3|
|32 | 21.25 | Ukraine | 36 | 38 | 3|
|33 | 20.9 | Israel | 32 | 42 | 3|
|34 | 20 | Luxembourg | 26 | 53 | 2|
|35 | 19.45 | Slovakia | 28 | 48 | 3|
|36 | 18.55 | Malta | 31 | 45 | 3|
|37 | 15.8 | Serbia | 37 | 33 | 4|
|38 | 14.9 | Cyprus | 34 | 39 | 3|
|39 | 12.8 | Romania | 40 | 32 | 4|
|40 | 12.7 | Bulgaria | 41 | 31 | 4|
|41 | 12.45 | Moldova | 38 | 32 | 4|
|42 | 11.4 | Montenegro | 39 | 32 | 4|
|43 | 9.55 | Albania | 44 | 23 | 5|
|44 | 7.2 | Bosnia and Herzegovina | 43 | 24 | 5|
|45 | 5.95 | North Macedonia | 45 | 22 | 5|
|46 | 5.45 | Georgia | 47 | 20 | 5|
|47 | 1.05 | Kosovo | 46 | 21 | 5|

### Trust in People Ranking
|Trust in People ranking| Score| Country| Overall Rank|Overall Score|Cluster|
| ------------------- | ---- | ------ | ----------- | -----------| ------|
|1 | 9.5 | Denmark | 2 | 73 | 1|
|2 | 9.3 | Norway | 3 | 72 | 1|
|3 | 8.9 | Finland | 1 | 74 | 1|
|4 | 8.3 | Sweden | 5 | 71 | 1|
|5 | 8.3 | Ireland | 6 | 70 | 1|
|6 | 8.3 | Iceland | 11 | 62 | 2|
|7 | 7.7 | Switzerland | 8 | 67 | 1|
|8 | 7.7 | Netherlands | 9 | 64 | 2|
|9 | 6.9 | Austria | 18 | 59 | 2|
|10 | 6.8 | Australia | 10 | 63 | 2|
|11 | 6.6 | Canada | 7 | 68 | 1|
|12 | 6.1 | Germany | 13 | 61 | 2|
|13 | 6 | Spain | 20 | 58 | 2|
|14 | 5.9 | United Kingdom | 15 | 60 | 2|
|15 | 5.6 | USA | 17 | 60 | 2|
|16 | 5.2 | Estonia | 4 | 71 | 1|
|17 | 5.2 | Belgium | 12 | 61 | 2|
|18 | 5.2 | Japan | 22 | 57 | 2|
|19 | 5.2 | Luxembourg | 26 | 53 | 2|
|20 | 5.1 | South Korea | 16 | 60 | 2|
|21 | 5 | Lithuania | 25 | 54 | 2|
|22 | 4.7 | Ukraine | 36 | 38 | 3|
|23 | 4.5 | Italy | 29 | 47 | 3|
|24 | 4.5 | Мalta | 31 | 45 | 3|
|25 | 4.5 | Hungary | 33 | 41 | 3|
|26 | 4.4 | France | 21 | 57 | 2|
|27 | 4.3 | Slovenia | 24 | 55 | 2|
|28 | 4.2 | Poland | 27 | 53 | 2|
|29 | 4.1 | Israel | 32 | 42 | 3|
|30 | 4 | Latvia | 23 | 55 | 2|
|31 | 4 | Montenegro | 39 | 32 | 4|
|32 | 3.9 | Czech  Republic | 19 | 58 | 2|
|33 | 3.9 | Slovakia | 28 | 48 | 3|
|34 | 3.5 | Bulgaria | 41 | 31 | 4|
|35 | 3.4 | Portugal | 14 | 60 | 2|
|36 | 3.4 | Serbia | 37 | 33 | 4|
|37 | 3.3 | North  Macedonia | 45 | 22 | 5|
|38 | 3.3 | Kosovo | 46 | 21 | 5|
|39 | 3.1 | Croatia | 30 | 45 | 3|
|40 | 3.1 | Turkey | 42 | 29 | 4|
|41 | 2.9 | Moldova | 38 | 32 | 4|
|42 | 2.9 | Romania | 40 | 32 | 4|
|43 | 2.7 | Bosnia  and  Herzegovina | 43 | 24 | 5|
|44 | 2.6 | Georgia | 47 | 20 | 5|
|45 | 2.5 | Greece | 35 | 38 | 3|
|46 | 2.4 | Cyprus | 34 | 39 | 3|
|47 | 2 | Albania | 44 | 23 | 5|

### Ranking Across Indicators
|Rank |	Country|Freedom House|	Reporters Without Borders |	Reading Lit|	Sci Lit	| Math Lit	|Tertiary Enrollment|	Trust|	E-participation |Overall	|	Cluster|
|---|---|---|---|---|---|---|---|---|---|---|---|
|1 | Finlad | 73 | 77 | 74 | 66 | 75 | 68 | 89 | 51 | 74 | 1|
|2 | Denmark | 73 | 81 | 65 | 67 | 60 | 57 | 95 | 79 | 73 | 1|
|3 | Norway | 78 | 85 | 64 | 63 | 59 | 58 | 93 | 47 | 72 | 1|
|4 | Estonia | 68 | 78 | 75 | 74 | 79 | 44 | 52 | 77 | 71 | 1|
|5 | Sweden | 74 | 78 | 67 | 63 | 63 | 58 | 83 | 52 | 71 | 1|
|6 | Ireland | 65 | 77 | 73 | 62 | 62 | 49 | 83 | 53 | 70 | 1|
|7 | Kanada | 65 | 66 | 74 | 69 | 73 | 54 | 66 | 63 | 68 | 1|
|8 | Switzerland | 72 | 67 | 67 | 70 | 61 | 41 | 77 | 49 | 67 | 1|
|9 | Netherlands | 74 | 59 | 57 | 72 | 65 | 65 | 77 | 43 | 64 | 2|
|10 | Australia | 60 | 52 | 66 | 58 | 65 | 85 | 68 | 80 | 63 | 2|
|11 | Iceland | 69 | 67 | 51 | 60 | 51 | 58 | 83 | 52 | 62 | 2|
|12 | Belgium | 73 | 61 | 61 | 67 | 63 | 55 | 52 | 22 | 61 | 2|
|13 | Germany | 63 | 66 | 63 | 62 | 65 | 48 | 61 | 29 | 61 | 2|
|14 | Portugal | 66 | 75 | 60 | 58 | 60 | 46 | 34 | 38 | 60 | 2|
|15 | United Kingdom | 56 | 60 | 66 | 63 | 66 | 45 | 59 | 38 | 60 | 2|
|16 | South Korea | 45 | 49 | 71 | 76 | 73 | 75 | 51 | 75 | 60 | 2|
|17 | USA | 59 | 50 | 67 | 51 | 65 | 61 | 56 | 72 | 60 | 2|
|18 | Austria | 60 | 57 | 56 | 62 | 59 | 61 | 69 | 57 | 59 | 2|
|19 | Czech Republic | 61 | 64 | 59 | 62 | 62 | 43 | 39 | 69 | 58 | 2|
|20 | Spain | 53 | 57 | 62 | 52 | 55 | 69 | 60 | 54 | 58 | 2|
|21 | France | 55 | 60 | 61 | 60 | 60 | 45 | 44 | 52 | 57 | 2|
|22 | Japan | 54 | 35 | 66 | 77 | 78 | 41 | 52 | 82 | 57 | 2|
|23 | Latvia | 55 | 61 | 54 | 55 | 57 | 67 | 40 | 56 | 55 | 2|
|24 | Slovenia | 59 | 42 | 62 | 67 | 67 | 54 | 43 | 54 | 55 | 2|
|25 | Lithuania | 61 | 70 | 52 | 52 | 54 | 46 | 50 | 0 | 54 | 2|
|26 | Luxembourg | 70 | 62 | 49 | 54 | 52 | 9 | 52 | 24 | 53 | 2|
|27 | Poland | 45 | 37 | 70 | 71 | 69 | 46 | 42 | 41 | 53 | 2|
|28 | Slovakia | 55 | 60 | 44 | 55 | 45 | 25 | 39 | 24 | 48 | 3|
|29 | Italy | 49 | 42 | 52 | 56 | 47 | 45 | 45 | 32 | 47 | 3|
|30 | Croatia | 36 | 46 | 54 | 44 | 49 | 43 | 31 | 53 | 45 | 3|
|31 | Malta | 59 | 30 | 39 | 48 | 42 | 47 | 45 | 78 | 45 | 3|
|32 | Israel | 46 | 27 | 49 | 43 | 44 | 37 | 41 | 51 | 42 | 3|
|33 | Hungary | 33 | 27 | 52 | 52 | 54 | 32 | 45 | 47 | 41 | 3|
|34 | Cyprus | 59 | 38 | 27 | 37 | 33 | 66 | 24 | 38 | 39 | 3|
|35 | Greece | 33 | 19 | 43 | 37 | 39 | 100 | 25 | 59 | 38 | 3|
|36 | Ukraine | 21 | 20 | 47 | 38 | 48 | 57 | 47 | 77 | 38 | 3|
|37 | Serbia | 26 | 30 | 34 | 35 | 33 | 44 | 34 | 61 | 33 | 4|
|38 | Moldova | 18 | 51 | 27 | 21 | 27 | 39 | 29 | 48 | 32 | 4|
|39 | Montenegro | 33 | 39 | 25 | 26 | 20 | 32 | 40 | 48 | 32 | 4|
|40 | Romania | 40 | 42 | 29 | 26 | 26 | 30 | 29 | 0 | 32 | 4|
|41 | Bulgaria | 35 | 26 | 25 | 29 | 25 | 50 | 35 | 53 | 31 | 4|
|42 | Turkey | 0 | 0 | 47 | 38 | 47 | 88 | 31 | 58 | 29 | 4|
|43 | BiH | 24 | 37 | 17 | 13 | 12 | 17 | 27 | 31 | 24 | 5|
|44 | Albania | 24 | 21 | 18 | 29 | 21 | 33 | 20 | 56 | 23 | 5|
|45 | North Macedonia | 7 | 42 | 12 | 7 | 19 | 21 | 33 | 52 | 22 | 5|
|46 | Kosovo | 28 | 40 | 0 | 0 | 0 | 21 | 33 | 54 | 21 | 5|
|47 | Georgia | 25 | 26 | 5 | 9 | 4 | 66 | 26 | 40 | 20 | 5|

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
