---
title: "Sonder"
date: 2022-11-15T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I'm really only posting this so I can find the word again.

Sonder - from the urban dictionary:
The realization that there aren't any main characters in the world and everyone has a complex life, thoughts, crushes, relatives, dreams and mind just as your own.

Essentially while you're the main character in your life, you're also a background character in someone else's.

Coined in 2012 by John Koenig, whose project, [The Dictionary of Obscure Sorrows](https://www.dictionaryofobscuresorrows.com/), aims to come up with new words for emotions that currently lack words. Inspired by German SONDER sonder- (“special") and French SONDER sonder (“to probe").

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
