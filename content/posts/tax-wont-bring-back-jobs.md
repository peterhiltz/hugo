---
title: "Tax Law Changes Won't Bring Back Jobs"
date: 2020-10-30T14:18:19-04:00
categories: ['posts']
tags: ['politics','tax law','economy']
showToc:  "true"
author: "Peter Hiltz"
---
I'm trying to stay away from taxes on this blog, but I was just asked again whether Biden's tax plan would bring back manufacturing jobs to the US. I'm always a little disappointed when someone asks this question and particularly disappointed if that person is in the Federal government. It indicates that the person asking the question is so fixated on "taxes" that they have not stopped to think about all the other aspects and will be spectacularly unsuccessful.

Taxes, particularly on income, are dwarfed by the cost savings in labor, lower safety requirements, lower environmental requirements. Why do so many people forget that income taxes are on net income, not gross revenue? The profit margin on manufacturing (unless you are Apple) tends to be thin. Raising tax rates even 50% on a thin profit margin doesn't move the needle against those other costs. (Tariffs on gross value can be a different matter, but take that argument up with your local economist.)

Even more important, now that the manufacturing jobs are already offshore, is the fact that all the supply chains are offshore as well. It isn't just a question of bringing back XYZ widget manufacturing. You have to bring back the ABC tiny screw manufacturing, DEF tool and die makers and the 1001 other pieces of the supply chain. That means training manufacturing skills that have disappeared (who is going to teach them?) and it would require bringing them back in quantity. In other words, it isn't sufficient to bring back just one of each, you need to have volume. Manufacturers in China and other parts of Asia have 100s of choices at every level of the supply chain, bringing efficiencies in price and quality.

If you want to have an industrial policy, by all means have an industrial policy. But actually think it through. People complain about how complicated the tax system is. It would be less complicated if it wasn't used as a tool for social engineering (by both Republicans and Democrats) in ways that don't actually accomplish anything.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief, but in this case yes, I am a &!*#@ expert on the subject.
