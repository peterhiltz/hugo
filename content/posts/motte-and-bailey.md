---
title: "Motte and Bailey"
date: 2022-02-13T14:18:19-04:00
categories: ['posts']
tags: ['politics','rhetoric']
showToc:  "true"
author: "Peter Hiltz"
---
I have run into an argumentation tactic several times in the last few years and I finally discovered that it has an actual name. Consider the following: You and I are arguing over something which has a common usage understanding. I can't convince you and then I insist that we are arguing over something using a technical definition different than common usage. I can prove my point using that definition. Have I won the original point?

No. I've effectively engaged in a rhetorical bait and switch. In this case I'm trying to use the fact that the word has two definitions, A and B, to claim that if I win using definition B, then I also win under definition A. I've seen this tactic referred to as either a "Motte and Bailey" or "concept swapping". Technically "concept swapping" is the substitution of one concept for another without the other party realizing it. A "Motte and Bailey" argument is where someone conflates two positions that share similarities, but one is modest and easy to defend and the other is controversial. They then shift their usage back and forth trying to claim that their defense of the modest position means they won the controversial position. I view both "Motte and Bailey" and "concept swapping" as the rhetorical equivalent of stage magician's sleight of hand.

The phrase "Motte and Bailey" comes from medieval times when a Motte was the strongly defendable stone tower and the Bailey was the desirable land around it, probably surrounded just by a ditch. You want to defend the Bailey, but it may not be defendable. So you retreat to the tower and throw insults at the opposition, claiming that if they can't take the tower, you have successfully defended the Bailey.

(Wikipedia)[https://en.wikipedia.org/wiki/Motte-and-bailey_fallacy] gives the following example of a "Motte and Bailey" argument:

- Person A: "I don't understand why people believe in astrology, there's no scientific evidence to support it."

- Person B: "The moon has enough pull to cause tides every day on Earth, but it has no effect on people? Are you trying to say humans are literal gods unaffected by nature? I guess evolution isn't real, either!"

Here, Person B has substituted an easy-to-defend motte:

  "Human beings are affected by natural forces, including the Moon's gravity."

for a controversial bailey claim:

  "Astrology's use of the positions of celestial bodies in the sky to make predictions about people's personality, characteristics, and behavior is scientifically valid."

Motte and Bailey tactics are used on all sides of the political spectrum. If you engage in internet searches, you can find both sides claiming the other is using this tactic.


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
