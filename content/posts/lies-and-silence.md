---
title: "Lies and Silence"
date: 2022-03-28T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I recently saw someone post a quote that reads as follows: "If you have something to say, then silence is a lie." I couldn't help myself and responded "But what if what you have to say is a lie?" I was immediately downvoted because the participants thought I was commenting on the author (who I will leave unnamed) rather than the quote itself. I think that speaks to the sorry state of affairs and the unwillingness to actually engage. Sides are chosen, comments made by your team are uncritically defended and comments made by the other team are rejected simply because of the source rather than the content.

I can occasionally get some actual thoughtful conversations going, but it is difficult. When I do, it seems to be when I have made it clear I hate everyone (so I'm not on "The Evil Side"). That tactic fails just as often as it suceeds because there are people (on both sides) who demand that I choose a side - the world must be black or white; there are only two teams etc.

Sigh. As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
