---
title: "Most Dangerous Jobs in the US and Where Do Those Numbers Come From?"
date: 2020-10-29T14:18:19-04:00
categories: ['posts']
tags: ['politics', 'life']
showToc:  "true"
author: "Peter Hiltz"
---
This blog is eclectic because I can get sidetracked by lots of different things. Yesterday was the origin of the word "scientist". Today it is data sourcing and analysis. WARNING: This is a data analysis rat hole expedition.

Someone made a comment to me that police were the 22nd most dangerous job in the US which triggered some recollection in my brain that I had seen a report where they were 16th. So, not in the top 15 most dangerous jobs, but a difference between 16th and 22nd. Where do those numbers actually come from?

The 22nd ranking comes in a Sept 29, 2020 article [here](https://advisorsmith.com/data/most-dangerous-jobs/), reporting a study that AdvisorSmith, a business insurance advisor in the US, did based on 2018 US Bureau of Labor Statistics data.

The 16th ranking comes from a report that 24/7 Wall Street (a USA Today content partner) did that was reported by USA Today [here](https://www.usatoday.com/story/money/2020/01/24/most-dangerous-jobs-25-most-risky-jobs-in-america/41040903/), last updated Jan 24, 2020. It too was based on 2018 US Bureau of Labor Statistics data.

For police, AdvisorSmith shows a death rate of 14 per 100,000, USA Today shows 13.7. That difference is just rounding. So how did they come up with the different rankings? Both reports show "top 25" job, fatality rate per 100,000 workers and annual pay. The USA Today provides some additional data of actual number of fatalities and non-fatal incidents. Both give a description of the most common causes. Let's look at the charts they provided before diving into their sources.

### AdvisorSmith
| Rank| Job| Death Rate| Pay|
| --- | --- | --- | --- |
|  1   | Logging Worker    | 111    | 41,230    |
| 2    | Pilots    |  53   | 121,430    |
| 3    | Derrick operators    | 46    | 51,390    |
| 4    | Roofers    | 41    | 42,000    |
| 5    | Garbage collectors    |  34   |  42,100   |
| 6    | Ironworkers    | 29    |  53,650   |
| 7    | Delivery Drivers    | 27    | 29,610    |
| 8    | Farmers    | 26    | 71,160    |
| 9    | Firefighting supervisors    | 20    | 82,010    |
| 10    | Powerlinemen     | 20    |  71,960    |
| 11   | Agricultural workers   | 20    | 25,840    |
| 12    | Crossing guards    | 19    | 29,760    |
| 13    | Crane operators    | 18    | 60,530    |
| 14    | Construction helpers    | 18    | 31,830    |
| 15     | Landscaping supervisors    | 18    | 52,340    |
| 16    | Highway maintenance workers    | 18    | 42,410    |
| 17    |  Cement masons   |  17   | 48,330    |
| 18    |  Small engine mechanics   | 15    | 37,840    |
| 19    |  Supervisors of mechanics   | 15    | 70,550    |
| 20    |  Heavy vehicle mechanics   | 14    | 51,990    |
| 21    |  Grounds maintenance workers   | 14    | 30,890    |
| 22    |  Police officers   | 14    | 67,600    |
| 23    |  Maintenance workers   | 14    | 39,080    |
| 24    |  Construction workers   | 13    | 36,000    |
| 25    |  Mining machine operators   | 11    | 56,530    |

### USA Today
| Rank| Job| Death Rate| Pay| Fatal | Non-Fatal|
| --- | --- | --- | --- | --- | --- |
| 1    | Logging    | 97.6    | 40,000 (1)   |56 | 1,040  |
| 2    | Fishing    | 77.4    |     | 30 |  |
| 3     | Pilots    | 58.9    | 115,000    |70 |490  |
| 4    | Roofers    | 51.5    |  39,000   |96 |2,060  |
| 5    | Garbage collectors    | 44.3    | 37,000    |37 |1,490  |
| 6    | Drivers    | 26    | 38,000    | 966 |78,520  |
| 7    | Farmers    | 24.7    | 68,000    | 257 | 280* |
| 8    | Ironworkers    | 23.6    | 54,000    |15 |800  |
| 9    | Construction supervisors    | 21    | 65,000    |144 |5,390  |
| 10    | Landscaping supervisors     | 20.2    | 48,000    |48 |1,990  |
| 11    | Powerlinemen     | 19.3    |  71,000   |29 |1,490  |
| 12    | Grounds maintenance workers    | 18.6    | 29.000    |225 | 13,030 |
| 13    | Misc agricultural workers    | 18    | 24,600    |157  | 13,160 |
| 14    | Construction helpers    | 15.8    | 31,200    |11 | 3,460 |
| 15    | Supervisors of mechanics    | 15.1    |66,140     |46 |3,100  |
| 16    | Police    | 13.7    | 61,380    |108 |380  |
| 17    | Construction workers    | 13    | 35,800    |259 | 20,430 |
| 18    | Maintenance workers    | 12.5    | 38,300    | 64 | 21,130 |
| 19    | Mining machine operators    | 11    | 52,700    | 9 | 370 |
| 20    | Construction equip. operators    | 10.6    | 47,810    | 39 | 2,280 |
| 21    | Heavy vehicle mechanics    | 9.5    | 47,350    | 39|3,610|
| 22    | Electricians    | 8.8    | 55,190    | 80 | 6,350 |
| 23    | Heating and A/C mechanics    | 8.5    | 28,530    | 40 | 6,900 |
| 24    | Athletes, coaches, related    | 7.6    | 33,530    | 22 | 1,090 |
| 25    | Industrial truck operators    | 6.8    | 34,750    | 44 | 4,970 |

So, what jobs are not matched in both reports?

|  Job| AdvisorSmith | USA Today|
| --- | --- | --- |
| Logging    | Yes | Yes |
| Fishing    |  | Yes |
| Pilots    |Yes | Yes |
| Derrick Operators|Yes||
| Roofers    |Yes | Yes |
| Garbage collectors |Yes | Yes |
| Drivers    | Yes | Yes |
| Farmers    | Yes | Yes |
| Firefighting Supervisors | Yes | |
| Ironworkers    |Yes | Yes |
| Construction supervisors    |   | Yes|
| Landscaping supervisors     |Yes | Yes |
| Highway maintenance workers  | Yes | |
| Cement Masons | Yes |   |
| Small Machine Mechanics | Yes |   |
| Powerlinemen     | Yes | Yes |
| Crossing Guards | Yes |   |
| Crane Operators | Yes |  |
| Grounds maintenance workers |Yes | Yes |
| Misc agricultural workers    | Yes | Yes |
| Construction helpers    | Yes | Yes |
| Supervisors of mechanics    |Yes | Yes |
| Police    | Yes | Yes |
| Construction workers    |Yes | Yes |
| Maintenance workers    | Yes | Yes |
| Mining machine operators    |Yes | Yes |
| Construction equip. operators    |   | Yes|
| Heavy vehicle mechanics    | Yes | Yes |
| Electricians    |   | Yes|
| Heating and A/C mechanics    |   | Yes|
| Athletes, coaches, related    |   | Yes |
| Industrial truck operators    |   | Yes |

AdvisorSmith shows Derrick operators, Firefighting Supervisors, Crossing guards, Highway maintenance workers, Crane operators, Cement masons and Small engine mechanics. USA Today shows Fishing, Construction supervisors, Construction equipment operators, Electricians, Heating and A/C mechanics, Athletes and related and Industrial truck operators. Fishing being missing from AdvisorSmith surprised me because it shows up as one of the more dangerous jobs in any of the worldwide reports. Then I looked at the BLS numbers and while they have 30 fatalities, BLS has no data on employment or median or mean wages for fishing. [See](https://www.bls.gov/oes/2018/may/oes_stru.htm#33-0000).

So we have different jobs listed, different fatality rates and different pay. It's almost time to dig into the original source and try to reverse engineer their numbers. The USA Today does have have a methodology section that said it looked at fatal injury rates for 71 detailed occupations. They also only considered occupations for at the finest level of detail, excluding those for which two NAICS codes were combined.

Because USA Today gave us the number of fatal accidents as well as the fatality rate, we can back into an implied number of workers and that will help us reverse engineering the data.
### USA Today Implied # of Workers
| Rank| Job| Death Rate| Fatalities | Implied # workers|
| --- | --- | --- | --- | --- | --- |
| 1    | Logging    | 97.6    |    56   | 57,000 |
| 2    | Fishing    | 77.4    |      30 |  38,700 |
| 3     | Pilots    | 58.9         |70   |118,000 |
| 4    | Roofers    | 51.5       |96  |186,000 |
| 5    | Garbage collectors    | 44.3       |37   |83,000 |
| 6    | Drivers    | 26    |  966 |3,715,000 |
| 7    | Farmers    | 24.7        | 257  | 1,040,485 |
| 8    | Ironworkers    | 23.6        |15   |63,000 |
| 9    | Construction supervisors    | 21        |144   |685,000 |
| 10    | Landscaping supervisors     | 20.2        |48   |237,623 |
| 11    | Powerlinemen     | 19.3     |29 |150,259 |
| 12    | Grounds maintenance workers    | 18.6        |225  |1,209,677 |
| 13    | Misc agricultural workers    | 18        |157  | 872,222|
| 14    | Construction helpers    | 15.8        |11 | 69,620 |
| 15    | Supervisors of mechanics    | 15.1         |46   | 304,636|
| 16    | Police    | 13.7     |108 |788,321 |
| 17    | Construction workers    | 13        |259  | 1,992,308 |
| 18    | Maintenance workers    | 12.5        | 64  | 512,000 |
| 19    | Mining machine operators    | 11        | 9  |81,818 |
| 20    | Construction equip. operators    | 10.6        | 39  | 367,925 |
| 21    | Heavy vehicle mechanics    | 9.5        | 39|410526|
| 22    | Electricians    | 8.8     | 80 |  909,091 |
| 23    | Heating and A/C mechanics    | 8.5        | 40  | 470588 |
| 24    | Athletes, coaches, related    | 7.6       | 22  | 289,474 |
| 25    | Industrial truck operators    | 6.8       | 44  | 647,059 |

### The Data Source
Now lets go to the [source](https://www.bls.gov/news.release/cfoi.nr0.htm). We'll start with police offers just because that was what piqued my interest in the first place.

In order to get the BLS data, we effectively have to go through two different sources. Occupational Employment Statistics comes here [here](https://www.bls.gov/oes/home.htm) for nearly 800 occupations (just a bit more than the 71 that USA Today looked at. The news release on the fatal occupational injuries is [here](https://www.bls.gov/news.release/cfoi.nr0.htm), but getting to the actual data is more difficult. The injuries homepage for BLS is [here](https://www.bls.gov/iif/) and fatalities only is [here](https://www.bls.gov/iif/oshcfoi1.htm). .

Looking at [this page](https://www.bls.gov/iif/oshwc/cfoi/cftb0322.htm) and searching for police, gives us a line showing an NAICS code of 92212, 111 total fatalities, 61 from violence and 44 from transportation incidents. So slightly higher than the 108 fatalities shown in the USA Today chart. [This chart](https://www.bls.gov/iif/oshwc/cfoi/cftb0323.htm) breaks it down a little more into 32 Roadway incidents involving motorized land vehicles, 1 Nonroadway incidents involving motorized land vehicles, 8	Pedestrian vehicular incidents and 47 Shooting by other person--intentional. The remaining 3 transportation incidents and 20 homicides are left to your imagination.

Did we get our first data match? Dropping the three unclassified transportation incidents would get us to 108, matching the USA Today number. No. We actually get our data match from this [table](https://www.bls.gov/iif/oshwc/cfoi/cftb0326.htm) which explicitly states 108 fatalities, broken down into 56 by violence and 45 transportation incidents for police officers in all jurisdictions. Whoops. How come we have 45 transportation incidents reported on this table and only 44 on the earlier [table](https://www.bls.gov/iif/oshwc/cfoi/cftb0322.htm). There is another line on this table for First-line supervisors of police and detectives, who had 7 violence related deaths and 3 traffic related deaths. That gives us 122 total fatalities. Hmmm. Back to the first table, there were 3 fatalities involving Federal police (all violence) and another 8 (4 violence and 4 transportation) from state police. That gives us 122 total, which matches the 122 from this table. But it tells us is that we need to be careful about our definition of police.

So, I feel like I'm comfortable with matching the USA Today police fatality number, but what about the rest? Remember that USA Today had a median pay of $61,380 and AdvisorSmith had $67,600? What are these differences? Police Officers have an oes code of 33-3051. So back to the employment statistics numbers we find [this page](https://www.bls.gov/oes/2018/may/oes333051.htm) which tells us that Police and Sheriff's Patrol Officers in May 2018 have a national median annual wage of $61,380, matching the USA Today numbers. Where did the AdvisorSmith number of $67,600 come from? That number came from [this page](https://www.bls.gov/oes/2019/may/oes333051.htm). Two things to call out here. First, AdvisorSmith uses the national mean instead of the national median. Second, this is a May 2019 number, not a May 2018 number. Since the fatality rates were for the entire 2018 calendar year and the pay numbers are mid year numbers, I can understand the two studies simply choosing different mid points, but the difference needs to be understood.

But now lets check the fatality rates. The May 2018 employment table shows police officer total employment, including federal and state at this level of 655,270. The May 2019 numbers show police officer employment 659,180. Now I can't make the fatality rates match. If we use the total jurisdiction employment numbers for 2018 and the 108 fatalities, I get a fatality rate of 16.5 per 100,000. For these employment numbers and a 13.7 fatality rate, we should be getting only 90 incidents.

Let's look some of the other occupations and see if there is a pattern. We will stick with the [BLS chart](https://www.bls.gov/iif/oshwc/cfoi/cftb0327.htm) that matched the 108 fatalities reported by USA Today for policemen. You will notice we have to build up some of these categories to match the USA Today fatality numbers.

|  Job| oes code | fatalities |Employment Per BLS Page | Wages (Median)| Cite|
| --- | --- | --- | --- | --- | --- |
| Logging    | 	45-4020  | 56 |30,410| 41,059||
| Logging (fallers)   | 	45-4021  | 42 |4,680| 44,080 |[cite](https://www.bls.gov/oes/2018/may/oes454021.htm)
| Logging (equip. operators)   | 	45-4022  | 7 |25,730|40,510|[cite](https://www.bls.gov/oes/2018/may/oes454022.htm)|
| Fishing    | 45-3010  | 30 ||||
| Pilots    | 53-2011 | 70 |82,890|140,340|[cite](https://www.bls.gov/oes/2018/may/oes532011.htm)|
| Derrick Operators|47-5010|20|11,310|46,120|[cite](https://www.bls.gov/oes/2018/may/oes475011.htm)|
| Roofers    |47-2181 | 96  |128,680|39,970|[cite](https://www.bls.gov/oes/2018/may/oes472181.htm)
| Garbage collectors |53-7081 |37  |118,520|37,260|[cite](https://www.bls.gov/oes/2018/may/oes537081.htm)|
| Drivers    | 53-3030 | 966  |2,857,500|41,616||
| Sales Drivers    | 53-3031 | 52  |414,860|24,700|[cite](https://www.bls.gov/oes/2018/may/oes533031.htm)|
| Heavy Truck Drivers    | 53-3032 | 831  |1,800,330|43,680|[cite](https://www.bls.gov/oes/2018/may/oes533032.htm)|
| Light Truck Drivers    | 53-3033 | 83  |915,310|32,810|[cite](https://www.bls.gov/oes/2018/may/oes533033.htm)|
| Farmers    | 11-9013 |257  |5,060|71,160|[cite](https://www.bls.gov/oes/current/oes119013.htm)|
| Firefighting Supervisors | 33-1021 |14 |65,920|76,330|[cite](https://www.bls.gov/oes/2018/may/oes331021.htm)|
| Ironworkers    |47-2221 | 15  |77,410|53,970|[cite](https://www.bls.gov/oes/2018/may/oes472221.htm)|
| Construction supervisors    | 47-1010  | 144|598,210|65,230|[cite](https://www.bls.gov/oes/2018/may/oes471011.htm)|
| Landscaping supervisors     |37-1012 |48  |101,390|48,220|[cite](https://www.bls.gov/oes/2018/may/oes371012.htm)|
| Highway maintenance workers  |47-4051  |14 |149,260|39,690|[cite](https://www.bls.gov/oes/2018/may/oes474051.htm)|
| Cement Masons | 47-2051 | 11  |186,400|43,000| [cite](https://www.bls.gov/oes/2018/may/oes472051.htm)|
| Small Engine | 49-3050 | 8  |46,850|35,848||
| Small Engine (motorcycle)| 49-3052 | 4  |15,090|36,790|[cite](https://www.bls.gov/oes/2018/may/oes493052.htm)|
| Small Engine (other)| 49-3053 | 3  |31,760|35,400|[cite](https://www.bls.gov/oes/2018/may/oes493053.htm)|
| Powerlinemen (Electical + Telcom)    | 49-9050 |39  |233,000| 64,462||
| Powerlinemen (Electrical only)     | 49-9051 |29  |114,800|70,910|[cite](https://www.bls.gov/oes/2018/may/oes499051.htm)|
| Powerlinemen (Telcom only)     | 49-9052 |10  |118,200|58,280|[cite](https://www.bls.gov/oes/2018/may/oes499052.htm)|
| Crossing Guards | 33-9091 | 14  |79,880|28,960|[cite](https://www.bls.gov/oes/2018/may/oes339091.htm)|
| Crane Operators | 53-7021 |9  |44,410|54140|[cite](https://www.bls.gov/oes/2018/may/oes537021.htm)|
| Grounds maintenance workers (no tree trimmers) |37-3011 |142  |913,480|29,000|[cite](https://www.bls.gov/oes/2018/may/oes373011.htm)|
| Grounds maintenance workers (tree trimmers) |37-3012 |225  |24,500|35,320|[cite](https://www.bls.gov/oes/2018/may/oes373012.htm)|
| Misc agricultural workers    |45-2090  |157  |325,200| 24,580||
| Misc agricultural workers    |45-2092  |78  |287,420|24,320|[cite](https://www.bls.gov/oes/2018/may/oes452092.htm)|
| Misc ranch workers    |45-2093  |60  |37,780|26,560|[cite](https://www.bls.gov/oes/2018/may/oes452093.htm)|
| Construction helpers    | 47-3010 | 11  |207,270| 31,327|
| Construction helpers masons   | 47-3011 |   | 24,340| 33,380|[cite](https://www.bls.gov/oes/2018/may/oes473011.htm)|
| Construction helpers carpenters   | 47-3012 |   |33,020| 30,880|[cite](https://www.bls.gov/oes/2018/may/oes473012.htm)|
| Construction helpers electricians   | 47-3013 |   |75,970| 31,410|[cite](https://www.bls.gov/oes/2018/may/oes473013.htm)|
| Construction helpers painters   | 47-3014 |   |10,600| 29,960|[cite](https://www.bls.gov/oes/2018/may/oes473014.htm)|
| Construction helpers plumbers   | 47-3015 |   |54,710| 30,980|[cite](https://www.bls.gov/oes/2018/may/oes473015.htm)|
| Construction helpers roofers   | 47-3016 |   |8,630| 30,390|[cite](https://www.bls.gov/oes/2018/may/oes473016.htm)|
| Supervisors of mechanics    |49-1011 | 46 |471,820|66,140|[cite](https://www.bls.gov/oes/2018/may/oes491011.htm)|
| Police    | 33-3051 |108  |661,330|61,380|[cite](https://www.bls.gov/oes/2018/may/oes333051.htm)|
| Construction workers    | 47-2061| 259 |1,001,470|35,800|[cite](https://www.bls.gov/oes/2018/may/oes472061.htm)|
| Maintenance workers    | 49-9071 |64  |1,384,240|38,300|[cite](https://www.bls.gov/oes/2018/may/oes499071.htm)|
| Mining machine operators    | 47-5041| 9 |14,710|54,520|[cite](https://www.bls.gov/oes/2018/may/oes475041.htm)|
| Construction equip. operators    | 47-2070  |51 |433,690|47,031 ||
| Construction equip. operators (paving)   | 47-2071  | |46,760|39,780|[cite](https://www.bls.gov/oes/2018/may/oes472071.htm)|
| Construction equip. operators (pile drivers)   | 47-2071  | |3,450|58,680|[cite](https://www.bls.gov/oes/2018/may/oes472072.htm)|
| Construction equip. operators (other)    | 47-2073  | |383,480| 47810|[cite](https://www.bls.gov/oes/2018/may/oes472073.htm)|
| Heavy vehicle mechanics    | 49-3031 |39  |264,860|47,350|[cite](https://www.bls.gov/oes/2018/may/oes493031.htm)|
| Electricians    | 47-2111  |80 |655,840|55,190|[cite](https://www.bls.gov/oes/2018/may/oes472111.htm)|
| Heating and A/C mechanics    | 49-9021  |40 |324,310|47,610|[cite](https://www.bls.gov/oes/2018/may/oes499021.htm)|
| Athletes, coaches, related    | 27-2020  | 22 | 247,770| 34,515||
| Athletes, coaches, related    | 27-2021  |  |10,800|50,650|[cite](https://www.bls.gov/oes/2018/may/oes272021.htm)|
| Athletes, coaches, related    | 27-2022  |  |236,970|33,780|[cite](https://www.bls.gov/oes/2018/may/oes272022.htm)|
| Industrial truck operators    | 53-7051  | 44 | 604,130| 34,750|[cite](https://www.bls.gov/oes/2018/may/oes537051.htm)|

Now that we've had a chance to look at the underlying data, I'm going to drop out the occupations for which we don't have full BLS data (like fishing). What are the remaining occupations with the highest chance of dying?

||Job|fatalities|Employment|Wages (Median)|Fatality Rate|Chance of dying annually|deathrate pay|
| --- | --- | --- | --- | --- | --- | --- | --- |
|1|Farmers|257|5,060|71,160|5079.1|5.08%|14|
|2|Grounds maintenance workers (tree trimmers)|225|24,500|35,320|918.4|0.92%|38|
|3|Logging (fallers)|42|4,680|44,080|897.4|0.90%|49|
|4|Athletes|20|10,800|50,650|185.2|0.19%|274|
|5|Logging|56|30,410|41,059|184.1|0.18%|223|
|6|Derrick Operators|20|11,310|46,120|176.8|0.18%|261|
|7|Misc ranch workers|60|37,780|26,560|158.8|0.16%|167|
|8|Pilots|70|82,890|140,340|84.4|0.08%|1,662|
|9|Roofers|96|128,680|39,970|74.6|0.07%|536|
|10|Mining machine operators|9|14,710|54,520|61.2|0.06%|891|
|11|Misc agricultural workers|157|325,200|24,580|48.3|0.05%|509|
|12|Landscaping supervisors|48|101,390|48,220|47.3|0.05%|1,019|
|13|Heavy Truck Drivers|831|1,800,330|43,680|46.2|0.05%|946|
|14|Drivers|966|2,857,500|41,616|33.8|0.03%|1,231|
|15|Garbage collectors|37|118,520|37,260|31.2|0.03%|1,194|
|16|Logging (equip. operators)|7|25,730|40,510|27.2|0.03%|1,489|
|17|Misc agricultural workers|78|287,420|24,320|27.1|0.03%|896|
|18|Motorcycle mechanics|4|15,090|36,790|26.5|0.03%|1,388|
|19|Construction workers|259|1,001,470|35,800|25.9|0.03%|1,384|
|20|Powerlinemen (Electrical only)|29|114,800|70,910|25.3|0.03%|2,807|
|21|Construction supervisors|144|598,210|65,230|24.1|0.02%|2,710|
|22|Firefighting Supervisors|14|65,920|76,330|21.2|0.02%|3,594|
|23|Crane Operators|9|44,410|54,140|20.3|0.02%|2,672|
|24|Ironworkers|15|77,410|53,970|19.4|0.02%|2,785|
|25|Crossing Guards|14|79,880|28,960|17.5|0.02%|1,652|
|26|Small Machine Mechanics|8|46,850|35,848|17.1|0.02%|2,099|
|27|Powerlinemen (Electical + Telcom)|39|233,000|64,462|16.7|0.02%|3,851|
|28|Police|108|661,330|61,380|16.3|0.02%|3,759|
|29|Grounds maintenance workers (no tree trimmers)|142|913,480|29,000|15.5|0.02%|1,866|
|30|Heavy vehicle mechanics|39|264,860|47,350|14.7|0.01%|3,216|
|31|Sales Drivers|52|414,860|24,700|12.5|0.01%|1,971|
|32|Heating and A/C mechanics|40|324,310|47,610|12.3|0.01%|3,860|
|33|Electricians|80|655,840|55,190|12.2|0.01%|4,524|
|34|Construction equip. operators|51|433,690|47,031|11.8|0.01%|3,999|
|35|Supervisors of mechanics|46|471,820|66,140|9.7|0.01%|6,784|
|36|Small Machine Mechanics (other)|3|31,760|35,400|9.4|0.01%|3,748|
|37|Highway maintenance workers|14|149,260|39,690|9.4|0.01%|4,232|
|38|Light Truck Drivers|83|915,310|32,810|9.1|0.01%|3,618|
|39|Powerlinemen (Telcom only)|10|118,200|58,280|8.5|0.01%|6,889|
|40|Industrial truck operators|44|604,130|34,750|7.3|0.01%|4,771|
|41|Cement Masons|11|186,400|43,000|5.9|0.01%|7,287|
|42|Construction helpers|11|207,270|31,327|5.3|0.01%|5,903|
|43|Maintenance workers|64|1,384,240|38,300|4.6|0.00%|8,284|

Sorted this way, policemen is the 28th most dangerous occupation. But let's look at it yet another way. Let's sort by pay for the risk taken and see which professions take the most risk for the least amount of compensation.

||Job|fatalities|Employment|Wages (Median)|Fatality Rate|Chance of dying annually|deathrate pay|
| --- | --- | --- | --- | --- | --- | --- | --- |
|1|Farmers|257|5,060|71,160|5079.1|5.08%|14|
|2|Grounds maintenance workers (tree trimmers)|225|24,500|35,320|918.4|0.92%|38|
|3|Logging (fallers)|42|4,680|44,080|897.4|0.90%|49|
|4|Misc ranch workers|60|37,780|26,560|158.8|0.16%|167|
|5|Logging|56|30,410|41,059|184.1|0.18%|223|
|6|Derrick Operators|20|11,310|46,120|176.8|0.18%|261|
|7|Athletes|20|10,800|50,650|185.2|0.19%|274|
|8|Misc agricultural workers|157|325,200|24,580|48.3|0.05%|509|
|9|Roofers|96|128,680|39,970|74.6|0.07%|536|
|10|Mining machine operators|9|14,710|54,520|61.2|0.06%|891|
|11|Misc agricultural workers|78|287,420|24,320|27.1|0.03%|896|
|12|Heavy Truck Drivers|831|1,800,330|43,680|46.2|0.05%|946|
|13|Landscaping supervisors|48|101,390|48,220|47.3|0.05%|1,019|
|14|Garbage collectors|37|118,520|37,260|31.2|0.03%|1,194|
|15|Drivers|966|2,857,500|41,616|33.8|0.03%|1,231|
|16|Construction workers|259|1,001,470|35,800|25.9|0.03%|1,384|
|17|Motorcycle mechanics|4|15,090|36,790|26.5|0.03%|1,388|
|18|Logging (equip. operators)|7|25,730|40,510|27.2|0.03%|1,489|
|19|Crossing Guards|14|79,880|28,960|17.5|0.02%|1,652|
|20|Pilots|70|82,890|140,340|84.4|0.08%|1,662|
|21|Grounds maintenance workers (no tree trimmers)|142|913,480|29,000|15.5|0.02%|1,866|
|22|Sales Drivers|52|414,860|24,700|12.5|0.01%|1,971|
|23|Small Machine Mechanics|8|46,850|35,848|17.1|0.02%|2,099|
|24|Crane Operators|9|44,410|54,140|20.3|0.02%|2,672|
|25|Construction supervisors|144|598,210|65,230|24.1|0.02%|2,710|
|26|Ironworkers|15|77,410|53,970|19.4|0.02%|2,785|
|27|Powerlinemen (Electrical only)|29|114,800|70,910|25.3|0.03%|2,807|
|28|Heavy vehicle mechanics|39|264,860|47,350|14.7|0.01%|3,216|
|29|Firefighting Supervisors|14|65,920|76,330|21.2|0.02%|3,594|
|30|Light Truck Drivers|83|915,310|32,810|9.1|0.01%|3,618|
|31|Small Machine Mechanics (other)|3|31,760|35,400|9.4|0.01%|3,748|
|32|Police|108|661,330|61,380|16.3|0.02%|3,759|
|33|Powerlinemen (Electical + Telcom)|39|233,000|64,462|16.7|0.02%|3,851|
|34|Heating and A/C mechanics|40|324,310|47,610|12.3|0.01%|3,860|
|35|Construction equip. operators|51|433,690|47,031|11.8|0.01%|3,999|
|36|Highway maintenance workers|14|149,260|39,690|9.4|0.01%|4,232|
|37|Electricians|80|655,840|55,190|12.2|0.01%|4,524|
|38|Industrial truck operators|44|604,130|34,750|7.3|0.01%|4,771|
|39|Construction helpers|11|207,270|31,327|5.3|0.01%|5,903|
|40|Supervisors of mechanics|46|471,820|66,140|9.7|0.01%|6,784|
|41|Powerlinemen (Telcom only)|10|118,200|58,280|8.5|0.01%|6,889|
|42|Cement Masons|11|186,400|43,000|5.9|0.01%|7,287|
|43|Maintenance workers|64|1,384,240|38,300|4.6|0.00%|8,284|

Hmmm. Police come in 32nd by this benchmark so there are at least 31 occupations with less pay for the amount of risk (just looking at fatalities).

I don't think I want to be a farmer. That being said, there is something that feels wrong about that farming number because I just don't believe that there are only 5,060 managerial farmers in the US. Obvious USA Today and AdvisorSmith don't think so either because they have much lower fatality rates. If we look at the BLS footnotes on the cited page, they state the employment numbers do not include self-employed. Ok, that is going to be a huge missing amount of people.  If I do a different run into the BLS database rather than the published pages and sum occ_code 11-9013 I get 31,150, which is still a far cry from where it needs to be. That also points to a different problem: Is the fatality number correct?

USA Today's fatality rate implies more than 1 million farmers and USA Today was using the number of fatalities which correspond to the BLS data for managerial farmers and the USA Today job title is "Farmers, ranchers, and other agricultural managers". Do the farming fatality rates need to be adjusted as well? Right now I can't tell.

As a side note, chief executives had 23 job related fatalities in 2018, 9 from violence, 10 from transportation incidents, and 4 apparently from other. Did you know that there were [195,530 chief executives](https://www.bls.gov/oes/2018/may/oes111011.htm) in the US in 2018? And only 5,060 managerial farmers?

Where are we vis-a-vis USA Today and AdvisorSmith? As you can see from the table below, there are some matches, but lots more non-matches. And this is all allegedly from the same source of data. Granted, I only put in six hours, but at least I showed my work.

|Job|fatalities|Employment|USA Today Deathrate per 100,000|AdvisorSmith Deathrate|Fatalities / Employment * 100,000|
| --- | --- | --- | --- | ---| ---|
|Farmers|257|5,060|24.7|26|5,079.05|
|Grounds maintenance workers (tree trimmers)|225|24,500|||918.37|
|Logging (fallers)|42|4,680|||897.44|
|Athletes|20|10,800|7.6||185.19|
|Logging|56|30,410|97.6|111|184.15|
|Derrick Operators|20|11,310||46|176.83|
|Misc ranch workers|60|37,780|||158.81|
|Pilots|70|82,890|58.9|53|84.45|
|Roofers|96|128,680|51.5|41|74.60|
|Mining machine operators|9|14,710|11|11|61.18|
|Misc agricultural workers|157|325,200|||48.28|
|Landscaping supervisors|48|101,390|20.2|18|47.34|
|Heavy Truck Drivers|831|1,800,330|||46.16|
|Drivers|966|2,857,500|26|27|33.81|
|Garbage collectors|37|118,520|44.3|34|31.22|
|Logging (equip. operators)|7|25,730|||27.21|
|Misc agricultural workers|78|287,420||20|27.14|
|Motorcycle mechanics|4|15,090|||26.51|
|Construction workers|259|1,001,470|13|13|25.86|
|Powerlinemen (Electrical only)|29|114,800|19.3|20|25.26|
|Construction supervisors|144|598,210|21||24.07|
|Firefighting Supervisors|14|65,920||20|21.24|
|Crane Operators|9|44,410||18|20.27|
|Ironworkers|15|77,410|23.6|29|19.38|
|Crossing Guards|14|79,880||19|17.53|
|Small Machine Mechanics|8|46,850||15|17.08|
|Powerlinemen (Electical + Telcom)|39|233,000|19.3||16.74|
|Police|108|661,330|13.7|14|16.33|
|Grounds maintenance workers (no tree trimmers)|142|913,480|18.6|14|15.54|
|Heavy vehicle mechanics|39|264,860|9.5|14|14.72|
|Sales Drivers|52|414,860|||12.53|
|Heating and A/C mechanics|40|324,310|8.5||12.33|
|Electricians|80|655,840|8.8||12.20|
|Construction equip. operators|51|433,690|10.6||11.76|
|Supervisors of mechanics|46|471,820|15.1|15|9.75|
|Small Machine Mechanics (other)|3|31,760||15|9.45|
|Highway maintenance workers|14|149,260||18|9.38|
|Light Truck Drivers|83|915,310|||9.07|
|Powerlinemen (Telcom only)|10|118,200|||8.46|
|Industrial truck operators|44|604,130|6.8||7.28|
|Cement Masons|11|186,400||17|5.90|
|Construction helpers|11|207,270|15.8|18|5.31|
|Maintenance workers|64|1,384,240|12.5||4.62|

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
