---
title: "Update on Kentucky Police Training - Warrior or Guardian"
date: 2020-11-30T14:18:19-04:00
categories: ['posts']
tags: ['politics','life']
showToc:  "true"
author: "Peter Hiltz"
---
An update on an [earlier post](https://www.peterhiltz.com/en/posts/guardian-or-warrior/) about a training deck used by the Kentucky State Police urging cadets to be ruthless killers and quoting Hitler advocating violence.

At the time that story broke on Oct 30, the Kentucky Justice and Public Safety Cabinet claimed the material had been removed in 2013. Since that date, state police have not replied to subsequent records requests. Maybe that particular slide deck was removed in 2013, but the [Lexington Herald Leader](https://www.kentucky.com/news/state/kentucky/article247508570.html) reported today, confirmed by the Governor and the Kentucky Justice and Public Safety Cabinet that another training video had been approved for training use in September of this year that featured Nazi symbols. I haven't seen any reports of whether the training video had more of the "relentless killer" training or just the objectionable symbol (which had no reason for being in a police training video).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
