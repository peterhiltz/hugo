---
title: "I Can Learn From Anyone"
date: 2020-09-27T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I'm an infovore. I take in a lot of information (or in the words of my sister-in-law - "more useless crap") and occasionally it makes new patterns that I can learn from. Its not just written information. I can learn watching two carpenters do things differently. I can learn from listening to different conversations about practically anything (maybe not sports because I really don't care). It doesn't matter the socioeconomic level, intelligence level or age level. A quote, sometimes attributed to Albert Einstein goes "Everyone is a genius. But if you judge a fish by its ability to climb a tree, it will live its whole life believing that it is stupid."

There is something I can learn from practically anyone and sometimes what you learn comes from inflection, or what is not said. It might be just whether the person is open-minded or close-minded on a subject but possibly different levels of open-minded or close-minded on something else. I might strongly disagree, but if I listen, there is probably something I can learn.

The correlative point is made by Will Rogers who said "Everybody is ignorant. Only on different subjects." Just because someone is a senior politician, wealthy businessperson or talented athlete or artist doesn't make them an expert on everything. Its also disappointing to discover that someone you think is very smart in a certain subject matter is completely wrong because they didn't actually understand the data they used as a starting point. (Yes, academic economists, I'm thinking about you.)

There are a few subjects I know a lot about. There are a few subjects I know a bit about. There are lots more subjects I know very little about. There are lots of subjects I would not think I was interested in, but, I can become interested in if I'm actively listening to someone who cares about the topic. Note the phrase "actively listening". Stephen R Covey allegedly said, “Most people listen with the intent to respond, not with the intent to understand.”

Psychologist George Miller found that most people actually engage in "competitive listening". They hear something and have a negative reaction, because they believe what the other person said is false. Because you can listen faster than someone can talk, most of your brain starts playing out a conversation with yourself about what they said, confirmation bias kicks in and real listening stops. At that point it starts to become more difficult for actual communication to happen. As Miller in 1980: “In order to understand what another person is saying, you have to assume that (their answer) is true and try to imagine what it could be true of.”

I can't make you care about other people, but maybe I can suggest that you can learn from other people even if you don't like them. That does, however, require that you actually listen. One of the many problems with echo chambers (political or otherwise) is they are not conducive to learning anything that you are not already predisposed to hear. They worsen the polarization problem. Polarization is a contributing factor to viewing the other side as subhuman, which contributes to ends justifies the means type thinking, which then makes violence (organized or not) acceptable.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
