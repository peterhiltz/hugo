---
title: "Frazz and Unnecessary Paranoia"
date: 2021-01-27T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
I like the daily comic strip [Frazz](https://www.gocomics.com/frazz). The main character is a young man who is a janitor at the elementary school that he went to as a child. He gets along with [teachers](https://www.gocomics.com/frazz/2021/01/03) and kids talk to him about [questions they don't want to ask an authority figure](https://www.gocomics.com/frazz/2020/11/21). There are also lots of strips involving kids [talking](https://www.gocomics.com/frazz/2020/07/31) to their teachers. It is a bit of homage to Calvin and Hobbes, with a little less snark. Just gentle observations on life. The sort of comic strip I buy books of the collected comics and give them to my mother as a Christmas present. So what is the fly in the ointment?

I unfortunately read the comments on [this particular strip](https://www.gocomics.com/frazz/2021/01/15). Frazz's [backstory](https://www.gocomics.com/frazz/2001/04/03) is that he wanted to be a songwriter but became a janitor because his songwriting career wasn't going anywhere. He kept writing and finally became successful, but kept the janitor job because he likes being at the school and around kids. Cue the "OMG he's a pedophile" crowd. Apparently males can only work at a school if they can't find a job somewhere else and you can prove you hate kids. I'm not quite sure how male teachers get allowed under this theory.

I spent my career as a professional paranoid, looking at all the things that could go wrong in a business transaction and planning how to deal with those possibilities before they happen. If someone wants to have something to be afraid of, I can give them plenty of real issues. Yes, there are bad people out there and you and your children need to take precautions. There are also bad people out there that you voted for and why weren't you paying attention then?

I wonder if the people trying to see creepiness in a comic strip are the same types of people who make fun of people with disabilities or, in earlier times, would try to burn them for being a witch. As one sane person in the comments said "Sometimes, people see only what they are looking for, even when it’s not there."

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
