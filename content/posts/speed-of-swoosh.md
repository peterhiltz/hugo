---
title: "The Speed of Swoosh"
date: 2020-10-13T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Have you ever heard somebody say something that you think is so incorrect that, to use physicist Wolfgang Pauli's phrase "Das ist nicht nur nicht richtig, es ist nicht einmal falsch!" — "That is not only not right, it is not even wrong!"? [As a result you see it as your duty to jump in to correct them](https://xkcd.com/386/). I am as guilty of that as anyone - why else this blog.

Pauli's comment was in relation to a very unclear research paper, but the phrase "it is not even wrong" has come to "imply that not only is someone not making a valid point in a discussion, they don't even understand the nature of the discussion itself, or the things that need to be understood in order to participate. [See https://rationalwiki.org/wiki/Not_even_wrong](https://rationalwiki.org/wiki/Not_even_wrong).

The other day someone posted the following quote from [Terry Pratchett](https://en.wikipedia.org/wiki/Terry_Pratchett):

*"Light thinks it travels faster than anything but it is wrong. No matter how fast light travels, it finds the darkness has always got there first, and is waiting for it."*

The very first response was to immediately chastise the author for completely misunderstanding physics. What is the speed of swoosh? It apparently has to happen before someone can jump to conclusions. There were a couple of exchanges with other commentators, but I'm still not sure that first respondent ever figured out that the quote is from a series of 41 fantasy books taking place on a flat world held up by four giant elephants standing on the back of a giant turtle that swims through space. Nonfiction physics is not the context.

Similarly when a news report was posted about [police killings more likely in agencies that get military gear, data shows](https://www.ajc.com/news/police-killings-more-likely-in-agencies-that-get-military-gear-data-shows/MBPQ2ZE3XFHR5NIO37BKONOCGI/) the first reaction to the headline on social media was "OR, military gear is more likely to be needed by agencies in high crime violent areas, data shows." Ah yes, another person who jumps in to correct a story based on reading the headline and not actually analyzing the story.

The story makes it clear that the newspaper’s analysis used the military’s database and paired it with a database of fatal police shootings from across the state, controlling for statistical variables like community income, rural-urban differences, racial makeup, and violent crime rates. If you want to disagree with how they tried to statistically control for the variables, have at it. I'll respect that so long as your analysis doesn't become another example for Wolfgang Pauli's statement. And yes, there actually were informed, thoughtful comments questioning how well the newspaper actually controlled for the variables.But if you respond with a knee jerk reaction to a headline without reading the text, I not only won't respect you, I will affirmatively disrespect you.

By the way, [President Trump apparently disagrees](https://thehill.com/homenews/administration/520652-trump-in-holiday-proclamation-bemoans-radical-activists-undermining) with [my note](https://www.peterhiltz.com/en/posts/oh-look-a-squirrel/). I don't expect that he is an avid reader of my blog.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
