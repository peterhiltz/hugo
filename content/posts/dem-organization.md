---
title: "Disorganization?"
date: 2020-11-14T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Just out of curiosity, I looked back at my phone and text messages over the course of the election season. I had two phone calls from campaigners before actual voting was possible and none after that. I counted 34 text messages from different groups campaigning for a democrat candidate, 32 of them for Biden, 27 of which thought I was someone named Sarah. I responded to each one, pointing out (1) I wasn't Sarah; (2) I've had my cellphone number since 2006 so this is not likely some change of number faux pas; and (3) I had already voted on the first possible day by dropping of my ballot at the City Clerk's office. 26 of the 27 promised to delete my name from the database. It became quickly obvious that the D's have no single source of truth so "deleting my name" from some database actually accomplished not much.

Regardless of which party you support, what did I learn from this? Apparently the Rs don't know I exist and the Ds are both wastefully disorganized and don't have any strategy down ballot. But both want me to vote for them to run the country? And before anyone jumps up and says "see government bad, capitalism good", I will state for the record that I have almost weekly dealings with businesses that I wouldn't hire to manage a tic-tac-toe game. Can we get a quality upgrade across the board everywhere?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
