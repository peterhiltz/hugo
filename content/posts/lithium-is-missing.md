---
title: "Universal Lithium Shortage"
date: 2020-12-30T14:18:19-04:00
categories: ['posts']
tags: ['space']
showToc:  "true"
author: "Peter Hiltz"
---
Did you know the entire universe seems to be short on lithium? Astronomers actually refer to it as the [Cosmological Lithium Problem](https://physics.unc.edu/the-cosmological-lithium-problem/). The current standard model of the Big Bang theory (not the tv show) matches up with observations about (a) microwave background radiation (b) cosmic expansion as measured by light from distant supernovae and (c) primordial abundances of hydrogen and helium. There is a bit of an issue with (c) however. The standard model predicates three times as much lithium (the third lightest element) than we can find.  Some of the papers trying to figure out possible solutions to the missing lithium are [Cosmological Solutions to the Lithium Problem](https://arxiv.org/pdf/1909.01245.pdf), [Be(n,p)7Li Reaction and the Cosmological Lithium Problem](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.121.042701), [Stellar lithium abundances support standard Big Bang scenario](https://wwwmpa.mpa-garching.mpg.de/mpa/research/current_research/hl2013-3/hl2013-3-en.html), [Cosmological Lithium Problem: A Different Approach](https://fiz.fmf.uni-lj.si/~dunja/AD/presentations/AD_2013-11-25_TP.pdf) and [The Cosmological Lithium Problem](https://www.nanowerk.com/news2/space/newsid=51232.php). I am not qualified to discuss any of those papers except to understand what they wrote in the conclusions.

Research at University of North Carolina (UNC) and the French National Centre for Scientific Research seems to show that the answer to the missing lithium cannot be found in nuclear physics. That leaves (a) poorly understood stellar processes, (b) exotic new physics beyond the "standard model" or (c) [insert your favorite detective plot here]. UNC, working with the University of Montreal and Los Alamos National Lab have recently found some of the missing lithium in the atmosphere of burned out stars (white dwarfs) . See report [here](https://phys.org/news/2020-12-white-dwarfs-cosmological-lithium-problem.html).

As you probably know, lithium is used in rechargeable batteries, drugs for Alzheimer's, bipolar disorder and other medical uses, lubricants, pottery, nuclear reactors, optics and it stimulates and strengthens bone formation. I don't know about you, but I'm wondering whether aliens were trying to recharge the white dwarfs or deal with stellar emotional issues.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
