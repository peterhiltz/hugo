---
title: "Salem Witch Trial Connections"
date: 2021-08-14T14:18:19-04:00
categories: ['posts']
tags: ['genealogy']
showToc:  "true"
author: "Peter Hiltz"
---
I made a comment to my wife about the fact I have ancestors on both sides of the Salem witch trials. So of course I had to prove it out against the family tree (old New England families with relatives who were very interested in genealogy; the family tree currently has over 7,500 people in the database). So, just for fun, here are the results:

# Actually Accused

- Winifred Henchman (1597-1671), eleventh great grandmother, accused and acquitted (1659 well before the Salem hysteria).
- Sarah Hooper (1650-1711), eighth great grandmother, accused and confessed to witchcraft. She, along with seven others, were scheduled to be executed in February. Governor Phips issued last minute reprieves for the condemned. She was the wife of Samuel Wardwell who was convicted and executed.
- Elizabeth Howe Jackson  (1635-1692), ninth great grand aunt. Convicted and hung.
- Margaret Skillings (1625-1706), another ninth great grand aunt. Accused but never tried.
- James Wakelee (), an eighth great granduncle. Accused of witchcraft in 1662 (much earlier than the Salem crazyness) and fled to Rhode Island, forfeiting a bond of 150 pounds.
- Samuel Wardwell (1643-1692), eighth great grandfather, accused, convicted and hung for witchcraft (fortune telling in his case). At one point he confessed when it was believed that confessing would save your life, but recanted his confession at trial on the grounds that he would not lie to save his soul. (He realized that he was going to be hanged anyway)  This "so enraged the court that he was immediately convicted and hanged at Salem on Sept 22, 1692.
- Mercy Wardwell (1673-1754), seventh great grandaunt. She was a daughter of Samuel Wardwell but was found innocent.

# Good Guys

### Defense Witnesses for John Proctor and his wife Elizabeth:

The following relatives risked their own lives by testifying for accused John Proctor and his wife Elizabeth (the Proctors were not relatives) as well as signing a petition opposing the execution sentences. John Proctor was hung but his wife was spared because she had a baby that was soon due.

- Lt. John Andrews (1621-1708), a ninth great grandfather,
- Reginald Foster (1636-1707), another ninth great grandfather,
- Reginald's father Reginald Foster Sr. (1594-1681), a tenth great grandfather,
- Issac Foster (1630-1690), another ninth grandfather,
- Thomas Low (1631-1712), ninth grand uncle.

### Other Interesting Finds

- Henry Herrick (1610-1671), tenth great grandfather, was fined a few shillings for "comforting an excommunicated person".

# Bad Guys

### On Jury sentencing accused persons to death for witchcraft

- Edward Griswold (1607-1691), eleventh great grandfather, foreman of jury which condemned Nathaniel Greensmith to death for witchcraft.
- Henry Herrick (son of Henry Herrick - see other under Good Guys, but not direct line) was a member of the jury and would eventually sign the apology for their decisions.

### Accusers

- Nathaniel Ingersoll (1633-1719), ninth great grand uncle. He was an accuser in at least seven cases.
- Ephraim Foster (1657-1746), first cousin ten times removed, testified against Samuel Wardwell.

### Other Participants

- Joseph Herrick (son of Henry Herrick - see other under Good Guys) was the local constable and arrested most of the people charged with witchcraft, but also allegedly was horrified at the prosecutions, wrote a petition on behalf of a victim and became a leader in the party that "rose against the fanaticism".

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
