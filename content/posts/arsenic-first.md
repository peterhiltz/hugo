---
title: "Arsenic Before Oxygen?"
date: 2020-09-26T14:18:19-04:00
categories: ['posts']
tags: ['science']
showToc:  "true"
author: "Peter Hiltz"
---
So we know that life has existed on Earth for billions of years (think bacteria, not dinosaurs) but we also know that bacterial life existed for 1.5 billion years before there was free oxygen present. Life as we generally know it requires oxygen serve as a vehicle for electrons gained and lost through various metabolic processes. How did it all work pre-oxygen?

There have been various theories posited, including life processes using hydrogen or sulfur or iron. Scientists have been discovering that microbial mats seemed to be breathing both sulfur and arsenic 3.5 billion years ago and they were actually more effective breathing arsenic. Last year researchers have also discovered other microbes currently living in the Pacific Ocean also breathing arsenic.

So what to us is a poison is what some other form of life might breath. It opens up new possibilities in what kind of life might exist in the universe besides the unknown bots you think you are having a conversation with on Facebook. I'll let you figure out your own metaphors on how this might affect your interpersonal interactions with people you don't like.

Links:
- [https://www.nature.com/articles/s43247-020-00025-2](https://www.nature.com/articles/s43247-020-00025-2)
- [https://today.uconn.edu/2020/09/without-oxygen-earths-early-microbes-relied-arsenic-sustain-life/](https://today.uconn.edu/2020/09/without-oxygen-earths-early-microbes-relied-arsenic-sustain-life/)
- [https://www.sciencealert.com/evidence-of-arsenic-breathing-microbes-found-off-the-coast-of-mexico](https://www.sciencealert.com/evidence-of-arsenic-breathing-microbes-found-off-the-coast-of-mexico)
- [https://www.livescience.com/life-on-earth-before-oxygen.html](https://www.livescience.com/life-on-earth-before-oxygen.html)

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
