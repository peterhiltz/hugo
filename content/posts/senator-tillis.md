---
title: "Senator Tillis, I Don't Understand"
date: 2020-12-03T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Senator Thom Tillis is [angry](https://torrentfreak.com/images/tillis-dorsey.txt) that Twitter is not sending a witness to an Anti-Piracy hearing he is holding in December. Setting aside the question of why the Senator is actually holding a hearing in December, I'm puzzled why the Senator thinks Twitter is a giant avenue of piracy. Lies and deceit, yes, but piracy?

The Senator's letter to Twitter claims that "*[Twitter] continues to host and permit rampant infringement of music files on its platform” and that it hasn’t taken any “meaningful steps to address the scale of the problem.*"

I'm sorry, how do you get music albums into a tweet? Is he confusing it with Twitch (in which case he shouldn't be running the hearing)?

The letter also states: "*I have heard that Twitter has been slow to respond to copyright infringement on its platform and also refused to negotiate licenses or business agreements with music publishers or record labels. In contrast, other major-social media companies have done the right thing and mitigated infringing activity on their platfoms by entering into negotiated license agreements to allow uses of music. Does Twitter seek licenses for the use of music? If so, in what instances? Has Twitter made efforts to negotiate license agreements with music publishers and record labels to ensure songwriters- and artists are compensated?*"

Ah. The RIAA wants Twitter to enter into a sitewide license for music that it doesn't use. Now I "understand". For the record, I will state that this blog does not have music other than lyrics which I personally wrote and have not licensed to the various music agencies. Therefore I decline to purchase a site license from the RIAA. Yes, I've lobbied in my professional career. I had ethics. Still do for that matter.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
