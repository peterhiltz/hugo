---
title: "Adding Insult to Self Inflicted Injury"
date: 2020-11-09T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
As you know, the UK is leaving the European Union by choice (aka Brexit) regardless of the economic damage that is expected to cause. As a result, the UK will now have to setup customs import and export locations for goods getting shipped to and from the EU. As part of that project, they are building a 27 acre lorry (truck for the Americans) parking lot in Kent to handle backups from the port in Dover. The construction is now [flooding](https://twitter.com/Jim_Cornelius/status/1325420900720766977) due to heavy rains. I haven't checked on the status at the 26 other parking lot sites that are being constructed. Maybe they should ask for some advice from the Dutch.

In other Brexit news, the UK has apparently [secured tariff wins in their trade deal with Japan](https://www.independent.co.uk/news/uk/politics/brexit-japan-trade-deal-liz-truss-exports-b1619263.html) for birds' eggs, raw hides, fur skins and ultra-strong spirits. None of which the UK exports. Per the Independent: "*Just 10 of 9,444 products will enjoy lower taxes. None of the 10 have been sold to Japan for at least three years. The government then acknowledged that 83 per cent of those gains would go to Japanese exporters, with only 17 per cent to the UK’s.*"

Isn't international trade fun?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
