---
title: "Microsoft Patents Recording and Scoring Meetings on Body Language"
date: 2020-12-01T14:18:19-04:00
categories: ['posts']
tags: ['work']
showToc:  "true"
author: "Peter Hiltz"
---
Ok, the title is just slightly over the top because Microsoft filed for the patents back in July but the filings just became public; it doesn't have them yet. [BBC News](https://www.bbc.co.uk/news/technology-55133141)

So apparently there would be sensors which "could" record,
- which invitees actually attend a meeting
- attendees body language and facial expressions
- amount of time each participant spent contributing to the meeting
- speech patterns "consistent with boredom and fatigue"

This information would be combined with other factors such as how efficient the meeting was, emotional sentiment expressed by participants and how comfortable the environment was into an overall quality score.

So let me get this straight. You have all sorts of training trying to teach people about diversity, how some people think and act and respond differently. I'm going to be really interested to see how the scoring system automatically adjusts to deal with if the room is 80% extrovert or 70% male or ......, time of day, day of week, how hard the team had been pushing before the meeting or the myriad of other factors that could affect the interpersonal behavior.

On the other hand, if Microsoft manages to pull off a scoring system to rival the gods, think of the fantasy sports leagues that would form around scoring meeetings, who was presenting, who was .... The mind boggles.

We haven't even mentioned the whole employee surveillance issue. You know, at some point I have the feeling that I will have to check every morning to see if some dream I had violated someone's prior patent. No, I am not going to record my dreams just so I can pay someone a royalty.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
