---
title: "2020 Philosophy Survey Part 1f: Science, Gender and Race"
date: 2022-02-19T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1f with the topics being Science, Gender and Race. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Science
The question is does the respondent support scientific realism or scientific anti-realism.
- [Scientific Realism](https://plato.stanford.edu/entries/scientific-realism/) is the view that well-confirmed scientific theories are approximately true; the entities they postulate do exist; and we have good reason to believe their main tenets. Realists argue that reliable claims can be made about both
observables and unobservables.
- [Scientific Anti-Realism](https://digital.library.adelaide.edu.au/dspace/bitstream/2440/103722/2/02whole.pdf) "says that all scientists can aspire to is a system of theories sufficient to account for the observable facts of the world and to facilitate predictive success in our interactions with it; the goal is no more than empirical adequacy, that is, that all the observable phenomena are as the theory describes or predicts––theories are only instruments for this purpose, they are no more than pragmatic, convenient and useful fictions, rather than truths... anti-realists say that true knowledge of unobservables
is impossible"
| Realism | Anti-realism | Agnostic |
|---------|--------------|----------|
| 72%     | 15%          | 5%       |
N = 1689

### Gender
See [Gender](https://plato.stanford.edu/entries/feminism-gender/)
The match-up possibilities here are:
1. Biological sex and gender are the same and biology is destiny. On the other hand, Judith Butler argues that both sex and gender are socially constructedand not biological.
2. Social and gender involves how socialization, child rearing and social pressures may determine gender.
3. Psychology and gender look at how physical brain processes (neurology and biopsychology), mental processes (cognitive psychology) and conditioning (behavioral psychology) as well as social practices impact gender.

| Biological | Psychological | Social | Unreal |
|------------|---------------|--------|--------|
| 29%        | 22%           | 63%    | 4%     |
N = 1653

### Race
| Biological | Social | Unreal |
|------------|--------|--------|
| 19%        | 63%    | 15%    |
N = 1649

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
