---
title: "Stop Comparing COVID-19 Death Rates"
date: 2020-10-10T14:18:19-04:00
publishDate: 2020-10-10T14:18:19-04:00
categories: ['posts']
tags: ['health','politics']
showToc:  "true"
author: "Peter Hiltz"
---
I really get tired of people comparing death rates from COVID-19 v. XYZ and thinking that is the end of the story - it isn't so bad. NO. As the [Mayo Clinic](https://www.mayoclinic.org/diseases-conditions/coronavirus/in-depth/coronavirus-long-term-effects/art-20490351) says, *most* people who have it recovery completely within a few weeks. But many people end up with lingering problems because it damages the lung and other organs.

- *Organ damage caused by COVID-19*

- *Organs that may be affected by COVID-19 include:*

- *Heart. Imaging tests taken months after recovery from COVID-19 have shown lasting damage to the heart muscle, even in people who experienced only mild COVID-19 symptoms. This may increase the risk of heart failure or other heart complications in the future.*

- *Lungs. The type of pneumonia often associated with COVID-19 can cause long-standing damage to the tiny air sacs (alveoli) in the lungs. The resulting scar tissue can lead to long-term breathing problems.*

- *Brain. Even in young people, COVID-19 can cause strokes, seizures and Guillain-Barre syndrome — a condition that causes temporary paralysis. COVID-19 may also increase the risk of developing Parkinson's disease and Alzheimer's disease.*

- *Blood clots and blood vessel problems*

- *COVID-19 can make blood cells more likely to clump up and form clots. While large clots can cause heart attacks and strokes, much of the heart damage caused by COVID-19 is believed to stem from very small clots that block tiny blood vessels (capillaries) in the heart muscle.*

- *Other organs affected by blood clots include the lungs, legs, liver and kidneys. COVID-19 can also weaken blood vessels, which contributes to potentially long-lasting problems with the liver and kidneys.*

- *Problems with mood and fatigue*

- *People who have severe symptoms of COVID-19 often have to be treated in a hospital's intensive care unit, with mechanical assistance such as ventilators to breathe. Simply surviving this experience can make a person more likely to later develop post-traumatic stress syndrome, depression and anxiety.*

- *Because it's difficult to predict long-term outcomes from the new COVID-19 virus, scientists are looking at the long-term effects seen in related viruses, such as severe acute respiratory syndrome (SARS).*

- *Many people who have recovered from SARS have gone on to develop chronic fatigue syndrome, a complex disorder characterized by extreme fatigue that worsens with physical or mental activity, but doesn't improve with rest. The same may be true for people who have had COVID-19.*

See also:
- [Nature Magazine](https://www.nature.com/articles/d41586-020-02598-6)
- [World Health Organization](https://www.who.int/docs/default-source/coronaviruse/risk-comms-updates/update-36-long-term-symptoms.pdf?sfvrsn=5d3789a6_2)
- [Medical News Today](https://www.medicalnewstoday.com/articles/long-term-effects-of-coronavirus)
- [Science Magazine](https://www.sciencemag.org/news/2020/07/brain-fog-heart-damage-covid-19-s-lingering-problems-alarm-scientists)
- [Forbe](https://www.forbes.com/sites/mattperez/2020/10/05/even-if-trump-beats-covid-19-long-term-effects-may-still-linger/#6cf256967faa)

If the only data you are counting is death rates, I would never hire you to do any data analysis.

Update: Here is a [story about reinfections worse than the original infection](https://www.webmd.com/lung/news/20201012/first-confirmed-us-case-of-covid-reinfections?fbclid=IwAR2FXH4gu9iAbjg8WLu6X8ACJf4KE5Rwgb_hRFkJhsvOMET0FoAiyUprjnc).

One of the scariest tweets I have ever seen was released by the US President: https://twitter.com/realdonaldtrump/status/1313186529058136070 Given that he was just given the strongest steroids known and a drug known for it creating a sense of euphoria, I'm not surprised by the text, but if you care about the safety of other people, releasing this was unconscionable.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
