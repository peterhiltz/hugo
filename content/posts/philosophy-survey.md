---
title: "2020 Philosophy Survey Part 1a: Aim of Philosophy, God and Eating Animals "
date: 2022-02-14T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1a with the topics being Aim of Philosophy, God and Eating Animals. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Aim of philosophy
I'm leaving this question in the post but I'm not sure whether philosophers actually agree on the definitions and distinctions between of "truth", "understanding" and "wisdom". Your mileage may vary.

| Truth | Understanding | Wisdom |
|-------|---------------|--------|
| 42%   | 56%           | 31%    |
N = 1771

### God
See [God](https://plato.stanford.edu/entries/god-ultimates/)
The allowed answers were only theist, atheist and agnostic. I do want to provide links to descriptions of a couple of other terms. [Pantheism](https://plato.stanford.edu/entries/pantheism/) generally is understood as God is identical with the cosmos, not distinct from the universe. [Panentheism](https://plato.stanford.edu/entries/panentheism/) keeps the separate identities of God and the universe, but maintains that they both influence each other while classical theism maintains that the influence only goes one way - God influences the universe but the universe does not influence God.

| Theist | Atheist | Agnostic |
|--------|---------|----------|
| 19%    | 67%     | 7%       |
N = 1770

### Eating animals and animal products
I'm keeping this in simply as an interesting social comment on academic philosophers.
| Omnivore | Vegetarian | Vegan 18% |
|----------|------------|-----------|
| 48%      | 26%        | 18%       |
N = 1764

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
