---
title: "Nothing is that Simple"
date: 2021-02-22T14:18:19-04:00
categories: ['posts']
tags: ['life','politics']
showToc:  "true"
author: "Peter Hiltz"
---
Sometimes (some people would say often) I feel the need to flag some study in an area I am completely unqualified to comment on the substance. I am, however, really good at pulling apart logic, generalizations and over-broad journalistic pronouncements, however, so I feel completely at ease in doing that. What else is the internet for? WARNING: Long read.

Cambridge University researchers just came out with a study entitled [The cognitive and perceptual correlates of ideological attitudes: a data-driven approach](https://royalsocietypublishing.org/doi/10.1098/rstb.2020.0424) with a rather astounding claim in the abstract: "*[W]e uncovered the specific psychological signatures of political, nationalistic, religious and dogmatic beliefs. Furthermore, data-driven analyses revealed that individuals’ ideological attitudes mirrored their cognitive decision-making strategies. Conservatism and nationalism were related to greater caution in perceptual decision-making tasks and to reduced strategic information processing, while dogmatism was associated with slower evidence accumulation and impulsive tendencies. Religiosity was implicated in heightened agreeableness and risk perception. Extreme pro-group attitudes, including violence endorsement against outgroups, were linked to poorer working memory, slower perceptual strategies, and tendencies towards impulsivity and sensation-seeking—reflecting overlaps with the psychological profiles of conservatism and dogmatism. Cognitive and personality signatures were also generated for ideologies such as authoritarianism, system justification, social dominance orientation, patriotism and receptivity to evidence or alternative viewpoints; elucidating their underpinnings and highlighting avenues for future research. Together these findings suggest that ideological worldviews may be reflective of low-level perceptual and cognitive functions.*"

The study notes that cognitive decision making strategies are much more important than demographics, but taking everything together, the study still only explains 23% of the variance in religiosity and dogmatism and 32% of the variance in political conservatism. I think that if your analysis only explains 23%-32% of a variance, claiming that you uncovered the specific psychological signatures may be a little overstating the situation.

There are a couple of items in the abstract that I want to flag before we go any farther. The claim that extreme pro-group attitudes were linked to poorer working memory was not stated in the actual report. The report did note that "*political and national is associated with reduced strategic information processing (reflecting variables associated with working memory capacity, planning, cognitive flexibility and other higher-order strategies) is consistent with a large body of literature indicating that right-wing ideologies are frequently associated with reduced analytical thinking and cognitive flexibility*". This **does not mean they have reduced analytical thinking capacity**. As other studies have pointed out, non-cognitive inputs also play into the decision making and in some personalities, those non-cognitive inputs are deemed more important. This also applies to the phrase "reduced strategic information processing". Don't confuse the weighting of an input with its ability.

### The Guardian Article

The study was immediately reported by Guardian as [People with extremist views less able to do complex mental tasks, research suggests](https://www.theguardian.com/science/2021/feb/22/people-with-extremist-views-less-able-to-do-complex-mental-tasks-research-suggests). The Guardian headline was immediately cited in left leaning social media circles as only meaning right wing extremists. Sigh. Not exactly an argument likely to get people on your side. One of the study authors is also one of the authors of another study [The partisan mind: Is extreme political partisanship related to cognitive inflexibility?](http://scholar.google.com/scholar_lookup?hl=en&volume=149&publication_year=2020&pages=407-418&journal=J.+Exp.+Psychol.+Gen.&author=L+Zmigrod&author=PJ+Rentfrow&author=TW+Robbins&title=The+partisan+mind%3A+is+extreme+political+partisanship+related+to+cognitive+inflexibility%3F) which concludes "*In a sample of over 700 U.S. citizens, partisan extremity was related to lower levels of cognitive flexibility, regardless of political orientation, across 3 independent cognitive assessments of cognitive flexibility. This was evident across multiple statistical analyses, including quadratic regressions, Bayes factor analysis, and interrupted regressions. These findings suggest that the rigidity with which individuals process and respond to nonpolitical information may be related to the extremity of their partisan identities. (APA PsycInfo Database Record (c) 2020 APA, all rights reserved)*"

The Guardian article stated "*A key finding was that people with extremist attitudes tended to think about the world in black and white terms, and struggled with complex tasks that required intricate mental steps, said lead author Dr Leor Zmigrod at Cambridge’s department of psychology. Individuals or brains that struggle to process and plan complex action sequences may be more drawn to extreme ideologies, or authoritarian ideologies that simplify the world,” she said. She said another feature of people with tendencies towards extremism appeared to be that they were not good at regulating their emotions, meaning they were impulsive and tended to seek out emotionally evocative experiences. “And so that kind of helps us understand what kind of individual might be willing to go in and commit violence against innocent others.*

At this point I want to scream a bit. "Struggle with complex tasks" is not in the report and is really a loaded term. What do you mean by "struggle"? Taking longer is not necessarily a "struggle". I can take more time simply by going over my analysis just making sure I am confident in my conclusion.

Similarly, "not good at regulating their emotions" is not in the study report. The character of Dr. Spock on Star Trek would claim that all humans have difficulty regulating their emotions. Again, a loaded phrase.

The Guardian goes on: "*Participants who are prone to dogmatism – stuck in their ways and relatively resistant to credible evidence – actually have a problem with processing evidence even at a perceptual level, the authors found. For example, when they’re asked to determine whether dots [as part of a neuropsychological task] are moving to the left or to the right, they just took longer to process that information and come to a decision,” Zmigrod said. In some cognitive tasks, participants were asked to respond as quickly and as accurately as possible. People who leant towards the politically conservative tended to go for the slow and steady strategy, while political liberals took a slightly more fast and furious, less precise approach.*"

Again, cue my screaming for responsible journalism. Characterizing dogmatism as "stuck in their ways and relatively resistant to credible evidence" is not in the study report. Neither is the claim that participants prone to dogmatism have a problem with processing evidence at a perceptual level. Again, taking longer to make a decision is not evidence of a "problem with processing evidence". This is particularly true given the sentence that political liberals take a slightly more fast and furious, less precise approach. If I was a political conservative, I would be pointing at this as evidence that the Guardian is opting for the knee jerk wrong conclusion over slow stead and right. Arrgh.

Enough with journalism; let's get into the actual study. However, before we look deeper into the study itself, it might be useful to note some earlier studies which looked at different factors. (You can also dive deeper into the footnotes of the study).

### Other Studies

A study from 2007 entitled [Neurocognitive correlates of liberalism and conservatism](https://2012election.procon.org/sourcefiles/Amodio_Neurocognitive_Correlates.pdf) looked at anterior cingulate cortex (the ACC) activity in detecting when one’s habitual response tendency is mismatched with responses required by the current situation and compared that to political orientation. The results of that study indicated that "*... political orientation, in part, reflects individual differences in the functioning of a general mechanism related to cognitive control and self-regulation. Stronger conservatism (versus liberalism) was associated with less neurocognitive sensitivity to response conflicts. At the behavioral level, conservatives were also more likely to make errors of commission. Although a liberal orientation was associated with better performance on the response-inhibition task examined here, conservatives would presumably perform  better on tasks in which a more fixed response style is optimal.*"

A similar study on ACC activity from 2011 [Political Orientations are Correlated with Brain Structure in Young Adults](https://www.sciencedirect.com/science/article/pii/S0960982211002892) claimed to have found correlations between political attitudes and brain structure - liberals were associated with more gray matter volume in the ACC and conservatism was associated with increased volume of the right amygdala which deals with emotional reactions. The study explicitly said that the data does not determine a causal role. "*Psychological differences between conservatives and liberals determined in this way map onto self-regulatory processes associated with conflict monitoring. Moreover, the amplitude of event-related potentials reflecting neural activity associated with conflict monitoring in the anterior cingulate cortex (ACC) is greater for liberals compared to conservatives. Thus, stronger liberalism is associated with increased sensitivity to cues for altering a habitual response pattern and with brain activity in anterior cingulate cortex.*"

The amygdala has many functions, including fear processing and individuals with a large amygdala are more sensitive to fear. The 2011 study continues: "*Conservatives respond to threatening situations with more aggression than do liberals [1] and are more sensitive to threatening facial expressions [5]. This heightened sensitivity to emotional faces suggests that individuals with conservative orientation might exhibit differences in brain structures associated with emotional processing such as the amygdala. Indeed, voting behavior is reflected in amygdala responses across cultures.*"

A study from 2020 entitled [Closed-minded cognition: Right-wing authoritarianism is negatively related to belief updating following prediction error](https://link.springer.com/article/10.3758/s13423-020-01767-y) found evidence that people with a desire for order, structure and preservation of social norms are less successful at correcting false beliefs in response to prediction error.

### The Actual Cambridge Study Report

The Cambridge study is trying to be data driven about cognition in an attempt to avoid concerns that "*psychologists of politics, nationalism and religion generate hypotheses and study designs that confirm their prior beliefs about the origins of social discord.*" The study focused on cognitive tasks and claimed "*This allowed us to address the question: what psychological factors are most predictive of individuals’ ideological orientations?*" Of course, by focusing on cognition (the ability to acquire factual information), the study ignores social, emotional and creative development and analysis, so I am a bit concerned that they miss the blinders they are wearing themselves.

The study looked at different ideological orientations (they claimed 16, but I only count 13 here):

 - social conservatism is a 7-item scale. Participants indicate their warmth towards a set of policies. Policies: abortion, traditional marriage, traditional values, family unit, religion, patriotism, military and national security. [Peter side comment: The study concludes that social conservatism and economic conservatism have different psychological profiles.]

 - economic conservatism is a 5-item scale. Participants indicate their warmth towards a set of policies. Policies: limited government, fiscal responsibility, welfare benefits, business, gun ownership. [Peter side comment: What does gun ownership have to do with economic conservatism?]

 - nationalism is a  9-item scale. Participants rate their agreement with statements such as ‘The United States is no more superior than any other country’ (reverse-coded) and ‘We should do anything necessary to increase the power of our country, even if it means war’.

 - patriotism is a 9-item scale. Participants rate their agreement with statements such as ‘I find the sight of the American flag very moving’ and ‘I have great love for my country’ [Peter side comment: Why do I feel like British researchers trying to figure out how to measure American patriotism is like watching someone trying to find a cat in a dark room?]

 - authoritarianism is a 4-item scale. Participants indicate whether they believe children ought to be ‘obedient’, ‘respectful’, and ‘well-mannered’ or ‘curious’, ‘independent’, and ‘self-reliant’ [Peter side comment: Why is this limited to parent-children relationships? It needs to be expanded into many more categories.]

 - social dominance orientation is a 4-item scale. Participants rate their agreement with statements such as ‘we should not push for group equality’ and ‘superior groups should dominate inferior groups’.

 - system justification is an 8-item scale. Participants are presented with statements such as ‘In general, American society is fair’ and ‘American society is set up so that people usually get what they deserve’

 - extreme pro-group actions is a 5-item scale. Participants are asked to rate their agreement with statements such as ‘I would fight someone insulting or making fun of America as a whole’ and ‘I would sacrifice my life if it saved another American's life’ [Peter side comment: these questions really miss the mark. At best they are more indicative of patriotism. They completely miss racial/religious/other cultural groupings.]

 - dogmatism 11-item is updated version of Altemeyer's measure of dogmatism [Peter side comment: Per [DOGMATIC DELUSIONS: Can Neuroscience and Social Psychology Help Us Understand How America Has Lost Its Mind?](https://www.thefreelibrary.com/DOGMATIC+DELUSIONS%3a+Can+Neuroscience+and+Social+Psychology+Help+Us...-a0650413070) "*In 1996 Bob Altemeyer proposed to define dogmatism as an unchangeable and unjustified certainty in one's beliefs and measured with a new DOG scale. Unfortunately, statisticians found as little correlation among items on the DOG questionnaire as on previous scales. So, although most of us feel confident that we can recognize it when we see it, "dogmatism" is harder to identify objectively.*"]

 - intellectual humility: Comprehensive Intellectual Humility Scale measuring four facets of intellectual humility:
   - Factor 1: independence of intellect and ego
   - Factor 2: openness to revising one's viewpoint
   - Factor 3: respect for others’ viewpoints
   - Factor 4: lack of intellectual overconfidence

 - importance of religion (Pew Research Centre)	participants were asked: ‘How important is religion in your life?’ Response options: not at all important, slightly important, moderately important, very important, extremely important

 - religious prayer frequency (Pew Research Centre)	participants were asked: ‘People practice their religion in different ways. Outside of attending religious services, how often do you pray?’ Response options: several times a day, once a day, a few times a week, once a week, a few times a month, seldom, never

 - religious service attendance frequency (Pew Research Centre)	participants were asked: ‘Aside from weddings and funerals, how often do you attend religious services?’ Response options: more than once a week, once a week, once or twice a month, a few times a year, seldom, never

 The study notes that:

 - The political conservatism factor, which reflects tendencies towards political conservatism and nationalism, was significantly associated with greater caution and temporal discounting and reduced strategic information processing in the cognitive domain, and by greater goal-directedness, impulsivity, and reward sensitivity, and reduced social risk-taking in the personality domain.

 - The dogmatism factor was significantly associated with reduced speed of evidence accumulation in the cognitive domain and by reduced social risk-taking and agreeableness as well as heightened impulsivity and **ethical risk-taking** in the personality domain. [Peter side comment: I could not find what they mean by "ethical risk-taking".]

 - The religiosity factor was also significantly associated with greater caution on speeded tasks, and reduced strategic information processing and social risk-taking, but in contrast to dogmatism and political conservatism, religiosity was associated with greater agreeableness and risk perception. [Peter side comment: I am surprised at the greater agreeableness characteristic but I haven't asked the authors what that really means.

### Conclusion
It is an interesting approach trying to get ideological signatures but as the actual report (as opposed to the abstract says), it still only explains at best a third of the variance in the real world.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
