---
title: "Government Obligations?"
date: 2020-10-09T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
For any of you small government types reading this, the following applies to state and local governments as well as the Federal govt.

The Supreme Court has held that the government (police or social services) does not have a duty to protect individuals from harm, even when they know harm will occur. See [DeShaney vs. Winnebago  489 U.S. 189 (1989)](https://caselaw.findlaw.com/us-supreme-court/489/189.html) and [Town of Castle Rock vs. Gonzales](https://www.law.cornell.edu/supct/html/04-278.ZS.html). See also [Warren v. District of Columbia, 444 A.2d 1 (D.C. Ct. of App. 1981)](https://caselaw.findlaw.com/us-dc-circuit/1061352.html), in which three women who were being held hostage by two men twice managed to telephone police and request their help. The police never came, and the three women were beaten, robbed, and raped during the following 14 hours. It is fairly obvious that the slogan "To Protect and Serve" does not mean protect and serve the population, but rather to protect and serve [insert the law, society, government, the police themselves, ???]. How do you end that sentence?

One could argue that police exist to maintain "peace" in general by enforcing laws and shutting down protests and riots, ignoring all the niceties about first amendment rights to freedom of speech, assembly and petition the government for a redress of grievances.

Chief Justice Rehnquist's opinion in DeShaney stated that there is no language in the 14th Amendment that says the state must protect the citizen from the actions private actors, just that the state cannot cause the harm. That would be somewhat understandable except for the doctrine of qualified immunity which pretty much guarantees that the state actors (as opposed to the state) are not liable either.

The legal doctrine of qualified immunity as defined by the courts provides that public servants who commit a wrongful act are shielded from civil liability unless the plaintiff can prove the official had notice that the act they were performing violated one or more constitutional rights that had been clearly established as a violation by a court, statute, law or policy adopted by their employing agency.

As an example of qualified immunity, two Fresno, California, police officers executing a search warrant in 2013 and, during the search allegedly stole over $225,000 worth of items from the property they were searching. A unanimous 9th Circuit U.S. Circuit Appeal Court panel ruled in [Jessop v. City of Fresno](https://cdn.ca9.uscourts.gov/datastore/opinions/2019/09/04/17-16756.pdf) that “the officers did not have clear notice that it [the theft] violated the [Constitution’s] Fourth Amendment.” As a result, they could not be sued by the owners of the property.

I've got three questions about Jessop that I can't seem to find answers. (1) Did qualified immunity just cover the officers or did it include the City of Fresno? (2) Qualified immunity does not apply if the state actor's actions violated statutory law. Clearly theft violates California state law (both civil tort law and criminal law. Why was this not discussed in the opinion? (3) Qualified immunity should have applied only to the assets seized pursuant to the warrant. The items allegedly stolen were not seized pursuant to the warrant, so how does qualified immunity apply?

In [Corbitt v. Michael Vickers](https://media.ca11.uscourts.gov/opinions/pub/files/201715566.pdf), a criminal suspect named Christopher Barnett entered Amy Corbitt's front yard where six children were also playing. The police followed in pursuit of Barnett. The officers held Barnett and the children at gunpoint, ordering them all to get on the ground. All complied, including Barnett, who was apprehended without incident. After restraining the suspect, police demanded that the children remain on the ground. While the children were on the ground obeying that order, police office Vickers shot twice at the Corbitt family dog, even though all parties agreed that the dog did not appear threatening. Both shots missed the dog, but the second one hit on of the children lying on the ground eighteen inches from the police officer. (No explanation is given on why a police officer with aim that bad can be given a gun.) Vickers claims accidently shooting the child provides qualified immunity because he can only be liable for intentional harm.

The court agreed that the child was "seized" or in custody. The court, however, held that "Vicker's action of intentionally firing at the dog and unintentionally shooting the child did not violate any clearly established Fourth Amendment rights and did not establish the use of excessive force. [The boy’s mother] “Corbitt failed to present us with any materially similar case from the United States Supreme Court, this Court or the Supreme Court of Georgia that would have given Vickers fair warning that his particular conduct violated the [Constitution’s] Fourth Amendment”.

In effect, bystanders have less protections than suspects.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
