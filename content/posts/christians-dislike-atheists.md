---
title: "Christians Dislike Atheists More Than Atheists Dislike Christians"
date: 2023-01-04T14:18:19-04:00
categories: ['posts']
tags: ['life','religion']
showToc:  "true"
author: "Peter Hiltz"
---
A [peer-reviewed study](https://www.secularismandnonreligion.org/articles/10.5334/snr.136/) found Christians hold more animosity towards atheists than atheists hold toward them. The going in assumption of the researchers was that both groups would be more favorable to their own in-group but it is actually asymmetric. The atheists do not really view Christians any worse than they view other atheists. On the other hand, Christians view atheists as worse than Muslims or convicted criminals.

Unlike some other studies, this one took a cross section of the American population rather than just getting samples from atheist organizations and samples from Christian churches. The belief is that this gets rid of the bias that results from activists on both sides.

On the atheist side, the researchers speculate (and they admit that it is speculation) that your typical atheist on the street knows plenty of Christians who they characterize as decent people and that offsets the hyperbole in the media.

On the Christian side, the researchers speculate a few different reasons why the typical Christian on the street has more antipathy towards atheists.

1. They don't "know" any atheists in real life and only hear about the activists. They may know people who are atheists, but don't advertise that fact, so there is no offsetting "decent atheist" to offset the portrayal of atheist activists in the media or in their church.

2. There may be an instinctual feeling that atheism is an existential threat to their religous/cultural/ethnic group. Atheists, as much as they dislike legislation allowing discrimination against minorities, do not view Christians as an existential threat to their "group" because there is no atheist "group" other than the very small groups of activists. Fear can have a huge impact on people's beliefs and behaviors.

Other reasons have been hypothecated by others.

3. Church leaders claim that atheists have no morals and are, therefore, not to trusted. The typical atheist response is "If you need threats of punishment (Hell) or bribery (Heaven) to be a good person, then maybe the problem is you. I do as much raping and murdering as I want, which is 0. So who actually has morals?" While logically true, that atheist response tends to be viewed as an accusation that the Christian is not a moral person, so the Christian gets upset. The fact that the Christian charge towards atheists is exactly the same is ignored.

4. Church and political leadership claim that atheists support of women's rights, ethnic minority rights and LGBTQ+ rights will "destroy traditional America". Most atheists are typically "live and let live" types but believe that denying equal rights is actually an attack on minorities, at which point they want to support the attacked underdog. Here we have the problem that if the minorities are not defended, they will never have equal rights, but does pounding the drum loudly alienate too many people to be successful?

5. Christian parents fear atheists will turn their children into atheists, which means that they will not be able to see their children in the afterlife.

Looking at what religious groups Christians collectively viewed favorably, there was a high degree of in-group preference, then a large drop to Jews, then another drop to Buddhists, Hindus and Muslims in that order, then a huge drop to Atheists. Now consider the hypothecated reasons for why Christians have more animosity to atheists. Why the huge distinction between atheists and the other religious groups? Maybe the thought is that atheists are a "home grown threat" and the other groups are viewed as foreign (somewhere else's problem) and therefore not a real existential threat?

To use a Hindu saying "All is Maya". (All is Illusion.) Perception drives so much of how you react to something or someone.

Again, like so many of my posts about tribalism problems, I don't have a good solution, particularly when so much of the animosity is stirred up by media and political, religious and business leaders who are not acting in good faith. Unlike most people, I do think about religion and philosophy and, obviously, am quite willing to talk about it. If more people can and are willing to initiate reaching across the divide just person to person, so there is some dilution of the hate messages, maybe we can ignore the hate mongers on all sides and find a bit more toleration.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
