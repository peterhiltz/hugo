---
title: "Winds of Change"
date: 2025-02-04T04:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Something I wrote a few months ago with nothing to do with current political events.
<!--more-->

## Winds of Change  
> The whisper of the winds of change			
> start softly and unclear   	         			
> We focus on the day to day       		         	
> and the customs we revere.  In the       		
> shelter of our daily life		        			
> the time just rolls on by                  			
> the echoes of the hours past            			
> drown out the breezes' sigh.               		
> 
> Nothing stays the same, the        			
> seasons come and go. The                 		
> sands of time are driven as               		
> the winds of changes blow, can you     		
> try to hold a storm when you            		
> hear the tempest roar?                    		
> We must reset our bearings, so we can	
> find our way to shore.  	           		
> 								
> We can track a hurricane                  		
> as it blows across the sea              			
> knowing when to batten down and        		
> knowing when to flee, but we           		
> miss the hint when the winds of change    	
> are still a quiet breeze, and we’re 			
> not prepared when the twister howls     		
> and we’re buried in debris.                  		
> 
> Nothing stays the same, the        			
> seasons come and go. The                 		
> sands of time are driven as               		
> the winds of changes blow, can you     		
> try to hold a storm when you            		
> hear the tempest roar?                    		
> We must reset our bearings, so we can	
> find our way to shore.  	           		
> 
> We don’t like curiosity           		       		
> we know that leads to change               		
> but when we finally realize               		
> somethings feeling strange               		
> we first deny and then attack           		
> the bearer of bad news	               			
> as if the gale that buffets us	   			
> will care about our views.    
> 
> Nothing stays the same, the        			
> seasons come and go. The                 		
> sands of time are driven as               		
> the winds of changes blow, can you     		
> try to hold a storm when you            		
> hear the tempest roar?                    		
> We must reset our bearings, so we can	
> find our way to shore. 

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.

