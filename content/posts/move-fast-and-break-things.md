---
title: "Move Fast and Break Things"
date: 2025-03-02T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I heard a news interviewer asking someone about how they felt about Elon Musk's applying normal business methods, specifically "move fast and break things" to the US Federal government and really wanted to be the one to answer the question. The question demonstrates a fundamental misunderstanding that must be corrected.

1. "Move Fast and Break Things" is not a business method, it is a Venture Capitalist approach to businesses. 

2. It is never applied to societal infrastructure. The government is societal infrastructure.

3. It "works" for VCs because they don't care if it breaks 9 companies out of 10 because they are betting that they will make more on the one company it works than the investments they lose on the other nine. There are no nine other governments to take over if this one breaks.

4. It "works" for VCs because even if they break all their toys, they are still wealthy enough to survive. They don't care about the employees or customers of the companies they break - nor do they care about the citizens of the US let alone the employees of the government.

Evil begins when people are treated as things. We've crossed that border. 

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.

