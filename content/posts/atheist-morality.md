---
title: "Morality - Religious and Atheists"
date: 2021-03-03T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
An interesting [study](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0246593) was recently released (Feb 24, 2021) on morality and atheists v. religious. It started from the observation that while attitudes in the US have improved towards minorities over time, attitudes towards atheists have not. According to a Pew poll in 2019, 44% of Americans think belief in God is necessary for morality (apparently people only behave well if someone is watching). The researchers decided to look at morality among the atheists and religious in the US (a religious country) and Sweden (a non-religious country) and investigate "*whether disbelievers differ from believers in how they conceptualize morality.*". The summary result is that both believers and disbelievers have a moral compass, but it is calibrated differently - believers are more likely to promote moral values that serve group cohesion.

In order to understand what they mean about group cohesion, I will pull from [Liberals and Conservatives Rely on Different Sets of Moral Foundations - Appendix A](https://fbaum.unc.edu/teaching/articles/JPSP-2009-Moral-Foundations.pdf)

**Ingroup:**
- Whether or not someone did something to betray his or her group
- Whether or not the action was done by a friend or relative of yours
- Whether or not someone showed a lack of loyalty
- Whether or not the action affected your group
- Whether or not someone put the interests of the group above his/her own
- If I knew that my brother had committed a murder, and the police were looking for him, I would turn him in [reverse scored].
- When it comes to close friendships and romantic relationships, it is okay for people to seek out only members of their own ethnic or religious group.
- Loyalty to one’s group is more important than one’s individual concerns.
- The government should strive to improve the well-being of people in our nation, even if it sometimes happens at the expense of people in other nations

**Authority:**
- Whether or not the people involved were of the same rank or status
- Whether or not someone failed to fulfill the duties of his or her role
- Whether or not someone showed a lack of respect for legitimate authority
- Whether or not an authority failed to protect his/her subordinates
- Whether or not someone respected the traditions of society
- Men and women each have different roles to play in society.
- If I were a soldier and disagreed with my commanding officer’s orders, I would obey anyway because that is my duty.
- Respect for authority is something all children need to learn.
- When the government makes laws, those laws should always respect the traditions and heritage of the country

**Purity:**
- Whether or not someone did something disgusting
- Whether or not someone violated standards of purity and decency
- Whether or not someone did something unnatural or degrading
- Whether or not someone acted in a virtuous or uplifting way
- Whether or not someone was able to control his or her desires
- People should not do things that are revolting to others, even if no one is harmed.
- I would call some acts wrong on the grounds that they are unnatural or disgusting.
- Chastity is still an important virtue for teenagers today, even if many don’t think it is.
- The government should try to help people live virtuously and avoid sin.

The above are considered moral values that serve group cohesion. Compare them to items like the following, which are considered more "individualistic":

**Harm:**
- Whether or not someone was harmed
- Whether or not someone suffered emotionally
- Whether or not someone used violence
- Whether or not someone cared for someone weak or vulnerable
- If I saw a mother slapping her child, I would be outraged.
- It can never be right to kill a human being.
- Compassion for those who are suffering is the most crucial virtue.
- The government must first and foremost protect all people from harm.

**Fairness:**
- Whether or not some people were treated differently than others
- Whether or not someone was denied his or her rights
- Whether or not someone acted unfairly
- Whether or not someone ended up profiting more than others
- If a friend wanted to cut in with me on a long line, I would feel uncomfortable because it wouldn’t be fair to those behind me.
- In the fight against terrorism, some people’s rights will have to be violated [reverse scored].
- Justice, fairness and equality are the most important requirements for a society.
- When the government makes laws, the number one principle should be ensuring that everyone is treated fairly.

The study started by looking at prior research charity donations and prejudice. With respect to charitable donations, previous research has already shown that if donations are given in private, there is no difference between believers and disbelievers. On the other hand church attendants tend to give more in public because churches solicit more often and believers are trying to manage the impressions of the other members of their group. With respect to prejudice, prior research had found that religiousity is associated with prejudice towards minority groups (ethnic, racial, gender, religious belief and others "minorities"). (The report does distinguish between religiousity, belief and church attendants. It also tried to control for gender, age, level of education and political orientation.)

Quick Summary:

Disbelievers and believers had similar morality when it came to individual morality (including protecting vulnerable individuals), but disbelievers were considerably weaker endorsers of morality tied to the integrity or cohesion of a group. This is consistent with the prior research finding religiousity having an positive association with prejudice towards minority groups. The results in Sweden were consistent with the results in the US.

Disbelief also seemed to be an indicator of more endorsement of consequentialist thinking (more inclined to judge the appropriateness of actions based on their consequences relative to the consequences of inaction). This was consistent between the US and Swedish studies.

In effect, disbelievers have a more constrained view of morality. They will tend to look at the consequences of an individual action on a case by case basis. Interestingly, this is apparently viewed as having lower moral character and less empathetic.

I do feel the need to quote Penn Jillette: "*I'm raping, and murdering all the people I want to as an atheist. It just happens that the number of rapes and murders want to commit is zero. And if the only thing holding you back from committing heinous acts is the fear of a God's righteous punishment, then you're just a truly terrible person.*"

I also feel the need to quote Robert Foster: "*You do the right things because they are the right things, not because you think you will get rewarded if you do or punished if you don't.*"

Side Note:

As a side note, there had been a proposal that people who struggle to understand other people's intentions and mental states (mentalizing) should have a hard time conceptualizing God. This study confirms other studies that mentalizing was not reliably related to disbelief. The study did, however, show that stronger mentalizing abilities where positively associated with both endorsement of individual morality and morality which supports the integrity of a group.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
