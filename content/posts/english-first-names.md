---
title: "English Names I Would Not Give to a Fictional Character"
date: 2022-04-29T14:18:19-04:00
categories: ['posts']
tags: ['humor']
showToc:  "true"
author: "Peter Hiltz"
---
I needed some random names, by nationality, for a project. So I turned to the intertubes and did a search for common last names for several countries and male and female baby names for those countries and then just did random combinations. Straightforward and relatively boring. I did a double take at the first English generated name and went back and looked at the very large list of actual english boy and girl names. The following is a list of actual English names I would not give to a fictional character even if they make just as much sense as the "meanings of names" alledgedly are. I guess I'm just boring.

(Girls) Baby, Cabin, Cad, Disney, Fable, Gypsy, Heir, Holiday, Hollywood, Jacobean, Mistletoe, Murgatroyd, Pagan, Pennsylvania, Royalty, Sidereal, Sincerely, Singleton, Symphony, Twilight, Zenith

(Boys) Africa, Alchemy, America, Baby, Bachelor, Beowulf, Cad, Calico, Carnation, Cash, Cashmere, Disney, Fable, Fountain, Gotham, Galahad, Gypsy, Heir, Holly, Hollywood, Mistletoe, Mosaic, Orchid,  Royalty, Royal, Rogue, Shakespeare, Singleton, Sidereal, Sir, Symphony, Twee, Twilight, Warrior, Zenith.

So here's a question. Why did I decide to leave "Warrior" as a woman's name and not as a man's name?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
