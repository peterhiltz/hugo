---
title: "2020 Philosophy Survey Part 1n: Other Minds, Ought Implies Can and Newcombe's Problem"
date: 2022-02-25T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1n with the topics being "Other Minds", "Ought Implies Can" and "Newcombe's Problem". I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Other minds
This question posed whether the respondents view the different categories as actually having "minds".

|        | Adult Humans | Cats | Fish | Flies | Worms | Plants | Particles | Newborn Babies | Future AI |
|--------|--------------|------|------|-------|-------|--------|-----------|----------------|-----------|
| Accept | 95%          | 89%  | 65%  | 35%   | 24%   | 7%     | 2%        | 84%            | 39%       |
| Reject | 0%           | 4%   | 15%  | 38%   | 47%   | 80%    | 89%       | 5%             | 27%       |
N = 1092

### Ought implies can
Does the respondent agree that if someone says "X ought to perform action Y", this implies that it is at least in principle possible to perform action Y?

| Yes | No  |
|-----|-----|
| 63% | 28% |
N = 1085

### Newcombe's Problem
See [Newcomb's problem](https://plato.stanford.edu/entries/prisoner-dilemma/). This variation on the Prisoner's Dilemma proposes the following:

There is a reliable predictor, another player, and two boxes designated A and B. The player is given a choice between taking only box B, or taking both boxes A and B. The player knows the following:[4]

- Box A is transparent and always contains a visible $1,000.
- Box B is opaque, and its content has already been set by the predictor:
 - If the predictor has predicted the player will take both boxes A and B, then box B contains nothing.
 - If the predictor has predicted that the player will take only box B, then box B contains $1,000,000.

By “cooperating” (choosing the opaque box), each player ensures that the other gets a million dollars (and a thousand extra for defecting). By “defecting” (choosing both boxes) each player ensures that he will get thousand dollars himself (and a million more if the other cooperates). As long as m>t>0, the structure of this game is an ordinary two-player, two-move PD (and any such PD can be represented in this form). Furthermore, the arguments for “one-boxing” and “two-boxing” in a Newcomb problem are the same as the arguments for cooperating and defecting in a prisoner's dilemma where there is positive correlation between the moves of the players. Two boxing is a dominant strategy: two boxes are better than one whether the first one is full or empty. On the other hand, if the predictor is reliable, the expected payoff for one-boxing is greater than the expected payoff for two-boxing.

| One Box | Two Boxes | Agnostic |
|---------|-----------|----------|
| 31%     | 39%       | 22%      |
N = 1071


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
