---
title: "Must Comment on New Wombat Discovery"
date: 2021-02-01T14:18:19-04:00
categories: ['posts']
tags: ['science']
showToc:  "true"
author: "Peter Hiltz"
---
It has been known for a long time that wombats produce cubical poo. Zoologist Eric Guiler noted it in 1960, but I think it is a safe bet that Tasmanian inhabitants knew a long time before that. Finally scientists have figured out [how they do it](https://phys.org/news/2021-02-wombats-excrete-poop-cubes.html). Historically scientists have said "that's weird" but never looked into how a round tube could create cubes. As so often happens in science, the discovery of "how" was accidental. Dr. Scott Carver from the University of Tasmania was dissecting a wombat cadaver as part of research into treating mange disease and noted that the cadaver had square poo in the intestines, so it was not "cubed" at exit. This led to science's most exciting statement: "Isn't that odd?"

Researchers in Australia have now tested the physical properties of the intestines (10 meters long in your typical bare faced wombat) and researchers at the Georgia Institute of Technology made mathematical models. Without harming any wombats in the research, they discovered that the intestines had two flexible regions and two stiff regions and the contractions (some 10,000 over five days) created the square corners. The corners are more [pronounced in drier conditions](https://arstechnica.com/science/2021/01/its-the-wombats-strange-intestines-not-its-anus-that-produces-cubed-poo/). Whether this new process will have applications in manufacturing is up to your imagination.

Of course this leads to the question of "why?" Wombats place feces at specific points in their territory. They have strong senses of smell and the theory is that this is their way of communicating with other wombats. Humans had to learn to carve no trespassing signs on wood and stone, wombats apparently decided to sh**tpost, something humans have only learned to do recently. Sorry, couldn't help myself.

A comment in arstechnica.com quipped: "*Somewhere there’s a pissed off Platypus who just realized they’ve been out-weirded by the Wombat.*"

Abstract [here](https://pubs.rsc.org/en/content/articlelanding/2021/sm/d0sm01230k#!divAbstract)
Video by researchers [here](https://twitter.com/i/status/1355167311574020100)

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
