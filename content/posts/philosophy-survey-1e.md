---
title: "2020 Philosophy Survey Part 1e: Metaethics, Analytic-Synthetic Distinction and Aesthetic Value"
date: 2022-02-18T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1e with the topics being Metaethics, Analytic-Synthetic Distinction and Aesthetic Value. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Metaethics
See [Meta-ethics](https://plato.stanford.edu/entries/metaethics/)
Take a statement like "Nigel has a moral obligation to keep his promise." A moral realist would say that this is (1) a moral claim, (2) that makes a factual claim and (3) the claim can be true or false depending on whether it has the facts right.

Anti-realists fall into one of two general categories: (1) Noncognitivists who think that moral claims do not pretend to report facts based on truth or falsehood (e.g. they are a way of expressing emotions or controlling others behavior or taking a stand for or against some thing or action) and (2) Error theorists who think that moral claims do purport to report facts in light of which they are true or false, but deny that any moral claims are actually true because the required facts cannot be found.

| [Moral Realism](https://plato.stanford.edu/entries/moral-realism/) | [Moral Anti-realism](https://plato.stanford.edu/search/r?entry=/entries/moral-anti-realism/&page=1&total_hits=111&pagesize=10&archive=None&rank=3&query=metaethics) |
|---------------|--------------------|
| 62%           | 26%                |
N = 1719

### Analytic-synthetic distinction
See [Analytic-synthetic distinction](https://plato.stanford.edu/entries/analytic-synthetic/)
Analytic sentences are ones whose truth depends on the meaning of its constituent terms and how they are combined. So, as the SEP says, "Ophthalmologists are doctors" is a sentence whose truth does not rely on the facts about the outside world. Saying "Ophthalmologists are rich" would be a sentence whose truth does depend upon the facts of the world and therefore is "synthetic". Philosophers who answer "No" to this question believe that analytic philosophy will, at some point in the future, be able to prove the truth of anything without relying on the facts of the world. Some of them argue that analytic philosophy must allow intuition whereby the truth of certain claims is simply grasped "an act of rational insight or rational intuition … [that] is seemingly (a) direct or immediate, nondiscursive, and yet also (b) intellectual or reason-governed … [It] depends upon nothing beyond an understanding of the propositional content itself…." Bonjour, L., 1998, In Defense of Pure Reason. The philosophers who answer "Yes" to the distinction respond that there is no way to distinguish claims of rational insight from folk belief, mere dogma or some deeply held empirical conviction.

| Yes | No  |
|-----|-----|
| 62% | 26% |
N = 1703

### Aesthetic value
See [Aesthetic value](http://plato.stanford.edu/entries/beauty/)
The question is whether aesthetic value has an objective measure or a subjective measure. Augustine asks whether things are beautiful because they give delight or whether they give delight because they are beautiful. Is beauty is in the eye of the beholder? Both Hume and Kant took the position that there is some objectiveness in the concept, but probably also subjectivity as well. Query whether this is really a question on the same level as truth and existence. Interestingly, while 1700 philosophers responded to this question, only 685 responded to a question on aesthetic experience.

| Objective | Subjective |
|-----------|------------|
| 44%       | 41%        |
N = 1700

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
