---
title: "Never Let a Good Crisis Go To Waste - Part Two"
date: 2020-10-06T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I can't possibly express the anger I feel at the irresponsibility of a tweet saying don't fear COVID. As of Oct 6, there have been 1,038,534 COVID deaths globally and 208,433 COVID deaths in the US. [https://covid19.who.int/](https://covid19.who.int/) There have been 35 million globally confirmed cases, resulting in hundreds of thousands of people who survived, but have respiratory and other problems caused by COVID. There is a reason to fear COVID but as I have expressed before - I can't make people care about other people. If you don't care about giving COVID to others, you are too far gone for me to reach. I'll just consider you to be a dispicable lifeform that can't be trusted and I should never do business with you.

### UK
Today's round of never let a good crisis go to waste starts in the UK with Home Secretary Priti Patel's speech at the Conservative Party Conference to criticise lawyers who defend migrants, linking them directly with traffickers who help asylum-seekers to cross borders.

Patel said: *“No doubt those who are well-rehearsed in how to play and profit from the broken system will lecture us on their grand theories about human rights. Those defending the broken system – the traffickers, the do-gooders, the lefty lawyers, the Labour party – they are defending the indefensible.”*

One of Shakespeare's lines that is continually misrepresented is *"The first thing we do, let's kill all the lawyers"* in Henry VI, Part II, Act IV, Scene II, Line 73. The line was spoken by Dick the Butcher who was trying to help Jack Cade create chaos and become king. The line was intended to show that lawyers and judges would try to keep justice and eliminate the chaos. Therefore, if Jack Cade was going to get and continue in power, they needed to get rid of the rule of law and that would require killing all the lawyers and judges.

Yes, there are unethical lawyers and judges just the same as there are unethical plumbers and politicians, electricians and entrepreneurs, salespeople and sheriffs. Ethical lawyers and judges are necessary for a system to offer justice for all. It is enlightening to see that Patel says that the do-gooders are trying to defend the broken system. The corollary would seem to be that Patel is one of the do-badders, trying to create a new system that only benefits the powerful. In other words, Jack Cade.

So, lets know look at Boris Johnson's comments at the same conferences. Speaking at a Q&A session at Conservative conference on Sunday, he said: "We have a massive opportunity now to use this unquestionable crisis, I mean it's been a huge thing for our country, to build back better. Covid has illuminated all sorts of problems we have in productivity, in getting things done, in the way Government works. Well, we can short-circuit some of these things, we can get them done faster. So let's do everything we can now to maximise the potential of the country. It has been a very very difficult time but come the spring the opportunity will be massive.

Consider yesterday's post [https://www.peterhiltz.com/en/posts/excel/](https://www.peterhiltz.com/en/posts/excel/) and how the Tory government gave a no-tender bid to companies that decided to use Excel instead of a real database. They short cut things and got it wrong.

Boris Johnson added: "On the Left, there are forces saying we must keep furlough going forever, we must keep paying people, the state must get bigger and bigger. And it will be up to us to say it is not for the state to preempt virtually half the wealth and spend it on behalf of people because that is not the government's job."

This sets it out nicely. The government's job is apparently not to spend money on behalf of people.

### US

Fox anchor Sandra Smith asked Trump Campaign Director of Communications Erin Perrine if President Trump will “change his messaging” and stop “downplaying” the coronavirus moving forward now that he has dealt with it personally, Perrine touted the importance of “firsthand experience.”

*“Firsthand experience is always going to change how someone relates to something that’s been happening,” she said. “The president has coronavirus right now. He is battling it head on, as toughly as only President Trump can. And of course that’s going to change the way that he speaks of it because it will be a firsthand experience...He has experience now fighting the coronavirus as an individual,Those first-hand experiences, Joe Biden, he doesn’t have those.”*

So the lesson here is reframe. Ignore the fact that the President's neglect on focusing on the reality of COVID made the problem worse and claim that someone who took proper safeguards should be downgraded because they learned their lessons without having to get their fingers burned.

I want to apply this denigration of learning to blue collar workers. Do you really think that master craftsmen should be treated the same as apprentices? Paid the same and believed the same? All that expertise that you developed on the job is worthless and I know as much as you even though I've never done your job? Is this the kind of respect you want?

Do you care about the troops if you don't wear a mask and the returned veterans have compromised lungs due to burn pit exposure? Or support not funding the VA medical treatment?

To quote Chris Evans "So, word to the wise: Unless you have a private helicopter, unlimited free healthcare, access to experimental drugs, and a team of a dozen doctors focused solely on keeping you alive, you should probably take COVID seriously."

I feel sorry for the secret service agents who are willing to put their lives on the line to protect their President, but are now in danger of their President exposing and killing them.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
