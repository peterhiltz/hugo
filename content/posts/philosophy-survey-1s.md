---
title: "2020 Philosophy Survey Part 1s: Well Being, Wittgenstein and Values in Science"
date: 2022-03-02T09:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1s with the topics being  Well Being, Wittgenstein and Values in Science. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Well-being
See [Well-being](https://plato.stanford.edu/entries/well-being/).
- [Hedonism](https://plato.stanford.edu/entries/well-being/#Hed) says that well-being consists of the greatest balance of pleasure over pain.
- [Desire Theories](https://plato.stanford.edu/entries/well-being/#DesThe) Economists have begun to see people’s well-being as consisting in the satisfaction of preferences or desires, the content of which could be revealed by the choices of their possessors. This made possible the ranking of preferences, the development of ‘utility functions’ for individuals, and methods for assessing the value of preference-satisfaction (using, for example, money as a standard).
- [Objective List](https://plato.stanford.edu/entries/well-being/#ObjLisThe) are usually understood as theories which list items constituting well-being that consist neither merely in pleasurable experience nor in desire-satisfaction. Such items might include, for example, knowledge or friendship. But it is worth remembering, for example, that hedonism might be seen as one kind of ‘list’ theory, and all list theories might then be opposed to desire theories as a whole.
| Hedonism/Experientialism | Desire Satisfaction | Objective List | Combination | Alternative | Agnostic |
|--------------------------|---------------------|----------------|-------------|-------------|----------|
| 13%                      | 19%                 | 53%            | 5%          | 5%          | 9%         |
N = 967

### Wittgenstein
See [Ludwig Wittgenstein](https://plato.stanford.edu/entries/wittgenstein/)
The question was do you prefer early or late Wittgenstein? Early Wittgenstein was about the application of logic to metaphysics. Later Wittgenstein was more revolutionary and focused on critiquing all of traditional philosophy, including his earlier work.
| Early | Late | Combination | Agnostic |
|-------|------|-------------|----------|
| 25%   | 58%  | 9%          | 9%       |
N = 963

### Values in science
The question was is ideal scientific reasoning necessarily sensitive or insensitive to non-epistemic values? (necessarily value-laden, can be either, or necessarily value-free?) Value-free means that social, ethical and political values should have no influence over the reasoning of scientists. Value-laden would mean that social, ethical or political values should have some influence over the reasoning of scientists.

| Value-free | value-laden | Either |
|------------|-------------|--------|
| 18%        | 44%         | 31%    |
N = 961

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
