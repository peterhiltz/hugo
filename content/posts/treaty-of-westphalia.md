---
title: "Happy Anniversary Treaty of Westphalia - You Founding Myth"
date: 2020-10-24T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
The "Treaty of Westphalia" aka "Peace of Westphalia" is the combination of treaties signed in October 1648 in Osnabruck and Munster ending the Thirty Years War which had killed eight million people, including 30% of the population of what is now Germany. To give an idea of how complicated the political situation was, there were 109 different delegations represented. Some [sources](https://www.historytoday.com/archive/months-past/treaty-westphalia) claim the peace conference had thousands of ancillary diplomats and support staff who needed housing and food despite the famine all around them caused by the war. The major players were the Autrian Emperor, the Kings of France, Spain, Sweden and Denmark, numerous German princes and the Dutch who were trying to get independence from Spain.

Not everyone was happy. Pope Innocent condemned it, probably because it allowed rulers to decide to be Protestants. In the papal bull entitled Zelo domus Dei, he declared it “null, void, invalid, iniquitous, unjust, damnable, reprobate, inane, and devoid of meaning for all time”. You could argue that he lost that point in the "Peace of Augsburg" in 1555, but he would apparently disagree.

Of course none of this mattered outside Europe.

What I actually want to talk about in this post is the disagreement on the impact of the treaties.

## Sovereignty View (Simplified)
Many international relations types claim that this was really the beginning of the "international system", saying that the treaties determined that each state, no matter how large or small, has exclusive sovereignty over its territory ("Westphalian Sovereignty"). This idea [gained prominence in the nineteeth century](https://securing-europe.wp.hum.uu.nl/bursting-the-bubbles-on-the-peace-of-westphalia-and-the-happiness-of-unlearning/) and the leading proponent for this view in the twentieth century was Leo Goss (Gross, Leo. “The Peace of Westphalia, 1648 - 1948.” The American Journal of International Law 42, no. 1 (January 1948): 20–41.)

"*Instead of heralding the era of a genuine international community of nations subordinated to the rule of the law of nations, [the Peace of Westphalia] led to the era of absolutist states, jealous of their territorial sovereignty to a point where the idea of an international community became an almost empty phrase and where international law came to depend upon the will of states more concerned with the preservation and expansion of their power than with the establishment of a rule of law.*"

What Gross is claiming here is that pre-Treaty of Westphalia, there was a system (if you can call it that) of overlapping sovereigns between the Pope, the Emperor of the Holy Roman Empire, and multiple other entities. Post-Treaty of Westphalia, the overlaps ceased. Gross, writing and acting as an advisor to the US and the UN at the end of World War II wanted some way to get order under international law.

Henry Kissinger wrote:

"*The Westphalian peace reflected a practical accommodation to reality, not a unique moral insight. It relied on a system of independent states refraining from interference in each other's domestic affairs and checking each other's ambitions through a general equilibrium of power. No single claim to truth or universal rule had prevailed in Europe's contests. Instead, each state was assigned the attribute of sovereign power over its territory. Each would acknowledge the domestic structures and religious vocations of its fellow states and refrain from challenging their existence.*" Kissinger, Henry (2014). World Order.

Some people appear to take the position that 1500 - 1648 Europe was a time of religious wars and post Westphalia we now have the modern era of secular wars. Hooray! Of course, when pressed, they fall back on "it was the trend" and "this is a generalization". Sort of like my "lies to children" theory.

A political scientist [commenting on reddit 6 years ago](https://old.reddit.com/r/AskHistorians/comments/2d29ci/what_were_the_main_causes_of_the_thirty_years_war/) said:

*This overarching concept is commonly divided into three principles.*

1) The territorial principle.

- *States have clear territorial borders*
- *Within these borders, states have the monopoly of the use of force*
- *States have the monopoly over the law within their domain*
- *States have the monopoly over taxation within their domain*

*This followed the proposed ideal of the unity of territory, state, people, nation and religion (all very fuzzy concepts, let's not delve into this. Important is here above all the unity of territory and religion as in the ruler of a state being the one to decide the states religion. This principle, called "cuius regio, eius religio" (who's territory, his religion), was the foundation of the Religious Peace of Augsburg of 1555, that failed to stabilize the Holy Roman Empire before the 30 Year's War and was thus enforced after the "Great Religious War".*)

2) The principle of sovereignty (and this is really interesting for us political scientists)

- *States are the only relevant actors in the international system*
- *States are sovereign and there is no superior instance (not even the Emperor)*
- *States have to and may help themselves towards the outside (with war being a legitimate ultima ratio)*
- *States are sovereign towards the inside (as in they may chose their political system at their free will)*
- *States are not accountable to any outside instance*

3) The principle of legality

- *Sovereign states are equals (a major point in modern theories of "anarchy in the international political system")*
- *Insofar as relations between states being fixed in law and/or treaties, this happens purely on a voluntary basis (no overlord can tell a state to enter into any relationship with another state)*
- *Cooperation between states should always happen out of the own interest of the involved states*

*The general gist of it is: States are sovereign entities. They have certain rights and are autonomous, equal actors in the international political system.It should be noted that this view is getting more and more criticized! Especially historians attack it, as it condenses a fairly long evolution in political systems and relations into a very short timeframe. Nonetheless, these basic principles are commonly called the Westphalian System.*


## "That is a Myth" View (Simplified)
Historians and, more recently [some political scientists](https://securing-europe.wp.hum.uu.nl/bursting-the-bubbles-on-the-peace-of-westphalia-and-the-happiness-of-unlearning/) seem to take the view that the diplomatic process of getting to the treaties was historically important in the development of diplomacy, but point out that the treaties themselves say nothing about sovereignty and should not be considered a defining moment of anything. See e.g. Osiander, Andreas. “Sovereignty, International Relations, and the Westphalian Myth.” International Organization 55, no. 2 (2001): 251–87.

"*Yet the actual treaties do not corroborate any of the claims quoted earlier: the settlement to which they refer is a figment of the imagination.*"

Osiander claims that Westphalia as a typical founding myth offers a clean account of how the "classical" European system was created.

[Sarang Shidore summarized political scientist Hall Gardner's argument as follows](https://www.h-net.org/reviews/showpdf.php?id=53869):

"*the standard interpretation of the Peace of Westphalia, the 1648 treaty that is widely seen to have inaugurated a new era in European and world affairs, by reifying state sovereignty as a global governing principle. Westphalian sovereignty, Gardner argues, is substantially a myth. While Westphalia did put aspects of state sovereignty in place, such as the right of almost three hundred German princes to be free of the control of the Holy Roman Empire, it also limited sovereignty in important ways, for instance, by “denying the doctrine of cuius regio, eius religio (the religion of the prince becomes the religion of the state) ... established by the 1555 Peace of Augsburg” (p. 118). Rather than a strict enshrining of the principle of noninterference, Westphalia legitimized “power sharing and joint sovereignty” by giving the new powers France and Sweden the right to interfere in the affairs of the German Protestant princes (p. 117). Another example of power sharing was the recognition of Switzerland as a confederal state.*"

Some historians see the effort taken to get to the treaty as a way-point towards diplomacy involving numerous individuals working in concert under an organized bureaucracy with an arranged strategy. See, e.g. Riches, Daniel. Protestant Cosmopolitanism and Diplomatic Culture: Brandenburg-Swedish Relations in the Seventeenth Century. Leiden: Brill, 2013. [Garrett Mattingly, Renaissance Diplomacy](https://archive.org/details/RenaissanceDiplomacy).

Even Gross admitted that you can't get to his conclusion just be reading the text of the treaties:

"*In order to find more adequate explanation it would seem appropriate to search not so much in the text of the treaties themselves as in their implications, in the broad conceptions on which they rest, and the developments to which they provided impetus.*"

## Reality
As Machiavelli said in The Prince:

"*But because I want to write what will be useful to anyone who understands, it seems to me better to concentrate on what really happens rather than on theories or speculations. ... I shall set aside fantasies about rulers, then, and consider what happens in fact. (The Prince, Ch. XV)*"

The reality on the ground is that regardless of what people claim is international law, countries are constantly interfering in each others domestic and international affairs while at the same time complaining about others interfering in theirs.

Like so many things, there is seldom a black and white turning point. There are many fits and starts, ebbs and flows, and often things cycling back to what drove people's thinking in an earlier state. What the Treaty of Westphalia might mean may depend on your audience.

## Where are We Going?
Post Cold War, [some political leaders including Tony Blair](https://en.wikipedia.org/wiki/Westphalian_sovereignty) have called for an erosion of absolute state sovereignty, justifying intervention on various grounds such as humanitarianism. Much of the rest of the world view this as another attempt at justifying colonialism by the US and Western Europe. At the end of the day, I view much of international relations as about power, game theory and shifting cultural alliances as about moral principles such as humanitarianism. I hope that I'm wrong.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
