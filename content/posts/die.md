---
title: "The DIE World View"
date: 2025-03-05T10:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Lynn Ungar posted a [really interesting note on substack](https://substack.com/inbox/post/158341579?utm_medium=web) talking about what she refers to as the Dominion, Individualism and Extraction (DIE) worldview.

- "Dominion is belief that a) you are superior and b) that your superiority entitles you to exert control over others who are inferior."

- "Individualism: Not to be confused with individuality, which celebrates the ways that each person is unique, individualism refuses to see the infinite, complex ways that systems are connected. It’s all about me, and I can just pretend that I can act on my own behalf without noticing or caring about others."

- "Extraction: The third pillar of the DIE world view is the notion that the appropriate relation to both people and the non-human world is to get as much from them as possible while giving as little as possible back in return."

I always find Lynn to be insightful and agree with her that this seems to be the worldview of those pushing the buttons controlling the US right now.

[Additional content added March 6,2025]

I saw an article yesterday claiming that the cost savings at the IRS by DOGE's headcount and program cuts would outweighed by the tax revenue foregone. True, but it misses the point of the cuts. The playbook intends to reduce, if not eliminate, regulation (and taxation) of business vis-a-vis consumers and customers. This is all in line with both Lynn's DIE worldview discussion as well as my commentary in the previous post on [Move Fast and Break Things](https://www.peterhiltz.com/en/posts/move-fast-and-break-things/).

By the way, I listened to Trump's speech to Congress last night and wanted to comment about his rationale for the tariffs. He  claimed that other countries have taken advantage of the US for too long. I can't speak to treaties other than tax treaties, but I can say that in the international tax area, the opposite has been the case, at least since the 1930s. I get really annoyed with people playing the victim when they themselves are acting like a middle school bully. They then say "See what you made me do" when the actual victim tries to protect itself. Unfortunately too many people believe the bully.

Quoting now from Derek Lowe re his [post](https://www.science.org/content/blog-post/continuing-crisis-part-vii-overview) yesterday in [https://www.science.org:(https://www.science.org):

> "It may seem surprising that highly educated people who are focused on science and technology would do the things that we’re seeing, but the word “power” is the key. If no one can say what you’re doing is wrong, or harmful, or unnecessary, or too expensive, or too wasteful, or too complex or too vulnerable or too destructive to the environment or anything else. . .well, you can get a lot done very quickly, can’t you? And if you’ve captured all the regulatory agencies and their enforcement powers, and captured the procurement and payment systems along with them, then you can funnel amazing amounts of money to your own businesses while bringing down the big governmental hammer on any would-be competitors. Or on anyone who might cause you any annoyance at all. People are going to have to get on board with your agenda, on your terms, or else. What’s not to like, if you have a titanic ego and loads of inchoate ambition?

> "That’s where Musk and Trump meet, in that last sentence. And that’s why we are seeing every possible part of the US government, domestic policy and foreign alike, being turned into patronage schemes, shakedowns, and protection rackets. It’s a horrifying and disgusting sight, but it all hangs together by these common themes."

> "So forget science, forget knowledge. No, what matters is money and power. Especially the second, because getting power is why you accumulate the money in the first place. Famous quotation time again, this time from O’Brien in Orwell’s “1984”:"

> "We know that no one ever seizes power with the intention of relinquishing it. Power is not a means; it is an end. One does not establish a dictatorship in order to safeguard a revolution; one makes the revolution in order to establish the dictatorship. The object of persecution is persecution. The object of torture is torture. The object of power is power."

> "He also had this to say, after asking Winston Smith how one man exerts power over another:"

> "Obedience is not enough. Unless he is suffering, how can you be sure that he is obeying your will and not his own? Power is in inflicting pain and humiliation."

> "You might be reminded of the various “anti-DEI” orders that have come down. Attacks on transsexuals are followed by attacks on women and on minorities of all kinds, and they won’t stop there. You might also be reminded of what Elon Musk himself said to Joe Rogan just the other day, that "empathy is a fundamental weakness of Western civilization”. If you believe that, then you will be just fine with the world that these people want to make."

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
