---
title: "Case Nightmare Orange"
date: 2020-12-20T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Suppose, in some far off land, there lived a president and his inner circle who had gaslit themselves into believing that an election had been stolen rather than lost. That somehow or other the opposition who couldn't organize themselves out of a paper bag had convinced the deep state and all the judges and republican owned voting machine manufacturers to change just the presidential votes.

Now suppose my post about [Your Brain on Grievances](https://www.peterhiltz.com/en/posts/brain-on-grievances/) has some validity and they decide on one more attempt to retain power when the Congress goes to count the votes. Indications are that the military will refuse to be used to institute martial law, but the Homeland Security is willing as are much of the state and local police forces.

Further suppose the president tweets to his followers to all come to the Capital on the day Congress counts the votes and "it will be wild". This gives a plausible rationale for positioning Homeland Security Officers around the Congress. Then, as almost the entire opposition party is centrally located, Homeland Security arrests just the opposition members of Congress. This leaves the president's party as the sole remaining members of Congress, allowing them to reject the votes, take over control of both houses of Congress and leave the current president in place.

The Congressional police would be overwhelmed and unlikely to put up much opposition. It would be a closer call whether the Secret Service protecting the president elect and vice president elect would fight back. As long as the military stays disengaged, there is no effective opposition. Much of the media would be outraged, but its not like the system has a way to protect itself. The ACLU can file as many lawsuits as it wants and win 100% of its cases, but judges have no way of enforcing judgment against an executive branch that ignores them.

Would there be any members of Congress in the president's party who actually believe in democracy over party control and have spines? Would the president call out the National Guard from state A to impose order on civil disturbances in state B? States' rights seems to only apply if the Federal government is taking a position counter to the beliefs of authoritarians.

Let's see. So far in 2020, opposition leaders have been arrested in Armenia, Belarus, Cambodia, Cameroon, Guinea, Hong Kong, the Ivory Coast, Pakistan, Tajikistan, Tanzania, Togo, Uganda, Zimbabwe. Oh yeah, and Russia decided poison was cheaper. Could it happen in your country?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
