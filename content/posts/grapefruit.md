---
title: "Grapefruit?"
date: 2020-10-07T14:18:19-04:00
categories: ['posts']
tags: ['health']
showToc:  "true"
author: "Peter Hiltz"
---
I like grapefruit. The real kind, not the pink sweet kind. However, I have just read an article [https://www.atlasobscura.com/articles/grapefruit-history-and-drug-interactions](https://www.atlasobscura.com/articles/grapefruit-history-and-drug-interactions) indicating that you need to be careful when drinking it and taking prescription medicine. Drug manufacturers determine dosage based on how much of the drug your body will actually ingest. Grapefruit knocks out several of the enzymes that would otherwise be reducing the drugs to an inactive form. Net result, your body actually absorbs a lot more of the drug than the dosage assumes and you overdose. Here’s a brief and incomplete list of some of the medications that research indicates get screwed up by grapefruit:

-  Benzodiazepines (Xanax, Klonopin, and Valium)
-  Amphetamines (Adderall and Ritalin)
-  Anti-anxiety SSRIs (Zoloft and Paxil)
-  Cholesterol-lowering statins (Lipitor and Crestor)
-  Erectile-dysfunction drugs (Cialis and Viagra)
-  Various over-the-counter meds (Tylenol, Allegra, and Prilosec)
-  And about a hundred others.

So all of those threatening side effects you hear on the US television ads for medicine are more likely to occur if you eat grapefruit or drink grapefruit juice. Who knew? The Canadian equivalent of the FDA requires warning grapefruit warning labels on a lot of drugs. The US, not so much. The warning is buried in the fine print for Lipitor and Xanax, but the US Food and Drug Administration says that there is not enough clinical evidence to **require** Zoloft, Viagra or Adderall to have a grapefruit juice interaction listed on the drug label. As usual, the US health care system leaves something to be desired.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
