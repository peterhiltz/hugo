---
title: "Queen of Method Singing?"
date: 2020-11-15T14:18:19-04:00
categories: ['posts']
tags: ['music']
showToc:  "true"
author: "Peter Hiltz"
---
There is a subtle, and sometimes not so subtle difference between singing for an audience, to an audience, with an audience and at an audience. And there can be good reasons for choosing any of those approaches. I think of the Dixie Chicks doing [Not Ready to Make Nice](https://www.youtube.com/watch?v=UFpaHMqz0nM) as a song that they could sing at an audience although that particular performance had a sympathetic crowd.

I can get irritated watching a singer be overly dramatic because it distracts me from the song itself. I fully agree that can be partly my own fault because I'm too detached emotionally from the rest of the audience. I don't fee the difference between a good singer/performer who is in sync with the audience and is amping up the audience's energy from a bad singer/performer who is just acting like an eight year old preening in front of the attention. Obviously this is outside the context of musical theater or opera where there is a plot and character and...

Then I ran across this [performance](https://www.youtube.com/watch?v=dVvlmpo5g9k&list=WL) of Lara Fabian singing Je Suis Malade. (The song is in French, but the video link has English and French subtitles. The title literally translates to "I am Sick", but in context it means more like heartbroken and being driven mad.) While she can still be a little too dramatic at times for my taste, she gave me a better appreciation for the singer being the character in a first person storytelling song. Her performance during the song has moments of almost just talking to herself, delicate flute like notes in her head voice and full voiced belts both a capella and with the orchestra. Unlike most singers who try to hide when they are taking a breath, she almost weaponizes her breathing and it helps to tell the story of the song. The song ends with her singing a 17 second note that feels like she can hold longer than the musicians. But what really hit me over the head was after the song ended and the audience was on their feet applauding, you can see it took her 25 seconds to come back out of the song. She was the character singing about her heartbreak. I knew her mother was Sicilian and could guess that the dramatics were culturally honest.

So another lesson learned. Get out of my own shoes and try to understand someone else from their position.

For those who don't know Lara Fabian (practically no one knows her in the US), she is a pop music singer/songwriter/producer who is well known in Europe and Canada. Her mother was Sicilian and her father was Belgian and she was enrolled in the Belgian Royal Conservatory of music at age 8. She represented Luxembourg in the 1988 Eurovision contest, eventually coming in fourth (Celine Dion won, representing Switzerland). She sings in seven languages: French, English, Spanish, Italian, Flemish, German and Russian and currently lives in Montreal. Other youtube examples where she is singing in english are [Mademoiselle Hyde](https://www.youtube.com/watch?v=DuSDdjU4S8M) (more musical theater); [Broken Vow](https://www.youtube.com/watch?v=FNVR0yK8Ec8) and [Adagio](https://www.youtube.com/watch?v=jKtNuLG5jAo).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
