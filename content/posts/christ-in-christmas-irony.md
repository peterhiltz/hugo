---
title: "Re: Christ in Christmas Irony"
date: 2024-12-22T06:18:19-04:00
categories: ['posts']
tags: ['religion','living']
showToc:  "true"
author: "Peter Hiltz"
---
Following up on my previous post. Last night I was one of a lineup of musicians playing at a folk music monthly concert. I was third in the lineup and pointed out that it was Winter Solstice Night and that the holiday season stretching from Oct 31 to Jan 15 is full of holidays for many religions. Two groups later, a singer announces that she is going to play a song that she thinks expresses the real meaning for the season. She then launches into a song that has taken all of the music from Lenard Cohen's song "Hallelujah" and replaced the lyrics with the Christian nativity story. 

I'm sure the irony of "the meaning for the season" and a Christian co-opting a Jewish songwriter's song was not lost on the next group of musicians - who were openly Jewish. I'm going to assume the implicit arrogance was unconscious but she may very well have thought my on stage comments were too "woke" and irritated her. 

I did get thanks after the show from a couple of families attending for being inclusive.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.


