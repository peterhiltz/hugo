---
title: Yes, you can celebrate with the competition!
date: 2021-07-31
categories: ['posts']
tags: ['happy']
showToc:  "true"
author: "Peter Hiltz"
---
From the Doctor Who tv episode "The Doctor Falls":

"Winning? Is that what you think it's about? I'm not trying to win. I'm not doing this because I want to beat someone, or because I hate someone, or because, because I want to blame someone. It's not because it's fun and God knows it's not because it's easy. It's not even because it works, because it hardly ever does. I do what I do, because it's right! Because it's decent! And above all, it's kind. It's just that. Just kind."

I think the question "Why can't everyone get along?" doesn't go far enough. You can actually celebrate an achievement by your "competitors". See, for example, this youtube video showing a South African swimmer breaking the world record in the Tokyo Olympics and getting hugged by her US competitors.

[All the competitors celebrating the winner's new world record.](https://www.youtube.com/watch?v=AOXZsBiGqIE&t=178s)
