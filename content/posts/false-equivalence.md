---
title: "False Equivalences"
date: 2021-11-01T14:18:19-04:00
categories: ['posts']
tags: ['politics','rhetoric']
showToc:  "true"
author: "Peter Hiltz"
---
I don't know about you, but I often see exchanges of the following type (replace "murder" with any of a thousand different behaviors which are unkind to other people):

1. Team Humanity member: "Murder is wrong. Gang A and Gang B murder people. Stop it."

2. Gang A member: "That is false equivalence. Gang B murders more people and more enthusiastically than we do."

3. Team Humanity member: "Stop murdering people."

4. Gang A member: "Stop looking at us, see point 2."

5. Gang B member: "Team Humanity agrees that Gang A murders people enthusiatically."

6. Gang B members all together: "Kill Gang A. Kill Gang A."

7. Team Humanity member: "Everyone stop murdering people."

8. Gang B members all together: "Kill Gang A. Kill Gang A."

9. Gang A members: "Now see what you've done, Team Humanity. This is all your fault."

I'm on Team Humanity. I hate you all. Just stop it.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
