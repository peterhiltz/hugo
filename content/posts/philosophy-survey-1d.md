---
title: "2020 Philosophy Survey Part 1d: Mind, Meaning of Life and Knowledge"
date: 2022-02-17T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1d with the topics being Mind, Meaning of Life and Knowledge. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Mind
[Physicalism](https://plato.stanford.edu/entries/physicalism/) takes the position that everything is physical and there is nothing over and above the physical. It is also referred to as materialism.
| Physicalism | Non-physicalism | Agnostic |
|-------------|-----------------|----------|
| 52%         | 32%             | 6%       |
N = 1733

### Meaning of Life
See [Meaning of life](https://plato.stanford.edu/entries/life-meaning/)
This question is interesting because there is a difference between "meaning OF life" and "meaning IN life". The first typically defined as "what is the meaning for existence for humans as a class of beings" and the second typically defined as what is the meaning for existence of an individual being. According to at least one philosopher, James Tartaglia, Professor of Metaphysical Philosophy at Keele University, asking what would confer meaning on a particular life is “essentially practical”. In his view, that is "psychology", not "philosophy". See [Metz's Quest for the Holy Grail](https://philpapers.org/rec/TARMQF). Tartaglia thinks that philosophers should only be looking at whether the human species has meaning, not individual people. Sigh. As much as I like theory, when it comes down to it, I plead guilty to looking for practical solutions to problems, so, Prof. Tartaglia, you do you.

In any event, the allowed answers for the question were "subjective", "objective", "non-existent", "combination of views", an "alternative view", "too unclear to answer", "no fact of the matter" and "agnostic/undecided". Given the limited number of answers allowed, I think that the "objective view" here includes not just naturalistic views, but also God and Soul centered views.

According to the SEP, subjectivism is:

> "According to this version of naturalism, meaning in life varies from person to person, depending on each one’s variable pro-attitudes. Common instances are views that one’s life is more meaningful, the more one gets what one happens to want strongly, achieves one’s highly ranked goals, or does what one believes to be really important (Trisel 2002; Hooker 2008). One influential subjectivist has recently maintained that the relevant mental state is caring or loving, so that life is meaningful just to the extent that one cares about or loves something (Frankfurt 1988, 80–94, 2004). Another recent proposal is that meaningfulness consists of “an active engagement and affirmation that vivifies the person who has freely created or accepted and now promotes and nurtures the projects of her highest concern” (Belliotti 2019, 183)."

According to the SEP, objective naturalists:

> " Objective naturalists believe that meaning in life is constituted at least in part by something physical beyond merely the fact that it is the object of a pro-attitude. Obtaining the object of some emotion, desire, or judgment is not sufficient for meaningfulness, on this view. Instead, there are certain conditions of the material world that could confer meaning on anyone’s life, not merely because they are viewed as meaningful, wanted for their own sake, or believed to be choiceworthy, but instead (at least partially) because they are inherently worthwhile or valuable in themselves."

> Morality (the good), enquiry (the true), and creativity (the beautiful) are widely held instances of activities that confer meaning on life, while trimming toenails and eating snow––along with the counterexamples to subjectivism above––are not. Objectivism is widely thought to be a powerful general explanation of these particular judgments: the former are meaningful not merely because some agent (whether it is an individual, her society, or even God) cares about them or judges them to be worth doing, while the latter simply lack significance and cannot obtain it even if some agent does care about them or judge them to be worth doing. From an objective perspective, it is possible for an individual to care about the wrong thing or to be mistaken that something is worthwhile, and not merely because of something she cares about all the more or judges to be still more choiceworthy. Of course, meta-ethical debates about the existence and nature of value are again relevant to appraising this rationale."

The SEP provides a few different articulations of a [God based objective view](https://plato.stanford.edu/entries/life-meaning/#GodCentView). First, "One’s existence is significant if and only if one fulfills a purpose God has assigned." Only God's purpose could be the source of invariant moral rules or objective values and teh lack of invariant moral rules and objective values would render our lives nonsensical. The second articulation is that for a finite condition to be meaningful, it must derive its meaning from another condition that has meaning. Cue "turtles all the way down" until you get to something so all-encompassing that it cannot and need not go beyond itself for meaning (i.e. God). A third, more moderate take, is that God would greatly enhance meaning in our lives even if we can find meaning without God.

The SEP also provides a few different articulations of a [Soul based objective view](https://plato.stanford.edu/entries/life-meaning/#SoulCentView). All of these require a belief in an afterlife and an eternal soul. First, Tolstoy suggested "for life to be meaningful something must be worth doing, that something is worth doing only if it will make a permanent difference to the world, and that doing so requires being immortal." The second proposal is "that life would be meaningless without a soul is that it is necessary for justice to be done, which, in turn, is necessary for a meaningful life." Third:

>"A third argument for thinking that having a soul is essential for any meaning is that it is required to have the sort of free will without which our lives would be meaningless. Immanuel Kant is known for having maintained that if we were merely physical beings, subjected to the laws of nature like everything else in the material world, then we could not act for moral reasons and hence would be unimportant. More recently, one theologian has eloquently put the point in religious terms: “The moral spirit finds the meaning of life in choice. It finds it in that which proceeds from man and remains with him as his inner essence rather than in the accidents of circumstances turns of external fortune....(W)henever a human being rubs the lamp of his moral conscience, a Spirit does appear. This Spirit is God....It is in the ‘Thou must’ of God and man’s ‘I can’ that the divine image of God in human life is contained” (Swenson 1949/2000, 27–28). Notice that, even if moral norms did not spring from God’s commands, the logic of the argument entails that one’s life could be meaningful, so long as one had the inherent ability to make the morally correct choice in any situation. That, in turn, arguably requires something non-physical about one’s self, so as to be able to overcome whichever physical laws and forces one might confront. The standard objection to this reasoning is to advance a compatibilism about having a determined physical nature and being able to act for moral reasons (e.g., Arpaly 2006; Fischer 2009, 145–77). It is also worth wondering whether, if one had to have a spiritual essence in order to make free choices, it would have to be one that never perished."

A final argument put forth by the SEP for a soul based objective view is similar to the final argument for a God based view - hey, even if you have meaning without a soul directed meaning, wouldn't you have more meaning with a soul directed meaning?

| [Subjective](https://plato.stanford.edu/entries/life-meaning/#Subj) | [Objective](https://plato.stanford.edu/entries/life-meaning/#Obje) | [Non-existent](https://plato.stanford.edu/entries/life-meaning/#Nihi) | Unclear | Agnostic |
|------------|-----------|--------------|-------------|----------|
| 33%        | 32%       | 16%          | 13%         | 4%       |
N = 1725

### Knowledge
See [Knowledge](https://plato.stanford.edu/entries/rationalism-empiricism/)
Rationalism claims that our knowledge of our minds is *primarily* drawn from our awareness of our mental operations. Rationalists also claim that we have knowledge of some truths in a particular subject matter as part of our nature. this is called the "Innate Knowledge Thesis". Just which subject matter is covered by this claim depends on the rationalist. Empiricists reject this claim. Many rationalists also claim the "Innate Concept Thesis" - We have some of the concepts we employ in a particular subject area as part of our rational nature. Many rationalists also claim that reason is superior to experience and that what we know *by intuition* is certain while what we believe (or think we know) by experience is uncertain.

Empiricism claims that our knowledge of external objects is *primarily* drawn from our sense experience which allows us to acquire knowledge of some subject matter. Empiricists tend to think that our knowledge about the external world does not necessarily have a high degree of certainty because we can never be sure that our sensory impressions are not part of a dream. They also tend to be skeptical of the Rationalist claim that intuition can provide certainty.

When arguing with each other, both sides tend to overstate the other's claims. You can be rationalist in one subject area and an empiricist in another area.

| Empiricism | Rationalism | Unclear | Combination | Agnostic |
|------------|-------------|-------------|-------------|----------|
| 44%        | 34%         | 13%         | 8%          | 4%       |
N = 1722

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
