---
title: "Thank You Aaron Van Langevelde"
date: 2020-11-25T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Thank you Aaron Van Langevelde, the Republican member of the Michigan board of state canvassers who said "We must not attempt to exercise power we simply don’t have. As John Adams once said, 'We are a government of laws, not men.' This board needs to adhere to that principle here today. This board must do its part to uphold the rule of law and comply with our legal duty to certify this election." As a result of his vote to certify the Michigan vote, transition has finally started towards the next administration, but he and his family have received death threats and now have police protection. Those of us who believe in integrity instead of winning at all costs can only hope that we would stand up to the test of courage as he did.

An article on the behind the scenes action can be found [here](https://www.politico.com/news/magazine/2020/11/24/michigan-election-trump-voter-fraud-democracy-440475).

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
