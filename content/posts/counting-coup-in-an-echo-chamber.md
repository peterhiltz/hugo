---
title: "Counting Coup in an Echo Chamber and Other Wastes of Time"
date: 2020-09-15T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
I watched Ed Miliband's speech in the UK Parliament and then watched all the social media posts from the "Remainders" congratulating each other. A typical title started "Boris Johnson's humiliation by Ed Miliband...". The same thing happens on the other side. It doesn't matter whether it is in the UK, the US, India or elsewhere and it doesn't matter what the subject is so long as there are at least two extremes on the position. Both sides talk only to themselves and while they are celebrating "counting coup", the other side is not listening and so nothing is accomplished.

The whole idea of "counting coup" among the Native American Indians on the great plains was a demonstration of courage engaging with the enemy **and** the enemy acknowledged the touch. In today's world, there is no demonstration of courage because there is no attempt at actual engagement by either side. "I really told those Libtards [or Karens or Snowflakes or SJWs or Rednecks or ...]". No, you didn't. You postured, pretending to be a big bad gorilla and then scuttled back to your safe place and the other side didn't acknowledge that you had scored, so you didn't accomplish anything.

Now, if you walk up to someone wearing a Covid mask and rip it off their face, calling them sheep, you have counted coup. You have also committed civil and criminal assault and battery and should be arrested and thrown in jail.

What I want to see from both sides is actual plans and strategies that maximize the benefits for everyone. So far the strategies I've seen have only been for executing disaster capitalism, which is a proactive form of socializing capital risk and privatizing capital profit. It worked well for Russian plutocrats coming out of the cold war but didn't seem to be particularly beneficial for the person in the street. The reason it gets support from a substantial percentage of the people on the street is that for 30% of the population, having a strong leader is better from a psychological standpoint than having a better living standard.

And no, "Gotcha" announcements are not a plan or strategy.

Of course, the problem with my desire is that neither side actually wants to maximize the benefits for everyone. Only the people in the middle want that. The extremes seem to have an goal that the other extreme loses more than they do and letting the world burn is apparently acceptable.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
