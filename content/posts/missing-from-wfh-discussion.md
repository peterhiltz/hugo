---
title: "Missing from the working from home debate"
date: 2020-09-29T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
There have been lots of discussions about whether employees are more or less efficient working from home. I certainly see lots of comments from people who are happier left alone to work from home. I also see occasional comments from parents trying to juggle working from home, helping their kids learning from home and how all the distractions at home get in the way of being efficient. What I have not seen is the impact on new young workers of not having informal face time training.

It might seem strange that I, as an introvert, bring this up. As a young lawyer, it was scary for my boss to suddenly show up in my office, drop something on my desk and say that we need to talk about X. At the same time, it was a learning and developmental experience - learning not just how to write the documents or do the research, but also learning how to deal with people, up and down the hierarchy ladder, right now, in your face. I wonder whether those people skill development moments won't be the same when the only communication is email and texting.

I am certainly not saying that all those "teaching moments" were valuable, regardless of whether they were "fun". I remember one senior partner telling me that the first 10 years you learn what the law says, the second 10 years learning what it actually means and the rest of your career learning to deal with people.

E-distancing may seem easier than a long commute and dealing with people real time, but I think it will be necessary for both younger workers and their managers to figure out how to do development training, particularly on the people skill side. And if your manager doesn't bring it up, you need to bring it up proactively for the sake of your own career and emotional sanity.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
