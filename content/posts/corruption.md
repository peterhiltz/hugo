---
title: "Its not Capitalism v. Communism, Its Corruption v. Everyone"
date: 2020-09-20T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
It is painful to watch the echo chambers missing what is happening right in front of their faces because they are so fixated on the enemy they have been brought up to hate. Is it that corruption is okay if its the guys behind our cheerleaders engaged in the corruption?

I see people point to the problems in Venezuela as demonstrating the evils of socialism. No, the problems in Venezuela demonstrate the evils of corruption. and corruption happens in business, large and small, it happens in democratic governments and autocratic governments, it happens in churches and charitable organizations. Day by day, real corruption in power grows while the populace fight themselves.

The class warfare certainly doesn't help. Look at Brexit in the UK. Generally speaking the well off voted for Remain and the poor voted for Leave. We can argue about the super-rich voting for (or paying for propaganda for) Leave, but that's why I used the phrase "well off".

I don't have a solution. The second amendment types talk about needing guns to protect themselves against the government are living in a fantasy world of action movies where the good guy always wins. Face it, you are outgunned by an undertrained police force with no trigger discipline. Protesting by taking a knee is labeled disrespectful and accomplishes nothing, but riots play into the hands of the authoritarians who use it as an excuse to crack down ever harder (you should have protested peacefully - except of course that didn't work either). Whistleblowers or other people within either business or government get punished or sidelined or ejected, eliminating their ability to try to limit the corruption from within.

I saw one comment on social media that people should "make the government explain what they said publicly and why it is so different now". Make? How? Look at the protests in Hong Kong - months of protests did nothing in the face of a government that wasn't going to blink. Very few governments feel shame anymore, so peaceful protests don't work and violent protests cause the loss of public support. Some governments don't even try to disguise the fact that they are trying to rig elections by limiting polling places, controlling the post office so as to preclude either ballots getting to voters or mail-in votes getting delivered as well as controlling the software that counts the votes and preventing auditing of the software.

Then you see comments like the following from charlote1 on Reddit in the context of Brexit: "The remainers apathy is grating. There is a “hope for the best” attitude which simply allows for a lack of personal engagement. And there is this pervasive effort “not to rock the boat” in fear of the racist neighbours next door or the brexiteer grand parents whose inheritance might save the day. But more scary even is the noticeable fear of a government which has turned fascist on a dime and showing blatant disregard for the rule of law. I believe that the successive Tory governments have finally turned the British into a deeply divided, compliant and politically disengaged workforce, malleable to the point where it could be motivated to vote for its own self imposed debasement."

Ok Charlote1, what is your solution besides complaining that the populace have become sheep? I agree that appeasement doesn't work. The problem is finding something that does.

By the way, most dictators/regimes take power “legally” within the system they replace.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
