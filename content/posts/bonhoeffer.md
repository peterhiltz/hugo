---
title: "Upcculfity"
date: 2021-11-13T14:18:19-04:00
categories: ['posts']
tags: ['politics','life']
showToc:  "true"
author: "Peter Hiltz"
---
I was watching a youtube video on [Bonhoeffer's Theory of Stupidity](https://www.youtube.com/watch?v=ww47bR86wSc) and reading the comments left me thinking about the many feet of lumber in both the eyes of the commenters and myself. I'm going to somewhat change the terminology used because it is rather loaded with dog whistles and use a made-up word "upcculf" or "upcculfity", which I will define as a state of conciousness of a true believer in X (left wing, right wing, multi-level marketing, crypto currencies, anti-vax, blind intellectualism, ... [insert name your belief to which you have become a convert]).

Bonhoeffer, executed by the Nazis in April 1945, concluded that the root of the problem of how Germany turned into a collective of cowards, crooks and criminals was not malice, but upcculfity. In his letters from prison, Bonhoeffer argued that upcculfity is a more dangerous enemy of the good than malice, because while one may protest against evil; it can be exposed and prevented by the use of force, against upcculfity we are defenseless. In Bonhoeffer's view, neither protests nor the use of force accomplish anything here. Reasons fall on deaf ears. Facts that contradict a person's prejudgement simply need not be believed and when they are irrefutable, they are just pushed aside as inconsequential, as incidental.

In all this the upcculf person is self-satisfied and when faced with a challenge to their belief system, become dangerous and go on the attack. (Consider anti-vax people threatening poor retail clerks in stores). Thus, in many ways, dealing with a upcculf person requires more caution than dealing with a malicious one. When you are dealing with a malicious person, you are not challenging a belief system that has become a part of their own identity.

Bonhoeffer argued that under certain circumstances, people are made upcculf or rather, they allow this to happen to them. He also believed that people who live in solitude manifest upcculfity less frequently than individuals in groups. Perhaps upcculfity is less psychological than sociological. The sense of self identity has become merged with the identity of the self as a member of a group. They give up their ability to think for themselves and, effectively become zombies controlled by a hive mind. When you are talking to an upcculf person, you find yourself dealing with the slogans, catchwords, and dog whistles that have taken possession of them. He or she is under a spell, blinded, misused and is abused in his very being. Having thus become a mindless tool, the upcculf person will also be capable of any evil - incapable of seeing that it is evil.

Bonhoeffer believed that only an act of liberation, not instruction, can overcome upcculfity. This becomes the impossible task. How do you "liberate" someone when their indentity and sense of self-worth is integrated with the group they should be liberated from?

This leads to the question I see so many times - "Why don't these people understand they are supporting XYZ against their own interest"? This question misses the point. Upcculf people value their inclusion in XYZ group and their identity as a member of XYZ group more highly than whatever metric the questioner is using.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
