---
title: "What Constitutes 'Research on Humans'?"
date: 2021-04-28T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Suppose you are an ethical researcher so you needed informed consent before experimenting on people. Now suppose a group of people writing really complicated software read email suggestions for patches on the software and any emailed patch will go through several of review before it is accepted. Finally, suppose you decide to "research" whether you can get patches accepted that look like they are positive on their face, but in reality create security holes in the software. Are you conducting experimenting on the review process or are you experimenting on people because people are the review process?

An Assistant Professor and a grad student at the University of Minnesota decided to conduct research on how to get bugs accepted by software developers. They decided to submit a lot of patches to the Linux kernel group, some useful, some that do nothing and some that would create potential vulnerabilities. Their research paper is actually titled "Stealthily Introducing Vulnerabilities in Open Source Software via Hypocrite Commits". (Hypocrite commits are seemingly beneficial software patches that in fact introduce other critical issues.)

For those who don't know, the Linux kernel group are the software developers, some paid, some volunteers, who work on the Linux operating system which runs most of the computers which actually run the internet. Reviewing email submissions takes time and they go through multiple levels of review. As a side note, the developers are known for not suffering fools gladly.

The researchers got caught doing this in November 2020 and at that time the Linux kernel group (and other researchers) complained to the university, saying that they did not appreciate being experimented on. One group of researchers expressed ethics concerns: "*They lied to people in order to assess their response, with no system in place for prior informed consent or debriefing.*" At that time there was some "interesting" CYA activity by the professor, claiming that they had sign-off by the ethics review board (apparently they checked with one member of the board after the complaints had been received).

The professor and his grad students produced a [paper](https://www-users.cs.umn.edu/~kjlu/papers/clarifications-hc.pdf) defending their behavior claimed: "*We send the emails to the Linux community and seek their feedback. The experiment is not to blame any maintainers but to reveal issues in the process. The IRB of University of Minnesota reviewed the procedures of the experiment and determined that this is not human research. We obtained a formal IRB-exempt letter. The experiment will not collect any personal data, individual behaviors, or personal opinions. It is limited to studying the patching process OSS communities follow, instead of individuals.*"

Consider the following excerpt from the paper "Does this project waste certain efforts of maintainers?

 "*Unfortunately, yes. We would like to sincerely apologize to the maintainers involved in the corresponding patch review process; this work indeed wasted their precious time.  We had carefully considered this issue, but could not figure out a better solution in this study.*"

Interestingly the final research paper claims they did not introduce bugs only "*safely demonstrate it is pracical*", but the draft paper said "*successfully introduced multiple exploitable vulnerabilities.*" It suggests such stupid recommendations as "make contributors agree not to introduce bugs" and "verify everyone's identity which is definitely an effective mitigation against malicious behaviour."

It looks like they were doing similar things in April 2021 and again got caught. This time the Linux kernel group leadership blew up and publicly banned all submissions from anyone at U. of Minn and any historical patches would have to be pulled out and re-reviewed (a huge amount of time required). That seems to have finally gotten serious attention at U. Minn.

From the Twitter feed of the Associate Department Head of the Computer Science Department leadership at U. of Minn. on the subject: https://twitter.com/lorenterveen/status/1384954220705722369

> I do work in Social Computing, and this situation is directly analogous to a number of incidents on Wikipedia quite awhile ago that led to that community and researchers reaching an understanding on research methods that are and are not acceptable.

and https://twitter.com/lorenterveen/status/1384966202301337603

> Yes! This was an IRB 'failure'. Again, in my area of Social Computing, it's been clear for awhile that IRBs often do not understand the issues and the potential risks/harms of doing research in online communities / social media.

One commenter suggested "Beyond any other damage they did, they are wasting the life time and energy of the kernel developers currently sorting out this mess. To me, that is on the same level as locking them up in a room against their will. This could be considered a felony. In any case, they should make up for the economical part of the damage they caused, that is be billed for the hours the kernel developers spend in resolving the matter at hand at a high market rate (and probably above for punitive damages)." Another commenter characterized it as a DDOS attack (attacking someone by using up all their resources). https://news.ycombinator.com/item?id=26895510

Per [Sarah Jamie Lewis](https://twitter.com/SarahJamieLewis/status/1384871385537908736) from openprivacy.ca "Anyway, this is the latest in a long line of computer science researchers stumbing into human subject research, desregarding any and all ethical considerations, getting a paper published, and leaving to find a new community fuck around in."

As one commenter said in this [thread](https://news.ycombinator.com/item?id=26887670) "The problem here is really that they’re wasting time of the maintainers without their approval. Any ethics board would require prior consent to this. Not only that, but they are also doing experiments on a community of people which is against their interest and also could be harmful by creating mistrust. Trust is a big issue, without it it is almost impossible for people to work meaningfully together. this actually seems more like sociological research except since it’s in the comp sci department the investigators don’t seem to be trained in acceptable (and legal) standards of conducting such research on human subjects. You definitely need prior consent when doing this sort of thing. Ideally this would be escalated to a research ethics committee at UMN because these researchers need to be trained in acceptable practices when dealing with human subjects. So to me it makes sense the subjects “opted out” and escalated to the university.

There are, of course, people defending the researchers, e.g. [here](https://twitter.com/KentonVarda/status/1385074808015126532). There seem to be a few different threads of thought in the defenders minds.

- Only university research is "governed" if you can call it that, by ethics boards. Commercial research has no such limitations. Why should university research be so limited?
- Computer security research is important and we need to know whether bad people or governments can get bugs through the review process, so this kind of research should not have its hands tied.
- The complete ban of U. of Minn. submissions was an over-reaction (PFH comment - the complaints in November didn't seem to work)
- If the bugs got caught, no harm, no foul so who cares.
- They were investigating the process, not physically experimenting on people, so the ethical concerns should not apply

My thoughts? As someone who actually maintains an open source software project (surprise to all of you who thought I was just a tax lawyer), dealing with the occasional software submission takes a lot of time. You need to verify that it works, write tests to make sure it doesn't create collateral damage and doesn't adversely affect anyone else. If anyone is submitting patches in bad faith, yeah, I'm going to be pretty upset with them wasting my time.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
