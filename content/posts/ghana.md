---
title: "My Sympathies for Ghana"
date: 2021-01-23T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
As far as I can tell, Ghana is getting hammered by Brexit.

Huh? What? Yes, 40% of Ghana's banana crop is sold to the UK. Now that the UK is no longer part of the EU and therefore the trade agreement which would have applied no longer exists, Ghana's bananas are subject to tariffs on import. One UK importer indicated it was 20k pounds a week in increased tariffs, not counting the additional red tape. The UK Department for International Trade blames Ghana for "failing to engage with us fully until it was too late". Coming from the UK government which delayed dealing with the consequences of Brexit in practically every area until it was too late, I might tend to side with Ghana. Apparently the UK negotiating position was an offer that would have "guaranteed their continued and lasting access to the UK market" in exchange for agreeing to a different tariff regime for UK goods than its neighbors, undermining the African customs union and badly damaging Ghana's relationships with its neighboring countries. Does anyone have a list of countries that doesn't act like a total jerk in trade deals?

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
