---
title: "What Can I Say?"
date: 2021-01-06T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
Ok. My December 20 post [Case Nightmare Orange](https://www.peterhiltz.com/en/posts/case-nightmare-orange/) was wrong. It considered whether President Trump might try to use Homeland Security forces to disrupt the Congressional certification of the Electoral College results. There was, however, a distinct lack of police manpower to deal with protests that everyone saw coming. Looking at this [video](https://twitter.com/elijahschaffer/status/1346966514990149639?s=21), there was no way those four police officers could deal with that mob. It didn't look like much more manpower than normal around the Capitol building.

Like so much else, everyone can claim that they were not "actively" involved in the attempted coup. Lots of people have pointed out the vast difference between the Metropolitan Police's treatment of BLM protesters and their treatment of the mob that stormed Capital Hill. There are videos circulating of Capitol police moving barriers aside and taking selfies with protesters. One young woman has died. Losing sucks. But now we know which politicians (and will soon know which police officers) put their allegiance to Trump ahead of their allegiance to the country and their oaths of office.

Some interesting comments on libertarian social media overnight:

"*My libertarian friends were calling Trump a fascist with brain damage from day one while my religious friends were calling him a moral Christian. That’s what I’ll be remembering for the next 20 years.*"

"*A lot of the problem is that a huge portion of the people who self-identify as Libertarians are just ultra-conservative Republicans who think it sounds cooler to say they're a Libertarian.*"

"*People asking for police reform? Shoot 'em with rubber bullets. People literally storming the capital building? Use the kids gloves. Fucking right wingers have gone full-blown authoritarianism and aren't even hiding it. Can't wait for the "there are no left libertarians, only right libertarians exist" crowd to come and explain why this is a totally appropriate response and that protesters earlier in the summer should be hanged.*"

"*What gets me is the people with the Gadsden flags. You’re waving a flag that stands for liberty and states’ rights while you demand that the federal government overrule the will of the people and the states. Not the brightest bunch out there.*"

"*sucks that most people's only experience with libertarians is loony far right wingers that simultaneously wave the blue line flag and gadsen flag at the same time lmao*"

UPDATE: [https://twitter.com/NatashaBertrand/status/1347214268740096002/photo/1](https://twitter.com/NatashaBertrand/status/1347214268740096002/photo/1) Steven Sund, Chief of Police for the US Capitol Police made a public statement that "*The USCP had a robust plan established to address anticipated First Amendment activities. But make no mistake - these mass riots were not First Amendment activities, they were criminal riotous behavior*". Oh. Is that supposed to make me feel better? You have a robust plan if everyone behaves?

French police officials [laid out multiple lapses they believe were systematic](https://www.businessinsider.com/trump-attempted-coup-federal-law-enforcement-capitol-police-2021-1?fbclid=IwAR0j-6x--5UHsY93_HkOjbQMVCmLg_q1_k4L1kIcyc_nZqyyQ3niOKCObuI):

- "*Large crowds of protesters needed to be managed far earlier by the police, who instead controlled a scene at the first demonstration Trump addressed, then ignored the crowd as it streamed toward the Capitol.*"

- "*It should have been surrounded, managed, and directed immediately, and that pressure never released.*"

- "*Because the crowd was not managed and directed, the official said, the protesters were able to congregate unimpeded around the Capitol, where the next major failure took place.*"

- "*It is unthinkable there was not a strong police cordon on the outskirts of the complex. Fences and barricades are useless without strong police enforcement. This is when you start making arrests, targeting key people that appear violent, anyone who attacks an officer, anyone who breaches the barricade. You have to show that crossing the line will fail and end in arrest.*"

- "*I cannot believe the failure to establish a proper cordon was a mistake. These are very skilled police officials, but they are federal, and that means they ultimately report to the president. This needs to be investigated.*"

- "*When the crowd reached the steps of the building, the situation was over. The police are there to protect the building from terrorist attacks and crime, not a battalion of infantry. That had to be managed from hundreds of meters away unless the police were willing to completely open fire, and I can respect why they were not.*"

One NATO official stated "*Today I am briefing my government that we believe with a reasonable level of certainty that Donald Trump attempted a coup that failed when the system did not buckle. I can't believe this happened.*"

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
