---
title: "2020 Philosophy Survey Part 1o: Chinese Room, Possible Worlds, Human Genetic Engineering"
date: 2022-02-26T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1o with the topics being "Chinese Room", "Possible Worlds" and "Human Genetic Engineering". I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Chinese Room
See [Chinese room](https://plato.stanford.edu/entries/chinese-room/). This is a thought experiment proposed by American philosopher John Searle. He imagines himself alone in a room following a computer program for responding to Chinese characters slipped under the door. Searle understands nothing of Chinese, and yet, by following the program for manipulating symbols and numerals just as a computer does, he sends appropriate strings of Chinese characters back out under the door, and this leads those outside to mistakenly suppose there is a Chinese speaker in the room. Does he, or the computer program, understand Chinese?

| Understands | Doesn't Understand | Unclear | Agnostic |
|-------------|--------------------|---------|----------|
| 18%         | 67%                | 6%      | 6%       |
N = 1031

### Possible Worlds
See [Possible worlds](https://plato.stanford.edu/entries/possible-worlds/)
I have to admit I read the SEP explanation twice and still didn't understand it. The following is an explanation I got from [stackexchange](https://philosophy.stackexchange.com/questions/16066/abstractionism-about-possible-worlds-and-the-contingently-non-concrete):

> "For the abstractionist, possible worlds don't (necessarily) have the properties they represent certain possibilities as having--- they merely represent.

> If you're a concretist, like David Lewis, you can claim that a possible world represents a certain possibility by simply being that possibility. So, to represent itself as being concrete the world just needs to be concrete. Similarly, if a world is to represent the possibility of a talking donkey, it will do so by containing a talking donkey as a part.

> For the abstractionist all possible worlds--- even the actual world ---are abstract. They represent possibilities in different ways. David Lewis lays out three different types of abstractionism (he terms the view "ersatzism" and the worlds "ersatz worlds"; that terminology has stuck in the literature, imo) based on how the worlds represent possibilities (See On The Plurality of Worlds, ch. 3).

> - Linguistic Ersatzism: This is the version defended by Robert M. Adams (will have to dig for a citation later) and holds that possible worlds represent by saying a possibility is a certain way. One version of this view holds that possible worlds are maximal consistent sets of propositions, completely specifying some way the world might be. The actual world is represented by a maximal consistent set of propositions describing the way the world actually is.
> - Pictorial Ersatzism: Possible worlds represent by isomorphism, like a picture or map. A map of the USA represents the USA in virtue of depicting 50 states in the right spatial arrangement, etc. This is how possible worlds represent, they're like pictures.
> - "Magical" Ersatzism: Possible worlds represent by magic. This is what Lewis calls views, like the property view of Robert Stalnaker, that don't explain--- and, Lewis claims, leave mysterious ---how worlds represent.

> The point here is that whereas a concretist can identify the actual world with this concrete thing we inhabit, an abstractionist will generally (always?) have an abstract surrogate for even the actual world. None of the abstractionist's worlds are (or could be) concrete, though many, if not all, represent possibilities involving concrete objects. Remember that not even "the actual world" is concrete for an abstractionist--- though it certainly represents a concrete possibility (ours!)."

| [Abstract](https://plato.stanford.edu/entries/possible-worlds/#AbstractPWs) | [Concrete](https://plato.stanford.edu/entries/possible-worlds/#Concretism) | Nonexistent | Agnostic |
|----------|----------|-------------|----------|
| 55%      | 5%       | 30%         | 5%         |
N = 1063

### Human genetic engineering
I think this one is obvious, so no additional description provided. I think that respondents might have responded differently depending on the fact pattern provided.
| Permissable | Impermissable | Agnostic |
|-------------|---------------|----------|
| 64%         | 19%           | 8%       |
N = 1059


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
