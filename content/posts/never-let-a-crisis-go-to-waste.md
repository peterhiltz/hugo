---
title: "Never Let a Good Crisis Go To Waste - Part One"
date: 2020-10-02T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
This post will likely get deleted as things progress, but my innate cynicism is too strong to keep quiet right now.

So, I just saw the news that President Trump tweeted that he had tested positive for COVID-19.  It is interesting that it happened the same day that a [Cornell study](https://www.google.com/amp/s/www.nytimes.com/2020/09/30/us/politics/trump-coronavirus-misinformation.amp.html) claimed the President was the single greatest source of covid disinformation. This also has national security implications. See a [twitter thread from Sam Vinograd, a national security expert, written before the news but after a White House Staffer tested positive](https://twitter.com/sam_vinograd/status/1311841999780286464). It is also interesting that two somewhat similar heads of states have tested positive and recovered (Boris Johnson in the UK and Jair Bolsanoro in Brazil).

Having spent my entire professional career as someone paid to be paranoid and to create contingency plans, my mind immediately wondered whether this was true or whether it was a contingency plan put into effect.

Possible War Game Scenarios
1. The media reaction to the first debate was bad. Having the President and his staff immediately go into quarantine for safety probably kills the prospect of any more debates for the next two weeks. This can be true regardless of whether the positive test for COVID-19 is true.

2. Testing positive for COVID-19 might gain some sympathy from voters on the fence or cause opposition voters to not bother going through the process to get ballots and vote. This can be true regardless of whether the positive test for COVID-19 is true.

3. Testing positive for COVID-19 and then recovering will show that either it isn't that bad providing a basis for demanding a re-opening of the economy. This can be true regardless of whether the positive test for COVID-19 is true.

4. Crazier conspiracy theory - If the President truly thought he was going to lose the election and was concerned about future prosecution, testing positive for COVID-19, leading to resignation during his term and then getting pardoned by Pence would mitigate but not eliminate that risk. I don't see the President being that pessimistic. This can be true regardless of whether the positive test for COVID-19 is true.

5. The President testing positive for COVID-19 could create the basis for a claim for a state of emergency to justify delaying the election. This can be true regardless of whether the positive test for COVID-19 is true.

6. The President testing positive for COVID-19, then recovering after taking a drug from a company in which he or his family have invested, would certainly help him recover some of the losses that his resort businesses have suffered from the COVID crisis. This can be true regardless of whether the positive test for COVID-19 is true.

7. Testing positive for COVID-19 and then recovering will show that the President is anointed by God to be President. This can be true regardless of whether the positive test for COVID-19 is true.

8. Insert your favorite conspiracy theory or contingency plan here.

Update: [@carolecadwalla on Twitter:](https://twitter.com/carolecadwalla/status/1311920391842078725)

*A reminder to all Americans that the net effect of our prime minister catching COVID-19 was that it prompted a surge of patriotic support. From which he emerged with renewed popularity. Which enabled him to tear up key functions of the state*
…
*It also silenced press & parliament at key moment in crisis from asking critical questions. And when it was announced he was moving to intensive care was moment of national shock. Everything about this is rocket fuel for ‘patriotism’*

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
