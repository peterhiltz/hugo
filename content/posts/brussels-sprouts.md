---
title: "Brussels Sprouts"
date: 2020-11-26T14:18:19-04:00
categories: ['posts']
tags: ['food']
showToc:  "true"
author: "Peter Hiltz"
---
Many of us grew up with a dislike for Brussels sprouts. One reason we knew - our parents, as taught by their parents, as taught by their parents .... back to some English ancestors boiled everything to death. Maybe it was for sanitary reasons, but as an old Asterix and Obelix cartoon claimed, the British boil everything so that it has that lovely "same" flavour. (Some people claim that Britain became a global empire just looking for good food. But that is another story.) I understand Dutch home cooking was much the same until the early 2000s. The overcooked reason changed as people discovered the joys of different ways of preparing them: roasted, seared, steaming the Brussels sprouts for only 2 minutes, not 10 or frying them.

The second reason was that they were bitter (the Brussels sprouts, not necessarily our parents). That started to change in the 1990s thanks to Dutch scientist Hans van Doorn who discovered what chemical compounds made them bitter. Seed companies then started looking through their archives for old varieties that had lower levels of those compounds and started cross breeding those with high yielding varieties. Interestingly enough, while most the this work was happening in The Netherlands, the news spread in the professional culinary scene in the US well before Europe.

Reference source: [NPR](https://www.npr.org/sections/thesalt/2019/10/30/773457637/from-culinary-dud-to-stud-how-dutch-plant-breeders-built-our-brussels-sprouts-bo). Also, the [relevant xkcd](https://xkcd.com/2241/).

Whatever you do, please don't drown them in melted cheese. If you drown them in that chemically created cheese "based" goo, please let me know and I'll take you out of my contacts list.


My sister Erin's recipe:

Cut the Brussels sprouts in half. Keep the leaves that fall off, they will turn into delicious chips.

In a bowl, mix a glug of olive oil, dash of cayenne pepper and salt, a forkful of chopped garlic, and a few ratchets of ground pepper. (Erin, I think a "glug" of olive oil depends on the size of the opening in the bottle.) Make sure you mix completely and rotate the bowl to evenly distribute. (Hint to Peter: Brownian motion is not sufficient to "mix completely". That works in your coffee, not here.) Throw in the prepared Brussels sprouts. Toss until every surface is lightly coated, but none is greasy. (If it is too greasy, revise your estimate of "glug".)

Spread onto sheet pan large enough to ensure a single layer with sprouts are only occasionally touching tangentially with the cut side down.

Put in preheated oven of 375-435F (depends on what else is cooking - there is a trade-off here. The hotter the oven and the shorter the time period in the oven, the less mushy they will be) and have patience (probably 30-50 minutes). Shake the pan every 15 minutes and check and remove stray individual leaves that are getting toasty and put them in a bowl as chips. Don’t take out until you can easily skewer the stalk with a fork.

If you have hot spots in your oven, consider rotating the pan to achieve even cooking.

For a slightly different finish, add either a spoon full of brown mustard or a few generous splashes of balsamic vinegar to the oil mixture prior to cooking or reduce balsamic vinegar in a pan and add it at the end.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
