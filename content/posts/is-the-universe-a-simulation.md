---
title: "Is the Universe a Simulation?"
date: 2020-10-15T14:18:19-04:00
categories: ['posts']
tags: ['life','religion','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
Warning: Very Long Post and Very Deep Rabbit Hole about something that doesn't really matter.

Elon Musk has publicly said that [we're most likely in a simulation](https://www.youtube.com/watch?v=VeIasZ6WbxA). Before we get into anything, please note that this is a hypothesis, not a theory.

## What is the Simulation Hypothesis?
The super simple description of the idea that the universe could be a simulation is you assume unlimited computing power and God/aliens create a sufficiently complex version of the Matrix, Inception, Minecraft or the Sims and we are just the characters running around inside a computer program. Does that idea say anything about creation, free will, philosophy, physics, religion or anything else? Is this a different way of thinking about my earlier [post](https://www.peterhiltz.com/en/posts/bit-players/) about we are the central characters in our own plays, but bit players in other people's plays?

There are actually two versions of the simulation hypothesis. The NPC version is generally credited to Philospher Nick Bostrom from Oxford University in his 2003 paper [Are We Living in a Computer Simulation?](https://academic.oup.com/pq/article-abstract/53/211/243/1610975) also found [at](https://www.simulation-argument.com/simulation.html). In that hypothesis, we don't exist in reality, we are just characters created by the simulation. It also means that the creators of our simulation could also create millions of other simulations, different or similar. This can lead to variations of the [many worlds interpretation of quantum mechanics](https://plato.stanford.edu/entries/qm-manyworlds/). See also [https://en.wikipedia.org/wiki/Many-worlds_interpretation](https://en.wikipedia.org/wiki/Many-worlds_interpretation). The second version, sometimes called the RPC version, is that our consciousnesses exist outside the simulation, but we only perceive what we see in the simulation (the version in the movie The Matrix). In the NPC version, consciousness is an emergent property of matter. In the RPC version consciousness is separate and distinct from matter.

Is this really any different from the creation story from any religion? If there is an afterlife, would that be a sequel to the simulation? More importantly, would it even matter and should we even care?

## Back to Elon Musk

Musk is a really smart guy, but lets look at how he came to his conclusion.

1. First he assumes that some civilization (call them alien, call it God, doesn't really matter) has the technology to simulate the universe in their universe's version of computers.

2. Second, he assumes that if they have the technology, they will use it a lot (lets say they will create 1 million simulations).

3. Third he assumes that each simulation will have the technology to create their own computer simulations of the universe. Then reapplying the second assumption to this level, each of those 1 million simulations create their own 1 million simulations. Add up (1M x 1M) + 1 gives you 1 trillion simulations + 1 "real" universe. Therefore the chances that we are not in a simulation is 1/(1 trillion + 1).

Seriously? Was this a joke? Lies, damned lies and probabilities predetermined by the assumptions you use? This looks like it is pretending to be [Bayesian probability](https://en.wikipedia.org/wiki/Bayesian_probability) but the unhinged misuse of the assumptions invalidates it. Bayesian probability is an approach to probability interpreted as quantification of a personal belief, trying to enable reasoning with hypotheses where the truth and falsehood is unknown. Under this approach you calculate prior probability, then update that to one or more posterior probabilities as you get new data. But you need to have some data to base the prior probabilities on. Musk just assumes all the required postulates.

Lets look back at Bostrom's original idea. Bostrom argues that at least one of the following three propositions is true:

1. Any technological species goes extinct before getting sufficiently advanced that they could simulate the universe (it being impossible to achieve is a subset of this proposition).

2. Any sufficiently advanced civilization is extremely unlikely to run a significant number of simulations of their evolutionary history.

3. If not 1 or 2, then any sufficiently advanced civilization will run a significant number of simulations of their evolutionary history, resulting in Elon Musk's probability analysis.

So Musk skipped possibilities 1 and 2 and assumed we were in 3.

[Neil deGrasse Tyson video explaining why we probably aren't in a Simulation](https://www.youtube.com/watch?v=pmcrG7ZZKUc) did a youtube video in 2020 and came up with a [Bayesian statistics](https://www.quantstart.com/articles/Bayesian-Statistics-A-Beginners-Guide/) (related to but not the same as Bayesian probability) argument that we were slightly more than 50/50 NOT living in a simulation. deGrasse Tyson assumes that that proposition 1 (civilizations never reach the required technological level) and proposition 3 should be given equal probabilities, in which case Musk's 1/(1 trillion + 1) only has a 50% probability of its own, so the chances were are not in a simulation is 50% + 1/(1 trillion + 1). What happened to proposition 2? Well, deGrasse Tyson looked at TV shows and assumed that since most TV shows are set in contemporary times, so simulations down the chain will have the "contemporary times" capability to create simulations themselves. Since we don't create simulations today, then we slightly more likely than 50/50 to be in reality and not in a simulation.

But there are actually a few more unstated assumptions underlying the probability analysis that could be taken into account. For example, does it matter if each simulation doesn't have the power of the civilization that created the simulation? In other words, at some point down the chain simulations don't have enough power to create more simulations. For purposes of Musk v. deGrasse Tyson, it doesn't really matter. Musk's exponential growth is substantially reduced, but he still would conclude we are more likely in a simulation and deGrasse Tyson would conclude we a slightly more likely than 50/50 we are not in a simulation. This was demonstrated by [Prof. David Kipping in Columbia University's Astronomy Department](https://www.mdpi.com/2218-1997/6/8/109/htm). Kipping demonstrates that you can use Bayesian statistics properly without this assumption and still get to the conclusion that we are probably not living in a simulation. However even his analysis depends on starting assumptions you can agree or disagree with. If you prefer youtube directed at non-science people to reading the paper, Prof. Kipping sets out his reasoning [here](https://www.youtube.com/watch?v=HA5YuwvJkpQ).

I think it is interesting that if you poke around the internet looking at the simulation hypothesis discussions, technologists assume the technology is possible, computer scientists don't believe the technology is possible, religious people point out that this is just another way of thinking about how God/Gods created the universe and philosophers think that it is a boring question (more about that later).

## Maybe an Approximation of Reality?

Neuroscientists might take the position that what we perceive is just our brain's approximation of reality, so we are effectively living in a simulation already.

One sidenote to the discussion if you are a computer or physics nerd deals with matching up quantum mechanics with how you would create a computer simulation while trying to save computing power resources. Computer games often do not create the entire game environment at once, they just create the bits that the player can see or will be able to see shortly. In the deGrasse Tyson videa, they suggest that maybe the speed of light limitation in physics exists to give the universe computer the time to create the reality ahead of a player's movement. I think that is a cute idea, but find the quantum mechanic argument more fascinating. Consider quantum physics's [Schrödinger's cat thought experiment](https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat) - the cat is both alive and dead until observed. In this case the cat (reality) is anything and nothing until it is procedurally generated so that it can be observed. Similarly, if a tree falls in a forest and was not observed, it didn't actually fall - the simulation computer is just saving resources and will retroactively make the tree fall if anyone does look there.

One of the questions is whether there is really any way to determine if we are in a simulation. So far the ideas seem to be limited to either looking for "glitches" (as if we would know one when we saw it) or looking for "pixelation". The pixelation idea disagrees with the unstated assumption described above that each level of simulation could actually create a simulation of reality equal to the true reality. Think of it the same way as looking at a Mario Brothers video game does not look like a movie, let alone reality. Is there some clock tick of the universe limit that we could find that doesn't match what we think true reality would have? Again, how would we know what true reality physics would be if we are in a simulation? Musk and deGrasse Tyson assume that the simulation creators just try to recreate their own reality, but I don't see any reason to accept that assumption.

Sidenote. There is an insanely complicated computer game/simulation that spends its time on calculating interactions of minutia rather than pretty graphics called [Dwarf Fortress](https://www.youtube.com/watch?v=VAhHkJQ3KgY); Wikipedia entry [here](https://en.wikipedia.org/wiki/Dwarf_Fortress). A legendary glitch in Dwarf Fortress was players discovering that cats were dying in taverns for no apparent reason. Debugging determined the cause in the following chain of events. Dwarfs drinking in the tavern would get called away to do some chore and would just throw their beer on the floor. Cats would randomly walk across the floor and the computer determined that alcohol on the floor would soak into the cats feet. The cats would clean themselves by licking their paws, thereby ingesting the alcohol. None of this was foreseen by the programmers, it was what is referred to as emergent behavior. The reason the cats were dying was that the computer overestimated the amount of alcohol the cats would ingest, but it did know the size of cats and about alcohol poisoning. As a result of those calculations, it determined that the cats would die of respiratory failure from alcohol poisoning.

## Is This God and the Creation Story by Another Name?

You [could](https://www.gotquestions.org/simulation-theory.html) argue that the simulation hypothesis is [just another creation story](https://www.christianitytoday.com/ct/2017/october-web-only/friend-have-you-heard-good-news-about-simulation-hypothesis.html). An omnipotent god creates the world. Musk and deGrasse Tyson's assumption that the simulator would likely simulate their own reality would be consistent with the Book of Genesis statement that God created man in his likeness. No, I am not going to go down that rabbit hole now or ever, but I will suggest that the simulation hypothesis could be considered consistent with one or plural gods.

New York University philosophy professor David Chalmers has [described](https://builtin.com/hardware/simulation-theory) the being responsible for this hyper-realistic simulation we may or may not be in as a “programmer in the next universe up,” perhaps one we mortals might consider a god of some sort — though not necessarily in the traditional sense. “[H]e or she may just be a teenager,” Chalmers said, “hacking on a computer and running five universes in the background… But it might be someone who is nonetheless omniscient, all-knowing and all-powerful about our world.”

But religion is usually more than just a creation story. What about afterlife, immortal souls and consequences for your actions in the hereafter? That could still exist in a computer simulation. In computer games, characters may be able to "respawn" back into the game after they "die" at different points and potentially with increased or decreased characteristics. You can even save your characters for use in a computer game sequel. The late British writer Iain Banks' science fiction book "Surface Detail" has both virtual heavens and hells which societies have set up and people's consciousness gets uploaded to a virtual reality. The virtual heavens are created for people tired of the physical world but don't want to "cease existing". The virtual hells were created for people they believe deserve everlasting punishment. These are, essentially, simulations.

To the extent you think about the problem of evil as an argument against theism, the issue still exists in the simulation hypothesis, although maybe a bit weaker. The problem of evil can be stated as why would God create a world with evil? One of the underlying unstated assumptions is that if there is a God, it realizes that it has created sentient being which are subject to the evil. The simulation hypothesis does not require that the creators of the simulation realize that the characters in the simulation are conscious beings and don't realize that "real" suffering is being caused. If you dig deep enough in the internet, you can find plenty of examples of teenagers playing computer simulation games and doing terrible things to the characters..

## Does This Have Any Relation to Philosophy?

Many philosophers take the position that the simulation hypothesis doesn't matter or at least that it matters no more or less than the existence of a God matters. If there is a God and an afterlife, that is the equivalent of a simulation operator and sequels with carryover characters. If there is a God and no afterlife, that is the equivalent of a simulation operator and no sequels with carryover characters. If there is no God, that is the equivalent of no simulation. Certainly [positivists](https://www.britannica.com/topic/positivism) [say](https://www.reddit.com/r/askphilosophy/comments/7a2woc/what_are_some_strong_criticisms_of_the_simulation) it doesn't matter. On other hand, why not have fun arguing over how many angels can dance on the head of a pin?

I've also seen people argue both for and against the idea that the simulation hypothesis is related to [Solipsism](https://iep.utm.edu/solipsis/) because both suggest that our observations are not trustworthy. The original idea of the Greek sophists (e.g. [Gorgias](https://en.wikipedia.org/wiki/Gorgias) 483 B.C. - 375 B.C.) was to show that objective knowledge is impossible. Solipsism can be viewed as the principle that for each person, "existence" means "my existence" and "my mental states" - everything "I" experience. Anything outside one's own mind is unsure. Taken to extremes, a solipsist has no empathy or sympathy for others because nothing exists other than the solipsist themselves.

[René Descartes](https://en.wikipedia.org/wiki/Ren%C3%A9_Descartes) tried to search for incontrovertible certainty and said "cognito, ergo sum" ("I think, therefore I am" in English or "Je pense, donc, je suis" in French). Essentially, you can't doubt your own existence unless you exist to doubt. If he stopped there, he would also be open to the simulation hypothesis. However, once he gets into dualism - mind and body are distinct but closely joined - I'm not so sure he could be consistent with the NPC version of the simulation hypothesis. He might still be consistent with the RPC version.

Irish philosopher [George Berkeley](https://en.wikipedia.org/wiki/George_Berkeley) took the position that the essence of objects is to be perceived ("Esse Es Percepi"), so you could certainly fit his thinking in with a simulation. He called it immaterialism but current philosophy teaching refers to it a subjective idealism.

[Immanuel Kant](https://en.wikipedia.org/wiki/Immanuel_Kant) took a slightly different position, holding that objects of experience exist because of our minds which categorize our experience. He also, however, believed that there were things that did exist independently of experience.

The Indian Advaita Vedanta school of philosophy holds that Brahman, the unitary consciousness appears as diversity in the world because of illusion (maya). Other schools of Vedanta accept some difference between individual souls and Brahman. Czech writer Martin Rezny [suggests](https://medium.com/words-of-tomorrow/what-the-ancients-have-to-say-about-the-simulation-theory-223383fe1692) that the idea of maya and its counterparts in Zen Buddism are very similar to the concept of living in a simulation.

Certainly there are people who seem to freak out at the thought that we might not be real. See e.g. [Simulation Theory Debunked](https://thethink.institute/articles/simulation-theory-debunked):

"*This question is another in a long line of skeptical threats. Skeptical threats are global threats in that they threaten everything we know, think we know, or could possibly know with that ancient epistemological enemy: skepticism. These types of arguments seek to provide one big defeater for all of our human knowledge and leave us with the specter of skepticism looming large over our beliefs.*"

The article tries to attack the thought experiment by arguing that we have yet to be able to produce strong artificial intelligence minds and some philosophers don't think we ever will. Hello, its a thought experiment that starts with the assumption that some alien civilization does have the technology. Saying that it doesn't work because we don't have the technology is missing the point. Not even its proponents claim that we have the technology today. I think they are unduly paranoid about skepticism.

[Philosopher David Chalmers](http://serious-science.org/skepticism-and-the-simulation-hypothesis-6189) notes that the simulation hypothesis is the modern version of Descarte's skepticism problem. How do we get beyond consciousness in order to answer the skeptical problem? Chalmers takes the position that even if we are in a simulation, it is still "perfectly real". This means that it is not a skeptical hypothesis (one where nothing exists) but a metaphysical hypothesis bout the underlying nature of reality.

Philosophy Professor Eric Schwitzgebel takes the position in [Kant Meets Cyberpunk](https://content.sciendo.com/view/journals/disp/11/55/article-p411.xml) that the simulation hypothesis is consistent with Kant's transcendental idealism but does note that two of the philosophers commenting on the paper strenuously object to this approach to Kant.

At the end of the day, its all fun and games unless someone takes it seriously.

## Does It Even Matter?

Let's set aside the probabilities of whether we are in a simulation and talk about whether it matters.Real world or simulation or [Plato's](https://www.studiobinder.com/blog/platos-allegory-of-the-cave/) [Cave](https://en.wikipedia.org/wiki/Allegory_of_the_cave), it doesn't impact people's daily lives and so who cares? The short answer is it shouldn't matter BUT it can matter to some people.

I'm going to go out on a limb and suggest that if it matters to people, it is likely to have negative consequences. Some people are terrified at the concept of nonexistence. Other people may take that in a completely different direction as they argue that since it is all just a simulation, then they shouldn't have to worry about consequences (hit restart and it all starts over again from the beginning). These are the sorts of people who then tend to behave terribly towards others because they now have an excuse to stop behaving. In other words, an extreme solipsist.

Absent that, maybe the following from Bartleby on Metafilter is helpful [comment](https://www.metafilter.com/189014/Do-We-Live-in-a-Simulation-Chances-Are-about-5050):

  "*It also puts things in perspective for people who are narcisistically thinking of simulation as some sort of Interventionist God. If you're simulating at Spiral_Cluster_2_final_master_rev2, then your individual existence isn't some malicious kid's copy of the Sims 3 where you're being attentively tormented. You're a procedurally generated pixel of 8-bit coral in the Dreamcast MAME cabinet port of Ecco the Dolphin, in a folder in a folder somewhere in an old Dropbox account that somebody set up three career changes ago and couldn't begin to guess the password to. The Heat Death of the Universe is just bitrot, or the final powering off of the Zune Music Marketplace servers. An accounting for the universe, the Ultimate Answers you seek, don't even exist at that granularity. Don't get so worked up about it.*"

## Do We Need to Worry About the Plug Being Pulled?

Philosophy Professor Preston Greene, Nanyang Technological University in Singapore thinks that proving we are living in a simulation could be [catastrophic](https://www.nytimes.com/2019/08/10/opinion/sunday/are-we-living-in-a-computer-simulation-lets-not-find-out.html). See his paper on [termination risk](https://philpapers.org/rec/GRETTR-5).

"*if our universe has been created by an advanced civilization for research purposes, then it is reasonable to assume that it is crucial to the researchers that we don’t find out that we’re in a simulation. If we were to prove that we live inside a simulation, this could cause our creators to terminate the simulation — to destroy our world.*"

See also [Simulation Typology and Termination Risks by Roman Yampolsky adn Alexey Turchin](https://philpapers.org/rec/TURSTA-9) for more down the rabbit hole thought experiments on when the plug will be pulled on us if we are in a simulation:

"*Humanity’s location at the beginning of the 21st century could be best explained by the fact that this period is in a scientific Fermi simulation by an alien civilization or future humanity-based AGI simulating variants of its own origin, which could be called a “singularity simulation”. This means that humanity could be tested for different scenarios of global catastrophic risks, and no matter what the result of the test is, the simulation of us would be turned off relatively soon, in tens to hundreds of years from now.*"

"*Also, humanity could be in a gaming simulation of some posthuman or alien beings, which is similar to a Fermi simulation but created for recreational purposes. This has even worse termination risks because it could be ended for arbitrary reasons (such as boredom).*"

## Does This Say Anything About Free Will or Determinism?

I think the simulation hypothesis says nothing about free will or determinism. People can't agree on whether we have free will when they assume that we live in "reality". If you assume free will means you are in total control of your thoughts and actions, quantum mechanic randomness is insufficient for "free will". If they can't agree on that, they certainly can't agree on whether characters in a simulation have free will. Note, at this point we are talking about a simulation, not puppet on a string type video games. (A person on reddit.com commented that if he is being controlled in a universal video game, he has autism and several physical disabilities and would appreciate it if the entity controlling him took it off hard mode.)

Certainly computer simulations can build in some randomness and quantum theory implies that our reality or simulation also has a random number generator of some type. That gets you away from a strong version of determinism but doesn't necessarily get you all the way to free will. If you can code a simulation of reality, you can probably code the illusion of free will.

You might also look at the Hulu TV series [Devs](https://www.hulu.com/series/devs-fd2f6cc3-dafc-4741-ae2e-d86494f3ca51) about a different take on free will and determinism as well as viewers who got extremely upset over how the characters acted based on their beliefs with respect to the simulations. Also interesting is the 2008 story [Responsibility](https://qntm.org/responsibility) where the simulation programmers discover that they don't dare turn the simulation off and the reader comments.

## Conclusion

Like religious belief in God/Gods, I have yet to see a proposal which can prove or disprove the simulation hypothesis. As a result, it becomes a question of faith. As implied earlier in this post, I don't see a distinction between a creation myth with God or a simulation hypothesis by one or more programmers. God = programmers. At the end of the day, I don't think it really matters. If there are extreme solipsists who use it to claim there are no consequences if they treat other "characters" badly, those people are going to be bad actors with or without a belief in the simulation hypothesis. But I had fun pulling these thoughts together as a distraction from politics..

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
