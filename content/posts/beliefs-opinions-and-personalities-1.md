---
title: "Beliefs, Opinions and Personalities"
date: 2020-09-08T06:18:19-04:00
categories: ['posts']
tags: ['politics', 'philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
I will posit this as my opinion - it is easier to change opinions than it is to change beliefs. The line drawing between opinions and beliefs is probably fuzzy, however. I would also suggest that your own personality will affect whether seeing facts contrary to your own opinion will cause you to change that opinion. I think this opinion of mine gets some support in a recent paper (abstract published at [Close Minded Cognition](https://link.springer.com/article/10.3758/s13423-020-01767-y)).

That paper determined that people scoring high in the desire for order, structure and preservation of social norms tend to be less successful at correcting erroneous beliefs when confronted by new information. For those people, a close-minded cognitive style negatively influences belief updating. (click on the title to see more)
<!--more-->
If you look at the paper, you need to avoid getting side tracked by their use of psychology terminology of referring to the desire for order, structure and preservation of social norms as "right wing authoritarianism (RWA)". **This is not "right wing political orientation", this is right wing authoritarianism as used by psychologists.** See [https://en.wikipedia.org/wiki/Right-wing_authoritarianism](https://en.wikipedia.org/wiki/Right-wing_authoritarianism). If you do want to get side tracked and argue about right and left wing authoritarianism, I suggest you read Bob Altemeyer's book "Th Authoritarians", available for free at [The Authoritarians](https://www.theauthoritarians.org). If you read comments in social media on this paper, you will notice that 90% are thinking this is political commentary and not psychological commentary. For purposes of this post, just focus on the definition as people scoring high in the desire for order, structure and preservation of social norms.

To quote from the abstract:

> "It is important to note that belief updating and motivated cognition likely reflect underlying cognitive tendencies that can span political boundaries.

> Radical beliefs have been linked to impaired belief updating and lower metacognitive sensitivity; on both ends of the political spectrum, radicals tend to be less sensitive to evidence that corrects mistakes.

> Likewise, political liberals and conservatives appear to be equally likely to reject evidence that contradicts prior beliefs.

> In the present study, we found that neither conservatism nor SDO scores were related to belief updating. [SDO = Social Dominance Orientation]

> RWA measures may therefore capture a cognitive style that is uniquely related to belief updating. However, radicalism and biased information processing likely influence belief updating in other ideological groups. By investigating the cognitive mechanisms of belief updating, we aim to cast light on the common factors that may underly radicalization across political boundaries."

I would have also thought that it is obvious that everything is a spectrum, but reading social media comments on the paper indicate that many people (a) want to see the world in black and white and (b) just automatically interpreting RWA as talking about political right wings.

I do not know how to deal with belief updating in an environment when political battles are fought with misinformation and disinformation and the warfare is mostly psychological.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
