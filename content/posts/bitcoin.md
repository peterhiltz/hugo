---
title: "Lets Talk About Bitcoin"
date: 2021-03-21T14:18:19-04:00
categories: ['posts']
tags: ['politics','technology']
showToc:  "true"
author: "Peter Hiltz"
---
Bitcoin is another example of the gig economy - outsourcing business costs to someone else who may or may not recover their costs. In addition, there is the question of its impact on the environment. Let's spend a minute thinking about what bitcoin really is. Bitcoin is a let's pretend commodity, limited by contract to 21 million bitcoins. Simplifying greatly, bitcoins are supposed to be a way to exchange value and anonymously with the transaction guaranteed by cryptoanalysis. Bitcoin outsources all that cryptoanalysis to  "miners" who buy computer equipment to do those calculations and pays them in bitcoin, slowly increasing the amount of bitcoin in circulation.

There are a few problems here. First, the fact that miners get paid in bitcoin is analogous to employees in a startup getting paid in stock instead of cash. Miners are looking for the pot of gold at the end of the rainbow. Second, miners only get paid if they are the first miners to solve the calculation for a transaction. There are lots of miners and only one (or one pool of miners) can be first. In addition, the compensation is ramped down over time.

Consider the following consequences:

- Multiple miners, but only one used unit of production, so lots of excess "manufacturing costs" in the system compared to the actual production. Even if the miners (and the computer hardware manufacturers) were using 100% renewable energy, there is a lot of computing power and actual hardware production that is going to waste.

- Multiple miners with both fixed and variable costs but who get no compensation because someone else beat them to the answer. And people think Amazon and Doordash delivery drivers are under paid? At least they get something. [Investopedia - How Does Bitcoin Mining Work](https://www.investopedia.com/tech/how-does-bitcoin-mining-work/) says that "All told, bitcoin mining is largely unprofitable for most individual miners as of this writing."

- It is not entirely clear what happens when the last bitcoin is issued. There will still be transactions that need the cryptoanalysis done in order to validate the transaction. Without the fantasy of getting paid in bitcoins which might skyrocket in value, who is going to do that work and who is going to pay them?

The first two consequences play into the argument that bitcoin is bad for the environment. The [digicononmist](https://digiconomist.net/bitcoin-energy-consumption) estimates that Bitcoin's carbon footprint (from the mining activity) is the same size as the country of New Zealand. Almost by definition, duplication of effort (the miners who didn't come in first) create useless energy and production costs in the system. The arguments against this seem to fall into the "Oh Look - A Squirrel" category. Consider the following statements made to [CNBC](https://www.cnbc.com/2021/02/05/bitcoin-btc-surge-renews-worries-about-its-massive-carbon-footprint.html):

“*Energy use in itself is not bad,” Meltem Demirors, chief strategy officer of digital asset management firm CoinShares, told CNBC. “Sending and storing emails uses energy. Yet, we don’t infer email to be bad because it consumes energy.*”

Seriously? Even spam at least has an end email product. All that excess bitcoin mining that doesn't come first doesn't even generate a product.

“*What we have here is people trying to decide what is or is not a good use of energy, and bitcoin is incredibly transparent in its energy use while other industries are much more opaque. Demirors questioned why the banking industry, for instance, wasn’t under more scrutiny for its energy usage.*”

Demirors is justifying bitcoin energy usage because you can estimate it, but you can't as easily estimate excess energy usage in other industries? Come on, bad is bad.

Forbes had an absolutely amazing hand waving [article in Feb 2021 claiming that the environmental arguments miss the mark](https://www.forbes.com/sites/rogerhuang/2021/02/16/arguments-that-bitcoin-harms-the-environment-through-wasteful-emissions-miss-the-mark/?sh=ea07ccf20a7b). Consider the concluding sentence: "*The explosive growth of bitcoin has not just been a story of those piling onto a deflationary asset to save themselves from a forced path of debt and lower wages. It has also been, in many ways, the story of the hidden costs of asset inflation that aren’t captured by the consumer price index — and the very story of what a society decides is valuable or wasteful. Arguments about bitcoin’s environmental impact and its 'wastefulness' miss the mark entirely without considering the nuances contained in the idea of economic waste.*" Wait a minute, what???

Going up further in the article, we get the following statement:

"*Bitcoin is not just a wonkish deflationary “proof-of-work” system. It is a way to inject liquidity and a democratic, decentralized, and international desire to target the incredible amount of waste in the current financial system and economic-political complex that defines the modern nation-state.*"

I'm not going to parse the article in this post, you can do that on your own, but the thrust seems to be that the current financial system is bad and bitcoin inserts some democracy into it, so stop talking about bad environmental impacts and be grateful that it might help the little guy. Not that I'm defending the current financial system, but I'm not sure that anything that outsources costs to gig workers who might not get paid should be classified as "helping the little guy". From that standpoint, it is the equivalent of buying lotto tickets, only with a worse environmental footprint.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
