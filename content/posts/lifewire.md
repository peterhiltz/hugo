---
title: "I Guess I Don't Match Lifewire"
date: 2020-11-02T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
So, trying to keep myself from fixating on the political news, I asked DuckDuckGo for suggestions on random websites. One of the suggestions was from [lifewire](https://www.lifewire.com/cool-websites-to-look-at-when-bored-3486362) that claimed "cool websites to look at when bored". It claimed "Whether you need to kill some time or you're in the mood to laugh, learn, or be inspired, this list of cool sites is all you need." It was a typical list with "what we like" and "what we don't like". So I thought I would amuse myself by commenting on their comments.

1. [Bored Panda](http://www.boredpanda.com/). Lifewire didn't like "Not all content is factual". Hmmm. Are you saying that they are dishonest or you don't like fiction.

2. [Brain Pickings](https://www.brainpickings.org/). Livewire describes this as "Try expanding your knowledge by diving deep into the incredibly useful and thought-provoking blog posts on Brain Pickings, which is a popular blog run by MIT fellow Maria Popova. She's the one who does all the research and writing for each post." Lifewire didn't like "Lots of text". Hahahahahahahahaha. Ok Lifewire, how are you supposed to get those researched thought-provoking posts into your brain without text?

4. [Laughing Squid](http://laughingsquid.com/) Lifewire comments that posts are kept pretty short too, making it perfect for browsing casually. Apparently Maria Popova from Brain Pickings needs to check out Laughing Squid to see how to make incredibly useful and thought provoking researched blog posts pithy so that thought provoking can be still done while browsing casually.

5. [Vsauce](http://www.vsauce.com/). Vsauce is apparently a Youtube channel that focuses on educational content. Lifewire didn't like that it had "some complex topics". Seriously? You fault an educational channel for "some complex topics"? Do you have the attention span of a butterfly? Did your education end after "See Spot run"?

6. [Oddee](http://www.oddee.com/) Lifewire liked that it had articles that cite sources. OMG. Can you do that and not have complex topics and not have lots of text and ...? Did Lifewire have multiple writers that didn't coordinate on likes and dislikes?

7. [Mental Floss](http://mentalfloss.com/). Lifewire says " Mental Floss will leave you feeling like you actually learned something during the time you wanted to pass while browsing the web. Describing itself as "the encylopaedia of everything," the site offers content on some of life's most interesting questions.

You can read articles, view lists, watch videos, take quizzes and even brush up on some smart facts with Mental Floss on everything from science to pop culture. So go ahead and expand your knowledge with this one!" Hmm. I did a quick check on length of articles on Brain Pickings and length of articles on Mental Floss. Without doing a scientific analysis, it looks like the length of articles might be the same, but Mental Floss might have more pictures. Lifewire didn't like that "Ads can be distracting". No kidding.

8. [The Useless Web](http://www.theuselessweb.com/). Clicking on this site will show you "the most pointless websites that exist on the internet." Lifewire didn't like "Hit or miss results". What would be a "miss" result from a website that randomly links you to the most pointless websites in existence? A really good one?

9. [Giphy](http://giphy.com/). Giphy is a search engine for animated GIFs. Lifewire liked "Easy to find trending and new images". Lifewire didn't like "search can be glitchy". Are the glitchy searching only on non-trending and older images? Curious people want to know.

10. [The Oatmeal](http://theoatmeal.com/). Lifewire didn't like that "not all content is family-friendly." Ok. Good warning. That also applies to the news.

13. [Reddit](https://www.reddit.com/). Lifewire says "f StumbleUpon isn't your thing, Reddit may be a good alternative." Uh. Guys. Stumbleupon no longer lives. Lifewire didn't like "Learning curve involved." I'm 64 and I didn't think there was a learning curve on Reddit. But maybe that tells me a bit more about Lifewire's dislike of "complex topics".

16. [Hyperbole and a Half](http://hyperboleandahalf.blogspot.com/). This apparently is a blog by "Allie Brosh, a young woman with a talent for telling her left story through detailed Microsoft Paint drawings." I'm not sure what her "left story" is or whether it is different from her "right story". Lifewire didn't like the fact that "new content is no longer added" and "some content is lengthy." I can understand why someone could be upset over the fact that a blogger is no longer adding content to a blog. But complaining that some content is lengthy is telling me more about Lifewire's attention span.

Ok. Now I'm bored.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
