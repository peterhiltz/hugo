---
title: "2020 Philosophy Survey Part 1l: Logic, Perceptual Experience, Proper names"
date: 2022-02-24T15:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1 with the topics being "Logic", "Perceptual Experience" and "Proper Names". I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Logic
In Western philosophy, classical logic is defined as set out in [classical logic](https://plato.stanford.edu/search/r?entry=/entries/logic-classical/&page=1&total_hits=1843&pagesize=10&archive=None&rank=0&query=classical%20logic). There are lots of non-classical logics. See e.g. [Free Logic](https://plato.stanford.edu/entries/logic-free/), [Intuitionistic Logic](https://plato.stanford.edu/entries/logic-intuitionistic/), [Modal Logic](https://plato.stanford.edu/entries/logic-modal/), [Many-Valued Logic](https://plato.stanford.edu/entries/wittgenstein/), [Fuzzy Logic](https://plato.stanford.edu/entries/logic-fuzzy/) and [Temporal Logic](https://plato.stanford.edu/entries/logic-temporal/). By the way, Indian philosophical logic starts with "A", "not A", "A and not A" and "not A and not not A". I think this fits into "non-classical" logic from a Western Philosophy perspective.

| [Classical] | Non-classical | Agnostic | Combination | Unclear |
|-----------|---------------|----------|-------------|-------------|
| 54%       | 26%           | 9%       | 5%          | 6%          |
N = 1415

### Perceptual experience
See [Perceptual Experience and Perceptual Justification](https://plato.stanford.edu/entries/perception-justification/), [Perception Problem](https://plato.stanford.edu/entries/perception-problem/), [Pain](https://plato.stanford.edu/entries/pain/) and [Perception Justification](https://plato.stanford.edu/entries/perception-justification/).

Per the SEP, "Perceptual experiences are often divided into the following three broad categories: veridical perceptions, illusions, and hallucinations. For example, when one has a visual experience as of a red object, it may be that one is really seeing an object and its red colour (veridical perception), that one is seeing a green object (illusion), or that one is not seeing an object at all (hallucination). Many maintain that the same account should be given of the nature of the conscious experience that occurs in each of these three cases."

- [Disjunctivism](https://plato.stanford.edu/entries/perception-disjunctive/) denies that the three experiences are the same, claiming that veridical perceptions have mind-independent objects as constituents of experience, but illusions and hullucinations do not have mind-independent objects as constituents of expericence.
- [Qualia](https://plato.stanford.edu/entries/qualia/), [Qualia Theory](https://plato.stanford.edu/entries/qualia-knowledge/). Look at something that is a bright color. When you see the bright color, you are the subject of a mental state with a very distinctive subjective character. There is something it is "like" for you to undergo that state. Qualia refers to the introspectively accessible, phenomenal aspects of our mental lives. There is a lot of debate on how that relates to the physical external world as well as the physical world inside your head.
- [Representationalism](https://plato.stanford.edu/entries/consciousness-representational/) or intentionalism holds that to experience a snow-covered churchyard is to directly perceptually represent such an object (i.e. to represent such an object but not in virtue of representing another more “immediate” object).
- [Sense-Datum](https://plato.stanford.edu/entries/sense-data/) hold that a sense-datum is just whatever it is that you are directly presented with that instantiates the sensible qualities which characterise the character of your experience. It could be real, it could be illusion and hallucinatory.

| [Disjunctivism](https://plato.stanford.edu/entries/perception-disjunctive/) | [Qualia Theory](https://plato.stanford.edu/entries/qualia-knowledge/) | [Representationalism](https://plato.stanford.edu/entries/consciousness-representational/) | [Sense-Datum](https://plato.stanford.edu/entries/sense-data/) | Agnostic |
|---------------|---------------|--------------------|-------------|----------|
| 16%           | 15%           | 39%                | 5%          | 15%      |
N = 1323

### Proper names
See [Names](https://plato.stanford.edu/entries/names/). See also [Mill, Frege and Russell: Proper Names and Their Philosophical Explanations](https://medium.com/@jamesconormiller/mill-frege-and-russell-proper-names-and-their-philosophical-explanations-589fef4100bc).
- [Fregean](https://plato.stanford.edu/entries/frege/#FreTheSenDen) is the view that names and descriptions also express a sense that accounts for its cognitive significance. The expressions ‘4’ and ‘8/2’ have the same denotation but express different senses, different ways of conceiving the same number. Mark Twain and Samuel Clemens deonote the same individual, but denote different senses.
- [Millian](https://plato.stanford.edu/entries/meaning/#RussSema) refers to the view that proper names are arbitrary linguistic identifiers of an entity without implying anything about its attributes.

| [Fregean](https://plato.stanford.edu/entries/frege/#FreTheSenDen) | [Millian](https://plato.stanford.edu/entries/meaning/#RussSema) | Agnostic |
|---------|---------|----------|
| 39%     | 39%     | 15%      |
N = 1269


As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
