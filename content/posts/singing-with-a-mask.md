---
title: "Singing with a mask"
date: 2021-07-26T14:18:19-04:00
categories: ['posts']
tags: ['music']
showToc:  "true"
author: "Peter Hiltz"
---
Someone raised a question about singing with a mask on, so experiment time! Obviously singing without a mask is more comfortable, but how does the sound change? I just threw these together for a listen (no production or effects or rehearsal). Yes, I know I'm flat in a few places - this was an experiment in singing with a mask, so not focused as much as I could have been.

- Childhood Dreams without mask
{{< audio mp3="../childhood-dreams-without-mask.mp3" >}}

- Childhood Dreams with mask
{{< audio mp3="../childhood-dreams-with-mask.mp3" >}}


# Childhood Dreams
Time-Signature: 4/4 Capo: 3
{{< highlight none>}}

       G              Bm        C              G
    We gather close together, we reminisce and tell
        G              D                   C               D
    the stories of our childhoods when our dreams began to jell
         C            Bm        C                     D
    When we foresaw a ladder of hopes and schemes and dares
    G            Bm                C              G
    leading up into the clouds and castles in the air


        Am               E        F                E
    The creatures of our dreaming returning to our thoughts
        Am                 E               G                  Am
    are precious handholds back in time to which our mem'ries caught
         F                 E           F                E
    Back then we were much younger and thought we could become
        Dm           C            G                     E
    the heroines and heros of the songs that we'd heard sung


    G               Bm          C                  G
    Now that we are older those childhood mem'ries come
    G                   D   C                     D
    Dreams of what we'd do, songs which we'd have sung
        C          Bm        C                 D
    And some of us succeeded some who took the dare
       G                Bm                C             G
    to climb the ladder of those dreams into the starry air


    Am                E                 F            E
    Others left their dreams behind and sang another song
       Am          E              G                Am
    distracted by another goal to which they now belong
      F                  E         F                E
    tonight we're back together comparing notes and schemes
    Dm                  C                   G                  Am
    what we've done and where we've been compared to childhood dreams

{{< / highlight >}}

You can decide for yourself whether it makes a difference. As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
