---
title: "2020 Philosophy Survey Part 1r: Rational Disagreement, Moral Principles and Gender Categories"
date: 2022-03-01T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1r with the topics being Rational Disagreement, Moral Principles and Gender Categories. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Rational disagreement
Can two people with the same evidence rationally disagree? Permissivism about rationality is the view that there is sometimes more than one rational response to a given body of evidence.

| non-permissivism | permissivism | Agnostic |
|------------------|--------------|----------|
| 19%              | 70%          | 5%       |
N = 995

### Moral Principles
See [Moral principles](https://plato.stanford.edu/entries/moral-particularism-generalism/)
Generally, moral generalism believes in moral principles. The hard part about moral particularism is that it can oppose moral "principles" in so many different contexts, fact patterns and variations that it is difficult to make a general statement about it.
| Moral Generalism | Moral Particularism | Agnostic |
|------------------|---------------------|----------|
| 55%              | 34%                 | 7%         |
N = 984

### Gender categories
The question was Gender Categories: revise, preserve or eliminate? Your guess is as good as mine.

| Preserve | Revise | Eliminate | Unclear | Agnostic |
|----------|--------|-----------|---------|----------|
| 20%      | 51%    | 16%       | 6%      | 8%       |
N = 983

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
