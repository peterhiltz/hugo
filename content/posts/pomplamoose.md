---
title: "Pomplamoose is Up For a Grammy - Best Jazz Vocal Ablum"
date: 2020-10-12T14:18:19-04:00
categories: ['posts']
tags: ['music']
showToc:  "true"
author: "Peter Hiltz"
---
[Pomplamoose](https://www.youtube.com/c/PomplamooseMusic/videos),  one of my favorite little jazz music groups is up for a Grammy in the Best Jazz Vocal Album category. The album is 'En Francais' which is a collection of French jazz standards. This is impressive seeing as They don't have a record label deal or a publicist other than their youtube channel. Nataly Dawn, the vocalist, spent a large part of her childhood in France and Belgium which is where she picked up a love for French jazz and her pronunciation. Try [Les copain d'abord](https://www.youtube.com/watch?v=igNe4l_svLU) by Georges Brassens, [Les Yeux Noirs](https://www.youtube.com/watch?v=gfiqW1WaGbw) or [Sous le Ciel de Paris](https://www.youtube.com/watch?v=Vol9dZ-t93s) by Edith Piaf. They also have an extensive collection of covers of pop songs with nice arrangements and a small number of originals. My favorite of the originals is [Bust Your Knee Caps](https://www.youtube.com/watch?v=FlDGFrP4NgI) which can be best described as a doo wop genre revenge song by a mafia princess when her boyfriend gets cold feet about the family business. The juxtaposition of a very innocent doo wop melody and harmony with terrifying lyrics is an interesting combination. Most of the videos on youtube are of Nataly, Jack Conte (her husband and a co-founder of Patreon) and whoever has joined with them in the recording studio. Clearly they are having fun e.g. [Monster Mashup with Tessa Violet](https://www.youtube.com/watch?v=rVE25pGBd9A) and that is the important bit.

Yes, Pomplamoose is french for grapefruit, but this post has nothing to do my post on [Grapefruit](https://www.peterhiltz.com/en/posts/grapefruit/) and Pomplamoose is safe to listen to with or without medication.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
