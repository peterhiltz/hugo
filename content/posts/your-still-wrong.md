---
title: "I've put myself in your shoes. You're still wrong"
date: 2020-11-18T14:18:19-04:00
categories: ['posts']
tags: ['politics']
showToc:  "true"
author: "Peter Hiltz"
---
A lot of times I find myself telling people that I don't disagree with them BUT... Everyone's ears and brain immediately shut down at the word "But". I know it, and still can't help myself. The word "AND"works much better. The point that I am often trying to make is asking the person whether they are actually trying to accomplish something or just rant.

To change a situation, you either need enough force to override the other side or you need to change minds on the other side. In order to change minds on the other side, you need to put yourself in their shoes, try to understand their point of view and then figure out how to approach a solution that also addresses their concerns.

I'm not saying that the other side is right. I'm not even assuming that a substantial group on the "other side" is even acting in good faith. You can put yourself in the other person's shoes and still decide they are wrong. Do you want to actually accomplish something? If so, what can you say or do that will change the mindset of the other side? Ranting that they are wrong won't change their mindset; they will just double down on their position. Ranting that they are evil won't change their mindset. Ranting that what they are saying is illogical or violates scientific fact really won't change their mindset. If you can't change their mindset, can you accomplish anything or are you counting on the ability to generate enough power on your side to win by force?

Someone commented yesterday that the President's base supports him because he hates the same people they do. I have a different take on that. I think the President's base supports him because he acts like he hates the same people they do. I don't think he cares enough about other people to hate them unless they oppose him. He has figured out the right buttons to push to get other people to support him, regardless of his actual positions. So stop preaching to the choir and figure out how to push buttons on the other side.

This is never easy and will be more difficult in what is increasingly becoming a post-truth world. I try to do this on a one on one basis and most times I am just trying to plant seeds that might grow in time. I freely admit I don't have suggestions on what buttons to push en masse to create a positive result. I do suggest people stop pushing buttons that causes an emotional doubling down by the other side.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
