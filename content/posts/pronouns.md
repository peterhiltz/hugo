---
title: "[Gender] in Pronouns"
date: 2020-10-25T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
The 2014 Nebula Award winner for best science fiction novel was Ancillary Justice by Ann Leckie. It was interesting (sometimes pleasantly, sometimes horrifyingly) to watch reactions to the use of pronouns in the book. The main character and narrator natively speaks a language that doesn't make gender distinctions in pronouns. (Finnish and Hungarian are similar in this respect). English, of course, does have gendered pronouns so to get the point across, the author has the narrator using feminine pronouns throughout the book except in dialog. If the narrator is engaging in a dialog with someone from another culture in another language which does use gender, the narrator is awkwardly guessing at the correct gender of any pronouns.

“*She was probably male, to judge from the angular mazelike patterns quilting her shirt. I wasn’t entirely certain. It wouldn’t have mattered, if I had been in Radch space. Radchaai don’t care much about gender, and the language they speak—my own first language—doesn’t mark gender in any way. The language we were speaking now did, and I could make trouble for myself if I used the wrong forms.*”

Reading the book for the first time six years ago, I noted the author's use and, typically for me, just went with it, completely missing the cultural point the author was making with that use. Then I started seeing completely outraged comments by readers who were incensed that they couldn't tell the gender of characters, at which point "dawn broke over Marblehead" as my New England grandmother would say.

As [one reviewer said](https://www.onthewhole.info/2015/02/snow-days-multiple-identity-and-ancilary-gender.html): "*As a result I  went through the book imagining most of the characters as female, which I found pleasantly different from most other SF I've read. (Joan Slonczewski's A Door into Ocean being a notable exception.) However, at times I had to remind myself that earlier in the book character X was referred to as having a male body and thus was physically male even though the character was referred to as "she" throughout the book and sometimes acted and was treated like a damsel in distress. My perception of the gender of the singular Justice of Toren identity also shifted from female to male to neutral depending on the character's thoughts and actions. All of which made me consider my own assumptions and stereotypes about gender.*"

I just recently read an [article](http://interfictions.com/translating-gender-ancillary-justice-in-five-languages-alex-dally-macfarlane/) about what translators went through trying to get the point across and thought I would dig a little deeper.

For the Hungarian translator, since Hungarian already has no default gender on pronouns, the translator [Csilla Kleinheincz](https://www.cheryl-morgan.com/?p=19653) had to get the point across using gendered nouns. So instead of using gender neutral nouns for 'child', 'cousin', 'parent', etc, she went with all feminine versions to make the author's point.

Bulgarian has gender for practically everything, but Milena Ilieva, the Bulgarian translator's biggest difficulty was with feminine forms for professions and military positions because the current feminine forms are felt to be "depreciative". As a result the translator ended up inventing her own feminine versions.

Bernhard Kempen, the German translator believes that his translation may be the first German language novel written in the generic feminine and ended up writing a preface trying to explain the issue.

The Hebrew translator Emanuel Lottem changed the title from "Ancillary Justice" to "Ancillary Integrity" because "justice" is masculine in Hebrew. “*For other objects, I’ll have to play it by ear: use feminine synonyms wherever possible, avoid using adjectives where they’re not essential, or use them in the feminine form where there’s no choice, hoping it won’t look absurd. I’ll have to ease the readers into this very gently.*”

The Japanese translator, Hideko Akao, took the approach of making all the grammar signatures as ambiguous as possible, noting that readers will have a feeling of strangeness which he hopes will be stimulating as they get "*bewildered when a character is referred to as ‘she’ since it is understandable that they have assumed ‘him’ as a male because of his ‘unfeminine’ language*".

I saw adverse reactions coming from two directions from English language readers. The first from people who see themselves as non-gendered who were upset that the use of 'she' genders everyone and the author should have used a non-gendered Spivak pronoun instead. See. e.g. [Post-binary Gender in SF](https://www.tor.com/2014/02/18/post-binary-gender-in-sf-ancillary-justice-by-ann-leckie/) and [Gender on the Mirror Empire and Ancillary Justice](https://meloukhia.net/2015/01/gender_on_the_mirror_empire_and_ancillary_justice/). I understand the point, but that would have prevented the author from making the intended cultural comment to the second group of outraged readers.

The second group viewed the default use of the feminine gender as a "gimmick" that confused the reader on the gender of characters and that violated the rights of the users to know the true gender o characters. I'm trying to reframe their argument as positively as possible. You can imagine what they actually said and I am not going to provide links.

In day to day life there is no blinking sign over everyone's head to tell me what your preferred pronoun is, and, if there was, I would most likely have been looking in a different direction anyway. So if you have a preferred pronoun, gendered or non-gendered, singular or plural, please let me know and I will use it towards you. That's just part of respect for other people.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
