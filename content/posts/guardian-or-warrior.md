---
title: "Guardian or Warrior?"
date: 2020-11-08T14:18:19-04:00
categories: ['posts']
tags: ['politics', 'life']
showToc:  "true"
author: "Peter Hiltz"
---
A [high school newspaper in Kentucky broke a story](https://manualredeye.com/90096/news/local/police-training-hitler-presentation/) on October 30 about a training slideshow used by the Kentucky State Police urging cadets to be "ruthless killers[s]" and quoting Hitler advocating violence. By 4:15 PM that day, the Kentucky Governor Andy Beshear responded with the following statement: “This is absolutely unacceptable. It is further unacceptable that I just learned about this through social media. We will collect all the facts and take immediate corrective action.”

The Communications Director for the Kentucky Justice and Public Safety Cabinet followed up an hour later “It is unacceptable that this material was ever included in the training of law enforcement. Our administration does not condone the use of this material. The material is not currently a part of any training materials and was removed in 2013.”.

The story was picked up the next day by the [New York Times](https://www.nytimes.com/2020/10/31/us/kentucky-state-police-hitler.html)

Internal investigations have apparently unearthed at least one more set of similar training material and the State Police Commissioner, Rodney Brewer submitted his resignation on Oct 3, effective Oct 4, which was accepted by the Kentucky Governor.

According to the news story, "The KSP presentation appears to draw heavily from nationally-known police trainer Dave Grossman, who delivers lectures to police forces nation-wide on his theory of “killology” teaching police officers to embrace “the responsibility to kill” and “making it possible for people to kill without conscious thought,” as he said during an [interview with PBS’s Frontline](https://www.pbs.org/wgbh/pages/frontline/shows/heart/interviews/grossman.html)."

Harvard Law Professor Seth Stoughton described the differences between the warrior and guardian mentality approach in a [2015 Harvard Law Review article](https://harvardlawreview.org/2015/04/law-enforcements-warrior-problem/).

"*Within law enforcement, few things are more venerated than the concept of the Warrior. Officers are trained to cultivate a “warrior mindset,” the virtues of which are extolled in books, [articles](http://www.policemag.com/channel/patrol/articles/2012/05/warrior-mindset.aspx), [interviews](http://www.policeone.com/hank-hayes/videos/5955798-Hank-Hayes-Warrior-Mindset [http://perma.cc/5JKL-JPK8) and seminars intended for a law enforcement audience.  See e.g. [2015 ILEETA Conference Schedule, Int’l Law Enforcement Educator & Trainers Ass’n](http://ileeta.org/wp-content/uploads/2014/09/2015-ILLETA-CONFERENCE-3_2_151.pdf)*"

"*Under this warrior worldview, officers are locked in intermittent and unpredictable combat with unknown but highly lethal enemies. As a result, officers learn to be afraid. That isn’t the word used in law enforcement circles, of course. Vigilant, attentive, cautious, alert, or observant are the terms that appear most often in police publications. But make no mistake, officers don’t learn to be vigilant, attentive, cautious, alert, and observant just because it’s fun. They do so because they are afraid. Fear is ubiquitous in law enforcement.*"

I confirmed the above paragraph with contacts in the San Jose, California police department.

Prof. Stoughon continued “*The guardian mindset emphasizes communication over commands, cooperation over compliance, and legitimacy over authority. And in the use-of-force context, the Guardian emphasizes patience and restraint over control, stability over action.*”

The high school students noted that there was a 2019 Florida State University Study on "warrior mentality" as exemplified by the Dave Grossman lectures v. "guardian mentality". A news story on the Florida State study is [here](
https://news.fsu.edu/news/education-society/2019/02/26/fsu-researcher-finds-data-driven-evidence-on-warrior-vs-guardian-policing/) and the abstract can be found [here](https://www.tandfonline.com/doi/full/10.1080/07418825.2018.1533031). Per the news story, "*The warrior concept is associated with the idea of militarizing policing and is consistent with the traditional view of police work — to search, chase and capture. However, the newer concept of guardian policing emphasizes social service, valuing community partnerships and establishing positive contacts. Working with police departments in Fayetteville, North Carolina and Tucson, Arizona, the study indicated that warrior and guardian models are two distinct approaches to policing. However, officers were able to adopt both mentalities. They also found officers who scored higher on the guardian measure were more likely to value communication, while higher scores on the warrior measure revealed greater importance of physical control and more favorable attitudes toward excessive use of force. McLean said the warrior mentality often leads to more use of force, making it more likely that the officer or the citizen gets injured.*"

“*Research has shown the guardian mentality has very positive outcomes,” McLean said. “While we recognize that you can hold a guardian and a warrior mentality at the same time, if you’re not already emphasizing guardianship in some aspects of your work, you’re not doing it to the best of your ability and possibly to the detriment of community relationships and well-being.*”

I think before the progressives continue beating the drum on "defunding the police", they should consider whether they can achieve a better result by refocusing mindsets. And before conservatives continue beating the drum on "law and order", they need to think about the fact that the police should be guardians of everyone. Oh, and GOOD JOB HIGH SCHOOL STUDENTS.

Sidenote: Colleen has convinced me that there are advantages for sending out police on mental health checks. She is a professional in these matters and I trust her judgment overriding my conclusions based on news stories. I just hope the judgment of the policeman on the scene is as good as hers.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
