---
title: "2020 Philosophy Survey Part 1b: External World, Free Will and A priori knowledge"
date: 2022-02-15T15:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---
This post is Part 1b with the topics being External World, Free Will and A priori knowledge. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.


### External world
Here the question is what is the "real world"? Idealism says everything which exists is just thought, and that there is no external reality. Skepticism says that you can never know the world in its true form. Non-skeptical realism says that the world exists and we can know it. The survey creators had the following comment: "We asked this one partly because of its centrality in the history of philosophy, and partly because we were especially interested in data about how many philosophers accept the “old, dead” positions that supposedly no-one accepts these days. Skepticism and idealism are often treated as gateways to reductio in contemporary discussion, for example, rather than as serious contenders for the truth. We would have liked to have an option for a view on which the external world is somehow mind-dependent without this being idealism (e.g. social constructivism), but we couldn’t find a good accessible generic term here. Of course we expected a big majority for non-skeptical realism, but we were interested to see whether there would be a good number of skeptics and idealists out there."

| [Idealism](https://plato.stanford.edu/entries/idealism/) | [Skepticism](https://plato.stanford.edu/entries/skepticism/) | Non-skeptical realism % |
|----------|------------|-------------------------|
| 7%       | 5%         | 80%                     |
N = 1764

### Free Will
See [Free will](https://plato.stanford.edu/entries/freewill/)
Libertarian free will is generally defined as the theoretical, consciously controlled ability to do otherwise in a given situation. Now consider the proposition that either something happens for a reason or doesn't or it happens randomly.

If your action (or decision to perform an action) happens randomly, the definition of libertarian free will is violated because you didn't have a consciously controlled ability to do somethign (it was random).

If something happens for a reason, then things are caused and you would have made the same decision every time (all other factors being the same). Again, the definition of libertarian free will is failed. This is determinism and most physicists believe in it (although they will also say that you think you have free will, so you might as well act like you do.

Most philosophers agree with determinism, but they don't want to let go of free will, so they've come up with the idea of compatibilism (aka soft determinism). They redefine "free will" as being able to do what you desire to do (ignoring the idea that your desire is predetermined). "Thus if I wanted to make a cup of tea and made it, I did it freely, and therefore have free will, even if I was always going to do so." [Jonathan M.S. Pearce](https://skepticink.com/tippling/2015/04/06/philosophy-101-philpapers-induced-7-free-will-compatibilism-libertarianism-or-no-free-will/). Personally I believe that compatibilism is a cop-out.

This debate obviously has huge ramifications in religion.

| [Compatibilism](https://plato.stanford.edu/entries/compatibilism/) | [Libertarian Free Will](https://plato.stanford.edu/entries/freewill/#LibeAccoSour) | [No Free Will](https://plato.stanford.edu/entries/determinism-causal/) |
|--------------|-----------------------|--------------|
| 59%          | 19%                   | 11%          |
N = 1758

### A priori knowledge
See [A priori knowledge](http://plato.stanford.edu/entries/apriori/)
The question is whether a person can have knowledge prior to having any empirical evidence or experience. Not being a philosopher, I'm a bit confused on the yes or no mutually exclusive nature of the question. Apparently the philosophers answering "No" take the position that you might have knowledge of the existence of things, events and states of affairs IF THEY EXIST, but you do not necessarily know they exist without empirical evidence.

A priori knowledge is different than an a priori proof. An a priori proof is a proof that reflects the causal order. Thus a sufficient reason would be a proof that is both a demonstration and an explanation. Pre-Kantian, it meant an argument from causes to effects and Leibniz sometimes uses it that way. In Kantian terms, an a priori proof is a proof that doesn’t require any input from sense experience.

| Yes | No  |
|-----|-----|
| 73% | 18% |
N = 1749

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
