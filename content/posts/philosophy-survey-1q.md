---
title: "2020 Philosophy Survey Part 1q: Consciousness, Mind Uploading and Hard Problem of Consciousness"
date: 2022-02-28T14:18:19-04:00
categories: ['posts']
tags: ['life','philosophy']
showToc:  "true"
author: "Peter Hiltz"
---

This post is Part 1q with the topics being Consciousness, Mind Uploading and The Hard Problem of Consciousness. I recently came across the [2020 Philosopher Papers Survey](https://survey2020.philpeople.org/survey/results/all) of 7,685 academic philosophers around the world. (I think < 1,800 actually responded). I then ran into my first problem - uhh, what do those answers mean? It reminded me of tax lawyers writing for other tax lawyers. One piece of advice I used to give younger tax lawyers when they were writing for a business audience - drop the nuance. Yes, we think it is important and a judge will think it is important, but it will be either missed by or confuse the business person audience. So this is my overly simplistic attempt at a layperson's understanding of what academic philosophers actually argue with each other about in 2020. I'm ordering this by number of responses rather than alphabetically or some bad attempt to categorize them and breaking them into three question bite sized pieces.

I tried to put a link in each subject to the [Stanford Encyclopedia of Philosophy (SEP)](https://plato.stanford.edu/) website where you can get more information on a subject.

When looking at the table under each subject, the "Unclear" category is the percentage of respondents saying that the question was too unclear to answer. Agnostic means that the respondent has no position or is undecided. All percentages rounded up. If the percentages do not add up to 100%, the other answers included "question not clear" as well as a combination or alternatives too small to count. The number of people responding to each question is at the bottom of the relevant table.

### Consciousness
See [Consciousness](https://plato.stanford.edu/entries/consciousness/)
See also [consciousness - Representational](https://plato.stanford.edu/entries/consciousness-representational/)

- [Dualism](https://plato.stanford.edu/entries/dualism/) is the theory that the mental and the physical (mind and body or mind and brain) are, somehow, radically different kinds of things. There are three different variets of dualism here. [Predicate dualism](https://plato.stanford.edu/entries/dualism/#PreDua) is the theory that psychological or mentalistic predicates are (a) essential for a full description of the world and (b) are not reducible to physicalistic predicates. [Property dualism](https://plato.stanford.edu/entries/dualism/#ProDua) says there are two types of property in the world. You can argue property dualism by saying that the qualitative nature of consciousness is not merely another way of categorizing states of the brain, but is a genuinely emergent phenomenon. [Substance Dualism](https://plato.stanford.edu/entries/dualism/#SubDua) seems to separate a body from a "person". Both are "substances" with "properties", but they have different identity conditions.
- [Functionalism](https://plato.stanford.edu/entries/functionalism/), per the SEP is "Functionalism is the doctrine that what makes something a thought, desire, pain (or any other type of mental state) depends not on its internal constitution, but solely on its function, or the role it plays, in the cognitive system of which it is a part. More precisely, functionalist theories take the identity of a mental state to be determined by its causal relations to sensory stimulations, other mental states, and behavior.".
- [Identity Theory](https://plato.stanford.edu/entries/mind-identity/), per the SEP is "Consider an experience of pain, or of seeing something, or of having a mental image. The identity theory of mind is to the effect that these experiences just are brain processes, not merely correlated with brain processes. Identity Theory would deny the qualia theory that experiences are fundamentally non-physical, psychical properties."
- [Eliminativism or Eliminative Materialism](https://plato.stanford.edu/entries/materialism-eliminative/) takes the position that certain common-sense mental states, such as beliefs and desires, do not exist. Since there is nothing that has the causal and semantic properties we attribute to beliefs (and many other mental states) it will turn out that there really are no such things.
- [Panpsychism](https://plato.stanford.edu/entries/panpsychism/) believes that conscious experience is everywhere - even electrons have conscious experience - but that there are also varying degrees of consciousness and more sophiscated consciousnesses are emergent phenomenon.


| [Dualism](https://plato.stanford.edu/entries/dualism/) | [Functionalism](https://plato.stanford.edu/entries/functionalism/) | [Identity Theory](https://plato.stanford.edu/entries/mind-identity/) | [Eliminativism](https://plato.stanford.edu/entries/materialism-eliminative/) | [Panpsychism](https://plato.stanford.edu/entries/panpsychism/) |
|---------|---------------|-----------------|---------------|-------------|
| 22%     | 33%           | 13%             |       5%        |  8%           |
N = 1020

### Mind uploading
Basically the question here is asking, if you could upload your mind into a computer, does your "mind" or "self" survive or did your "mind" or "self" die and something else has been created?
| Survival | Death | Agnostic |
|----------|-------|----------|
| 27%      | 54%   | 8%         |
N = 1016


### Hard problem of consciousness
See [Hard problem of consciousness](https://iep.utm.edu/hard-con/)
The actual question was whether there is a "hard problem of consciousness"? Per the Internet Encyclopedia of Philosophy: "The hard problem of consciousness is the problem of explaining why any physical state is conscious rather than nonconscious.  It is the problem of explaining why there is “something it is like” for a subject in conscious experience, why conscious mental states “light up” and directly appear to the subject.  The usual methods of science involve explanation of functional, dynamical, and structural properties—explanation of what a thing does, how it changes over time, and how it is put together.  But even after we have explained the functional, dynamical, and structural properties of the conscious mind, we can still meaningfully ask the question, Why is it conscious? "

| Yes | No  | Agnostic |
|-----|-----|----------|
| 62% | 30% | 4%       |
N = 998

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
