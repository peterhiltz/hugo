---
title: "We Are All Bit Players In Other People's Plays"
date: 2020-09-30T14:18:19-04:00
categories: ['posts']
tags: ['life']
showToc:  "true"
author: "Peter Hiltz"
---
Most of us are the main character in the plays running through our heads. As those scenes happen and as we review them later, all the other people are either focused on us or are NPCs (non-playing characters). And that's fine so long as we remember that while our play is happening, every other person is the center of their own play and we are not the main character in their plays. The plays overlap and there are different monologues running in each characters heading depending on your perspective.

In any event, I think there can be several benefits to remembering the existence of the billions of plays happening concurrently.

If there is a problem, you as the playwright might need to reach out to the playwright of other plays to get critical background information. Maybe the dramatic tension can be resolved by you trying to understand what the other person's script might be saying. Maybe while you may have been in Act III, Scene 2 of this person's play, there was critical info from Act I, Scene 6 that you don't know about.

It isn't always about you. I think it was Joy Marino who said "Stop breaking your own heart by exaggerating your place in other people's lives." So you didn't get the girl, or guy or ... You thought you were going to be the co-star of their play and it turned out that you only had a small supporting role. That's ok. Every actor/actress auditions for lots of roles they don't get. Life (and your play) goes on.

it might help giving people the respect they deserve by remembering that while you are the superstar of your show, they are superstars in their own right in theirs.

Of course, there are people for whom the Carly Simon lyrics "You're so vain you probably think this song is about you" is apropos.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
