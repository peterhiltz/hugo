---
title: "Either they work or they don't. NOOOOOO! "
date: 2021-07-30T14:18:19-04:00
categories: ['posts']
tags: ['life','science','pandemic']
showToc:  "true"
author: "Peter Hiltz"
---
I recently had a useless discussion with someone who was unwilling to either wear a mask or vaccinate and took the position: "If the vaccines work why the fear, and if they do not work why take the risk of getting them?" His refusal to wear a mask was based on his trust in his immune system "I'm healthy and if you wear a mask you are living in fear and halfway dead already."

Let me put my position into tiny baby steps:

1. Vaccines are not some bug zapper electrical fence that kills a disease on contact. That isn't how they work. They work by strengthening your immune system so that it can deal with a disease faster and harder. Even if you are vaccinated, you can get the disease and pass it on to others while your body's strengthened immune system prevents the disease from causing severe harm to you.

2. Covid is a real danger. I've had two friends and two extended family members die from COVID, half a dozen more friends who have long term breathing problems from getting COVID and another dozen medical friends working till exhaustion in ICUs dealing with patients on respirators because of COVID. There is something to be afraid of.

3. No one has ever said the vaccines are 100% effective. 93% effective is a hell of a lot better than 0% effective. The statement "either they work or they don't" pretends we are living in a fantasy binary world.

4. Not everyone can take the vaccine. I have a four month old grandson who can't get it. Yes, I'm vaccinated, but if you read points 2 and 3, am I supposed to be happy that someone who is only concerned about themselves will pass on the disease to me and I should pass it on to my grandson?

5. Even if you only care about yourself, just because you are not "old" and in good health doesn't mean it isn't a danger to you. [https://www.nbc12.com/2021/07/30/i-should-have-gotten-damn-vaccine-father-5-dies-covid-age-39/](https://www.nbc12.com/2021/07/30/i-should-have-gotten-damn-vaccine-father-5-dies-covid-age-39/); [https://www.nbc12.com/2021/07/21/unvaccinated-doctor-sends-warning-others-after-catching-delta-variant/](https://www.nbc12.com/2021/07/21/unvaccinated-doctor-sends-warning-others-after-catching-delta-variant/).

6. No, the covid vaccines do not enter your DNA. [https://qbi.uq.edu.au/article/2021/07/no-covid-19-does-not-enter-our-dna](https://qbi.uq.edu.au/article/2021/07/no-covid-19-does-not-enter-our-dna)

When an honestly mistaken person is confronted with the truth, he is either no longer mistaken, or no longer honest.

As usual, feel free to disagree using this [contact link](https://www.peterhiltz.com/en/contact/). My world view is a hypothesis, not a belief.
